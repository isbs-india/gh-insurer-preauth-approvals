Senttoinsurer web application has been deployed first in Nov 2014 and further enhanced as it goes.  Senttoinsurer tasks provides the following features through leftmenu.

Developed in Java
Team: Mr. Prabhakar, Mr. Anand, Mr. Raviteja (Database), Ms Ratnam (Database) and Mr. Surya Kiran

Current Maintainance Team:
Mr. Rajesh Pudi (Database)
Mr. Prabhakar & Mr. Anand

URL for Live : http://iauth.goodhealthtpa.net:8085/SentToInsurer
URL for Stage : http://stage.goodhealthtpa.net:8080/SentToInsurer
Login : http://iauth.goodhealthtpa.net:8085/SentToInsurer/LoginAction
Database : corp 100.73

Folder Structure

/SourceCode : contains the actual source code (Eclipse Workbench)
/Deploy : Deployment files of various version
/Resources : Various documents and other referential assets used for this project
/DB : Various database scripts

LeftMenu Options:

Preauth Search
PreAuth For Approval
PreAuth For Denial
PreAuth Sent To Insurer
Logout