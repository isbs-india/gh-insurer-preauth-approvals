package resource;

/**
 * @author Govind
 *
 */
public interface IMessage { 
	
	public static final int CRITICAL = -1;
	  public static final int NONE = 0;
	  public static final int INFO = 1;
	  public static final int ERROR = 2;
	  public static final int WARNING = 3;
	  public static final int DEBUG = 4;	  
	  public static final int SENSITIVE = 500;// This variable is added to hide the sensitive informations which is being displayed in the console. The log.level in CE_PROPERTIES Table is 300.Added by G.Gokul dt:30/09/2005

	 
	  public void println(int level,java.lang.Object message);
	  public void printStackTrace(int level,java.lang.Exception e);
	  public boolean isEnabled(int level);

}
