// FrontEnd Plus GUI for JAD
// DeCompiled : MessageLog.class

package resource;

import java.io.*;
import java.net.InetAddress;
import java.util.Date;

// Referenced classes of package com.zaax.messagelog:
//            IMessage

public class MessageLog
    implements IMessage, Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int logLevel;
    private static MessageLog theInstance = null;

    private MessageLog()
    {
        logLevel = 5;
    }

    public void println(Object message)
    {
        println(4, message);
    }

    public void println(int level, Object message)
    {
        if(isEnabled(level))
        {
            //int threadID = Thread.currentThread().hashCode();
           // String threadName = Thread.currentThread().getName();
            StringBuilder log = new StringBuilder(getCallerInfo());
            switch(level)
            {
            case 0: // '\0'
                log.append("****NONE*****");
                break;

            case 1: // '\001'
                log.append("****INFO*****");
                break;

            case 2: // '\002'
                log.append("****ERROR*****");
                break;

            case 3: // '\003'
                log.append("****WARNING*****");
                break;

            case 4: // '\004'
                log.append("****DEBUG*****");
                break;

            case 500: 
                log.append("****SENSITIVE*****");
                break;

            default:
                log.append("****NONE*****");
                break;
            }
            Date currentTime = new Date();
            log.append(currentTime.toString());
            log.append(": ");
            if(message != null)
            {
                log.append(message.toString());
                try
                {
                    if(level == 2)
                    {
                    	  String computername=InetAddress.getLocalHost().getCanonicalHostName();
    					  String computeraddr=InetAddress.getLocalHost().getHostAddress();
    					  String computernme=InetAddress.getLocalHost().getHostName();
    					  System.out.println("ERROR AT SYSTEM:"+computername+" - "+computeraddr+" - "+computernme);
                    }
                }
                catch(Exception e)
                {
                    System.out.println((new StringBuilder("Error in sendmail in Messagelog==")).append(e).toString());
                }
            } else
            {
                log.append("null");
            }
            System.out.println(log.toString());
        }
    }

    public boolean isEnabled(int level)
    {
        return level <= logLevel;
    }

    public void printStackTrace(int level, Exception e)
    {
        ByteArrayOutputStream ops = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(ops);
        e.printStackTrace(ps);
        println(level, ops);
    }

    public static MessageLog getInstance()
    {
        if(theInstance == null)
            theInstance = new MessageLog();
        return theInstance;
    }

    public String stackTraceToString(Exception e)
    {
        StringWriter expStringWr = new StringWriter();
        PrintWriter expPrintWr = new PrintWriter(expStringWr, true);
        e.printStackTrace(expPrintWr);
        String expMsg = (new StringBuilder(String.valueOf(e.toString()))).append("\n StackTrace : ").append(expStringWr.toString()).toString();
        return expMsg;
    }

    public String getCallerInfo()
    {
       // String callingLineStr = null;
        StringBuffer sb1 = new StringBuffer();
       // int MY_STACK_TRACE_POS = 2;
        try
        {
            Throwable ex = new Throwable();
            StackTraceElement stackElements[] = ex.getStackTrace();
            int slen = stackElements.length;
            if(slen >= 2)
            {
                StackTraceElement selem = stackElements[2];
                sb1.append("(");
                sb1.append(selem.getFileName());
                sb1.append(":");
                sb1.append(selem.getLineNumber());
                sb1.append(") ");
            } else
            {
                sb1.append("N/A ");
            }
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder("excp in getCallerInfo ")).append(e.getMessage()).toString());
            sb1.append("N/A excp ");
        }
        return sb1.toString();
    }

    public void logInstrumentation(int homeId, int personId, String callerInfo, String message)
    {
        StringBuffer logBuf = new StringBuffer("");
        Date currentTime = new Date();
        logBuf.append((new StringBuilder(",time,")).append(currentTime.toString()).toString());
        logBuf.append((new StringBuilder(",message,")).append(message).toString());
        logBuf.append("\n");
        println(2, logBuf.toString());
    }

}