package com.ghpl.model.iauth;

public class OSTicketBean {
    private int ticketId;
    private String ticketEmailId;

	public String getTicketEmailId() {
		return ticketEmailId;
	}

	public void setTicketEmailId(String ticketEmailId) {
		this.ticketEmailId = ticketEmailId;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
}
