package com.ghpl.model.iauth;

public class DocumentBean {
	
   private int serialNo;
	private String claimId; 
	private String collectionMaindocTypeId;
	private String collectionSubdocTypeId;
	private String fileName;
	private String actualFileName;
	private String scanningDate;
	private String scanningGTS;
	private String folderName;
	private int moduleId;
	private String ccnUnique;
	
	
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getFolderName() {
		return folderName;
	}
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	public String getCollectionMaindocTypeId() {
		return collectionMaindocTypeId;
	}
	public void setCollectionMaindocTypeId(String collectionMaindocTypeId) {
		this.collectionMaindocTypeId = collectionMaindocTypeId;
	}
	public String getCollectionSubdocTypeId() {
		return collectionSubdocTypeId;
	}
	public void setCollectionSubdocTypeId(String collectionSubdocTypeId) {
		this.collectionSubdocTypeId = collectionSubdocTypeId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getActualFileName() {
		return actualFileName;
	}
	public void setActualFileName(String actualFileName) {
		this.actualFileName = actualFileName;
	}
	public String getScanningDate() {
		return scanningDate;
	}
	public void setScanningDate(String scanningDate) {
		this.scanningDate = scanningDate;
	}
	public String getScanningGTS() {
		return scanningGTS;
	}
	public void setScanningGTS(String scanningGTS) {
		this.scanningGTS = scanningGTS;
	}
	
}
