package com.ghpl.model.iauth;

import java.io.Serializable;

public class DocsDetailsCorpBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int collectionMainDocTypeId;
	private int collectionSubDocTypeId;
	private String scanningDate;
	private String fileName;
	private int ftpFlag;
	private String actualFileName;
	private String serverIp;
	private int serverPort;
	private String typeOfDoc;
	private String fullPath;
	private String folder;
	
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	public String getTypeOfDoc() {
		return typeOfDoc;
	}
	public void setTypeOfDoc(String typeOfDoc) {
		this.typeOfDoc = typeOfDoc;
	}
	public int getCollectionMainDocTypeId() {
		return collectionMainDocTypeId;
	}
	public void setCollectionMainDocTypeId(int collectionMainDocTypeId) {
		this.collectionMainDocTypeId = collectionMainDocTypeId;
	}
	public int getCollectionSubDocTypeId() {
		return collectionSubDocTypeId;
	}
	public void setCollectionSubDocTypeId(int collectionSubDocTypeId) {
		this.collectionSubDocTypeId = collectionSubDocTypeId;
	}
	public String getScanningDate() {
		return scanningDate;
	}
	public void setScanningDate(String scanningDate) {
		this.scanningDate = scanningDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getFtpFlag() {
		return ftpFlag;
	}
	public void setFtpFlag(int ftpFlag) {
		this.ftpFlag = ftpFlag;
	}
	public String getActualFileName() {
		return actualFileName;
	}
	public void setActualFileName(String actualFileName) {
		this.actualFileName = actualFileName;
	}
	public String getServerIp() {
		return serverIp;
	}
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}
	public int getServerPort() {
		return serverPort;
	}
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
