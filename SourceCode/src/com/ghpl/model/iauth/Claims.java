package com.ghpl.model.iauth;

import java.util.ArrayList;
import java.util.List;

public class Claims {
	
	private String claimId;
	private String letterDate;
	private String subject;
	private String patientName;
	private String empId;
	private String policyNo;
	private String selfCardId;
	private String pCardId;
	private String doa;
	private String dod;
	private String billNo;
	private String remarks;
	private String selfAddress;
	private String hospitalAddress;
	private String insurerComName;
	private String hospitalName;
	
	private String id;
	private String cliamCurrentStatus;
	private String reportStatus;
	private String crkLastModified;
	private int claimType;
	private String selfName;
	private String checkinFavour;
	private String currentStatus;
	private String ipNo;
	private String filePath;
	private int moduleId;
	private String printName;
	private int serialNo;
	private String hospBankName;
	private String hospBankAccountNo;
	
	private float paidAmt;
	private int currency;
	private String corporateName;
	private String relationship;
	private String invoiceNo;
	private String payeeName;
	private String diagnosis;
	private String claimedAmt;
	private String disallowedAmt;
	private String disCount;
	private String coPayment;
	private String settledAmt;
	private String tdsAmt;
	private String serviceTaxAmt;
	private String chequeNo;
	private String chequeDate;
	private int suppCounter;
	private String transactionNo;
	private String transactionDt;
	private int transType;
	
	private String claimedAmtTotal;
	private String disallowedAmtTotal;
	private String settledAmtTotal;
	private String serviceTaxwithTotal;
	private String chequekAmt;
	private String bankAccno;
	private String bankName;
	private String policyHolderName;
	private String reportDate;
	private String cardId;
	
	private int mclaimType;
	private int sclaimType;
	private String supclaimTypeDescription;
	private String status;
	private String claimTypevar;
	private String preAuthappNo;
	private String module;
	private String reqAmt;
	private String recomAmt;
	private String deduAmt;
	private String ccnUnique;
		
	
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public String getReqAmt() {
		return reqAmt;
	}
	public void setReqAmt(String reqAmt) {
		this.reqAmt = reqAmt;
	}
	public String getRecomAmt() {
		return recomAmt;
	}
	public void setRecomAmt(String recomAmt) {
		this.recomAmt = recomAmt;
	}
	public String getDeduAmt() {
		return deduAmt;
	}
	public void setDeduAmt(String deduAmt) {
		this.deduAmt = deduAmt;
	}
	
	
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getPreAuthappNo() {
		return preAuthappNo;
	}
	public void setPreAuthappNo(String preAuthappNo) {
		this.preAuthappNo = preAuthappNo;
	}
	public String getClaimTypevar() {
		return claimTypevar;
	}
	public void setClaimTypevar(String claimTypevar) {
		this.claimTypevar = claimTypevar;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSupclaimTypeDescription() {
		return supclaimTypeDescription;
	}
	public void setSupclaimTypeDescription(String supclaimTypeDescription) {
		this.supclaimTypeDescription = supclaimTypeDescription;
	}
	public int getMclaimType() {
		return mclaimType;
	}
	public void setMclaimType(int mclaimType) {
		this.mclaimType = mclaimType;
	}
	public int getSclaimType() {
		return sclaimType;
	}
	public void setSclaimType(int sclaimType) {
		this.sclaimType = sclaimType;
	}
	public String getHospBankAccountNo() {
		return hospBankAccountNo;
	}
	public void setHospBankAccountNo(String hospBankAccountNo) {
		this.hospBankAccountNo = hospBankAccountNo;
	}
	public String getHospBankName() {
		return hospBankName;
	}
	public void setHospBankName(String hospBankName) {
		this.hospBankName = hospBankName;
	}
	
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccno() {
		return bankAccno;
	}
	public void setBankAccno(String bankAccno) {
		this.bankAccno = bankAccno;
	}
	public String getClaimedAmtTotal() {
		return claimedAmtTotal;
	}
	public void setClaimedAmtTotal(String claimedAmtTotal) {
		this.claimedAmtTotal = claimedAmtTotal;
	}
	public String getDisallowedAmtTotal() {
		return disallowedAmtTotal;
	}
	public void setDisallowedAmtTotal(String disallowedAmtTotal) {
		this.disallowedAmtTotal = disallowedAmtTotal;
	}
	public String getSettledAmtTotal() {
		return settledAmtTotal;
	}
	public void setSettledAmtTotal(String settledAmtTotal) {
		this.settledAmtTotal = settledAmtTotal;
	}
	public String getServiceTaxwithTotal() {
		return serviceTaxwithTotal;
	}
	public void setServiceTaxwithTotal(String serviceTaxwithTotal) {
		this.serviceTaxwithTotal = serviceTaxwithTotal;
	}
	public String getChequekAmt() {
		return chequekAmt;
	}
	public void setChequekAmt(String chequekAmt) {
		this.chequekAmt = chequekAmt;
	}
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	public String getTransactionDt() {
		return transactionDt;
	}
	public void setTransactionDt(String transactionDt) {
		this.transactionDt = transactionDt;
	}
	public int getTransType() {
		return transType;
	}
	public void setTransType(int transType) {
		this.transType = transType;
	}
	public int getSuppCounter() {
		return suppCounter;
	}
	public void setSuppCounter(int suppCounter) {
		this.suppCounter = suppCounter;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getPayeeName() {
		return payeeName;
	}
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getClaimedAmt() {
		return claimedAmt;
	}
	public void setClaimedAmt(String claimedAmt) {
		this.claimedAmt = claimedAmt;
	}
	public String getDisallowedAmt() {
		return disallowedAmt;
	}
	public void setDisallowedAmt(String disallowedAmt) {
		this.disallowedAmt = disallowedAmt;
	}
	public String getDisCount() {
		return disCount;
	}
	public void setDisCount(String disCount) {
		this.disCount = disCount;
	}
	public String getCoPayment() {
		return coPayment;
	}
	public void setCoPayment(String coPayment) {
		this.coPayment = coPayment;
	}
	public String getSettledAmt() {
		return settledAmt;
	}
	public void setSettledAmt(String settledAmt) {
		this.settledAmt = settledAmt;
	}
	public String getTdsAmt() {
		return tdsAmt;
	}
	public void setTdsAmt(String tdsAmt) {
		this.tdsAmt = tdsAmt;
	}
	public String getServiceTaxAmt() {
		return serviceTaxAmt;
	}
	public void setServiceTaxAmt(String serviceTaxAmt) {
		this.serviceTaxAmt = serviceTaxAmt;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public int getCurrency() {
		return currency;
	}
	public void setCurrency(int currency) {
		this.currency = currency;
	}
	public float getPaidAmt() {
		return paidAmt;
	}
	public void setPaidAmt(float paidAmt) {
		this.paidAmt = paidAmt;
	}
	public String getSelfAddress() {
		return selfAddress;
	}
	public void setSelfAddress(String selfAddress) {
		this.selfAddress = selfAddress;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getPrintName() {
		return printName;
	}
	public void setPrintName(String printName) {
		this.printName = printName;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getIpNo() {
		return ipNo;
	}
	public void setIpNo(String ipNo) {
		this.ipNo = ipNo;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getpCardId() {
		return pCardId;
	}
	public void setpCardId(String pCardId) {
		this.pCardId = pCardId;
	}
	public String getCheckinFavour() {
		return checkinFavour;
	}
	public void setCheckinFavour(String checkinFavour) {
		this.checkinFavour = checkinFavour;
	}
	public String getSelfCardId() {
		return selfCardId;
	}
	public void setSelfCardId(String selfCardId) {
		this.selfCardId = selfCardId;
	}
	
	public String getSelfName() {
		return selfName;
	}
	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public int getClaimType() {
		return claimType;
	}
	public void setClaimType(int claimType) {
		this.claimType = claimType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCliamCurrentStatus() {
		return cliamCurrentStatus;
	}
	public void setCliamCurrentStatus(String cliamCurrentStatus) {
		this.cliamCurrentStatus = cliamCurrentStatus;
	}
	public String getReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}
	public String getCrkLastModified() {
		return crkLastModified;
	}
	public void setCrkLastModified(String crkLastModified) {
		this.crkLastModified = crkLastModified;
	}
	public String getInsurerComName() {
		return insurerComName;
	}
	public void setInsurerComName(String insurerComName) {
		this.insurerComName = insurerComName;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public String getLetterDate() {
		return letterDate;
	}
	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = dod;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/*public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}*/
	

}
