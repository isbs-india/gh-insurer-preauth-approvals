package com.ghpl.model.iauth;

import java.io.Serializable;

public class UiicDataBean implements Serializable {

	private static final long serialVersionUID = 1L;
    private String ccnUnique;
	private String insuredName;
	private String patientName;
	private String age;
	private String sex;
	private String relationWithInsured;
	private String balanceSumInsured;
	private String policyNumber;
	private String sumInsured;
	private String policyTypeIndividualOrGmc;
	private String dateOfInception;
	private String hospitalName;
	private String city;
	private String state;
	private String diagnosis;
	private String managementOrMedicalOrEmergency;
	private String dateOfAdmission;
	private String expectedDurationOfStay;
	private String roomType;
	private String roomRentIcuRent;
	private String roomRentEligibility;
	private String noOfDaysStayed;
	private String gipsaPpaAmountRequested;
	private String gipsaPpnAmountRecomm;
	private String gipsaPpnReasonsForDeduction;
	private String roomRentIcuRentAmountRequested;
	private String roomRentAmountRecomm;
	private String roomRentReasonsForDeduction;
	private String procedureAmountRequested;
	private String procedureAmountRecomm;
	private String procedureReasonsForDeduction;
	private String pharmacyAmountRequested;
	private String pharmacyAmountRecomm;
	private String pharmacyReasonsForDeduction;
	private String otanesthesiaAmountRequested;
	private String otanesthesiaAmountRecomm;
	private String otanesthesiaReasonsDeduction;
	private String labInvestigationsAmtRequested;
	private String labInvestigationsAmtRecomm;
	private String labInvestigationrsnDeduction;
	private String consultationAmountRequested;
	private String consultationAmountRecomm;
	private String consultationReasonsForDeduc;
	private String othersAmountRequested;
	private String othersAmountRecomm;
	private String othersReasonsForDeduction;
	private String totalAmountRequested;
	private String totalAmountRecomm;
	private String totalReasonsForDeduction;
	private String diseaseTreatmentAreCovered;
	private String expensesPaid;
	private String allHeadersInBreakDown;
	private String theCostOfDiagnosticsExceed;
	private String processingDoctorsNameandtpa;
	private String createdDate;
	private String tpaName;
	private String ppnNetworkHospitals;
	private String categoryHospitaltertyPlus;
	private String emailId;
	private String ccEmailid;
	private String policyDescription;
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private int preauthAppNo;

	//New columns
	private int underWriterId;
	private int issuingOfficePrimaryKey;
	
	private String policyID;
	private String cardId;
	private String insuredId;



	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}


	public int getPreauthAppNo() {
		return preauthAppNo;
	}
	public void setPreauthAppNo(int preauthAppNo) {
		this.preauthAppNo = preauthAppNo;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getRelationWithInsured() {
		return relationWithInsured;
	}
	public void setRelationWithInsured(String relationWithInsured) {
		this.relationWithInsured = relationWithInsured;
	}
	public String getBalanceSumInsured() {
		return balanceSumInsured;
	}
	public void setBalanceSumInsured(String balanceSumInsured) {
		this.balanceSumInsured = balanceSumInsured;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPolicyTypeIndividualOrGmc() {
		return policyTypeIndividualOrGmc;
	}
	public void setPolicyTypeIndividualOrGmc(String policyTypeIndividualOrGmc) {
		this.policyTypeIndividualOrGmc = policyTypeIndividualOrGmc;
	}
	public String getDateOfInception() {
		return dateOfInception;
	}
	public void setDateOfInception(String dateOfInception) {
		this.dateOfInception = dateOfInception;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getManagementOrMedicalOrEmergency() {
		return managementOrMedicalOrEmergency;
	}
	public void setManagementOrMedicalOrEmergency(
			String managementOrMedicalOrEmergency) {
		this.managementOrMedicalOrEmergency = managementOrMedicalOrEmergency;
	}
	public String getDateOfAdmission() {
		return dateOfAdmission;
	}
	public void setDateOfAdmission(String dateOfAdmission) {
		this.dateOfAdmission = dateOfAdmission;
	}
	public String getExpectedDurationOfStay() {
		return expectedDurationOfStay;
	}
	public void setExpectedDurationOfStay(String expectedDurationOfStay) {
		this.expectedDurationOfStay = expectedDurationOfStay;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomRentIcuRent() {
		return roomRentIcuRent;
	}
	public void setRoomRentIcuRent(String roomRentIcuRent) {
		this.roomRentIcuRent = roomRentIcuRent;
	}
	public String getRoomRentEligibility() {
		return roomRentEligibility;
	}
	public void setRoomRentEligibility(String roomRentEligibility) {
		this.roomRentEligibility = roomRentEligibility;
	}
	public String getNoOfDaysStayed() {
		return noOfDaysStayed;
	}
	public void setNoOfDaysStayed(String noOfDaysStayed) {
		this.noOfDaysStayed = noOfDaysStayed;
	}
	public String getGipsaPpaAmountRequested() {
		return gipsaPpaAmountRequested;
	}
	public void setGipsaPpaAmountRequested(String gipsaPpaAmountRequested) {
		this.gipsaPpaAmountRequested = gipsaPpaAmountRequested;
	}
	public String getGipsaPpnAmountRecomm() {
		return gipsaPpnAmountRecomm;
	}
	public void setGipsaPpnAmountRecomm(String gipsaPpnAmountRecomm) {
		this.gipsaPpnAmountRecomm = gipsaPpnAmountRecomm;
	}
	public String getGipsaPpnReasonsForDeduction() {
		return gipsaPpnReasonsForDeduction;
	}
	public void setGipsaPpnReasonsForDeduction(String gipsaPpnReasonsForDeduction) {
		this.gipsaPpnReasonsForDeduction = gipsaPpnReasonsForDeduction;
	}
	public String getRoomRentIcuRentAmountRequested() {
		return roomRentIcuRentAmountRequested;
	}
	public void setRoomRentIcuRentAmountRequested(
			String roomRentIcuRentAmountRequested) {
		this.roomRentIcuRentAmountRequested = roomRentIcuRentAmountRequested;
	}
	public String getRoomRentAmountRecomm() {
		return roomRentAmountRecomm;
	}
	public void setRoomRentAmountRecomm(String roomRentAmountRecomm) {
		this.roomRentAmountRecomm = roomRentAmountRecomm;
	}
	public String getRoomRentReasonsForDeduction() {
		return roomRentReasonsForDeduction;
	}
	public void setRoomRentReasonsForDeduction(String roomRentReasonsForDeduction) {
		this.roomRentReasonsForDeduction = roomRentReasonsForDeduction;
	}
	public String getProcedureAmountRequested() {
		return procedureAmountRequested;
	}
	public void setProcedureAmountRequested(String procedureAmountRequested) {
		this.procedureAmountRequested = procedureAmountRequested;
	}
	public String getProcedureAmountRecomm() {
		return procedureAmountRecomm;
	}
	public void setProcedureAmountRecomm(String procedureAmountRecomm) {
		this.procedureAmountRecomm = procedureAmountRecomm;
	}
	public String getProcedureReasonsForDeduction() {
		return procedureReasonsForDeduction;
	}
	public void setProcedureReasonsForDeduction(String procedureReasonsForDeduction) {
		this.procedureReasonsForDeduction = procedureReasonsForDeduction;
	}
	public String getPharmacyAmountRequested() {
		return pharmacyAmountRequested;
	}
	public void setPharmacyAmountRequested(String pharmacyAmountRequested) {
		this.pharmacyAmountRequested = pharmacyAmountRequested;
	}
	public String getPharmacyAmountRecomm() {
		return pharmacyAmountRecomm;
	}
	public void setPharmacyAmountRecomm(String pharmacyAmountRecomm) {
		this.pharmacyAmountRecomm = pharmacyAmountRecomm;
	}
	public String getPharmacyReasonsForDeduction() {
		return pharmacyReasonsForDeduction;
	}
	public void setPharmacyReasonsForDeduction(String pharmacyReasonsForDeduction) {
		this.pharmacyReasonsForDeduction = pharmacyReasonsForDeduction;
	}
	public String getOtanesthesiaAmountRequested() {
		return otanesthesiaAmountRequested;
	}
	public void setOtanesthesiaAmountRequested(String otanesthesiaAmountRequested) {
		this.otanesthesiaAmountRequested = otanesthesiaAmountRequested;
	}
	public String getOtanesthesiaAmountRecomm() {
		return otanesthesiaAmountRecomm;
	}
	public void setOtanesthesiaAmountRecomm(String otanesthesiaAmountRecomm) {
		this.otanesthesiaAmountRecomm = otanesthesiaAmountRecomm;
	}
	public String getOtanesthesiaReasonsDeduction() {
		return otanesthesiaReasonsDeduction;
	}
	public void setOtanesthesiaReasonsDeduction(String otanesthesiaReasonsDeduction) {
		this.otanesthesiaReasonsDeduction = otanesthesiaReasonsDeduction;
	}
	public String getLabInvestigationsAmtRequested() {
		return labInvestigationsAmtRequested;
	}
	public void setLabInvestigationsAmtRequested(
			String labInvestigationsAmtRequested) {
		this.labInvestigationsAmtRequested = labInvestigationsAmtRequested;
	}
	public String getLabInvestigationsAmtRecomm() {
		return labInvestigationsAmtRecomm;
	}
	public void setLabInvestigationsAmtRecomm(String labInvestigationsAmtRecomm) {
		this.labInvestigationsAmtRecomm = labInvestigationsAmtRecomm;
	}
	public String getLabInvestigationrsnDeduction() {
		return labInvestigationrsnDeduction;
	}
	public void setLabInvestigationrsnDeduction(String labInvestigationrsnDeduction) {
		this.labInvestigationrsnDeduction = labInvestigationrsnDeduction;
	}
	public String getConsultationAmountRequested() {
		return consultationAmountRequested;
	}
	public void setConsultationAmountRequested(String consultationAmountRequested) {
		this.consultationAmountRequested = consultationAmountRequested;
	}
	public String getConsultationAmountRecomm() {
		return consultationAmountRecomm;
	}
	public void setConsultationAmountRecomm(String consultationAmountRecomm) {
		this.consultationAmountRecomm = consultationAmountRecomm;
	}
	public String getConsultationReasonsForDeduc() {
		return consultationReasonsForDeduc;
	}
	public void setConsultationReasonsForDeduc(String consultationReasonsForDeduc) {
		this.consultationReasonsForDeduc = consultationReasonsForDeduc;
	}
	public String getOthersAmountRequested() {
		return othersAmountRequested;
	}
	public void setOthersAmountRequested(String othersAmountRequested) {
		this.othersAmountRequested = othersAmountRequested;
	}
	public String getOthersAmountRecomm() {
		return othersAmountRecomm;
	}
	public void setOthersAmountRecomm(String othersAmountRecomm) {
		this.othersAmountRecomm = othersAmountRecomm;
	}
	public String getOthersReasonsForDeduction() {
		return othersReasonsForDeduction;
	}
	public void setOthersReasonsForDeduction(String othersReasonsForDeduction) {
		this.othersReasonsForDeduction = othersReasonsForDeduction;
	}
	public String getTotalAmountRequested() {
		return totalAmountRequested;
	}
	public void setTotalAmountRequested(String totalAmountRequested) {
		this.totalAmountRequested = totalAmountRequested;
	}
	public String getTotalAmountRecomm() {
		return totalAmountRecomm;
	}
	public void setTotalAmountRecomm(String totalAmountRecomm) {
		this.totalAmountRecomm = totalAmountRecomm;
	}
	public String getTotalReasonsForDeduction() {
		return totalReasonsForDeduction;
	}
	public void setTotalReasonsForDeduction(String totalReasonsForDeduction) {
		this.totalReasonsForDeduction = totalReasonsForDeduction;
	}
	public String getDiseaseTreatmentAreCovered() {
		return diseaseTreatmentAreCovered;
	}
	public void setDiseaseTreatmentAreCovered(String diseaseTreatmentAreCovered) {
		this.diseaseTreatmentAreCovered = diseaseTreatmentAreCovered;
	}
	public String getExpensesPaid() {
		return expensesPaid;
	}
	public void setExpensesPaid(String expensesPaid) {
		this.expensesPaid = expensesPaid;
	}
	public String getAllHeadersInBreakDown() {
		return allHeadersInBreakDown;
	}
	public void setAllHeadersInBreakDown(String allHeadersInBreakDown) {
		this.allHeadersInBreakDown = allHeadersInBreakDown;
	}
	public String getTheCostOfDiagnosticsExceed() {
		return theCostOfDiagnosticsExceed;
	}
	public void setTheCostOfDiagnosticsExceed(String theCostOfDiagnosticsExceed) {
		this.theCostOfDiagnosticsExceed = theCostOfDiagnosticsExceed;
	}
	public String getProcessingDoctorsNameandtpa() {
		return processingDoctorsNameandtpa;
	}
	public void setProcessingDoctorsNameandtpa(String processingDoctorsNameandtpa) {
		this.processingDoctorsNameandtpa = processingDoctorsNameandtpa;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getTpaName() {
		return tpaName;
	}
	public void setTpaName(String tpaName) {
		this.tpaName = tpaName;
	}
	public String getPpnNetworkHospitals() {
		return ppnNetworkHospitals;
	}
	public void setPpnNetworkHospitals(String ppnNetworkHospitals) {
		this.ppnNetworkHospitals = ppnNetworkHospitals;
	}
	public String getCategoryHospitaltertyPlus() {
		return categoryHospitaltertyPlus;
	}
	public void setCategoryHospitaltertyPlus(String categoryHospitaltertyPlus) {
		this.categoryHospitaltertyPlus = categoryHospitaltertyPlus;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailid() {
		return ccEmailid;
	}
	public void setCcEmailid(String ccEmailid) {
		this.ccEmailid = ccEmailid;
	}
	public String getPolicyDescription() {
		return policyDescription;
	}
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}
