package com.ghpl.model.iauth;

import java.io.Serializable;

public class BhratiAxaDataBean implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private String patientName;
	private String employeeName;
	private String employeeId;
	private String corporateName;
	private String sumInsured;
	private String requestedAmount;
	private String approvedAmount;
	private String emailId;
	private String ccEmailId;
	private String policyDescription;
	
	//NEW COLUMNS
	private int underWriterId;
	private int issuingOfficePrimaryKey;


	private String policyNumber="";
	private String balanceSi="";
	private String claimType="";
	private String policyPeriod="";
	private String initialInterimFinal="";
	private String hospitalName="";
	private String doaDod="";
	private String diagnosis="";
	private String treatment=""; 
	private String tariff="";
	private String anyNegotiations="";
	private String tpaRecommendations="";
	
	private String policyID;
	private String cardId;
	private String insuredId;
	private String ccnUnique;

	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getBalanceSi() {
		return balanceSi;
	}
	public void setBalanceSi(String balanceSi) {
		this.balanceSi = balanceSi;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getPolicyPeriod() {
		return policyPeriod;
	}
	public void setPolicyPeriod(String policyPeriod) {
		this.policyPeriod = policyPeriod;
	}
	public String getInitialInterimFinal() {
		return initialInterimFinal;
	}
	public void setInitialInterimFinal(String initialInterimFinal) {
		this.initialInterimFinal = initialInterimFinal;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getDoaDod() {
		return doaDod;
	}
	public void setDoaDod(String doaDod) {
		this.doaDod = doaDod;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getTreatment() {
		return treatment;
	}
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}
	public String getTariff() {
		return tariff;
	}
	public void setTariff(String tariff) {
		this.tariff = tariff;
	}
	public String getAnyNegotiations() {
		return anyNegotiations;
	}
	public void setAnyNegotiations(String anyNegotiations) {
		this.anyNegotiations = anyNegotiations;
	}
	public String getTpaRecommendations() {
		return tpaRecommendations;
	}
	public void setTpaRecommendations(String tpaRecommendations) {
		this.tpaRecommendations = tpaRecommendations;
	}
	
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public String getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(String approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailId() {
		return ccEmailId;
	}
	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}
	public String getPolicyDescription() {
		return policyDescription;
	}
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
