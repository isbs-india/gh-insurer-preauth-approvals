package com.ghpl.model.iauth;

import java.io.Serializable;

public class ReligareDataBean implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private String patientName;
	private String employeeName;
	private String age;
	private String gender;
	private String relationShip;
	private String sumInsured;
	private String packagee;
	private String hospitalName;
	private String city;
	private String doa;
	private String dod;
	private String durationOfStay;
	private String roomType;
	private String diagnosis;
	private String roomRentPaid;
	private String approvedAmount;
	private String requestedAmount;
	private String amtPaid;
	private String amtClaimed;
	private String copayPer;
	private String planOfTreatment;
	private String drRemarks;
	private String emailId;
	private String ccEmailid;
	
	//New columns
	private int underWriterId;
	private int issuingOfficePrimaryKey;
	
	private String policyID;
	private String cardId;
	private String insuredId;

	
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPackagee() {
		return packagee;
	}
	public void setPackagee(String packagee) {
		this.packagee = packagee;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = dod;
	}
	public String getDurationOfStay() {
		return durationOfStay;
	}
	public void setDurationOfStay(String durationOfStay) {
		this.durationOfStay = durationOfStay;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getRoomRentPaid() {
		return roomRentPaid;
	}
	public void setRoomRentPaid(String roomRentPaid) {
		this.roomRentPaid = roomRentPaid;
	}
	public String getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(String approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public String getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public String getAmtPaid() {
		return amtPaid;
	}
	public void setAmtPaid(String amtPaid) {
		this.amtPaid = amtPaid;
	}
	public String getAmtClaimed() {
		return amtClaimed;
	}
	public void setAmtClaimed(String amtClaimed) {
		this.amtClaimed = amtClaimed;
	}
	public String getCopayPer() {
		return copayPer;
	}
	public void setCopayPer(String copayPer) {
		this.copayPer = copayPer;
	}
	public String getPlanOfTreatment() {
		return planOfTreatment;
	}
	public void setPlanOfTreatment(String planOfTreatment) {
		this.planOfTreatment = planOfTreatment;
	}
	public String getDrRemarks() {
		return drRemarks;
	}
	public void setDrRemarks(String drRemarks) {
		this.drRemarks = drRemarks;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailid() {
		return ccEmailid;
	}
	public void setCcEmailid(String ccEmailid) {
		this.ccEmailid = ccEmailid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
	
}
