package com.ghpl.model.iauth;

import java.io.Serializable;

public class EmailDetailsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String emailId;
	private String ccEmailId;
		
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailId() {
		return ccEmailId;
	}
	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
