package com.ghpl.model.iauth;

import java.io.Serializable;

public class ItgiDataBean implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private String cardId;
	private String hospitalName;
	private String admDate;
	private String roomType;
	private String lengthOfStay;
	private String diagnosis;
	private String tpaFinalBillEstimate;
	private String emailId;
	private String ccEmailId;
	private String mobileHospRep;
	private String benificiaryName;

	private String medicalSurgDayCare;
	private String grName;
	private String city;
	private String mobileDoctor;
	private String teleBeneficiary;
	private String icuAdmissionRoom;
	private String roomIcuTariff;
	private String consultantCharges;
	private String surgeryCharges;
	private String otCharges;
	private String implantsCostIf_any;
	private String alAmountAskedByNsp;
	private String preNegotiatedTariff;
	private String rmoNursing;
	private String medicineCost;
	private String invstCosts;
	private String anaesthesiaCharges;
	private String anyOtherExpenses;
	private String recommendedAlamtBytpa;
	private String complaintsWithDuration;
	private String clinicalFindings;
	private String testsDoneSoFar;
	private String medicalDetails;
	private String surgeryRx;
	
	private String policyID;
	private String insuredId;
	
	//New columns
	private int underWriterId;
	private int issuingOfficePrimaryKey;
	
	
	
	
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getAdmDate() {
		return admDate;
	}
	public void setAdmDate(String admDate) {
		this.admDate = admDate;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getLengthOfStay() {
		return lengthOfStay;
	}
	public void setLengthOfStay(String lengthOfStay) {
		this.lengthOfStay = lengthOfStay;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getTpaFinalBillEstimate() {
		return tpaFinalBillEstimate;
	}
	public void setTpaFinalBillEstimate(String tpaFinalBillEstimate) {
		this.tpaFinalBillEstimate = tpaFinalBillEstimate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailId() {
		return ccEmailId;
	}
	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getMobileHospRep() {
		return mobileHospRep;
	}
	public void setMobileHospRep(String mobileHospRep) {
		this.mobileHospRep = mobileHospRep;
	}
	public String getBenificiaryName() {
		return benificiaryName;
	}
	public void setBenificiaryName(String benificiaryName) {
		this.benificiaryName = benificiaryName;
	}
	public String getMedicalSurgDayCare() {
		return medicalSurgDayCare;
	}
	public void setMedicalSurgDayCare(String medicalSurgDayCare) {
		this.medicalSurgDayCare = medicalSurgDayCare;
	}
	public String getGrName() {
		return grName;
	}
	public void setGrName(String grName) {
		this.grName = grName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getMobileDoctor() {
		return mobileDoctor;
	}
	public void setMobileDoctor(String mobileDoctor) {
		this.mobileDoctor = mobileDoctor;
	}
	public String getTeleBeneficiary() {
		return teleBeneficiary;
	}
	public void setTeleBeneficiary(String teleBeneficiary) {
		this.teleBeneficiary = teleBeneficiary;
	}
	public String getIcuAdmissionRoom() {
		return icuAdmissionRoom;
	}
	public void setIcuAdmissionRoom(String icuAdmissionRoom) {
		this.icuAdmissionRoom = icuAdmissionRoom;
	}
	public String getRoomIcuTariff() {
		return roomIcuTariff;
	}
	public void setRoomIcuTariff(String roomIcuTariff) {
		this.roomIcuTariff = roomIcuTariff;
	}
	public String getConsultantCharges() {
		return consultantCharges;
	}
	public void setConsultantCharges(String consultantCharges) {
		this.consultantCharges = consultantCharges;
	}
	public String getSurgeryCharges() {
		return surgeryCharges;
	}
	public void setSurgeryCharges(String surgeryCharges) {
		this.surgeryCharges = surgeryCharges;
	}
	public String getOtCharges() {
		return otCharges;
	}
	public void setOtCharges(String otCharges) {
		this.otCharges = otCharges;
	}
	public String getImplantsCostIf_any() {
		return implantsCostIf_any;
	}
	public void setImplantsCostIf_any(String implantsCostIf_any) {
		this.implantsCostIf_any = implantsCostIf_any;
	}
	public String getAlAmountAskedByNsp() {
		return alAmountAskedByNsp;
	}
	public void setAlAmountAskedByNsp(String alAmountAskedByNsp) {
		this.alAmountAskedByNsp = alAmountAskedByNsp;
	}
	public String getPreNegotiatedTariff() {
		return preNegotiatedTariff;
	}
	public void setPreNegotiatedTariff(String preNegotiatedTariff) {
		this.preNegotiatedTariff = preNegotiatedTariff;
	}
	public String getRmoNursing() {
		return rmoNursing;
	}
	public void setRmoNursing(String rmoNursing) {
		this.rmoNursing = rmoNursing;
	}
	public String getMedicineCost() {
		return medicineCost;
	}
	public void setMedicineCost(String medicineCost) {
		this.medicineCost = medicineCost;
	}
	public String getInvstCosts() {
		return invstCosts;
	}
	public void setInvstCosts(String invstCosts) {
		this.invstCosts = invstCosts;
	}
	public String getAnaesthesiaCharges() {
		return anaesthesiaCharges;
	}
	public void setAnaesthesiaCharges(String anaesthesiaCharges) {
		this.anaesthesiaCharges = anaesthesiaCharges;
	}
	public String getAnyOtherExpenses() {
		return anyOtherExpenses;
	}
	public void setAnyOtherExpenses(String anyOtherExpenses) {
		this.anyOtherExpenses = anyOtherExpenses;
	}
	public String getRecommendedAlamtBytpa() {
		return recommendedAlamtBytpa;
	}
	public void setRecommendedAlamtBytpa(String recommendedAlamtBytpa) {
		this.recommendedAlamtBytpa = recommendedAlamtBytpa;
	}
	public String getComplaintsWithDuration() {
		return complaintsWithDuration;
	}
	public void setComplaintsWithDuration(String complaintsWithDuration) {
		this.complaintsWithDuration = complaintsWithDuration;
	}
	public String getClinicalFindings() {
		return clinicalFindings;
	}
	public void setClinicalFindings(String clinicalFindings) {
		this.clinicalFindings = clinicalFindings;
	}
	public String getTestsDoneSoFar() {
		return testsDoneSoFar;
	}
	public void setTestsDoneSoFar(String testsDoneSoFar) {
		this.testsDoneSoFar = testsDoneSoFar;
	}
	public String getMedicalDetails() {
		return medicalDetails;
	}
	public void setMedicalDetails(String medicalDetails) {
		this.medicalDetails = medicalDetails;
	}
	public String getSurgeryRx() {
		return surgeryRx;
	}
	public void setSurgeryRx(String surgeryRx) {
		this.surgeryRx = surgeryRx;
	}

}
