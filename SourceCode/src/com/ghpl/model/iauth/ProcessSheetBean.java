package com.ghpl.model.iauth;

import java.util.ArrayList;
import java.util.List;

public class ProcessSheetBean {
	
	List<Claims> claimDetailsList = new ArrayList<Claims>();
	List<DocumentBean> documentList = new ArrayList<DocumentBean>();
	
	
	public List<Claims> getClaimDetailsList() {
		return claimDetailsList;
	}
	public void setClaimDetailsList(List<Claims> claimDetailsList) {
		this.claimDetailsList = claimDetailsList;
	}
	public List<DocumentBean> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<DocumentBean> documentList) {
		this.documentList = documentList;
	}
}
