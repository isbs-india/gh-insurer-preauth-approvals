package com.ghpl.model.iauth;

public class PreAuthXMLBean {
    private String preappno;
    private String claimID;
    private String preauthflag;
    private String filename;
    private String typeOfDoc;
    private String module;
    private String actualfilename;
    private String maindocid;
    private String subdocid;
    private String srcofclaim;
    
	public String getSrcofclaim() {
		return srcofclaim;
	}
	public void setSrcofclaim(String srcofclaim) {
		this.srcofclaim = srcofclaim;
	}
	public String getActualfilename() {
		return actualfilename;
	}
	public void setActualfilename(String actualfilename) {
		this.actualfilename = actualfilename;
	}
	public String getMaindocid() {
		return maindocid;
	}
	public void setMaindocid(String maindocid) {
		this.maindocid = maindocid;
	}
	public String getSubdocid() {
		return subdocid;
	}
	public void setSubdocid(String subdocid) {
		this.subdocid = subdocid;
	}
	public String getPreappno() {
		return preappno;
	}
	public void setPreappno(String preappno) {
		this.preappno = preappno;
	}
	public String getClaimID() {
		return claimID;
	}
	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}
	public String getPreauthflag() {
		return preauthflag;
	}
	public void setPreauthflag(String preauthflag) {
		this.preauthflag = preauthflag;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getTypeOfDoc() {
		return typeOfDoc;
	}
	public void setTypeOfDoc(String typeOfDoc) {
		this.typeOfDoc = typeOfDoc;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	

}
