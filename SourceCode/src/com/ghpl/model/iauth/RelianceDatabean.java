package com.ghpl.model.iauth;

import java.io.Serializable;

public class RelianceDatabean implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private String caseType;
	private String claimType;
	private String preauthCaseId;
	private String claimNoUrlNo;
	private String patientName;
	private String  age;
	private String   hospitalName;
	private String  hospitalType;
	private String  diagnosis;
	private String   corporateName;
	private String   policyNo;
	private String   sumInsured;
	private String   balanceSumInsured;
	private String   lineOfTreatment;
	private String   remarks;
	private String   requestedAmount;
	private String   initialAmountPaid;
	private String   payableAmount;
	private String   corporateBuffer;
	private String   emailId;
	private String   ccemailId;
	private String employeeId;
	private String policyDescription;
	
	//New columns
	private int underWriterId;
	private int issuingOfficePrimaryKey;
	
	private String policyID;
	private String cardId;
	private String insuredId;

	
	
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getPolicyDescription() {
		return policyDescription;
	}
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getPreauthCaseId() {
		return preauthCaseId;
	}
	public void setPreauthCaseId(String preauthCaseId) {
		this.preauthCaseId = preauthCaseId;
	}
	public String getClaimNoUrlNo() {
		return claimNoUrlNo;
	}
	public void setClaimNoUrlNo(String claimNoUrlNo) {
		this.claimNoUrlNo = claimNoUrlNo;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalType() {
		return hospitalType;
	}
	public void setHospitalType(String hospitalType) {
		this.hospitalType = hospitalType;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getCorporateName() {
		return corporateName;
	}
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getBalanceSumInsured() {
		return balanceSumInsured;
	}
	public void setBalanceSumInsured(String balanceSumInsured) {
		this.balanceSumInsured = balanceSumInsured;
	}
	public String getLineOfTreatment() {
		return lineOfTreatment;
	}
	public void setLineOfTreatment(String lineOfTreatment) {
		this.lineOfTreatment = lineOfTreatment;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public String getInitialAmountPaid() {
		return initialAmountPaid;
	}
	public void setInitialAmountPaid(String initialAmountPaid) {
		this.initialAmountPaid = initialAmountPaid;
	}
	public String getPayableAmount() {
		return payableAmount;
	}
	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}
	public String getCorporateBuffer() {
		return corporateBuffer;
	}
	public void setCorporateBuffer(String corporateBuffer) {
		this.corporateBuffer = corporateBuffer;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcemailId() {
		return ccemailId;
	}
	public void setCcemailId(String ccemailId) {
		this.ccemailId = ccemailId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	 
  	  
}
