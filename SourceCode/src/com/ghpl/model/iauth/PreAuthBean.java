package com.ghpl.model.iauth;

import java.io.ByteArrayInputStream;
import java.util.List;

public class PreAuthBean {
	private int moduleId;
	private String moduleName;
	private String rid;
	private String uwCode;
	private int policyId;
	private int insuredId;
	private String bp;
	private String temp;
	private String rs;
	private String pa;
	private int maternity;
	private String gVal;
	private String pVal;
	private String lVal;
	private String aVal;
	private String lmp;
	private String edDate;
	private String modeOfDelivery;
	private int accident;
	private String drugInflunce;
	private String mlcNo;
	private int deliveryType;
	private String firstConsultDoctor;
	private String firstConsultDate;
	private String ipno;
	private String billNo;
	private int userId;
	private int moduleUserId;
	private String branch;
	private int tpaId;
	private String patContactPerson;
	private String patMobileNo;
	private String patLandLineNo;
	private String patEmail;
	private String patAddress;
	private String nspContactPerson;
	private String nspMobileNo;
	private String nspLandLineNo;
	private String nspEmail;
	private String nspAddress;
	private String claimFromSource;
	private String kayakoId;
	private String patBedNo;
	private String pbid;
	private int mcSymId;
	private String mcSymDetails;
	private String alignmentDuration;
	private int investId;
	private String investDetails;
	private String investPropDetails;
	private int diseaseId;
	private String diseaseDesc;
	private String diseaseCodeRange;
	private String diseaseCodeRangeDesc;
	private String procedure;
	private int procedureId;
	private String procedureDesc;
	private String hospitalId;
	private String hospitalFrom;
	private String hospitalTo;
	private String claimedHospAmt;
	private String doctorName;
	private int noofDays;
	private int mainHeadId;
	private int subHeadId;
	private String ReqAmt;
	private int SubSubHeadId;
	private String finalStatus;
	private int claimId;
	private int reAuthFlag;
	private String xmlString;
	private String cardid;
	private int age;
	private String preAuthFlag;
	private String remarks;
	private String roomType;
	private String role;
	private String moduleData;
	private int PreauthNo;
	private int claimStatusId;
	private String doj;
	private String dol;
	private String dob;
	private String dod;
	private String userName;
	private String filename;
	
	private String inceptionDate;
	private String typeOfDoc;
	private String randomVal;
	private String cardHolderName;
	private String empid;
	private String sumInsured;
	private String policyNo;
	private String policyHolderName;
	private String relation;
	private String policyFrom;
	private String policyTo;
	private String enrolmentExcep;
	private String blocking;
	private int familyId;
	private String currentDate;
	private int serialNo;
	private String selfName;
	private String doa;
	private String reqType;
	private String hospName;
	private String osticketId;
	private String hospId;
	private int roleId;
	private int renewalStatus;
	private String insuredStatus;
	private String bano;
	private String uwId;
	private String gender;
	private String preAuthUrl;
	
	private String branchName;
	private String tpaid;
	private String hospAddress;
	private String issusingofficeid;
	private String selfcardid;
	private int createdById;
	private int totalNoDocs;
	private int statusId;
	private String companyName;
	private int companyId;
	private int transStatusId;
	private String collectionDate;
	private String insurerReply;
	private String insurerRemarks;
	private int claimUpdateCNT;
	private int numberUpdates;
	private String isppn;
	private String copayper;
	private String copayamount;
	private int claimcatLimt;
	
	private String diseaseCode;
	private String disceaseDescription;
	
	
	private String procedureCode;
	private String precedureName;
	private String MedicoActionName;
	
	private int currentStatusId;
	private String currentStatusRet;
	private String nextStatusRet;
	private String insurerSentCount;
	private String denialFlag;
	private String authAmt;
	private String approvedAmtTotal;
	private String finalApproveAmtTotal;
	
	private int nextStatus;
	
	private int remarksId;
	private int letterTypeId;
	//private int reauthFlag;
	private String auditorComments;
	private int reAuthFlagNotation;
	
	private double accidentHospitalisationOnly;
	private double ambulance;
	private double attendantAllowance;
	private double corporateFloater;
	private double criticalIllnessBuffer;
	private double criticalIllnessExclusion;
	private double criticalIllnessOnly;
	private double dailyCash;
	private double dayCare;
	private double dental;
	private double diseaseFloater;
	private double domHosp;
	private double domiciliary;
	private double familyFloater;
	private double healthCheckUp;
	private double hospitalisation;
	private double icu;
	private double opd;
	private double optical;
	private double personalAccidentCover;
	private double pharmacy;
	private double postHospitalisation;
	private double postNatal;
	private double preHospitalisation;
	private double preNatal;
	private double roomRent;
	private double surgeryCoverOnly;
	private double maternityLimt;
	
	private int floater;
	
	private int outputMessageId;
	private String outputMessage;
	private int coverId;
	private double denialAmt;
	private int insurerSentCountDenial;
	private String medicoComments;
	private String auditStatus;
    private int preauthFlow;
    private int appAmtFlag;
    private int ghplStatusId;
    private ByteArrayInputStream authorizationBeanObject;
 	public ByteArrayInputStream getAuthorizationBeanObject() {
		return authorizationBeanObject;
	}
	public void setAuthorizationBeanObject(
			ByteArrayInputStream authorizationBeanObject) {
		this.authorizationBeanObject = authorizationBeanObject;
	}
	public String getDiseaseCodeRangeDesc() {
		return diseaseCodeRangeDesc;
	}
	public void setDiseaseCodeRangeDesc(String diseaseCodeRangeDesc) {
		this.diseaseCodeRangeDesc = diseaseCodeRangeDesc;
	}
	public int getGhplStatusId() {
		return ghplStatusId;
	}
	public void setGhplStatusId(int ghplStatusId) {
		this.ghplStatusId = ghplStatusId;
	}
	public String getAuditStatus() {
		return auditStatus;
	}
	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}
	public int getPreauthFlow() {
		return preauthFlow;
	}
	public void setPreauthFlow(int preauthFlow) {
		this.preauthFlow = preauthFlow;
	}
	public int getAppAmtFlag() {
		return appAmtFlag;
	}
	public void setAppAmtFlag(int appAmtFlag) {
		this.appAmtFlag = appAmtFlag;
	}
	public String getNextStatusRet() {
		return nextStatusRet;
	}
	public void setNextStatusRet(String nextStatusRet) {
		this.nextStatusRet = nextStatusRet;
	}
	public String getCurrentStatusRet() {
		return currentStatusRet;
	}
	public void setCurrentStatusRet(String currentStatusRet) {
		this.currentStatusRet = currentStatusRet;
	}
	public String getProcedure() {
		return procedure;
	}
	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}
	public int getPreauthNo() {
		return PreauthNo;
	}
	public void setPreauthNo(int preauthNo) {
		PreauthNo = preauthNo;
	}
	public String getDiseaseCodeRange() {
		return diseaseCodeRange;
	}
	public void setDiseaseCodeRange(String diseaseCodeRange) {
		this.diseaseCodeRange = diseaseCodeRange;
	}
	public String getMedicoComments() {
		return medicoComments;
	}
	public void setMedicoComments(String medicoComments) {
		this.medicoComments = medicoComments;
	}
	public int getInsurerSentCountDenial() {
		return insurerSentCountDenial;
	}
	public void setInsurerSentCountDenial(int insurerSentCountDenial) {
		this.insurerSentCountDenial = insurerSentCountDenial;
	}
	public double getDenialAmt() {
		return denialAmt;
	}
	public void setDenialAmt(double denialAmt) {
		this.denialAmt = denialAmt;
	}
	public int getReAuthFlagNotation() {
		return reAuthFlagNotation;
	}
	public void setReAuthFlagNotation(int reAuthFlagNotation) {
		this.reAuthFlagNotation = reAuthFlagNotation;
	}
	public String getApprovedAmtTotal() {
		return approvedAmtTotal;
	}
	public void setApprovedAmtTotal(String approvedAmtTotal) {
		this.approvedAmtTotal = approvedAmtTotal;
	}
	public int getModuleUserId() {
		return moduleUserId;
	}
	public void setModuleUserId(int moduleUserId) {
		this.moduleUserId = moduleUserId;
	}
	public double getMaternityLimt() {
		return maternityLimt;
	}
	public void setMaternityLimt(double maternityLimt) {
		this.maternityLimt = maternityLimt;
	}
	public int getCoverId() {
		return coverId;
	}
	public void setCoverId(int coverId) {
		this.coverId = coverId;
	}
	public int getFloater() {
		return floater;
	}
	public void setFloater(int floater) {
		this.floater = floater;
	}
	public int getOutputMessageId() {
		return outputMessageId;
	}
	public void setOutputMessageId(int outputMessageId) {
		this.outputMessageId = outputMessageId;
	}
	public String getOutputMessage() {
		return outputMessage;
	}
	public void setOutputMessage(String outputMessage) {
		this.outputMessage = outputMessage;
	}
	public double getAccidentHospitalisationOnly() {
		return accidentHospitalisationOnly;
	}
	public void setAccidentHospitalisationOnly(double accidentHospitalisationOnly) {
		this.accidentHospitalisationOnly = accidentHospitalisationOnly;
	}
	public double getAmbulance() {
		return ambulance;
	}
	public void setAmbulance(double ambulance) {
		this.ambulance = ambulance;
	}
	public double getAttendantAllowance() {
		return attendantAllowance;
	}
	public void setAttendantAllowance(double attendantAllowance) {
		this.attendantAllowance = attendantAllowance;
	}
	public double getCorporateFloater() {
		return corporateFloater;
	}
	public void setCorporateFloater(double corporateFloater) {
		this.corporateFloater = corporateFloater;
	}
	public double getCriticalIllnessBuffer() {
		return criticalIllnessBuffer;
	}
	public void setCriticalIllnessBuffer(double criticalIllnessBuffer) {
		this.criticalIllnessBuffer = criticalIllnessBuffer;
	}
	public double getCriticalIllnessExclusion() {
		return criticalIllnessExclusion;
	}
	public void setCriticalIllnessExclusion(double criticalIllnessExclusion) {
		this.criticalIllnessExclusion = criticalIllnessExclusion;
	}
	public double getCriticalIllnessOnly() {
		return criticalIllnessOnly;
	}
	public void setCriticalIllnessOnly(double criticalIllnessOnly) {
		this.criticalIllnessOnly = criticalIllnessOnly;
	}
	public double getDailyCash() {
		return dailyCash;
	}
	public void setDailyCash(double dailyCash) {
		this.dailyCash = dailyCash;
	}
	public double getDayCare() {
		return dayCare;
	}
	public void setDayCare(double dayCare) {
		this.dayCare = dayCare;
	}
	public double getDental() {
		return dental;
	}
	public void setDental(double dental) {
		this.dental = dental;
	}
	public double getDiseaseFloater() {
		return diseaseFloater;
	}
	public void setDiseaseFloater(double diseaseFloater) {
		this.diseaseFloater = diseaseFloater;
	}
	public double getDomHosp() {
		return domHosp;
	}
	public void setDomHosp(double domHosp) {
		this.domHosp = domHosp;
	}
	public double getDomiciliary() {
		return domiciliary;
	}
	public void setDomiciliary(double domiciliary) {
		this.domiciliary = domiciliary;
	}
	public double getFamilyFloater() {
		return familyFloater;
	}
	public void setFamilyFloater(double familyFloater) {
		this.familyFloater = familyFloater;
	}
	public double getHealthCheckUp() {
		return healthCheckUp;
	}
	public void setHealthCheckUp(double healthCheckUp) {
		this.healthCheckUp = healthCheckUp;
	}
	public double getHospitalisation() {
		return hospitalisation;
	}
	public void setHospitalisation(double hospitalisation) {
		this.hospitalisation = hospitalisation;
	}
	public double getIcu() {
		return icu;
	}
	public void setIcu(double icu) {
		this.icu = icu;
	}
	public double getOpd() {
		return opd;
	}
	public void setOpd(double opd) {
		this.opd = opd;
	}
	public double getOptical() {
		return optical;
	}
	public void setOptical(double optical) {
		this.optical = optical;
	}
	public double getPersonalAccidentCover() {
		return personalAccidentCover;
	}
	public void setPersonalAccidentCover(double personalAccidentCover) {
		this.personalAccidentCover = personalAccidentCover;
	}
	public double getPharmacy() {
		return pharmacy;
	}
	public void setPharmacy(double pharmacy) {
		this.pharmacy = pharmacy;
	}
	public double getPostHospitalisation() {
		return postHospitalisation;
	}
	public void setPostHospitalisation(double postHospitalisation) {
		this.postHospitalisation = postHospitalisation;
	}
	public double getPostNatal() {
		return postNatal;
	}
	public void setPostNatal(double postNatal) {
		this.postNatal = postNatal;
	}
	public double getPreHospitalisation() {
		return preHospitalisation;
	}
	public void setPreHospitalisation(double preHospitalisation) {
		this.preHospitalisation = preHospitalisation;
	}
	public double getPreNatal() {
		return preNatal;
	}
	public void setPreNatal(double preNatal) {
		this.preNatal = preNatal;
	}
	public double getRoomRent() {
		return roomRent;
	}
	public void setRoomRent(double roomRent) {
		this.roomRent = roomRent;
	}
	public double getSurgeryCoverOnly() {
		return surgeryCoverOnly;
	}
	public void setSurgeryCoverOnly(double surgeryCoverOnly) {
		this.surgeryCoverOnly = surgeryCoverOnly;
	}
	public String getAuditorComments() {
		return auditorComments;
	}
	public void setAuditorComments(String auditorComments) {
		this.auditorComments = auditorComments;
	}
	public int getNextStatus() {
		return nextStatus;
	}
	public void setNextStatus(int nextStatus) {
		this.nextStatus = nextStatus;
	}
	
	public int getRemarksId() {
		return remarksId;
	}
	public void setRemarksId(int remarksId) {
		this.remarksId = remarksId;
	}
	public int getLetterTypeId() {
		return letterTypeId;
	}
	public void setLetterTypeId(int letterTypeId) {
		this.letterTypeId = letterTypeId;
	}
	
	public int getCurrentStatusId() {
		return currentStatusId;
	}
	public void setCurrentStatusId(int currentStatusId) {
		this.currentStatusId = currentStatusId;
	}
	public String getInsurerSentCount() {
		return insurerSentCount;
	}
	public void setInsurerSentCount(String insurerSentCount) {
		this.insurerSentCount = insurerSentCount;
	}
	public String getDenialFlag() {
		return denialFlag;
	}
	public void setDenialFlag(String denialFlag) {
		this.denialFlag = denialFlag;
	}
	public String getAuthAmt() {
		return authAmt;
	}
	public void setAuthAmt(String authAmt) {
		this.authAmt = authAmt;
	}
	public String getFinalApproveAmtTotal() {
		return finalApproveAmtTotal;
	}
	public void setFinalApproveAmtTotal(String finalApproveAmtTotal) {
		this.finalApproveAmtTotal = finalApproveAmtTotal;
	}
	public String getMedicoActionName() {
		return MedicoActionName;
	}
	public void setMedicoActionName(String medicoActionName) {
		MedicoActionName = medicoActionName;
	}
	public String getDiseaseCode() {
		return diseaseCode;
	}
	public void setDiseaseCode(String diseaseCode) {
		this.diseaseCode = diseaseCode;
	}
	public String getDisceaseDescription() {
		return disceaseDescription;
	}
	public void setDisceaseDescription(String disceaseDescription) {
		this.disceaseDescription = disceaseDescription;
	}
	public String getProcedureCode() {
		return procedureCode;
	}
	public void setProcedureCode(String procedureCode) {
		this.procedureCode = procedureCode;
	}
	public String getPrecedureName() {
		return precedureName;
	}
	public void setPrecedureName(String precedureName) {
		this.precedureName = precedureName;
	}
	public String getCopayper() {
		return copayper;
	}
	public void setCopayper(String copayper) {
		this.copayper = copayper;
	}
	public String getCopayamount() {
		return copayamount;
	}
	public void setCopayamount(String copayamount) {
		this.copayamount = copayamount;
	}
	public int getClaimcatLimt() {
		return claimcatLimt;
	}
	public void setClaimcatLimt(int claimcatLimt) {
		this.claimcatLimt = claimcatLimt;
	}
	public String getIsppn() {
		return isppn;
	}
	public void setIsppn(String isppn) {
		this.isppn = isppn;
	}
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = dod;
	}
	public int getClaimUpdateCNT() {
		return claimUpdateCNT;
	}
	public void setClaimUpdateCNT(int claimUpdateCNT) {
		this.claimUpdateCNT = claimUpdateCNT;
	}
	public int getNumberUpdates() {
		return numberUpdates;
	}
	public void setNumberUpdates(int numberUpdates) {
		this.numberUpdates = numberUpdates;
	}
	public String getInsurerReply() {
		return insurerReply;
	}
	public void setInsurerReply(String insurerReply) {
		this.insurerReply = insurerReply;
	}
	public String getInsurerRemarks() {
		return insurerRemarks;
	}
	public void setInsurerRemarks(String insurerRemarks) {
		this.insurerRemarks = insurerRemarks;
	}
	public String getRandomVal() {
		return randomVal;
	}
	public void setRandomVal(String randomVal) {
		this.randomVal = randomVal;
	}
	public String getTypeOfDoc() {
		return typeOfDoc;
	}
	public void setTypeOfDoc(String typeOfDoc) {
		this.typeOfDoc = typeOfDoc;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getTransStatusId() {
		return transStatusId;
	}
	public void setTransStatusId(int transStatusId) {
		this.transStatusId = transStatusId;
	}
	public String getCollectionDate() {
		return collectionDate;
	}
	public void setCollectionDate(String collectionDate) {
		this.collectionDate = collectionDate;
	}
	public int getTotalNoDocs() {
		return totalNoDocs;
	}
	public void setTotalNoDocs(int totalNoDocs) {
		this.totalNoDocs = totalNoDocs;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public int getCreatedById() {
		return createdById;
	}
	public void setCreatedById(int createdById) {
		this.createdById = createdById;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}
	public String getEnrolmentExcep() {
		return enrolmentExcep;
	}
	public void setEnrolmentExcep(String enrolmentExcep) {
		this.enrolmentExcep = enrolmentExcep;
	}
	public String getBlocking() {
		return blocking;
	}
	public void setBlocking(String blocking) {
		this.blocking = blocking;
	}
	public int getFamilyId() {
		return familyId;
	}
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getSelfName() {
		return selfName;
	}
	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	public String getReqType() {
		return reqType;
	}
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public String getOsticketId() {
		return osticketId;
	}
	public void setOsticketId(String osticketId) {
		this.osticketId = osticketId;
	}
	public String getHospId() {
		return hospId;
	}
	public void setHospId(String hospId) {
		this.hospId = hospId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getRenewalStatus() {
		return renewalStatus;
	}
	public void setRenewalStatus(int renewalStatus) {
		this.renewalStatus = renewalStatus;
	}
	public String getInsuredStatus() {
		return insuredStatus;
	}
	public void setInsuredStatus(String insuredStatus) {
		this.insuredStatus = insuredStatus;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getUwId() {
		return uwId;
	}
	public void setUwId(String uwId) {
		this.uwId = uwId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPreAuthUrl() {
		return preAuthUrl;
	}
	public void setPreAuthUrl(String preAuthUrl) {
		this.preAuthUrl = preAuthUrl;
	}
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getTpaid() {
		return tpaid;
	}
	public void setTpaid(String tpaid) {
		this.tpaid = tpaid;
	}
	public String getHospAddress() {
		return hospAddress;
	}
	public void setHospAddress(String hospAddress) {
		this.hospAddress = hospAddress;
	}
	public String getIssusingofficeid() {
		return issusingofficeid;
	}
	public void setIssusingofficeid(String issusingofficeid) {
		this.issusingofficeid = issusingofficeid;
	}
	public String getSelfcardid() {
		return selfcardid;
	}
	public void setSelfcardid(String selfcardid) {
		this.selfcardid = selfcardid;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getDol() {
		return dol;
	}
	public void setDol(String dol) {
		this.dol = dol;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getClaimStatusId() {
		return claimStatusId;
	}
	public void setClaimStatusId(int claimStatusId) {
		this.claimStatusId = claimStatusId;
	}
	
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleData() {
		return moduleData;
	}
	public void setModuleData(String moduleData) {
		this.moduleData = moduleData;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPreAuthFlag() {
		return preAuthFlag;
	}
	public void setPreAuthFlag(String preAuthFlag) {
		this.preAuthFlag = preAuthFlag;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getUwCode() {
		return uwCode;
	}
	public void setUwCode(String uwCode) {
		this.uwCode = uwCode;
	}
	public int getPolicyId() {
		return policyId;
	}
	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}
	public int getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(int insuredId) {
		this.insuredId = insuredId;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public String getRs() {
		return rs;
	}
	public void setRs(String rs) {
		this.rs = rs;
	}
	public String getPa() {
		return pa;
	}
	public void setPa(String pa) {
		this.pa = pa;
	}
	public int getMaternity() {
		return maternity;
	}
	public void setMaternity(int maternity) {
		this.maternity = maternity;
	}
	public String getgVal() {
		return gVal;
	}
	public void setgVal(String gVal) {
		this.gVal = gVal;
	}
	public String getpVal() {
		return pVal;
	}
	public void setpVal(String pVal) {
		this.pVal = pVal;
	}
	public String getlVal() {
		return lVal;
	}
	public void setlVal(String lVal) {
		this.lVal = lVal;
	}
	public String getaVal() {
		return aVal;
	}
	public void setaVal(String aVal) {
		this.aVal = aVal;
	}
	public String getLmp() {
		return lmp;
	}
	public void setLmp(String lmp) {
		this.lmp = lmp;
	}
	public String getEdDate() {
		return edDate;
	}
	public void setEdDate(String edDate) {
		this.edDate = edDate;
	}
	public String getModeOfDelivery() {
		return modeOfDelivery;
	}
	public void setModeOfDelivery(String modeOfDelivery) {
		this.modeOfDelivery = modeOfDelivery;
	}
	public int getAccident() {
		return accident;
	}
	public void setAccident(int accident) {
		this.accident = accident;
	}
	public String getDrugInflunce() {
		return drugInflunce;
	}
	public void setDrugInflunce(String drugInflunce) {
		this.drugInflunce = drugInflunce;
	}
	public String getMlcNo() {
		return mlcNo;
	}
	public void setMlcNo(String mlcNo) {
		this.mlcNo = mlcNo;
	}
	public int getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(int deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getFirstConsultDoctor() {
		return firstConsultDoctor;
	}
	public void setFirstConsultDoctor(String firstConsultDoctor) {
		this.firstConsultDoctor = firstConsultDoctor;
	}
	public String getFirstConsultDate() {
		return firstConsultDate;
	}
	public void setFirstConsultDate(String firstConsultDate) {
		this.firstConsultDate = firstConsultDate;
	}
	public String getIpno() {
		return ipno;
	}
	public void setIpno(String ipno) {
		this.ipno = ipno;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getTpaId() {
		return tpaId;
	}
	public void setTpaId(int tpaId) {
		this.tpaId = tpaId;
	}
	public String getPatContactPerson() {
		return patContactPerson;
	}
	public void setPatContactPerson(String patContactPerson) {
		this.patContactPerson = patContactPerson;
	}
	public String getPatMobileNo() {
		return patMobileNo;
	}
	public void setPatMobileNo(String patMobileNo) {
		this.patMobileNo = patMobileNo;
	}
	public String getPatLandLineNo() {
		return patLandLineNo;
	}
	public void setPatLandLineNo(String patLandLineNo) {
		this.patLandLineNo = patLandLineNo;
	}
	public String getPatEmail() {
		return patEmail;
	}
	public void setPatEmail(String patEmail) {
		this.patEmail = patEmail;
	}
	public String getPatAddress() {
		return patAddress;
	}
	public void setPatAddress(String patAddress) {
		this.patAddress = patAddress;
	}
	public String getNspContactPerson() {
		return nspContactPerson;
	}
	public void setNspContactPerson(String nspContactPerson) {
		this.nspContactPerson = nspContactPerson;
	}
	public String getNspMobileNo() {
		return nspMobileNo;
	}
	public void setNspMobileNo(String nspMobileNo) {
		this.nspMobileNo = nspMobileNo;
	}
	public String getNspLandLineNo() {
		return nspLandLineNo;
	}
	public void setNspLandLineNo(String nspLandLineNo) {
		this.nspLandLineNo = nspLandLineNo;
	}
	public String getNspEmail() {
		return nspEmail;
	}
	public void setNspEmail(String nspEmail) {
		this.nspEmail = nspEmail;
	}
	public String getNspAddress() {
		return nspAddress;
	}
	public void setNspAddress(String nspAddress) {
		this.nspAddress = nspAddress;
	}
	public String getClaimFromSource() {
		return claimFromSource;
	}
	public void setClaimFromSource(String claimFromSource) {
		this.claimFromSource = claimFromSource;
	}
	public String getKayakoId() {
		return kayakoId;
	}
	public void setKayakoId(String kayakoId) {
		this.kayakoId = kayakoId;
	}
	public String getPatBedNo() {
		return patBedNo;
	}
	public void setPatBedNo(String patBedNo) {
		this.patBedNo = patBedNo;
	}
	public String getPbid() {
		return pbid;
	}
	public void setPbid(String pbid) {
		this.pbid = pbid;
	}
	public int getMcSymId() {
		return mcSymId;
	}
	public void setMcSymId(int mcSymId) {
		this.mcSymId = mcSymId;
	}
	public String getMcSymDetails() {
		return mcSymDetails;
	}
	public void setMcSymDetails(String mcSymDetails) {
		this.mcSymDetails = mcSymDetails;
	}
	public String getAlignmentDuration() {
		return alignmentDuration;
	}
	public void setAlignmentDuration(String alignmentDuration) {
		this.alignmentDuration = alignmentDuration;
	}
	public int getInvestId() {
		return investId;
	}
	public void setInvestId(int investId) {
		this.investId = investId;
	}
	public String getInvestDetails() {
		return investDetails;
	}
	public void setInvestDetails(String investDetails) {
		this.investDetails = investDetails;
	}
	public String getInvestPropDetails() {
		return investPropDetails;
	}
	public void setInvestPropDetails(String investPropDetails) {
		this.investPropDetails = investPropDetails;
	}
	public int getDiseaseId() {
		return diseaseId;
	}
	public void setDiseaseId(int diseaseId) {
		this.diseaseId = diseaseId;
	}
	public String getDiseaseDesc() {
		return diseaseDesc;
	}
	public void setDiseaseDesc(String diseaseDesc) {
		this.diseaseDesc = diseaseDesc;
	}
	public int getProcedureId() {
		return procedureId;
	}
	public void setProcedureId(int procedureId) {
		this.procedureId = procedureId;
	}
	public String getProcedureDesc() {
		return procedureDesc;
	}
	public void setProcedureDesc(String procedureDesc) {
		this.procedureDesc = procedureDesc;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalFrom() {
		return hospitalFrom;
	}
	public void setHospitalFrom(String hospitalFrom) {
		this.hospitalFrom = hospitalFrom;
	}
	public String getHospitalTo() {
		return hospitalTo;
	}
	public void setHospitalTo(String hospitalTo) {
		this.hospitalTo = hospitalTo;
	}
	public String getClaimedHospAmt() {
		return claimedHospAmt;
	}
	public void setClaimedHospAmt(String claimedHospAmt) {
		this.claimedHospAmt = claimedHospAmt;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public int getNoofDays() {
		return noofDays;
	}
	public void setNoofDays(int noofDays) {
		this.noofDays = noofDays;
	}
	public int getMainHeadId() {
		return mainHeadId;
	}
	public void setMainHeadId(int mainHeadId) {
		this.mainHeadId = mainHeadId;
	}
	public int getSubHeadId() {
		return subHeadId;
	}
	public void setSubHeadId(int subHeadId) {
		this.subHeadId = subHeadId;
	}
	public String getReqAmt() {
		return ReqAmt;
	}
	public void setReqAmt(String reqAmt) {
		ReqAmt = reqAmt;
	}
	public int getSubSubHeadId() {
		return SubSubHeadId;
	}
	public void setSubSubHeadId(int subSubHeadId) {
		SubSubHeadId = subSubHeadId;
	}
	public String getFinalStatus() {
		return finalStatus;
	}
	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}
	public int getClaimId() {
		return claimId;
	}
	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}
	public int getReAuthFlag() {
		return reAuthFlag;
	}
	public void setReAuthFlag(int reAuthFlag) {
		this.reAuthFlag = reAuthFlag;
	}
	public String getXmlString() {
		return xmlString;
	}
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	
	
	
	
	
	
	
	
}
