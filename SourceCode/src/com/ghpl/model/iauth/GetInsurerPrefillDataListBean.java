package com.ghpl.model.iauth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import com.ghpl.model.iauth.DocsDetailsCorpBean;
//import com.ghpl.model.iauth.UiicDataBean;
//import com.ghpl.model.iauth.BhratiAxaDataBean;


public class GetInsurerPrefillDataListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
	private UiicDataBean uiicBean;
	private NIADataBean niaBean;
	private DocsDetailsCorpBean docsBean;
	private BhratiAxaDataBean bhaxaBean;
	private RelianceDatabean relianceBean;
    private ItgiDataBean itgiBean;	
    private ReligareDataBean religareBean;
    private ClaimDenialBean clmDenialBean;
    private String fileName;
    private String ccnUnique;
   

    public String getCcnUnique() {
		return ccnUnique;
	}

	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
    private EmailDetailsBean emailCommonDetaisBean;
	
	
	public EmailDetailsBean getEmailCommonDetaisBean() {
		return emailCommonDetaisBean;
	}

	public void setEmailCommonDetaisBean(EmailDetailsBean emailCommonDetaisBean) {
		this.emailCommonDetaisBean = emailCommonDetaisBean;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public ClaimDenialBean getClmDenialBean() {
		return clmDenialBean;
	}
	public void setClmDenialBean(ClaimDenialBean clmDenialBean) {
		this.clmDenialBean = clmDenialBean;
	}
	public ReligareDataBean getReligareBean() {
		return religareBean;
	}
	public void setReligareBean(ReligareDataBean religareBean) {
		this.religareBean = religareBean;
	}
	public RelianceDatabean getRelianceBean() {
		return relianceBean;
	}
	public void setRelianceBean(RelianceDatabean relianceBean) {
		this.relianceBean = relianceBean;
	}
	public BhratiAxaDataBean getBhaxaBean() {
		return bhaxaBean;
	}
	public void setBhaxaBean(BhratiAxaDataBean bhaxaBean) {
		this.bhaxaBean = bhaxaBean;
	}
	public List<DocsDetailsCorpBean> getDocsDetailsCorpList() {
		return docsDetailsCorpList;
	}
	public void setDocsDetailsCorpList(List<DocsDetailsCorpBean> docsDetailsCorpList) {
		this.docsDetailsCorpList = docsDetailsCorpList;
	}
	public UiicDataBean getUiicBean() {
		return uiicBean;
	}
	public void setUiicBean(UiicDataBean uiicBean) {
		this.uiicBean = uiicBean;
	}
	public DocsDetailsCorpBean getDocsBean() {
		return docsBean;
	}
	public void setDocsBean(DocsDetailsCorpBean docsBean) {
		this.docsBean = docsBean;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ItgiDataBean getItgiBean() {
		return itgiBean;
	}
	public void setItgiBean(ItgiDataBean itgiBean) {
		this.itgiBean = itgiBean;
	}

	public void setNIABean(NIADataBean niaDataBeanObj) {
		this.niaBean = niaDataBeanObj;
		
	}
	public NIADataBean getNIABean() {
		return niaBean;
	}

}
