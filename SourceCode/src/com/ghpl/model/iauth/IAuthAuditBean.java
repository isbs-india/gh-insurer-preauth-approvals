package com.ghpl.model.iauth;

public class IAuthAuditBean {
  
   private int serialNo;
   private int preAuthNo;
   private String claimId;
   private int moduleId;
   private String moduleName;
   private String statusId;
   private int reAuthFlag;
   private String cardId;
   private String memberName;
   private String procedureName;
   private String uwCode;
   private String createddate;
   private String approvalType;
   private String uwId;
   private String ccnUnique;

   
   public String getCcnUnique() {
	return ccnUnique;
}
public void setCcnUnique(String ccnUnique) {
	this.ccnUnique = ccnUnique;
}
public String getUwId() {
	return uwId;
}
public void setUwId(String uwId) {
	this.uwId = uwId;
}
public String getApprovalType() {
	return approvalType;
}
public void setApprovalType(String approvalType) {
	this.approvalType = approvalType;
}
public int getSerialNo() {
	return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public int getPreAuthNo() {
		return preAuthNo;
	}
	public void setPreAuthNo(int preAuthNo) {
		this.preAuthNo = preAuthNo;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public int getReAuthFlag() {
		return reAuthFlag;
	}
	public void setReAuthFlag(int reAuthFlag) {
		this.reAuthFlag = reAuthFlag;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getProcedureName() {
		return procedureName;
	}
	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}
	public String getUwCode() {
		return uwCode;
	}
	public void setUwCode(String uwCode) {
		this.uwCode = uwCode;
	}
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
   
   

}
