package com.ghpl.model.iauth;

import java.util.ArrayList;
import java.util.List;

public class IAuthBean {
	private String cardid;
	private String cardHolderName;
	private String empid;
	private String sumInsured;
	private String sumInsuredConvert;
	private String policyNo;
	private String policyId;
	private String policyHolderName;
	private String relation;
	private int age;
	private String dob;
	private String policyFrom;
	private String policyTo;
	private String doj;
	private String dol;
	private String dod;
	private String enrolmentExcep;
	private String blocking;
	private int familyId;
	private String insuredId;
	private String currentDate;
	private int serialNo;
	private String selfName;
	private String doa;
	private String reqType;
	private String hospName;
	private String osticketId;
	private String ticketEmailId;
	private String hospId;
	private String moduleData;
	private int roleId;
	private String auditStatus;
    private int preauthFlow;
    private int appAmtFlag;
	private String renewalStatus;
	private String insuredStatus;
	private String bano;
	private String uwCode;
	private String uwId;
	private String rid;
	private String gender;
	private String preAuthUrl;
	private double approveAmtTotal;
	private double reqAmtTotal;
	private String copayper;
	private String copayAmt;
	private String finalApproveAmtTotal;
	private int moduleId;
	private String approvedAmtTotal;
	private String icdCode;
	private String icdCodeRange;
	private String diseaseCodeRangeDesc;
	private String icdDesc;
	private String procedureCode;
	private String procedure;
	private String procedureDesc;
	private int currentStatusId;
	private String currentStatusRet;
	private String nextStatusRet;
	private int remarksId;
	private double approveAmtTotalByCopay;
	private int claimCategories;
	private int floater;
	private int transStatusId;
	private String utilizedAmtTotal;
	private String maternityutilizedAmtTotal;
	
	private String inceptionDate;
	private String policyType;
    
	private String setAmtTotal;
	private String setAmtTotalValue;
	private String processAmtTotal;
	private String availableBalance;
	private String availableBalanceConvert;
	private double setAndProcessTotal;
	private int userId;
	private int moduleUserId;
	private String branchName;
	private String tpaid;
	private String hospAddress;
	private String issusingofficeid;
	private String selfcardid;
	private int createdById;
	private int reauthFlag;
	private String userName;
	private String status;
	
	private int preauthNo;
	private String patAddress;
	private String patEmail;
	private String patMobile;
	//private String polType;
	private int noofdays;
	private String nameofdr;
	private String ptcontactname;
	private String hospcontactname;
	private String ptmobileno;
	private String hospmobileno;
	private String ptlandlineno;
	private String hosplandlineno;
	private String ptemailid;
	private String hospemailid;
	private String ptaddress;
	private String srcofclaim;
	private String bedno;
	private String maternity;
	private String accdent;
	private String gval;
	private String pval;
	private String lval;
	private String aval;
	private String lmp;
	private String edd;
	private String modeofdelivery;
	private String alcohal;
	private String firmlcno;
	private String ipno;
	private String billno;
	private int breakUpListSize;
	private String deleveryRemarks;
	private String drugInfluance;
	private String firNo;
	private String isPpn;
	private String nonMedicoComments;
	private String claimId;
	private int insurerSentCount;
	private int insurerSentCountDenial;
	private String denialFlag;
	private String authAmt;
	private int policyLimtListSize;
	private double denialAmt;
	private String medicoId;
	private String underWrierName;
	
    private int nextStatus;
	private int transId;
	private int letterTypeId;
	private String auditAction;
	
	private String category;
	private String subCategory;
	private String claimType;
	private int claimCategoryId;
	 private int ghplStatusId;
	 private String diagnosis;
	 private String finYear;
	 private String hospitalId;
	 private String hospitalName;
	 private String hospitalAddress;
	 private String ccnUnique;
	 private int suppCounter;
	 
	 
	
	
	public int getSuppCounter() {
		return suppCounter;
	}
	public void setSuppCounter(int suppCounter) {
		this.suppCounter = suppCounter;
	}
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public String getDiseaseCodeRangeDesc() {
		return diseaseCodeRangeDesc;
	}
	public void setDiseaseCodeRangeDesc(String diseaseCodeRangeDesc) {
		this.diseaseCodeRangeDesc = diseaseCodeRangeDesc;
	}
	public String getMedicoId() {
		return medicoId;
	}
	public void setMedicoId(String medicoId) {
		this.medicoId = medicoId;
	}
	public String getUnderWrierName() {
		return underWrierName;
	}
	public void setUnderWrierName(String underWrierName) {
		this.underWrierName = underWrierName;
	}
	public String getIcdCodeRange() {
		return icdCodeRange;
	}
	public void setIcdCodeRange(String icdCodeRange) {
		this.icdCodeRange = icdCodeRange;
	}
	public String getFinYear() {
		return finYear;
	}
	public void setFinYear(String finYear) {
		this.finYear = finYear;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getMaternityutilizedAmtTotal() {
		return maternityutilizedAmtTotal;
	}
	public void setMaternityutilizedAmtTotal(String maternityutilizedAmtTotal) {
		this.maternityutilizedAmtTotal = maternityutilizedAmtTotal;
	}
	public int getGhplStatusId() {
		return ghplStatusId;
	}
	public void setGhplStatusId(int ghplStatusId) {
		this.ghplStatusId = ghplStatusId;
	}
	public String getAuditStatus() {
		return auditStatus;
	}
	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}
	public int getPreauthFlow() {
		return preauthFlow;
	}
	public void setPreauthFlow(int preauthFlow) {
		this.preauthFlow = preauthFlow;
	}
	public int getAppAmtFlag() {
		return appAmtFlag;
	}
	public void setAppAmtFlag(int appAmtFlag) {
		this.appAmtFlag = appAmtFlag;
	}
	public String getProcedure() {
		return procedure;
	}
	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}
	public String getNextStatusRet() {
		return nextStatusRet;
	}
	public void setNextStatusRet(String nextStatusRet) {
		this.nextStatusRet = nextStatusRet;
	}
	public String getCurrentStatusRet() {
		return currentStatusRet;
	}
	public void setCurrentStatusRet(String currentStatusRet) {
		this.currentStatusRet = currentStatusRet;
	}
	public String getAvailableBalanceConvert() {
		return availableBalanceConvert;
	}
	public void setAvailableBalanceConvert(String availableBalanceConvert) {
		this.availableBalanceConvert = availableBalanceConvert;
	}
	public int getPreauthNo() {
		return preauthNo;
	}
	public void setPreauthNo(int preauthNo) {
		this.preauthNo = preauthNo;
	}
	public double getReqAmtTotal() {
		return reqAmtTotal;
	}
	public void setReqAmtTotal(double reqAmtTotal) {
		this.reqAmtTotal = reqAmtTotal;
	}
	private double floaterbalanceAmt;
	
	public double getFloaterbalanceAmt() {
		return floaterbalanceAmt;
	}
	public void setFloaterbalanceAmt(double floaterbalanceAmt) {
		this.floaterbalanceAmt = floaterbalanceAmt;
	}
	public int getClaimCategoryId() {
		return claimCategoryId;
	}
	public void setClaimCategoryId(int claimCategoryId) {
		this.claimCategoryId = claimCategoryId;
	}
		public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	//These are the letter Remarks entered by medico to be displayed in Auditor screen
	private String medicoLetterRemarks;
	private String medicoInternalRemarks;
	private String auditorComments;
	
	//These are used to display the ICD & PROCEDURECODE in Editable mode
	private String icdCodevalue;
	private String procedureCodeValue;
	
	private String icdCodeId;
	private String procedureCodeId;
	
	
	public String getMedicoInternalRemarks() {
		return medicoInternalRemarks;
	}
	public void setMedicoInternalRemarks(String medicoInternalRemarks) {
		this.medicoInternalRemarks = medicoInternalRemarks;
	}
	public String getAuditorComments() {
		return auditorComments;
	}
	public void setAuditorComments(String auditorComments) {
		this.auditorComments = auditorComments;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getIcdCodevalue() {
		return icdCodevalue;
	}
	public void setIcdCodevalue(String icdCodevalue) {
		this.icdCodevalue = icdCodevalue;
	}
	public String getProcedureCodeValue() {
		return procedureCodeValue;
	}
	
	public void setProcedureCodeValue(String procedureCodeValue) {
		this.procedureCodeValue = procedureCodeValue;
	}
	
	public String getIcdCodeId() {
		return icdCodeId;
	}
	public void setIcdCodeId(String icdCodeId) {
		this.icdCodeId = icdCodeId;
	}
	public String getProcedureCodeId() {
		return procedureCodeId;
	}
	public void setProcedureCodeId(String procedureCodeId) {
		this.procedureCodeId = procedureCodeId;
	}
	
	
	
	
	
	public String getMedicoLetterRemarks() {
		return medicoLetterRemarks;
	}
	public void setMedicoLetterRemarks(String medicoLetterRemarks) {
		this.medicoLetterRemarks = medicoLetterRemarks;
	}
	
	
	public int getInsurerSentCountDenial() {
		return insurerSentCountDenial;
	}
	public void setInsurerSentCountDenial(int insurerSentCountDenial) {
		this.insurerSentCountDenial = insurerSentCountDenial;
	}
	public double getDenialAmt() {
		return denialAmt;
	}
	public void setDenialAmt(double denialAmt) {
		this.denialAmt = denialAmt;
	}
	public String getUtilizedAmtTotal() {
		return utilizedAmtTotal;
	}
	public void setUtilizedAmtTotal(String utilizedAmtTotal) {
		this.utilizedAmtTotal = utilizedAmtTotal;
	}
	public int getTransStatusId() {
		return transStatusId;
	}
	public void setTransStatusId(int transStatusId) {
		this.transStatusId = transStatusId;
	}
	public int getClaimCategories() {
		return claimCategories;
	}
	public void setClaimCategories(int claimCategories) {
		this.claimCategories = claimCategories;
	}
	public int getFloater() {
		return floater;
	}
	public void setFloater(int floater) {
		this.floater = floater;
	}
	public double getApproveAmtTotalByCopay() {
		return approveAmtTotalByCopay;
	}
	public void setApproveAmtTotalByCopay(double approveAmtTotalByCopay) {
		this.approveAmtTotalByCopay = approveAmtTotalByCopay;
	}
	
	public String getIcdCode() {
		return icdCode;
	}
	public void setIcdCode(String icdCode) {
		this.icdCode = icdCode;
	}
	public String getIcdDesc() {
		return icdDesc;
	}
	public void setIcdDesc(String icdDesc) {
		this.icdDesc = icdDesc;
	}
	public String getProcedureCode() {
		return procedureCode;
	}
	public void setProcedureCode(String procedureCode) {
		this.procedureCode = procedureCode;
	}
	public String getProcedureDesc() {
		return procedureDesc;
	}
	public void setProcedureDesc(String procedureDesc) {
		this.procedureDesc = procedureDesc;
	}
	public int getCurrentStatusId() {
		return currentStatusId;
	}
	public void setCurrentStatusId(int currentStatusId) {
		this.currentStatusId = currentStatusId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getApprovedAmtTotal() {
		return approvedAmtTotal;
	}
	public void setApprovedAmtTotal(String approvedAmtTotal) {
		this.approvedAmtTotal = approvedAmtTotal;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getFinalApproveAmtTotal() {
		return finalApproveAmtTotal;
	}
	public void setFinalApproveAmtTotal(String finalApproveAmtTotal) {
		this.finalApproveAmtTotal = finalApproveAmtTotal;
	}
	
	
	
	public int getModuleUserId() {
		return moduleUserId;
	}
	public void setModuleUserId(int moduleUserId) {
		this.moduleUserId = moduleUserId;
	}
	public String getSetAmtTotalValue() {
		return setAmtTotalValue;
	}
	public void setSetAmtTotalValue(String setAmtTotalValue) {
		this.setAmtTotalValue = setAmtTotalValue;
	}
	public String getAuditAction() {
		return auditAction;
	}
	public void setAuditAction(String auditAction) {
		this.auditAction = auditAction;
	}
	public int getRemarksId() {
		return remarksId;
	}
	public void setRemarksId(int remarksId) {
		this.remarksId = remarksId;
	}
	public int getNextStatus() {
		return nextStatus;
	}
	public void setNextStatus(int nextStatus) {
		this.nextStatus = nextStatus;
	}
	public int getTransId() {
		return transId;
	}
	public void setTransId(int transId) {
		this.transId = transId;
	}
	public int getLetterTypeId() {
		return letterTypeId;
	}
	public void setLetterTypeId(int letterTypeId) {
		this.letterTypeId = letterTypeId;
	}
	public int getPolicyLimtListSize() {
		return policyLimtListSize;
	}
	public void setPolicyLimtListSize(int policyLimtListSize) {
		this.policyLimtListSize = policyLimtListSize;
	}
	public String getDenialFlag() {
		return denialFlag;
	}
	public void setDenialFlag(String denialFlag) {
		this.denialFlag = denialFlag;
	}
	public String getAuthAmt() {
		return authAmt;
	}
	public void setAuthAmt(String authAmt) {
		this.authAmt = authAmt;
	}
	public int getInsurerSentCount() {
		return insurerSentCount;
	}
	public void setInsurerSentCount(int insurerSentCount) {
		this.insurerSentCount = insurerSentCount;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public double getApproveAmtTotal() {
		return approveAmtTotal;
	}
	public void setApproveAmtTotal(double approveAmtTotal) {
		this.approveAmtTotal = approveAmtTotal;
	}
	public String getCopayper() {
		return copayper;
	}
	public void setCopayper(String copayper) {
		this.copayper = copayper;
	}
	public String getCopayAmt() {
		return copayAmt;
	}
	public void setCopayAmt(String copayAmt) {
		this.copayAmt = copayAmt;
	}
	public String getTicketEmailId() {
		return ticketEmailId;
	}
	public void setTicketEmailId(String ticketEmailId) {
		this.ticketEmailId = ticketEmailId;
	}
	public String getNonMedicoComments() {
		return nonMedicoComments;
	}
	public void setNonMedicoComments(String nonMedicoComments) {
		this.nonMedicoComments = nonMedicoComments;
	}
	public String getIsPpn() {
		return isPpn;
	}
	public void setIsPpn(String isPpn) {
		this.isPpn = isPpn;
	}
	public String getFirNo() {
		return firNo;
	}
	public void setFirNo(String firNo) {
		this.firNo = firNo;
	}
	public String getDrugInfluance() {
		return drugInfluance;
	}
	public void setDrugInfluance(String drugInfluance) {
		this.drugInfluance = drugInfluance;
	}
	public String getDeleveryRemarks() {
		return deleveryRemarks;
	}
	public void setDeleveryRemarks(String deleveryRemarks) {
		this.deleveryRemarks = deleveryRemarks;
	}
	
	public int getBreakUpListSize() {
		return breakUpListSize;
	}
	public void setBreakUpListSize(int breakUpListSize) {
		this.breakUpListSize = breakUpListSize;
	}
	public int getNoofdays() {
		return noofdays;
	}
	public void setNoofdays(int noofdays) {
		this.noofdays = noofdays;
	}
	public String getIpno() {
		return ipno;
	}
	public void setIpno(String ipno) {
		this.ipno = ipno;
	}
	public String getBillno() {
		return billno;
	}
	public void setBillno(String billno) {
		this.billno = billno;
	}
	public String getModeofdelivery() {
		return modeofdelivery;
	}
	public void setModeofdelivery(String modeofdelivery) {
		this.modeofdelivery = modeofdelivery;
	}
	public String getMaternity() {
		return maternity;
	}
	public void setMaternity(String maternity) {
		this.maternity = maternity;
	}
	public String getAccdent() {
		return accdent;
	}
	public void setAccdent(String accdent) {
		this.accdent = accdent;
	}
	public String getGval() {
		return gval;
	}
	public void setGval(String gval) {
		this.gval = gval;
	}
	public String getPval() {
		return pval;
	}
	public void setPval(String pval) {
		this.pval = pval;
	}
	public String getLval() {
		return lval;
	}
	public void setLval(String lval) {
		this.lval = lval;
	}
	public String getAval() {
		return aval;
	}
	public void setAval(String aval) {
		this.aval = aval;
	}
	public String getLmp() {
		return lmp;
	}
	public void setLmp(String lmp) {
		this.lmp = lmp;
	}
	public String getEdd() {
		return edd;
	}
	public void setEdd(String edd) {
		this.edd = edd;
	}
	
	public String getAlcohal() {
		return alcohal;
	}
	public void setAlcohal(String alcohal) {
		this.alcohal = alcohal;
	}
	public String getFirmlcno() {
		return firmlcno;
	}
	public void setFirmlcno(String firmlcno) {
		this.firmlcno = firmlcno;
	}
	public String getSrcofclaim() {
		return srcofclaim;
	}
	public void setSrcofclaim(String srcofclaim) {
		this.srcofclaim = srcofclaim;
	}
	public String getBedno() {
		return bedno;
	}
	public void setBedno(String bedno) {
		this.bedno = bedno;
	}
	public String getPtcontactname() {
		return ptcontactname;
	}
	public void setPtcontactname(String ptcontactname) {
		this.ptcontactname = ptcontactname;
	}
	public String getHospcontactname() {
		return hospcontactname;
	}
	public void setHospcontactname(String hospcontactname) {
		this.hospcontactname = hospcontactname;
	}
	public String getPtmobileno() {
		return ptmobileno;
	}
	public void setPtmobileno(String ptmobileno) {
		this.ptmobileno = ptmobileno;
	}
	public String getHospmobileno() {
		return hospmobileno;
	}
	public void setHospmobileno(String hospmobileno) {
		this.hospmobileno = hospmobileno;
	}
	public String getPtlandlineno() {
		return ptlandlineno;
	}
	public void setPtlandlineno(String ptlandlineno) {
		this.ptlandlineno = ptlandlineno;
	}
	public String getHosplandlineno() {
		return hosplandlineno;
	}
	public void setHosplandlineno(String hosplandlineno) {
		this.hosplandlineno = hosplandlineno;
	}
	public String getPtemailid() {
		return ptemailid;
	}
	public void setPtemailid(String ptemailid) {
		this.ptemailid = ptemailid;
	}
	public String getHospemailid() {
		return hospemailid;
	}
	public void setHospemailid(String hospemailid) {
		this.hospemailid = hospemailid;
	}
	public String getPtaddress() {
		return ptaddress;
	}
	public void setPtaddress(String ptaddress) {
		this.ptaddress = ptaddress;
	}
	public String getNameofdr() {
		return nameofdr;
	}
	public void setNameofdr(String nameofdr) {
		this.nameofdr = nameofdr;
	}
	
	public String getPatAddress() {
		return patAddress;
	}
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = dod;
	}
	public void setPatAddress(String patAddress) {
		this.patAddress = patAddress;
	}
	public String getPatEmail() {
		return patEmail;
	}
	public void setPatEmail(String patEmail) {
		this.patEmail = patEmail;
	}
	public String getPatMobile() {
		return patMobile;
	}
	public void setPatMobile(String patMobile) {
		this.patMobile = patMobile;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSumInsuredConvert() {
		return sumInsuredConvert;
	}
	public void setSumInsuredConvert(String sumInsuredConvert) {
		this.sumInsuredConvert = sumInsuredConvert;
	}
	public int getCreatedById() {
		return createdById;
	}
	public void setCreatedById(int createdById) {
		this.createdById = createdById;
	}
	public String getIssusingofficeid() {
		return issusingofficeid;
	}
	public void setIssusingofficeid(String issusingofficeid) {
		this.issusingofficeid = issusingofficeid;
	}
	public String getSelfcardid() {
		return selfcardid;
	}
	public void setSelfcardid(String selfcardid) {
		this.selfcardid = selfcardid;
	}
	public String getHospAddress() {
		return hospAddress;
	}
	public void setHospAddress(String hospAddress) {
		this.hospAddress = hospAddress;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getModuleData() {
		return moduleData;
	}
	public void setModuleData(String moduleData) {
		this.moduleData = moduleData;
	}
	public String getHospId() {
		return hospId;
	}
	public void setHospId(String hospId) {
		this.hospId = hospId;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	public String getReqType() {
		return reqType;
	}
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public String getOsticketId() {
		return osticketId;
	}
	public void setOsticketId(String osticketId) {
		this.osticketId = osticketId;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getTpaid() {
		return tpaid;
	}
	public void setTpaid(String tpaid) {
		this.tpaid = tpaid;
	}
	public double getSetAndProcessTotal() {
		return setAndProcessTotal;
	}
	public void setSetAndProcessTotal(double setAndProcessTotal) {
		this.setAndProcessTotal = setAndProcessTotal;
	}
	public String getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}
	public String getSetAmtTotal() {
		return setAmtTotal;
	}
	public void setSetAmtTotal(String setAmtTotal) {
		this.setAmtTotal = setAmtTotal;
	}
	public String getProcessAmtTotal() {
		return processAmtTotal;
	}
	public void setProcessAmtTotal(String processAmtTotal) {
		this.processAmtTotal = processAmtTotal;
	}
	public String getSelfName() {
		return selfName;
	}
	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	
	public String getPreAuthUrl() {
		return preAuthUrl;
	}
	public void setPreAuthUrl(String preAuthUrl) {
		this.preAuthUrl = preAuthUrl;
	}
	public String getUwCode() {
		return uwCode;
	}
	public void setUwCode(String uwCode) {
		this.uwCode = uwCode;
	}
	public String getUwId() {
		return uwId;
	}
	public void setUwId(String uwId) {
		this.uwId = uwId;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getRenewalStatus() {
		return renewalStatus;
	}
	public void setRenewalStatus(String renewalStatus) {
		this.renewalStatus = renewalStatus;
	}
	public String getInsuredStatus() {
		return insuredStatus;
	}
	public void setInsuredStatus(String insuredStatus) {
		this.insuredStatus = insuredStatus;
	}
	
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getDol() {
		return dol;
	}
	public void setDol(String dol) {
		this.dol = dol;
	}
	public String getEnrolmentExcep() {
		return enrolmentExcep;
	}
	public void setEnrolmentExcep(String enrolmentExcep) {
		this.enrolmentExcep = enrolmentExcep;
	}
	public String getBlocking() {
		return blocking;
	}
	public void setBlocking(String blocking) {
		this.blocking = blocking;
	}
	public int getFamilyId() {
		return familyId;
	}
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	
	 

}
