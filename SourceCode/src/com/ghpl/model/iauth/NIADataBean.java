package com.ghpl.model.iauth;

public class NIADataBean {

	private String claimId;
	private String ccnUniqe;
	private int reauthFlag;
	private int moduleId;
	private String networks;
	private String expectedDurationOfStay;
	private String processingDoctor;
	private String tpaName;
	private String emailId;
	private String ccEmailId;
	private String policyDiscription;
	private String code;
	private int uwId;
	private int issuingOfficeKey;
	  //FORMAT FOR CASHLESS APPROVAL Rs. 1 LAKH AND ABOVE
	private String claimNo;
	private String policyNo;
	private String policyPeriod;
	private String insured;
	private String patient;
	private String policyType;
	private String incase;
	private String copayment;
	private String preauthType;
	private String dateOfInception;
	private long balSuminsured;
	private String relation;
	private String sex;
	private String age;
	
	private String policynoAndPolicyperiad;
	private String genderAndAge;

	 //Underwriting history of Patient
	private long sumInsured;
	private String policyNo1;
	private String validity;
	private String hospName;
	private String city;
	private String state;
	private String disease;
	private String diagnosis;
	private String lineOfTreatment;
	private String procedureincRefrencerate;
	private String averageCost;
	private String packageRate;
	private String dateofAdmission;
	private String dateofDischarge;
	private String detailsOfIdProof;
	private String roomType;
	
	private String cityState;
	private String diseaseAndDiagnosis;
	private String policyNoAndValidity;

	  //Claim history of Patient
	private String disease1;
	private String procedure;
	private String amount;
	private String ppn;
	private String roomRent;
	private String icdCode;
	private String applicablePPN;
	private String noOfDaysRoom;
	private String noOfDaysICU;
	private String ICUPerDay;
	private String roomRentEligibility;
	private String category;

	private String diseaseAndProcedure;
	private String ppnAndCategory;
//////////////////////////
	private String gipsAppnHospAphb;
	private String gipsAppnTpa;
	private String gipsAppnResonDod;
	private String roomRentRequest;
	private String roomRentPaid;
	private String roomRentDeducted;
	private String profChgHospRequest;
	private String profChgPaid;
	private String profChgDeducted;
	private String phrHospRequest;
	private String phrPaid;
	private String phrDeducted;
	private String otHospRequest;
	private String otPaid;
	private String otDeducted;
	//private String userid="";
	private String labInvHospRequest="";
	private String labInvPaid="";
	private String labInvDeducted="";
	private String conChrHospRequest="";
	private String conChrPaid="";
	private String conChrDeducted="";
	private String othersHospRequest="";
	private String othersPaid="";
	private String othersDeducted="";
	private String totalHospRequest="";
	private String totalPaid="";
	private String totalDeducted="";
	private String specification="";
	private String nameAndQual="";
	private String dateAndTime="";
	private String diseaseTreatment="";
	private String ppnPackage="";
	private String expencesAllowed="";
	private String anyAmountAllowedInExcessByTPA="";
	private String id;
	private String createdDate;
	private String letterRemarks;
	
	private String policyID;
	private String cardId;
	private String insuredId;

	
	public String getCcnUniqe() {
		return ccnUniqe;
	}
	public void setCcnUniqe(String ccnUniqe) {
		this.ccnUniqe = ccnUniqe;
	}
	public String getPolicyID() {
		return policyID;
	}
	public void setPolicyID(String policyID) {
		this.policyID = policyID;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getNetworks() {
		return networks;
	}
	public void setNetworks(String networks) {
		this.networks = networks;
	}
	public String getExpectedDurationOfStay() {
		return expectedDurationOfStay;
	}
	public void setExpectedDurationOfStay(String expectedDurationOfStay) {
		this.expectedDurationOfStay = expectedDurationOfStay;
	}
	public String getProcessingDoctor() {
		return processingDoctor;
	}
	public void setProcessingDoctor(String processingDoctor) {
		this.processingDoctor = processingDoctor;
	}
	public String getTpaName() {
		return tpaName;
	}
	public void setTpaName(String tpaName) {
		this.tpaName = tpaName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailId() {
		return ccEmailId;
	}
	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}
	public String getPolicyDiscription() {
		return policyDiscription;
	}
	public void setPolicyDiscription(String policyDiscription) {
		this.policyDiscription = policyDiscription;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getUwId() {
		return uwId;
	}
	public void setUwId(int uwId) {
		this.uwId = uwId;
	}
	public int getIssuingOfficeKey() {
		return issuingOfficeKey;
	}
	public void setIssuingOfficeKey(int issuingOfficeKey) {
		this.issuingOfficeKey = issuingOfficeKey;
	}
	public String getClaimNo() {
		return claimNo;
	}
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getPolicyPeriod() {
		return policyPeriod;
	}
	public void setPolicyPeriod(String policyPeriod) {
		this.policyPeriod = policyPeriod;
	}
	public String getInsured() {
		return insured;
	}
	public void setInsured(String insured) {
		this.insured = insured;
	}
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getIncase() {
		return incase;
	}
	public void setIncase(String incase) {
		this.incase = incase;
	}
	public String getCopayment() {
		return copayment;
	}
	public void setCopayment(String copayment) {
		this.copayment = copayment;
	}
	public String getPreauthType() {
		return preauthType;
	}
	public void setPreauthType(String preauthType) {
		this.preauthType = preauthType;
	}
	public String getDateOfInception() {
		return dateOfInception;
	}
	public void setDateOfInception(String dateOfInception) {
		this.dateOfInception = dateOfInception;
	}
	public long getBalSuminsured() {
		return balSuminsured;
	}
	public void setBalSuminsured(long balSuminsured) {
		this.balSuminsured = balSuminsured;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPolicynoAndPolicyperiad() {
		return policynoAndPolicyperiad;
	}
	public void setPolicynoAndPolicyperiad(String policynoAndPolicyperiad) {
		this.policynoAndPolicyperiad = policynoAndPolicyperiad;
	}
	public String getGenderAndAge() {
		return genderAndAge;
	}
	public void setGenderAndAge(String genderAndAge) {
		this.genderAndAge = genderAndAge;
	}
	public long getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(long sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPolicyNo1() {
		return policyNo1;
	}
	public void setPolicyNo1(String policyNo1) {
		this.policyNo1 = policyNo1;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	public String getLineOfTreatment() {
		return lineOfTreatment;
	}
	public void setLineOfTreatment(String lineOfTreatment) {
		this.lineOfTreatment = lineOfTreatment;
	}
	public String getProcedureincRefrencerate() {
		return procedureincRefrencerate;
	}
	public void setProcedureincRefrencerate(String procedureincRefrencerate) {
		this.procedureincRefrencerate = procedureincRefrencerate;
	}
	public String getAverageCost() {
		return averageCost;
	}
	public void setAverageCost(String averageCost) {
		this.averageCost = averageCost;
	}
	public String getPackageRate() {
		return packageRate;
	}
	public void setPackageRate(String packageRate) {
		this.packageRate = packageRate;
	}
	public String getDateofAdmission() {
		return dateofAdmission;
	}
	public void setDateofAdmission(String dateofAdmission) {
		this.dateofAdmission = dateofAdmission;
	}
	public String getDateofDischarge() {
		return dateofDischarge;
	}
	public void setDateofDischarge(String dateofDischarge) {
		this.dateofDischarge = dateofDischarge;
	}
	public String getDetailsOfIdProof() {
		return detailsOfIdProof;
	}
	public void setDetailsOfIdProof(String detailsOfIdProof) {
		this.detailsOfIdProof = detailsOfIdProof;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getCityState() {
		return cityState;
	}
	public void setCityState(String cityState) {
		this.cityState = cityState;
	}
	public String getDiseaseAndDiagnosis() {
		return diseaseAndDiagnosis;
	}
	public void setDiseaseAndDiagnosis(String diseaseAndDiagnosis) {
		this.diseaseAndDiagnosis = diseaseAndDiagnosis;
	}
	public String getPolicyNoAndValidity() {
		return policyNoAndValidity;
	}
	public void setPolicyNoAndValidity(String policyNoAndValidity) {
		this.policyNoAndValidity = policyNoAndValidity;
	}
	public String getDisease1() {
		return disease1;
	}
	public void setDisease1(String disease1) {
		this.disease1 = disease1;
	}
	public String getProcedure() {
		return procedure;
	}
	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPpn() {
		return ppn;
	}
	public void setPpn(String ppn) {
		this.ppn = ppn;
	}
	public String getRoomRent() {
		return roomRent;
	}
	public void setRoomRent(String roomRent) {
		this.roomRent = roomRent;
	}
	public String getIcdCode() {
		return icdCode;
	}
	public void setIcdCode(String icdCode) {
		this.icdCode = icdCode;
	}
	public String getApplicablePPN() {
		return applicablePPN;
	}
	public void setApplicablePPN(String applicablePPN) {
		this.applicablePPN = applicablePPN;
	}
	public String getNoOfDaysRoom() {
		return noOfDaysRoom;
	}
	public void setNoOfDaysRoom(String noOfDaysRoom) {
		this.noOfDaysRoom = noOfDaysRoom;
	}
	public String getNoOfDaysICU() {
		return noOfDaysICU;
	}
	public void setNoOfDaysICU(String noOfDaysICU) {
		this.noOfDaysICU = noOfDaysICU;
	}
	public String getICUPerDay() {
		return ICUPerDay;
	}
	public void setICUPerDay(String iCUPerDay) {
		ICUPerDay = iCUPerDay;
	}
	public String getRoomRentEligibility() {
		return roomRentEligibility;
	}
	public void setRoomRentEligibility(String roomRentEligibility) {
		this.roomRentEligibility = roomRentEligibility;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDiseaseAndProcedure() {
		return diseaseAndProcedure;
	}
	public void setDiseaseAndProcedure(String diseaseAndProcedure) {
		this.diseaseAndProcedure = diseaseAndProcedure;
	}
	public String getPpnAndCategory() {
		return ppnAndCategory;
	}
	public void setPpnAndCategory(String ppnAndCategory) {
		this.ppnAndCategory = ppnAndCategory;
	}
	public String getGipsAppnHospAphb() {
		return gipsAppnHospAphb;
	}
	public void setGipsAppnHospAphb(String gipsAppnHospAphb) {
		this.gipsAppnHospAphb = gipsAppnHospAphb;
	}
	public String getGipsAppnTpa() {
		return gipsAppnTpa;
	}
	public void setGipsAppnTpa(String gipsAppnTpa) {
		this.gipsAppnTpa = gipsAppnTpa;
	}
	public String getGipsAppnResonDod() {
		return gipsAppnResonDod;
	}
	public void setGipsAppnResonDod(String gipsAppnResonDod) {
		this.gipsAppnResonDod = gipsAppnResonDod;
	}
	public String getRoomRentRequest() {
		return roomRentRequest;
	}
	public void setRoomRentRequest(String roomRentRequest) {
		this.roomRentRequest = roomRentRequest;
	}
	public String getRoomRentPaid() {
		return roomRentPaid;
	}
	public void setRoomRentPaid(String roomRentPaid) {
		this.roomRentPaid = roomRentPaid;
	}
	public String getRoomRentDeducted() {
		return roomRentDeducted;
	}
	public void setRoomRentDeducted(String roomRentDeducted) {
		this.roomRentDeducted = roomRentDeducted;
	}
	public String getProfChgHospRequest() {
		return profChgHospRequest;
	}
	public void setProfChgHospRequest(String profChgHospRequest) {
		this.profChgHospRequest = profChgHospRequest;
	}
	public String getProfChgPaid() {
		return profChgPaid;
	}
	public void setProfChgPaid(String profChgPaid) {
		this.profChgPaid = profChgPaid;
	}
	public String getProfChgDeducted() {
		return profChgDeducted;
	}
	public void setProfChgDeducted(String profChgDeducted) {
		this.profChgDeducted = profChgDeducted;
	}
	public String getPhrHospRequest() {
		return phrHospRequest;
	}
	public void setPhrHospRequest(String phrHospRequest) {
		this.phrHospRequest = phrHospRequest;
	}
	public String getPhrPaid() {
		return phrPaid;
	}
	public void setPhrPaid(String phrPaid) {
		this.phrPaid = phrPaid;
	}
	public String getPhrDeducted() {
		return phrDeducted;
	}
	public void setPhrDeducted(String phrDeducted) {
		this.phrDeducted = phrDeducted;
	}
	public String getOtHospRequest() {
		return otHospRequest;
	}
	public void setOtHospRequest(String otHospRequest) {
		this.otHospRequest = otHospRequest;
	}
	public String getOtPaid() {
		return otPaid;
	}
	public void setOtPaid(String otPaid) {
		this.otPaid = otPaid;
	}
	public String getOtDeducted() {
		return otDeducted;
	}
	public void setOtDeducted(String otDeducted) {
		this.otDeducted = otDeducted;
	}
	public String getLabInvHospRequest() {
		return labInvHospRequest;
	}
	public void setLabInvHospRequest(String labInvHospRequest) {
		this.labInvHospRequest = labInvHospRequest;
	}
	public String getLabInvPaid() {
		return labInvPaid;
	}
	public void setLabInvPaid(String labInvPaid) {
		this.labInvPaid = labInvPaid;
	}
	public String getLabInvDeducted() {
		return labInvDeducted;
	}
	public void setLabInvDeducted(String labInvDeducted) {
		this.labInvDeducted = labInvDeducted;
	}
	public String getConChrHospRequest() {
		return conChrHospRequest;
	}
	public void setConChrHospRequest(String conChrHospRequest) {
		this.conChrHospRequest = conChrHospRequest;
	}
	public String getConChrPaid() {
		return conChrPaid;
	}
	public void setConChrPaid(String conChrPaid) {
		this.conChrPaid = conChrPaid;
	}
	public String getConChrDeducted() {
		return conChrDeducted;
	}
	public void setConChrDeducted(String conChrDeducted) {
		this.conChrDeducted = conChrDeducted;
	}
	public String getOthersHospRequest() {
		return othersHospRequest;
	}
	public void setOthersHospRequest(String othersHospRequest) {
		this.othersHospRequest = othersHospRequest;
	}
	public String getOthersPaid() {
		return othersPaid;
	}
	public void setOthersPaid(String othersPaid) {
		this.othersPaid = othersPaid;
	}
	public String getOthersDeducted() {
		return othersDeducted;
	}
	public void setOthersDeducted(String othersDeducted) {
		this.othersDeducted = othersDeducted;
	}
	public String getTotalHospRequest() {
		return totalHospRequest;
	}
	public void setTotalHospRequest(String totalHospRequest) {
		this.totalHospRequest = totalHospRequest;
	}
	public String getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(String totalPaid) {
		this.totalPaid = totalPaid;
	}
	public String getTotalDeducted() {
		return totalDeducted;
	}
	public void setTotalDeducted(String totalDeducted) {
		this.totalDeducted = totalDeducted;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getNameAndQual() {
		return nameAndQual;
	}
	public void setNameAndQual(String nameAndQual) {
		this.nameAndQual = nameAndQual;
	}
	public String getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	public String getDiseaseTreatment() {
		return diseaseTreatment;
	}
	public void setDiseaseTreatment(String diseaseTreatment) {
		this.diseaseTreatment = diseaseTreatment;
	}
	public String getPpnPackage() {
		return ppnPackage;
	}
	public void setPpnPackage(String ppnPackage) {
		this.ppnPackage = ppnPackage;
	}
	public String getExpencesAllowed() {
		return expencesAllowed;
	}
	public void setExpencesAllowed(String expencesAllowed) {
		this.expencesAllowed = expencesAllowed;
	}
	public String getAnyAmountAllowedInExcessByTPA() {
		return anyAmountAllowedInExcessByTPA;
	}
	public void setAnyAmountAllowedInExcessByTPA(
			String anyAmountAllowedInExcessByTPA) {
		this.anyAmountAllowedInExcessByTPA = anyAmountAllowedInExcessByTPA;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLetterRemarks() {
		return letterRemarks;
	}
	public void setLetterRemarks(String letterRemarks) {
		this.letterRemarks = letterRemarks;
	}
	
	
	}
