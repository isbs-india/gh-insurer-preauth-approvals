package com.ghpl.model.iauth;

public class IAuthMailBean {
	private String claimId;
	private int policyID;
	private int insuredId;
	private int actionType;
	private int smsRefNo;
	private int emailSentStatus;
	private String module;
	private String recordCreatedOn;
	private String emailCC;
	private String emailBCC;
	private String sms;
	private String emailId;
	private String mobileNo;
	private String cardId;
	private int moduleLetterId;
	private String smsTemplate;
	private String emailTemplate;
	private String mailSubject;
	private int moduleId;
	private int letterTypeId;
	private int letterId;
	private String mailSmsResponse;
	private int reAuthFlag;
	private int suppCounter;
	private String ccnUnique;
	
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public int getReAuthFlag() {
		return reAuthFlag;
	}
	public void setReAuthFlag(int reAuthFlag) {
		this.reAuthFlag = reAuthFlag;
	}
	public int getSuppCounter() {
		return suppCounter;
	}
	public void setSuppCounter(int suppCounter) {
		this.suppCounter = suppCounter;
	}
	public String getMailSmsResponse() {
		return mailSmsResponse;
	}
	public void setMailSmsResponse(String mailSmsResponse) {
		this.mailSmsResponse = mailSmsResponse;
	}
	public int getLetterId() {
		return letterId;
	}
	public void setLetterId(int letterId) {
		this.letterId = letterId;
	}
	public int getLetterTypeId() {
		return letterTypeId;
	}
	public void setLetterTypeId(int letterTypeId) {
		this.letterTypeId = letterTypeId;
	}
	public int getEmailSentStatus() {
		return emailSentStatus;
	}
	public void setEmailSentStatus(int emailSentStatus) {
		this.emailSentStatus = emailSentStatus;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public int getPolicyID() {
		return policyID;
	}
	public void setPolicyID(int policyID) {
		this.policyID = policyID;
	}
	public int getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(int insuredId) {
		this.insuredId = insuredId;
	}
	public int getActionType() {
		return actionType;
	}
	public void setActionType(int actionType) {
		this.actionType = actionType;
	}
	public int getSmsRefNo() {
		return smsRefNo;
	}
	public void setSmsRefNo(int smsRefNo) {
		this.smsRefNo = smsRefNo;
	}
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getRecordCreatedOn() {
		return recordCreatedOn;
	}
	public void setRecordCreatedOn(String recordCreatedOn) {
		this.recordCreatedOn = recordCreatedOn;
	}
	public String getEmailCC() {
		return emailCC;
	}
	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}
	public String getEmailBCC() {
		return emailBCC;
	}
	public void setEmailBCC(String emailBCC) {
		this.emailBCC = emailBCC;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public int getModuleLetterId() {
		return moduleLetterId;
	}
	public void setModuleLetterId(int moduleLetterId) {
		this.moduleLetterId = moduleLetterId;
	}
	public String getSmsTemplate() {
		return smsTemplate;
	}
	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}
	public String getEmailTemplate() {
		return emailTemplate;
	}
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	
}
