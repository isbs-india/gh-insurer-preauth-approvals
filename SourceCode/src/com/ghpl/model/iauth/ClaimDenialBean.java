package com.ghpl.model.iauth;

import java.io.Serializable;

public class ClaimDenialBean implements Serializable {


	private static final long serialVersionUID = 1L;
	private String claimid;
	private int reauthFlag;
	private int moduleId;
	private String moduleName;
	private String patientName;
	private String employeeName;
	private String employeeId;
	private String corporatename;
	private String sumInsured;
	private String requestedAmount;
	private String approvedAmount;
	private String policydesc;
	private String emailId;
	private String ccEmailId;
	private String medicoRemarks;
	private String ccnUnique;
	
	
	
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	//New columns
	private int underWriterId;
	private int issuingOfficePrimaryKey;
	
	private String policyId;
	private String insurerId;
	private String cardId;
	
	
	public int getUnderWriterId() {
		return underWriterId;
	}
	public void setUnderWriterId(int underWriterId) {
		this.underWriterId = underWriterId;
	}
	public int getIssuingOfficePrimaryKey() {
		return issuingOfficePrimaryKey;
	}
	public void setIssuingOfficePrimaryKey(int issuingOfficePrimaryKey) {
		this.issuingOfficePrimaryKey = issuingOfficePrimaryKey;
	}
	public String getClaimid() {
		return claimid;
	}
	public void setClaimid(String claimid) {
		this.claimid = claimid;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getCorporatename() {
		return corporatename;
	}
	public void setCorporatename(String corporatename) {
		this.corporatename = corporatename;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public String getApprovedAmount() {
		return approvedAmount;
	}
	public void setApprovedAmount(String approvedAmount) {
		this.approvedAmount = approvedAmount;
	}
	public String getPolicydesc() {
		return policydesc;
	}
	public void setPolicydesc(String policydesc) {
		this.policydesc = policydesc;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCcEmailId() {
		return ccEmailId;
	}
	public void setCcEmailId(String ccEmailId) {
		this.ccEmailId = ccEmailId;
	}
	public String getMedicoRemarks() {
		return medicoRemarks;
	}
	public void setMedicoRemarks(String medicoRemarks) {
		this.medicoRemarks = medicoRemarks;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getInsurerId() {
		return insurerId;
	}
	public void setInsurerId(String insurerId) {
		this.insurerId = insurerId;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	

}
