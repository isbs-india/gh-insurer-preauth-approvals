package com.ghpl.model.iauth;

import java.io.Serializable;

public class BackToAuditBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String claimId;
	private int PreauthAppNo;
	private int reauthFlag;
	private int moduleUserId;
	private int moduleId;
	private String remarks;
	private int processingStatus;
	private int userId;
	private String ccnUnique;
	
	
	public String getCcnUnique() {
		return ccnUnique;
	}
	public void setCcnUnique(String ccnUnique) {
		this.ccnUnique = ccnUnique;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getProcessingStatus() {
		return processingStatus;
	}
	public void setProcessingStatus(int processingStatus) {
		this.processingStatus = processingStatus;
	}
	public String getClaimId() {
		return claimId;
	}
	public void setClaimId(String claimId) {
		this.claimId = claimId;
	}
	public int getPreauthAppNo() {
		return PreauthAppNo;
	}
	public void setPreauthAppNo(int preauthAppNo) {
		PreauthAppNo = preauthAppNo;
	}
	public int getReauthFlag() {
		return reauthFlag;
	}
	public void setReauthFlag(int reauthFlag) {
		this.reauthFlag = reauthFlag;
	}
	public int getModuleUserId() {
		return moduleUserId;
	}
	public void setModuleUserId(int moduleUserId) {
		this.moduleUserId = moduleUserId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
