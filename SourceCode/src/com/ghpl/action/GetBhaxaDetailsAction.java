package com.ghpl.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.GetInsurerPrefillDataListBean;
import com.ghpl.model.iauth.LoginBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;

/**
 * Servlet implementation class GetBhaxaDetailsAction
 */
@WebServlet("/GetBhaxaDetailsAction")
public class GetBhaxaDetailsAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger logger =Logger.getLogger("GetUiicDetailsAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBhaxaDetailsAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int moduleId=0; 
		String name_of_user_obj_1=null;
   	    int userid_of_user=0;
   	    int moduleUserId=0;
   	    GetInsurerPrefillDataListBean finalBean=null;
   	    String claimid="0";
        int reauthflag=0;
        String moduleName=null;
        String ccnUnique="";
		
		
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				
				LoginBean lb=new LoginBean();
				session= request.getSession();
					lb=(LoginBean)session.getAttribute("user");
				     
				    moduleId=Integer.parseInt(request.getParameter("moduleId"));
				    claimid=request.getParameter("claimId")!=null?request.getParameter("claimId"):"0";
				    ccnUnique=request.getParameter("ccnUnique")!=null?request.getParameter("ccnUnique"):"0";
				    reauthflag=Integer.parseInt(request.getParameter("reAuthFlag")!=null?request.getParameter("reAuthFlag"):"0");
				    moduleName=request.getParameter("moduleName")!=null?request.getParameter("moduleName"):"0";
				    
				//RETRIEVING LIST DATA FROM GHPL IAUTH_PREAUTH TABLE
				finalBean = SendToInsurerManager.getbhaxaDataPrefill(moduleId, claimid, reauthflag);
				if(finalBean!=null){
					finalBean.setCcnUnique(ccnUnique);
					request.setAttribute("finalBean", finalBean);
					RequestDispatcher rd = request.getRequestDispatcher("./BharatiAxa_insurerFormat.jsp");
					rd.forward(request,response);
				}
		}else{
			Cookie[] cookies = request.getCookies();
	    	if(cookies != null){
	    	for(Cookie cookie : cookies){
	    		if(cookie.getName().equals("JSESSIONID")){
	    			//System.out.println("JSESSIONID="+cookie.getValue());
	    			cookie.setValue("");
	    			cookie.setPath("/");
	    			cookie.setMaxAge(0);
	    			response.addCookie(cookie);
	    			break;
	    		}
	    	}
	    	}
			if(session != null){
	    		session.invalidate();
	    	}
			 response.sendRedirect("./login.jsp");
		}
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
		
		
	
	}

}
