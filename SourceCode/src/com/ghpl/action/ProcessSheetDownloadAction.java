package com.ghpl.action;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.ghpl.model.iauth.PreAuthBean;
import com.ghpl.util.Constants;
import com.ghpl.util.MyProperties;

/**
 * Servlet implementation class ProcessSheetDownloadAction
 */
@WebServlet("/ProcessSheetDownloadAction")
@MultipartConfig(fileSizeThreshold=1024*1024*2,	// 2MB 
maxFileSize=1024*1024*2	)	// 2MB
public class ProcessSheetDownloadAction extends HttpServlet {
	static Logger logger =Logger.getLogger("ProcessSheetDownloadAction");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessSheetDownloadAction() {
        super();
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				// String claimId=null; 
				 //String collectionMaindocTypeId=null;
				// String collectionSubdocTypeId=null;
				 String fileName=null;
				 String actualFileName=null;
				 String scanningDate=null;
				// String scanningGTS=null;
				String FTPHOST=null;
				String FTPUName=null;
				String FTPPwd=null;
				String folderName=null;
				//String status= null;
				//PreAuthBean preAuthBean =new PreAuthBean();
				MyProperties myResources = new MyProperties();
				FTPHOST = myResources.getMyProperties("ftp_host");
				FTPUName = myResources.getMyProperties("ftp_username");
				FTPPwd = myResources.getMyProperties("ftp_password");
			    FTPClient client = new FTPClient();
			    boolean success=false;
			        try {
			                client.connect(FTPHOST);
				            client.login(FTPUName, FTPPwd);
				            client.setFileType(FTP.BINARY_FILE_TYPE);
				            client.enterLocalPassiveMode();
				          //  FileOutputStream fos = null;  
				            File downloadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir");  
				            int moduleId=0;
							if(request.getParameter("moduleId")!=null){
								moduleId=Integer.parseInt(request.getParameter("moduleId"));
							}
							
							if(request.getParameter("fileName")!=null && !request.getParameter("fileName").equals("")) {
								fileName = request.getParameter("fileName").trim();
	                        }
				            if(request.getParameter("actualFileName")!=null && !request.getParameter("actualFileName").equals("")) {
				            	actualFileName = request.getParameter("actualFileName").trim();
				            }
				            if(request.getParameter("scanningDate")!=null && !request.getParameter("scanningDate").equals("")) {
				            	scanningDate = request.getParameter("scanningDate").trim();
				            }
				            if(request.getParameter("folderName")!=null && !request.getParameter("folderName").equals("")) {
				            	folderName = request.getParameter("folderName").trim();
				            }
				            /*if(request.getParameter("status")!=null && !request.getParameter("status").equals("")) {
				            	status = request.getParameter("status");
				            }*/
							String ftpDownloadPath=null;
							String pathcurrent=null;
							String constantspath=null;
							String curDateString = scanningDate;
							if(moduleId==1){
								constantspath=myResources.getMyProperties("uploadGHPL");
								pathcurrent=constantspath+curDateString;
							    ftpDownloadPath=pathcurrent;
					    	 }else if(moduleId==2 || moduleId==4){
					    		 constantspath=myResources.getMyProperties("uploadAB");
					    		 pathcurrent=constantspath+curDateString;
								 ftpDownloadPath=pathcurrent;
					    	 }
							
							String filePath = ftpDownloadPath +"/"+ fileName;
							if(actualFileName==null){
								actualFileName=fileName;
							}
							String downloadTempdir= downloadPath+File.separator+actualFileName;
							 File downloadFile2 = new File(downloadTempdir);
							 OutputStream outputStream2=null;
							 InputStream inputStream=null;
						         outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
						         inputStream = client.retrieveFileStream(filePath);
						        byte[] bytesArray = new byte[4096];
						        int bytesRead = -1;
						        if(inputStream!=null){
							        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
							        	outputStream2.write(bytesArray, 0, bytesRead);
							        }
							        success = client.completePendingCommand();
							       /* if (success) {
							        }*/
							        outputStream2.close();
							        inputStream.close();
						        }
								
						        if (success) {
								 //File downloadFile = new File(filePath);
							        FileInputStream inStream = new FileInputStream(downloadFile2);
							        // if you want to use a relative path to context root:
							        // obtains ServletContext
							        ServletContext context = getServletContext();
							         
							        // gets MIME type of the file
							        String mimeType = context.getMimeType(filePath);
							        if (mimeType == null) {        
							            // set to binary type if MIME mapping not found
							            mimeType = "application/octet-stream";
							        }
							         
							        // modifies response
							        response.setContentType(mimeType);
							        response.setContentLength((int) downloadFile2.length());
							         
							        // forces download
							        String headerKey = "Content-Disposition";
							        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile2.getName());
							        response.setHeader(headerKey, headerValue);
							         
							        // obtains response's output stream
							        OutputStream outStream = response.getOutputStream();
							         
							        byte[] buffer = new byte[8912];
							        int bytesRead1 = -1;
							         
							        while ((bytesRead1 = inStream.read(buffer)) != -1) {
							            outStream.write(buffer, 0, bytesRead1);
							        }
							         
							        inStream.close();
							        outStream.close();
							        downloadFile2.delete();
				            	//getServletContext().getRequestDispatcher("/ProcessSheetListAction").forward(request, response);
				            	if(!response.isCommitted()){
									RequestDispatcher rd = request.getRequestDispatcher("./ProcessSheetListAction");
									rd.forward(request,response);
									return;
								}
						        }else{
						        	request.setAttribute("documentnotfound", Constants.documentnotfound);
						        	getServletContext().getRequestDispatcher("/documentdownload.jsp").forward(request, response);
						        }
				            
				        }catch (Exception e) {
				            e.printStackTrace();
				            logger.error(e, e);
				        }finally{
				        	if(client!=null){
				        		client=null;
				        	}
				        }
				
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
					session.setAttribute("moduleid", 0);
		    		session.invalidate();
		    	}
				 response.sendRedirect("./login.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e);
				getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
				SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("SentTOInsurer_EXCEPTION_ProcessSheetDownloadAction",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
	
	}
	
	
}
