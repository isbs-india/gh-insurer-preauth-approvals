package com.ghpl.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.IAuthAuditBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;

/**
 * Servlet implementation class AuditListActionDeny
 */
@WebServlet("/AuditListActionDeny")
public class AuditListActionDeny extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 Logger logger =Logger.getLogger("AuditListActionDeny");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuditListActionDeny() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				
				List<IAuthAuditBean> auditList = new ArrayList<IAuthAuditBean>();
				//RETRIEVING LIST DATA FROM GHPL IAUTH_PREAUTH TABLE
				auditList = SendToInsurerManager.getAuditListDenialDetails(1);
				if(auditList!=null){
					request.setAttribute("auditList", auditList);
					RequestDispatcher rd = request.getRequestDispatcher("./preauthdenytoinsurer.jsp");
					rd.forward(request,response);
				}
		}else{
			Cookie[] cookies = request.getCookies();
	    	if(cookies != null){
	    	for(Cookie cookie : cookies){
	    		if(cookie.getName().equals("JSESSIONID")){
	    			//System.out.println("JSESSIONID="+cookie.getValue());
	    			cookie.setValue("");
	    			cookie.setPath("/");
	    			cookie.setMaxAge(0);
	    			response.addCookie(cookie);
	    			break;
	    		}
	    	}
	    	}
			if(session != null){
	    		session.invalidate();
	    	}
			 response.sendRedirect("./login.jsp");
		}
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
