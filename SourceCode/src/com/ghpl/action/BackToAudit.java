package com.ghpl.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.BackToAuditBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;
import com.ghpl.util.Util;

/**
 * Servlet implementation class BackToAudit
 */
@WebServlet("/BackToAudit")
public class BackToAudit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger logger =Logger.getLogger("BackToAudit");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BackToAudit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				
				//int userid
				//drname
				//moduleName

				int moduleUserId = 0;
				int moduleId = 0;
				String claimId = "";
				String ccnUnique="";
				int rauthFlag = 0;
				int preAuthAppno = 0;
				String backtoauditremarks=null;
				int userId=0;
				ccnUnique=request.getParameter("ccnUnique")!=null?request.getParameter("ccnUnique"):"0";
				moduleUserId=Util.stringToInt(request.getParameter("moduleUserId")!=null?request.getParameter("moduleUserId"):"0");
				moduleId=Util.stringToInt(request.getParameter("moduleId")!=null?request.getParameter("moduleId"):"0");
				claimId=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
				rauthFlag=Util.stringToInt(request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0");
				preAuthAppno=Util.stringToInt(request.getParameter("preAuthAppno")!=null?request.getParameter("preAuthAppno"):"0");
				backtoauditremarks=request.getParameter("backtoauditremarks")!=null?request.getParameter("backtoauditremarks"):"NA";
				userId=Util.stringToInt(request.getParameter("userid")!=null?request.getParameter("userid"):"0");
				
				BackToAuditBean backToAuditBeanObj = null;
				backToAuditBeanObj = new BackToAuditBean();
				
				backToAuditBeanObj.setModuleUserId(moduleUserId);
				backToAuditBeanObj.setModuleId(moduleId);
				backToAuditBeanObj.setClaimId(claimId);
				backToAuditBeanObj.setReauthFlag(rauthFlag);
				backToAuditBeanObj.setPreauthAppNo(preAuthAppno);
				backToAuditBeanObj.setRemarks(backtoauditremarks);
				backToAuditBeanObj.setUserId(userId);
				backToAuditBeanObj.setCcnUnique(ccnUnique);
				
				backToAuditBeanObj = SendToInsurerManager.updateClaimStatusBackToAudit(backToAuditBeanObj,moduleId);
				
				if(backToAuditBeanObj!=null){
					
					if(backToAuditBeanObj.getProcessingStatus() == 0)
					{
					 request.setAttribute("heading","Failure Message");	
					 request.setAttribute("msg","<center><b>Process Of Sending Back Claim To Medico Failed</b></center>");
					 request.setAttribute("ccnUnique",ccnUnique);
					 request.setAttribute("claimid",claimId);
					 request.setAttribute("reauthflag",rauthFlag);
					 request.setAttribute("mailres","<center><b>Process Of Sending Back Claim To Medico Failed</b></center>");
					}
					else if(backToAuditBeanObj.getProcessingStatus() == 1)
					{
						SendToInsurerManager.updateIAuthPreAuthBackToAudit(backToAuditBeanObj,1);
						
						 request.setAttribute("heading","Successfully Message");	
						 request.setAttribute("msg","<center><b>Claim Successfully Sent Back To Medico</b></center>");
						 request.setAttribute("ccnUnique",ccnUnique);
						 request.setAttribute("claimid",claimId);
						 request.setAttribute("reauthflag",rauthFlag);
						 request.setAttribute("mailres","<center><b>Claim Successfully Sent Back To Medico</b></center>");
					}
					else if (backToAuditBeanObj.getProcessingStatus() == 2)
					{
						 request.setAttribute("heading","Successfully Message");	
						 request.setAttribute("msg","<center><b>Claim Is Not In Valid Status. Kindly Check</b></center>");
						 request.setAttribute("ccnUnique",ccnUnique);
						 request.setAttribute("claimid",claimId);
						 request.setAttribute("reauthflag",rauthFlag);
						 request.setAttribute("mailres","<center><b>Claim Is Not In Valid Status. Kindly Check</b></center>");
					}
					else
					{
						 request.setAttribute("heading","Successfully Message");	
						 request.setAttribute("msg","<center><b>There Is Some Issue in The Claim processing. Kindly Reprocess</b></center>");
						 request.setAttribute("ccnUnique",ccnUnique);
						 request.setAttribute("claimid",claimId);
						 request.setAttribute("reauthflag",rauthFlag);
						 request.setAttribute("mailres","<center><b>There Is Some Issue in The Claim processing. Kindly Reprocess</b></center>");
					}
					
					RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
					rd.forward(request,response);
					
				}
		}else{
			Cookie[] cookies = request.getCookies();
	    	if(cookies != null){
	    	for(Cookie cookie : cookies){
	    		if(cookie.getName().equals("JSESSIONID")){
	    			//System.out.println("JSESSIONID="+cookie.getValue());
	    			cookie.setValue("");
	    			cookie.setPath("/");
	    			cookie.setMaxAge(0);
	    			response.addCookie(cookie);
	    			break;
	    		}
	    	}
	    	}
			if(session != null){
	    		session.invalidate();
	    	}
			 response.sendRedirect("./login.jsp");
		}
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION - BackToAudit",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	
	}

}
