package com.ghpl.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.LoginBean;
import com.ghpl.persistence.login.LoginDAOManager;
import com.ghpl.util.Crypt;

/**
 * Servlet implementation class LoginAction
 */
@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	Logger logger = Logger.getLogger("LoginAction");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
		HttpSession session = request.getSession(true); 
		if(session != null && !session.isNew()) {
			String uname=null;
			String encriptPwd=null;
			String salt="am";
			int serverip=0;
			LoginBean user=null;
			try{
				RequestDispatcher rd =null;
				
				String userName=null;
				if(request.getParameter("userName")!=null){
					userName=request.getParameter("userName");
				}
				String pwd=null;
				if(request.getParameter("userName")!=null){
					pwd=request.getParameter("pwd");
				}
				
				if(request.getParameter("serverip")!=null){
					serverip=Integer.parseInt(request.getParameter("serverip"));
				}
				//String hostName = request.getRemoteAddr();
				
				String hostName = request.getHeader("X-FORWARDED-FOR");  
				   if (hostName == null) {  
					   hostName = request.getRemoteAddr();  
				   }
				//logger.info(userName   +"   ----   "+   pwd   +"  ---  "+  serverip   +"   ---    "+hostName);
				
				if(userName!=null && !userName.equals("")){
					uname=userName;
				}
				if(pwd!=null && !pwd.equals("")){
					// Crypt cryptobj = new Crypt();
					 encriptPwd = Crypt.crypt(salt, pwd);
				}
				
				if ((!uname.equals("") && uname !=null)&&(!encriptPwd.equals("") && encriptPwd !=null) && (serverip != 0)) { 
					try {
						user = new LoginBean();
						user= LoginDAOManager.getUserDetails(uname, encriptPwd, serverip, hostName);
						if(user!=null){
							 session.setAttribute("user", user);
							 session.setAttribute("userName", user.getLoginUserName());
							 session.setAttribute("roleId",user.getRoleId());
							 //session.setAttribute("moduleUserId", user.getgetModuleUserId())
							 
							 System.out.println(user.getRoleId());
							 if(user.getRoleId()==17 || user.getRoleId()==40){
								 rd = request.getRequestDispatcher("/AuditListAction");
								 rd.forward(request,response);
							 }else{
								 rd = request.getRequestDispatcher("/error.jsp");
								 rd.forward(request,response);
							 }
						}else{
							if(session != null){
								session.invalidate();
					    	}
							 request.setAttribute("errorMessage", "Invalid username or password"); 
							 rd = request.getRequestDispatcher("/index.jsp");
							 rd.forward(request,response);
						}
					} catch (DAOException e) {
						e.printStackTrace();
					}
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		} else {
			if(session != null){
	    		session.invalidate();
	    	}
		    response.sendRedirect("./index.jsp");
		}
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();//pbs.getStaticMailId()
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}*/

}
