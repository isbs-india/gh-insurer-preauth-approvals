package com.ghpl.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.ProcessSheetBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;

/**
 * Servlet implementation class ProcessSheetListAction
 */
@WebServlet("/ProcessSheetListAction")
public class ProcessSheetListAction extends HttpServlet {
	static Logger logger =Logger.getLogger("ProcessSheetListAction");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcessSheetListAction() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doPost(request,response);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				int moduleId =0;
				String ccnNumber = null;
				if(request.getParameter("claimId")!=null){
					ccnNumber = request.getParameter("claimId");
				}
				
				String ccnUnique = null;
				if(request.getParameter("ccnUnique")!=null){
					ccnUnique = request.getParameter("ccnUnique");
				}
				if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
				}
				ProcessSheetBean processSheetBean =new ProcessSheetBean();
				if(moduleId==1){
					processSheetBean=SendToInsurerManager.getProcessSheetDetailsByCCN(ccnUnique, moduleId);
					List <ProcessSheetBean> processList= new ArrayList<ProcessSheetBean>();
					processList.add(processSheetBean);
					request.setAttribute("processList", processList);
					RequestDispatcher rd = request.getRequestDispatcher("./processsheet.jsp");
					rd.forward(request,response);
				}else if(moduleId==2 || moduleId==4){
					processSheetBean=SendToInsurerManager.getProcessSheetDetailsByCCNRET(ccnUnique, moduleId);
					List <ProcessSheetBean> processList= new ArrayList<ProcessSheetBean>();
					processList.add(processSheetBean);
					request.setAttribute("processList", processList);
					RequestDispatcher rd = request.getRequestDispatcher("./processsheetret.jsp");
					rd.forward(request,response);
				}
				
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
					session.setAttribute("moduleid", 0);
		    		session.invalidate();
		    	}
				 response.sendRedirect("./index.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.error(e);
			}
	}

}
