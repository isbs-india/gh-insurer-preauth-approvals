package com.ghpl.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.GetInsurerPrefillDataListBean;
import com.ghpl.model.iauth.LoginBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;

/**
 * Servlet implementation class DisplayInsurerDocAction
 */
@WebServlet("/DisplayInsurerDocAction")
public class DisplayInsurerDocAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Logger logger =Logger.getLogger("DisplayInsurerDocAction");
    
    public DisplayInsurerDocAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int moduleId=0; 
   	    GetInsurerPrefillDataListBean finalBean=null;
   	    String claimid="";
        int reauthflag=0;
        
        
        try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
			     
				    //moduleId=Integer.parseInt(request.getParameter("moduleId"));
				    claimid=request.getParameter("ccn")!=null?request.getParameter("ccn"):"0";
				    reauthflag=Integer.parseInt(request.getParameter("reauthflag")!=null?request.getParameter("reauthflag"):"0");
				    moduleId=Integer.parseInt(request.getParameter("moduleid")!=null?request.getParameter("moduleid"):"0");
				    
				    
				//RETRIEVING LIST DATA FROM GHPL IAUTH_PREAUTH TABLE
				finalBean = SendToInsurerManager.getClaimDownloadDocData(claimid, reauthflag,moduleId);
				if(finalBean!=null){
					finalBean.setCcnUnique(claimid);
					request.setAttribute("finalBean", finalBean);
					RequestDispatcher rd = request.getRequestDispatcher("./"+finalBean.getFileName());
					rd.forward(request,response);
				}
				else
				{
					request.setAttribute("claimid", claimid);
					request.setAttribute("reauthflag", reauthflag);
					request.setAttribute("msg", "Kindly search with valid claimId which was already sent to insurer");
					request.setAttribute("mailres", "Kindly search with valid claimId which was already sent to insurer");
					RequestDispatcher rd = request.getRequestDispatcher("status.jsp");
					rd.forward(request,response);
				}
		}else{
			Cookie[] cookies = request.getCookies();
	    	if(cookies != null){
	    	for(Cookie cookie : cookies){
	    		if(cookie.getName().equals("JSESSIONID")){
	    			//System.out.println("JSESSIONID="+cookie.getValue());
	    			cookie.setValue("");
	    			cookie.setPath("/");
	    			cookie.setMaxAge(0);
	    			response.addCookie(cookie);
	    			break;
	    		}
	    	}
	    	}
			if(session != null){
	    		session.invalidate();
	    	}
			 response.sendRedirect("./login.jsp");
		}
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
        
        
	}

}
