package com.ghpl.action;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.IAuthMailBean;
import com.ghpl.util.Constants;
import com.ghpl.util.DBConn;
import com.ghpl.util.MyProperties;
import com.ghpl.util.ProcedureConstants;

/**
 * Servlet implementation class Denial_SendMailAction
 */
@WebServlet("/Denial_SendMailAction")
public class Denial_SendMailAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 Logger logger =Logger.getLogger("Denial_SendMailAction");  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Denial_SendMailAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				IAuthMailBean iAuthMailBean = new IAuthMailBean();	
				Connection con = null;
				String fileName=null;
				//String extension=null;
				String filePath=null;
				String FTPHOST=null;
				String FTPUName=null;
				String FTPPwd=null;
				int errorflg=0;
				String errormsg="";
				String clmid = "";
				String rauthflg = "";
				String userid="";
				int res=5;
				String mailres = "";
				String emailid="";
				String ccemailid="";
				int moduleId=0;
				String ftpFolderName="";
				int moduleUserId=0;
				int len = 0;
				Logger logger =Logger.getLogger("Denial_sendmail.jsp");

				String ccnUnique="";
				clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
				ccnUnique=request.getParameter("ccnUnique")!=null?request.getParameter("ccnUnique"):"0";
				emailid=request.getParameter("emailid")!=null?request.getParameter("emailid"):"0";
				logger.info("emailid -- " + emailid);
				len =emailid.length();
				ccemailid=request.getParameter("ccemailid")!=null?request.getParameter("ccemailid"):"0";
				/* moduleId=Integer.parseInt(request.getParameter("moduleId")!=null?request.getParameter("moduleId"):"0");
				moduleUserId=Integer.parseInt(request.getParameter("moduleUserId")!=null?request.getParameter("moduleUserId"):"0"); */

				if((request.getParameter("moduleId")!=null) && !(request.getParameter("moduleId").equals(""))){
					moduleId = Integer.parseInt(request.getParameter("moduleId"));
				}else{
					moduleId =0;
				}
				if(request.getParameter("moduleUserId")!=null && !(request.getParameter("moduleUserId").equals(""))){
					moduleUserId = Integer.parseInt(request.getParameter("moduleUserId"));
				}else{
					moduleUserId =0;
				}

				System.out.println("moduleId : "+moduleId);
				System.out.println("moduleUserId : "+moduleUserId);
				/********************FTP CREDENTIALS **************************************/

				MyProperties Gdbp = new MyProperties();

				FTPHOST = Gdbp.getMyProperties("ftp_host");
				FTPUName = Gdbp.getMyProperties("ftp_username");
				FTPPwd = Gdbp.getMyProperties("ftp_password");
				//ftpFolderName=Gdbp.getMyProperties("ftp_folder");


				//FOR DENIAL

					 String patientname = "";
					 String empname = "";
					 String empid = "";
					 String corpname = "";
					 String suminsured = "";
					 String reqamt = "";
					 String apramt = "";
					 String medicoRemarks="NA";
					 
					 String policyId="";
					 String cardId="";
					 String insuredId="";
					// String userid="";
					// String clmid = "";
					// String rauthflg = "";
					

				try{  
					
					clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
					rauthflg=request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0";
					userid=request.getParameter("userid")!=null?request.getParameter("userid"):"0";
					
					
					 patientname=request.getParameter("patientname")!=null?request.getParameter("patientname"):"NA";
					 empname=request.getParameter("empname")!=null?request.getParameter("empname"):"NA";
					 empid=request.getParameter("empid")!=null?request.getParameter("empid"):"0";
					 corpname=request.getParameter("corpname")!=null?request.getParameter("corpname"):"NA";
					 suminsured=request.getParameter("suminsured")!=null?request.getParameter("suminsured"):"0";
					 reqamt=request.getParameter("reqamt")!=null?request.getParameter("reqamt"):"0";
					 apramt=request.getParameter("apramt")!=null?request.getParameter("apramt"):"0";
					 clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
					 rauthflg=request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0";
					 userid=request.getParameter("userid")!=null?request.getParameter("userid"):"NA";
					 medicoRemarks=request.getParameter("medicoremarks")!=null?request.getParameter("medicoremarks"):"NA";
					 
					 policyId=request.getParameter("policyId")!=null?request.getParameter("policyId"):"NA";	 
					 cardId=request.getParameter("cardId")!=null?request.getParameter("cardId"):"NA";
					 insuredId=request.getParameter("insuredId")!=null?request.getParameter("insuredId"):"NA";
					
					//FOR CORP MODULE RELATED CLAIMS 
				     if( (moduleId == 1) || (moduleId == 2) || (moduleId == 4) )
				     {
					 con=DBConn.getMyConnection(moduleId);  
					
					 
					 CallableStatement stmt=con.prepareCall(ProcedureConstants.PROC_FOR_INSERT_DENIAL_CORP_TO_INSURER);
					 stmt.registerOutParameter(1, OracleTypes.NUMBER);
					 stmt.registerOutParameter(2, OracleTypes.VARCHAR);
					 stmt.setString(3,clmid);
					 stmt.setString(4,rauthflg);
					 stmt.setInt(5,moduleUserId);
					 logger.info("clmid : "+clmid);
					 logger.info("rauthflg : "+rauthflg);
					 logger.info("moduleUserId : "+moduleUserId);
					  
					  stmt.execute();
					  
					  errorflg=stmt.getInt(1);
					  errormsg=stmt.getString(2);
					  
					  
					  
					  String moduleName="";
					    if(moduleId==1){
							 moduleName="Corp";
						 }else if(moduleId==2){
							 moduleName="Bank Assurance";
						 }else if(moduleId==4){
				  		 moduleName="Individual";
				  		
				  	     }
						iAuthMailBean.setModule(moduleName);
						iAuthMailBean.setModuleId(moduleId);
						iAuthMailBean.setModuleLetterId(0);
						iAuthMailBean.setLetterTypeId(154);
						iAuthMailBean.setClaimId(clmid);
						iAuthMailBean.setReAuthFlag(Integer.parseInt(rauthflg));
						iAuthMailBean.setSuppCounter(0);
						iAuthMailBean.setPolicyID(Integer.parseInt(policyId));
						iAuthMailBean.setCardId(cardId);
						iAuthMailBean.setInsuredId(Integer.parseInt(insuredId)); 
						
						logger.info("ModuleLetterId :"+0);
						 logger.info("LetterTypeId :"+154); 
						 logger.info("SuppCounter :"+0);			 
						 logger.info("moduleName :"+moduleName);
						 logger.info("moduleId :"+moduleId); 
						 logger.info("claimno :"+clmid);
						 logger.info("rauthflg :"+rauthflg);
					     logger.info("policyId :"+Integer.parseInt(policyId));  
						 logger.info("cardId :"+cardId);
						 logger.info("insuredId :"+Integer.parseInt(insuredId)); 

					}//if CORP MODULE
					else
					{
						 errorflg=1;
						 errormsg="Not valid Module";
						 
					}


				//END
						
				}//try
				catch(Exception e)
				{
					System.out.println("EXCEPTION IN PREAUTH DENIAL: "+e);
					logger.error("EXCEPTION IN PREAUTH DENIAL: "+e);
					errorflg=1;
					request.setAttribute("heading","Fail Message");	
					request.setAttribute("msg","<center><b>CLAIM SENT TO INSURER FOR CONFIRMATION</b></center>");
					request.setAttribute("claimid",clmid);
					request.setAttribute("reauthflag",rauthflg);
					request.setAttribute("mailres","<center><b>Please Contact IT For Rectification</b> \n EXCEPTION IN PREAUTH DENIAL: "+e+"</center>");
					RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
					rd.forward(request,response);
					/* request.setAttribute("message","<center><b>Please Contact IT For Rectification</b>\n"+e.getMessage()+"</center>");
					RequestDispatcher rd = request.getRequestDispatcher("./errMessage.jsp");
					rd.forward(request,response);  */
				}//catch
				finally
				{
					try{

						if(con != null)
						{
						con.close();
						}
				}
				catch(Exception e){
					logger.error("Error -- " + e.getMessage());
					System.out.println("ERROR in FINALLY:"+e.getMessage());
					e.printStackTrace();
				}
				}

				//END
				if(errorflg == 0){

					//UPDATING THE STATUS IN GHPL MODULE- IAUTH_PREAUTH table
						try
						{
						if( (moduleId == 1) || (moduleId == 2) || (moduleId == 4) )
					     {
						 con=DBConn.getMyConnection(1);  
						 CallableStatement stmt=con.prepareCall(ProcedureConstants.PROC_FOR_UPDATE_STATUS_IAUTHPREAUTH_INSURER_SENT_DENIAL);
						  stmt.setInt(1, Integer.parseInt(userid));
						  stmt.setString(2, ccnUnique);
						  stmt.setInt(3, Integer.parseInt(rauthflg));
						  stmt.setInt(4, moduleId);
						  stmt.execute();
						  stmt.close();
						  
					     }
						
						}catch(Exception e)
						{
							System.out.println("EXCEPTION IN PREAUTH DENIAL: "+e);
							logger.error("EXCEPTION IN PREAUTH DENIAL: "+e);
							errorflg=1;
							request.setAttribute("heading","Fail Message");	
							request.setAttribute("msg","<center><b>CLAIM SENT TO INSURER FOR CONFIRMATION</b></center>");
							request.setAttribute("claimid",clmid);
							request.setAttribute("reauthflag",rauthflg);
							request.setAttribute("mailres","<center><b>Please Contact IT For Rectification</b> \n EXCEPTION IN PREAUTH DENIAL: "+e+"</center>");
							RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
							rd.forward(request,response);
						}//catch
						finally
						{
							try{
						  
								if(con != null)
						        {
								con.close();
						        }
						}
						catch(Exception e){
							logger.error("Error -- " + e.getMessage());
							System.out.println("ERROR in FINALLY:"+e.getMessage());
							e.printStackTrace();
						}
							
						}	
				ArrayList alist=new ArrayList();
				  	
				if(!"0".equals(emailid) && len > 5)
				{
				   			
					
					HashMap submap=new HashMap();
					
					
					String totdocs=request.getParameter("totdocs")!=null?request.getParameter("totdocs"):"0";
					int totaldoc=Integer.parseInt((totdocs));
					
					String[] docname=request.getParameterValues("doc");
					
					
					 if (docname != null) 
					   {
					      for (int i = 0; i < docname.length; i++) 
					      {
					         System.out.println ("DOCCCCC:"+docname[i]);
					         submap.put("id"+i,docname[i]);
					         alist.add(submap);
					      }
					    
					   }
					
					SendMail_preAuth sm = new SendMail_preAuth();
					SendGrid.Response mailresponse = null;
					try{
					//DEV MESSAGE
					//mailresponse = sm.SendMail_SGGrid_Denial(iAuthMailBean,Constants.MAIL_INSURER_SUBJECT_DENIAL+clmid,Constants.MAIL_INSURER_TEXT_DENIAL.replaceAll("<medicoRemarks>", medicoRemarks),clmid+"_"+rauthflg+".xls","santhosh.c@isharemail.in","","",alist,moduleId);
					//LIVE MESSAGE	
					mailresponse = sm.SendMail_SGGrid_Denial(iAuthMailBean,Constants.MAIL_INSURER_SUBJECT_DENIAL+clmid,Constants.MAIL_INSURER_TEXT_DENIAL.replaceAll("<medicoRemarks>", medicoRemarks),clmid+"_"+rauthflg+".xls",emailid,ccemailid,"",alist,moduleId);
					}
					catch(Exception e){
						request.setAttribute("message","<center><b>Please Contact IT For Rectification</b>\n"+e+"</center>");
						
						request.setAttribute("heading","Fail Message");	
						request.setAttribute("msg","<center><b>CLAIM SENT TO INSURER FOR CONFIRMATION</b></center>");
						request.setAttribute("claimid",clmid);
						request.setAttribute("reauthflag",rauthflg);
						request.setAttribute("mailres","<center><b>Please Contact IT For Rectification</b> \n EXCEPTION IN Mial process:"+e+"</center>");
						RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
						rd.forward(request,response);
						
					}
					if(mailresponse!=null){
						  if(mailresponse.getCode() ==200)
							{
								mailres="MAIL SUCCESSFULLY SENT to emailIds "+emailid+" WITH CC to"+ccemailid;
							}
							else
							{
								mailres="MAIL SENDING FAILED to emailIds "+emailid+" WITH CC to"+ccemailid;
							} 
						}
					
					
				  }//if
				  else
				  {
					  
					  HashMap submap=new HashMap();
						String totdocs=request.getParameter("totdocs")!=null?request.getParameter("totdocs"):"0";
						int totaldoc=Integer.parseInt((totdocs));
						String[] docname=request.getParameterValues("doc");
						 if (docname != null) 
						   {
						      for (int i = 0; i < docname.length; i++) 
						      {
						         System.out.println ("DOCCCCC:"+docname[i]);
						         submap.put("id"+i,docname[i]);
						         alist.add(submap);
						      }
						    
						   }
					  
					    SendMail_preAuth sm = new SendMail_preAuth();
						SendGrid.Response mailresponse = null;
					   mailresponse = sm.SendMail_SGGrid_Denial(iAuthMailBean,Constants.MAIL_INSURER_SUBJECT_DENIAL+clmid,Constants.MAIL_INSURER_TEXT_DENIAL.replaceAll("<medicoRemarks>", medicoRemarks),clmid+"_"+rauthflg+".xls",emailid,ccemailid,"",alist,moduleId);
						  
					   if(mailresponse!=null){
						  if(mailresponse.getCode() ==200)
							{
								mailres="MAIL SUCCESSFULLY SENT to emailIds "+emailid+" WITH CC to"+ccemailid;
							}
							else
							{
								mailres="MAIL SENDING FAILED to emailIds "+emailid+" WITH CC to"+ccemailid;
							} 
						}
					//mailres="MAILID NOT PROVIDED FOR RESPECTIVE ISSUING OFFICE. Please contact IT";
				  }

					System.out.println("AFTER MAIL");
					
					System.out.println("INSIDE ID ERRORFLAG=0");
				    
				    request.setAttribute("heading","Successfully Message");	
					request.setAttribute("msg","<center><b>CLAIM SENT TO INSURER FOR DENIAL</b></center>");
					request.setAttribute("claimid",clmid);
					 request.setAttribute("ccnUnique",ccnUnique);
					request.setAttribute("reauthflag",rauthflg);
					request.setAttribute("mailres",mailres);
					 RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
			 			rd.forward(request,response);
				}else{
				 
					System.out.println("INSIDE ELSE ERRORFLAG=1");
					
				  request.setAttribute("heading","Warning Message");	
				  request.setAttribute("msg","<center><b>CLAIM SENDING TO INSURER FOR DENIAL FAILED. please try Again. </b>"+errormsg+"</center>");
				  request.setAttribute("claimid",clmid);
				  request.setAttribute("ccnUnique",ccnUnique);
				  request.setAttribute("reauthflag",rauthflg);
				  request.setAttribute("mailres","Mail Not Sent");
				  RequestDispatcher rd = request.getRequestDispatcher("./status.jsp");
		 			rd.forward(request,response);
				} 
				
				
				
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			//System.out.println("JSESSIONID="+cookie.getValue());
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
		    		session.invalidate();
		    	}
				 response.sendRedirect("./login.jsp");
			}
			
		}catch(Exception e){
			e.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
