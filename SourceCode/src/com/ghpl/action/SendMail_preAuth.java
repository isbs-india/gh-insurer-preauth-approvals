package com.ghpl.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.ghpl.exception.SendGridException;
import com.ghpl.model.iauth.IAuthMailBean;
import com.ghpl.util.Constants;
import com.ghpl.util.MyProperties;



public class SendMail_preAuth
{
	
	private static Logger logger=Logger.getLogger("SendMail_preAuth");
	// ResourceBundle myResources;
	String username;
	String password;
	String mail_transport_protocol;
	String mail_smtp_port;
	String mail_smtp_host;
	String mail_smtp_auth;
	String mail_messageText;
	String sec_username;
	String sec_password;
	String sec_mail_transport_protocol;
	String sec_mail_smtp_port;
	String sec_mail_smtp_host;
	String sec_mail_smtp_auth;
	String sec_mail_messageText;
	String bccMailID;
	String sec_bccMailID;
	String mail_from;
	String sec_mail_from;

	String preauthBcc=null;

	String fileName=null;
	//String extension=null;
	String filePath=null;
	String FTPHOST=null;
	String FTPUName=null;
	String FTPPwd=null;

	String docpath=null;
	String docpath_http=null;
	String FTP_IAUTH_PATH;  
	String ftpFolder = null;


	public int SendMail_preauthcron1(String subject, String messageText,String displayfilenme,String TO,String cc,String bcc)
			throws MessagingException
			{
		// System.out.println("enter send mail"+messageText);
		int k=0;  
		MyProperties Gdbp = new MyProperties();   
		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		//mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		mail_transport_protocol=Gdbp.getMyProperties("preauth_transport_protocol");
		//FTP_IAUTH_PATH = Gdbp.getMyProperties("FTP_IAUTH_PATH");
		ftpFolder = Gdbp.getMyProperties("ftp_folder");

		try
		{
			Properties props = new Properties();
			props.put("mail.transport.protocol", mail_transport_protocol);
			props.put("mail.smtp.port", mail_smtp_port);
			props.put("mail.smtp.host", mail_smtp_host);
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.ssl.trust", "*");
			props.put("mail.smtp.starttls.enable", "true");
			Authenticator auth = new SMTPAuthenticator();

			Session session = Session.getInstance(props, auth);
			MimeMessage message = new MimeMessage(session);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			// Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{
				InternetAddress fromAddress = new InternetAddress(mail_from);
				// System.out.println("fromAddress"+fromAddress);

				message.setFrom(fromAddress);
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();
				if(b==true)
				{
					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);

					//CC*******************
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();
					for(int l = 0; l < toarraycc.length; l++)
					{
						Matcher v=p.matcher(toarraycc[l]);
						boolean h=v.matches();
						if(h==true)
						{
							StringTokenizer st = new StringTokenizer(toarraycc[l],",");
							while (st.hasMoreTokens()) {
								ccemailidarr.add(st.nextToken());
							}//while
						}//if h

					}//for CC
					int sizecc = ccemailidarr.size(); 
					// System.out.println("SIZE CC"+sizecc);
					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++){
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
					}
					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);

					//CC END

					//BCC*******************
					String toarraybcc[] = bcc.split(",");
					ArrayList bccemailidarr=new ArrayList();
					for(int l = 0; l < toarraybcc.length; l++)
					{
						Matcher vh=p.matcher(toarraybcc[l]);
						boolean hh=vh.matches();
						if(hh==true)
						{
							StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
							while (st.hasMoreTokens()) {
								bccemailidarr.add(st.nextToken());
							}//while
						}//if h

					}//for BCC
					int sizebcc = bccemailidarr.size(); 
					// System.out.println("SIZE CC"+sizecc);
					InternetAddress bccAddress[] = new InternetAddress[sizebcc];
					for(int bm = 0; bm < sizebcc; bm++){
						bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
					}
					message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);
					//BCC END  
					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					message.setContent(multipart);

					Transport.send(message);

					k=1;
				}//if b is true
			}//for  LOOP FOR TO EMAILID

		}
		catch(Exception ex)
		{
			logger.error((new StringBuilder("Error in sendmail from SendtoInsurer Application ::")).append(ex).toString());
			k=2;
		}
		return k;
			}
	
	
	
	
	
	public SendGrid.Response SendMail_SGGrid_CORP(IAuthMailBean iAuthMailBean,String subject, 
			String messageText,String fileName,String TO,String cc,String bcc,ArrayList arr,int moduleId)
	        throws MessagingException, FileNotFoundException, IOException, SendGridException
	    {
			int k=0;
			URL url=null;
			MyProperties Gdbp =new MyProperties();
			ftpFolder = Gdbp.getMyProperties("ftp_folder");
	       /* username = myResources.getMyProperties("primary_username");
	        password = myResources.getMyProperties("primary_password");
	        mail_transport_protocol = myResources.getMyProperties("primary_transport_protocol");
	        mail_smtp_port = myResources.getMyProperties("primary_smtp_port");
	        mail_smtp_host = myResources.getMyProperties("primary_smtp_host");
	        mail_smtp_auth = myResources.getMyProperties("primary_smtp_auth");
	        mail_messageText = myResources.getMyProperties("primary_messageText");*/
			
			
			FTPUName = Gdbp.getMyProperties("ftp_username");
			FTPPwd = Gdbp.getMyProperties("ftp_password");
			FTPHOST = Gdbp.getMyProperties("ftp_host");
			docpath=Gdbp.getMyProperties("DOCPATH_pr");
			docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");
			
			
			username = Gdbp.getMyProperties("preauth_username");
			password = Gdbp.getMyProperties("preauth_password");
			mail_from = Gdbp.getMyProperties("preauth_from");
			// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
			mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
			mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
			mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
			mail_messageText = Gdbp.getMyProperties("preauth_messageText");
			preauthBcc = Gdbp.getMyProperties("preauth_bcc");
			
			 switch(moduleId)
				{
				case 1:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_GHPL;
					break;
				case 2:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
					break;	
				case 4:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
					break;
				
				}
	      
	        mail_from = Gdbp.getMyProperties("primary_from");
	        
	        SendGrid sendgrid = new SendGrid(Gdbp.getMyProperties("SendGridApikey"));
	        
	        SendGrid.Email email = new SendGrid.Email();
	        
	        Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	        // TO="prabhakar.g@isharemail.in;anand.kumar@isharemail.in;raviteja.k@isharemail.in";
	        //bcc="prabhakar.g@isharemail.in;anand.kumar@isharemail.in";
	        //cc="raviteja.k@isharemail.in;anand.kumar@isharemail.in";
	        String toarray[] = TO.split(";");
            ArrayList toemailidarr=new ArrayList();
            ArrayList ccemailidarr= null;
            ArrayList bccemailidarr= null;
            String tempEmailId="";
            for(int i = 0; i < toarray.length; i++)
            {
	            	String emailId="";
	            	if(toarray[i]!=null && !toarray[i].equals("NA") && !toarray[i].equals("0") && !toarray[i].equals("")){
	            		emailId=toarray[i];
	            	}
	            	if(!tempEmailId.equals(emailId)){
		         		Matcher g=p.matcher(toarray[i]);
		         		boolean b=g.matches();
		         		if(b==true)
		         		{
		         			StringTokenizer stto = new StringTokenizer(toarray[i],",");
		      				while (stto.hasMoreTokens()) {
		      					toemailidarr.add(stto.nextToken());
		      				}//while
		            	}
		         		tempEmailId=toarray[i];
	            	}
            }
    	 	//CC*******************
            if(cc!=null &&! cc.equals("")){
            String toarraycc[] = cc.split(";");
            ccemailidarr=new ArrayList();
            String tempccEmailId="";
            for(int l = 0; l < toarraycc.length; l++)
            {
            	
            	String emailIdCC="";
            	if(toarraycc[l]!=null && !toarraycc[l].equals("NA") && !toarraycc[l].equals("0") && !toarraycc[l].equals("")){
            		emailIdCC=toarraycc[l];
            	}
            	if(!tempccEmailId.equals(emailIdCC)){
	        		Matcher v=p.matcher(toarraycc[l]);
	        		boolean h=v.matches();
	        		if(h==true)
	        		{
	        			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	      				while (st.hasMoreTokens()) {
	      					ccemailidarr.add(st.nextToken());
	      				}//while
	        		}//if h
	        		tempccEmailId=toarraycc[l];
            	}// if tempccEmailId 
    		}//for CC
            }   
	      //CC END
		                 
		    //BCC*******************
            if(bcc!=null && !bcc.equals("")){
	         String toarraybcc[] = bcc.split(";");
	          bccemailidarr=new ArrayList();
	          String tempbccEmailId="";
	         for(int l = 0; l < toarraybcc.length; l++)
	         {
	        	 String emailIdBCC="";
	            	if(toarraybcc[l]!=null && !toarraybcc[l].equals("NA") && !toarraybcc[l].equals("0") && !toarraybcc[l].equals("")){
	            		emailIdBCC=toarraybcc[l];
	            	}
	            	if(!tempbccEmailId.equals(emailIdBCC)){
			        	 Matcher vh=p.matcher(toarraybcc[l]);
			        	 boolean hh=vh.matches();
			     			if(hh==true)
			     			{
			     				StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
			     				while (st.hasMoreTokens()){
			     					bccemailidarr.add(st.nextToken());
			     				}//while
			     			}//if h
			     			tempbccEmailId=toarraybcc[l];
	            	}
	         
	 		}//for BCC
            }
            int sizeto = toemailidarr.size(); 
            // System.out.println("SIZE CC"+sizecc);
             String toAddress[] = new String[sizeto];
             if(sizeto>0){
	                 for(int m = 0; m < sizeto; m++){
	                	 toAddress[m] = new String(toemailidarr.get(m).toString());
	                 }
             }
	           String ccAddress[]=null;
	           if(ccemailidarr!=null){
	        	   	int sizecc = ccemailidarr.size();
	        	   	 ccAddress = new String[sizecc];
	        	   	 if(sizecc>0){
		        	   	 for(int m = 0; m < sizecc; m++){
			            	 ccAddress[m] = new String(ccemailidarr.get(m).toString());
			             }
	        	   	 }
	           }
	           String bccAddress[]= null;
	           if(bccemailidarr!=null){
	        	   int sizebcc = bccemailidarr.size();  
	              bccAddress = new String[sizebcc];
	              if(sizebcc>0){
		             for(int m = 0; m < sizebcc; m++){
		            	 bccAddress[m] = new String(bccemailidarr.get(m).toString());
		             }
	              }
	           }
	        email.addTo(toAddress); // dev configuration   
	        if(bccAddress!=null){
	        email.addBcc(bccAddress);
	        }
	        if(ccAddress!=null){
	        	email.addCc(ccAddress);
	        }
	        email.setFrom(mail_from);
	        email.setSubject(subject);
	        email.setHtml(messageText);
	        //email.setHtml(prop.getMyProperties("mail_messageText"));
	        String MAILFILEPATH = ftpFolder+"CORP"+File.separator+fileName;
	       // File file= new File(MAILFILEPATH+File.separator+fileName);
	        File file= new File(MAILFILEPATH);
	        email.addAttachment(fileName, file);
	        
	        
	        
	        //ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


			int index=0;
			String ftp_http_FileName;
			String docName;

			while(index < arr.size())
			{
				HashMap data=(HashMap) arr.get(index);

				logger.info("arr.size():"+arr.size());

				//messageBodyPart = new MimeBodyPart();

				//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
				ftp_http_FileName="";
				fileName="";
				ftp_http_FileName=data.get("id"+index).toString();

				String MyFileNameArr[]=ftp_http_FileName.split("~");
				System.out.println("MyFileNameArr[] : "+MyFileNameArr);


				if("1".equals(MyFileNameArr[1]))
				{

					docName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					System.out.println("docName : "+docName);
					
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

					// System.out.println("filename FTP:"+fileName);
					// System.out.println("extension FTP:"+extension); 
					// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

					url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
					System.out.println("ftp url : "+url);
					
					// System.out.println("FTPurl:"+url);
				}//IF FTP FILE
				else
				{
					url = new URL(docpath_http+""+MyFileNameArr[0]);
					fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					System.out.println("fileName : "+fileName);
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());
					
				}
				 System.out.println("filename HTTP:"+MyFileNameArr[2]);
				 URLDataSource udsource = new URLDataSource(url);
				 InputStream is = udsource.getInputStream();
				
				email.addAttachment(MyFileNameArr[2],is);
				
				//END
				
				
				index++;
			}//while

			logger.info("After While");
			//message.setContent(multipart);

			//END DOCS ATTACHMENT
	       
	      
	        email.addUniqueArg("MYSUBJECT", subject);
	        email.addUniqueArg("MYSENDER", mail_from);
	        email.addUniqueArg("MODID", iAuthMailBean.getModuleId()+"");
	        email.addUniqueArg("MODLETID", iAuthMailBean.getModuleLetterId()+"");
	        email.addUniqueArg("LET_TY_ID", iAuthMailBean.getLetterTypeId()+"");
	        email.addUniqueArg("CLAIMID", iAuthMailBean.getClaimId()+"");
	        email.addUniqueArg("REAUTHFLAG", iAuthMailBean.getReAuthFlag()+"");
	        email.addUniqueArg("SUPPCNT", iAuthMailBean.getSuppCounter()+"");
	        email.addUniqueArg("POLICYID", iAuthMailBean.getPolicyID()+"");
	        email.addUniqueArg("GHPL_CARDID", iAuthMailBean.getCardId());
	        email.addUniqueArg("INSUREDID", iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details start-------------------");
	        logger.info("addUniqueArg MYSUBJECT  "+subject);
	        logger.info("addUniqueArg MYSENDER   "+mail_from);
	        logger.info("addUniqueArg MODID "+iAuthMailBean.getModuleId());
	        logger.info("addUniqueArg MODLETID "+ iAuthMailBean.getModuleLetterId()+"");
	        logger.info("addUniqueArg LET_TY_ID "+ iAuthMailBean.getLetterTypeId()+"");
	        logger.info("addUniqueArg CLAIMID "+ iAuthMailBean.getClaimId()+"");
	        logger.info("addUniqueArg REAUTHFLAG "+iAuthMailBean.getReAuthFlag()+"");
	        logger.info("addUniqueArg SUPPCNT "+ iAuthMailBean.getSuppCounter()+"");
	        logger.info("addUniqueArg POLICYID "+ iAuthMailBean.getPolicyID()+"");
	        logger.info("addUniqueArg GHPL_CARDID "+ iAuthMailBean.getCardId());
	        logger.info("addUniqueArg INSUREDID "+iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details end---------------");
	        
	        
	        
	        SendGrid.Response response = sendgrid.send(email);
	        
	        file.delete();
	        /*if(response.getCode()==400 && !response.getStatus()){
	        	emailSentStatus= mailsend.SendMail_preauthcron1(mailSubject, emailfinalvalue,fileName,"prabha.gogula@gmail.com","","");
	        	//emailSentStatus=  mailsend.SendMail_preauthcron1(mailSubject, emailfinalvalue,fileName,letterListBean.getEmailto(),emailCC,emailBCC);
	        }else{
	        	emailSentStatus =1;
	        }*/
	        
			return response;
	    }
	
	
	
	
	public SendGrid.Response SendMail_SGGrid_Retail(IAuthMailBean iAuthMailBean,String subject, 
			String messageText,String fileName,String TO,String cc,String bcc,ArrayList arr,int moduleId)
	        throws MessagingException, FileNotFoundException, IOException, SendGridException
	    {
			int k=0;
			URL url=null;
			MyProperties Gdbp =new MyProperties();
			ftpFolder = Gdbp.getMyProperties("ftp_folder");
	       /* username = myResources.getMyProperties("primary_username");
	        password = myResources.getMyProperties("primary_password");
	        mail_transport_protocol = myResources.getMyProperties("primary_transport_protocol");
	        mail_smtp_port = myResources.getMyProperties("primary_smtp_port");
	        mail_smtp_host = myResources.getMyProperties("primary_smtp_host");
	        mail_smtp_auth = myResources.getMyProperties("primary_smtp_auth");
	        mail_messageText = myResources.getMyProperties("primary_messageText");*/
			
			
			FTPUName = Gdbp.getMyProperties("ftp_username");
			FTPPwd = Gdbp.getMyProperties("ftp_password");
			FTPHOST = Gdbp.getMyProperties("ftp_host");
			docpath=Gdbp.getMyProperties("DOCPATH_pr");
			docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");
			
			
			username = Gdbp.getMyProperties("preauth_username");
			password = Gdbp.getMyProperties("preauth_password");
			mail_from = Gdbp.getMyProperties("preauth_from");
			// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
			mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
			mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
			mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
			mail_messageText = Gdbp.getMyProperties("preauth_messageText");
			preauthBcc = Gdbp.getMyProperties("preauth_bcc");
			
			switch(moduleId)
			{
			case 4:
				FTP_IAUTH_PATH = Constants.UIA_FTP_DOC_PATH;
				break;
			case 5:
				FTP_IAUTH_PATH = Constants.NIA_FTP_DOC_PATH;
				break;	
			case 6:
				FTP_IAUTH_PATH = Constants.ORH_FTP_DOC_PATH;
				break;
			case 7:
				FTP_IAUTH_PATH = Constants.NIC_FTP_DOC_PATH;
				break;
			case 8:
				FTP_IAUTH_PATH = Constants.NAV_FTP_DOC_PATH;
				break;	
			}
	      
	        mail_from = Gdbp.getMyProperties("primary_from");
	        
	        SendGrid sendgrid = new SendGrid(Gdbp.getMyProperties("SendGridApikey"));
	        SendGrid.Email email = new SendGrid.Email();
	        Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	        // TO="prabhakar.g@isharemail.in;anand.kumar@isharemail.in;raviteja.k@isharemail.in";
	        //bcc="prabhakar.g@isharemail.in;anand.kumar@isharemail.in";
	        //cc="raviteja.k@isharemail.in;anand.kumar@isharemail.in";
	        String toarray[] = TO.split(";");
            ArrayList toemailidarr=new ArrayList();
            ArrayList ccemailidarr= null;
            ArrayList bccemailidarr= null;
            String tempEmailId="";
            for(int i = 0; i < toarray.length; i++)
            {
	            	String emailId="";
	            	if(toarray[i]!=null && !toarray[i].equals("NA") && !toarray[i].equals("0") && !toarray[i].equals("")){
	            		emailId=toarray[i];
	            	}
	            	if(!tempEmailId.equals(emailId)){
		         		Matcher g=p.matcher(toarray[i]);
		         		boolean b=g.matches();
		         		if(b==true)
		         		{
		         			StringTokenizer stto = new StringTokenizer(toarray[i],",");
		      				while (stto.hasMoreTokens()) {
		      					toemailidarr.add(stto.nextToken());
		      				}//while
		            	}
		         		tempEmailId=toarray[i];
	            	}
            }
    	 	//CC*******************
            String toarraycc[] = cc.split(";");
            ccemailidarr=new ArrayList();
            String tempccEmailId="";
            for(int l = 0; l < toarraycc.length; l++)
            {
            	
            	String emailIdCC="";
            	if(toarraycc[l]!=null && !toarraycc[l].equals("NA") && !toarraycc[l].equals("0") && !toarraycc[l].equals("")){
            		emailIdCC=toarraycc[l];
            	}
            	if(!tempccEmailId.equals(emailIdCC)){
	        		Matcher v=p.matcher(toarraycc[l]);
	        		boolean h=v.matches();
	        		if(h==true)
	        		{
	        			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	      				while (st.hasMoreTokens()) {
	      					ccemailidarr.add(st.nextToken());
	      				}//while
	        		}//if h
	        		tempccEmailId=toarraycc[l];
            	}// if tempccEmailId 
    		}//for CC
	           
	      //CC END
		                 
		    //BCC*******************
	         String toarraybcc[] = bcc.split(";");
	          bccemailidarr=new ArrayList();
	          String tempbccEmailId="";
	         for(int l = 0; l < toarraybcc.length; l++)
	         {
	        	 String emailIdBCC="";
	            	if(toarraybcc[l]!=null && !toarraybcc[l].equals("NA") && !toarraybcc[l].equals("0") && !toarraybcc[l].equals("")){
	            		emailIdBCC=toarraybcc[l];
	            	}
	            	if(!tempbccEmailId.equals(emailIdBCC)){
			        	 Matcher vh=p.matcher(toarraybcc[l]);
			        	 boolean hh=vh.matches();
			     			if(hh==true)
			     			{
			     				StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
			     				while (st.hasMoreTokens()){
			     					bccemailidarr.add(st.nextToken());
			     				}//while
			     			}//if h
			     			tempbccEmailId=toarraybcc[l];
	            	}
	         
	 		}//for BCC
          
            int sizeto = toemailidarr.size(); 
            // System.out.println("SIZE CC"+sizecc);
             String toAddress[] = new String[sizeto];
             if(sizeto>0){
	                 for(int m = 0; m < sizeto; m++){
	                	 toAddress[m] = new String(toemailidarr.get(m).toString());
	                 }
             }
	           String ccAddress[]=null;
	           if(ccemailidarr!=null){
	        	   	int sizecc = ccemailidarr.size();
	        	   	 ccAddress = new String[sizecc];
	        	   	 if(sizecc>0){
		        	   	 for(int m = 0; m < sizecc; m++){
			            	 ccAddress[m] = new String(ccemailidarr.get(m).toString());
			             }
	        	   	 }
	           }
	           String bccAddress[]= null;
	           if(bccemailidarr!=null){
	        	   int sizebcc = bccemailidarr.size();  
	              bccAddress = new String[sizebcc];
	              if(sizebcc>0){
		             for(int m = 0; m < sizebcc; m++){
		            	 bccAddress[m] = new String(bccemailidarr.get(m).toString());
		             }
	              }
	           }
	        email.addTo(toAddress); // dev configuration   
	        if(bccAddress!=null){
	        email.addBcc(bccAddress);
	        }
	        if(ccAddress!=null){
	        	email.addCc(ccAddress);
	        }
	        email.setFrom(mail_from);
	        email.setSubject(subject);
	        email.setHtml(messageText);
	        //email.setHtml(prop.getMyProperties("mail_messageText"));
	        String MAILFILEPATH = ftpFolder+"RET"+File.separator+fileName;
	       // File file= new File(MAILFILEPATH+File.separator+fileName);
	        File file= new File(MAILFILEPATH);
	        email.addAttachment(fileName, file);
	        
	        
	        
	        //ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


			int index=0;
			String ftp_http_FileName;
			String docName;

			while(index < arr.size())
			{
				HashMap data=(HashMap) arr.get(index);

				logger.info("arr.size():"+arr.size());

				//messageBodyPart = new MimeBodyPart();

				//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
				ftp_http_FileName="";
				fileName="";
				ftp_http_FileName=data.get("id"+index).toString();

				String MyFileNameArr[]=ftp_http_FileName.split("~");
				System.out.println("MyFileNameArr[] : "+MyFileNameArr);


				if("1".equals(MyFileNameArr[1]))
				{

					docName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					System.out.println("docName : "+docName);
					
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

					// System.out.println("filename FTP:"+fileName);
					// System.out.println("extension FTP:"+extension); 
					// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

					url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
					System.out.println("ftp url : "+url);
					
					// System.out.println("FTPurl:"+url);
				}//IF FTP FILE
				else
				{
					url = new URL(docpath_http+""+MyFileNameArr[0]);
					fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					System.out.println("fileName : "+fileName);
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());
					
				}
				 System.out.println("filename HTTP:"+MyFileNameArr[2]);
				 URLDataSource udsource = new URLDataSource(url);
				 InputStream is = udsource.getInputStream();
				
				 
				/*
				System.out.println("HTTPurl:"+url);
				logger.info("FINAL URL:"+url);
				URL urlpath = new URL(url.toString());
				URLConnection urlcon=urlpath.openConnection(); 
				System.out.println("Protocol: "+urlpath.getProtocol());  
				System.out.println("Host Name: "+urlpath.getHost());  
				System.out.println("Port Number: "+urlpath.getPort());  
				System.out.println("File Name: "+urlpath.getFile());  
				InputStream stream=urlcon.getInputStream();   
				System.out.println("Protocol: "+urlpath.getProtocol());  
				System.out.println("Host Name: "+urlpath.getHost());  
				System.out.println("Port Number: "+urlpath.getPort());  
				System.out.println("File Name: "+urlpath.getFile());  */
				

				
				//File f = new File(url.getFile());
				//System.out.println("File : "+f);
				
				// email.addFile({filename: "myfile.xlsx",url:theUrl});
				
				email.addAttachment(MyFileNameArr[2],is);
				
				
				//email.addAttachment(MyFileNameArr[2], f);
				
				
				/*messageBodyPart.setDataHandler(new DataHandler(sourcedocs));
				//messageBodyPart.setFileName(fileName);
				messageBodyPart.setFileName(MyFileNameArr[2]);

				multipart.addBodyPart(messageBodyPart);*/

				//END


				// message.setContent(multipart);
				index++;
			}//while

			logger.info("After While");
			//message.setContent(multipart);

			//END DOCS ATTACHMENT
	       
	      
	        email.addUniqueArg("MYSUBJECT", subject);
	        email.addUniqueArg("MYSENDER", mail_from);
	        email.addUniqueArg("MODID", iAuthMailBean.getModuleId()+"");
	        email.addUniqueArg("MODLETID", iAuthMailBean.getModuleLetterId()+"");
	        email.addUniqueArg("LET_TY_ID", iAuthMailBean.getLetterTypeId()+"");
	        email.addUniqueArg("CLAIMID", iAuthMailBean.getClaimId()+"");
	        email.addUniqueArg("REAUTHFLAG", iAuthMailBean.getReAuthFlag()+"");
	        email.addUniqueArg("SUPPCNT", iAuthMailBean.getSuppCounter()+"");
	        email.addUniqueArg("POLICYID", iAuthMailBean.getPolicyID()+"");
	        email.addUniqueArg("GHPL_CARDID", iAuthMailBean.getCardId());
	        email.addUniqueArg("INSUREDID", iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details start-------------------");
	        logger.info("addUniqueArg MYSUBJECT  "+subject);
	        logger.info("addUniqueArg MYSENDER   "+mail_from);
	        logger.info("addUniqueArg MODID "+iAuthMailBean.getModuleId());
	        logger.info("addUniqueArg MODLETID "+ iAuthMailBean.getModuleLetterId()+"");
	        logger.info("addUniqueArg LET_TY_ID "+ iAuthMailBean.getLetterTypeId()+"");
	        logger.info("addUniqueArg CLAIMID "+ iAuthMailBean.getClaimId()+"");
	        logger.info("addUniqueArg REAUTHFLAG "+iAuthMailBean.getReAuthFlag()+"");
	        logger.info("addUniqueArg SUPPCNT "+ iAuthMailBean.getSuppCounter()+"");
	        logger.info("addUniqueArg POLICYID "+ iAuthMailBean.getPolicyID()+"");
	        logger.info("addUniqueArg GHPL_CARDID "+ iAuthMailBean.getCardId());
	        logger.info("addUniqueArg INSUREDID "+iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details end---------------");
	        
	        
	        
	        SendGrid.Response response = sendgrid.send(email);
	        
	        file.delete();
			return response;
	    }
	
	
	
	public SendGrid.Response SendMail_SGGrid_Denial(IAuthMailBean iAuthMailBean,String subject, 
			String messageText,String fileName,String TO,String cc,String bcc,ArrayList arr,int moduleId)
	        throws MessagingException, FileNotFoundException, IOException, SendGridException
	    {
			int k=0;
			URL url=null;
			MyProperties Gdbp =new MyProperties();
			ftpFolder = Gdbp.getMyProperties("ftp_folder");
			FTPUName = Gdbp.getMyProperties("ftp_username");
			FTPPwd = Gdbp.getMyProperties("ftp_password");
			FTPHOST = Gdbp.getMyProperties("ftp_host");
			docpath=Gdbp.getMyProperties("DOCPATH_pr");
			docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");
			
			
			username = Gdbp.getMyProperties("preauth_username");
			password = Gdbp.getMyProperties("preauth_password");
			mail_from = Gdbp.getMyProperties("preauth_from");
			// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
			mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
			mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
			mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
			mail_messageText = Gdbp.getMyProperties("preauth_messageText");
			preauthBcc = Gdbp.getMyProperties("preauth_bcc");
			
			 switch(moduleId)
				{
				case 1:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_GHPL;
					break;
				case 2:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
					break;	
				case 4:
					FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
					break;
				
				
				}
	      
	        mail_from = Gdbp.getMyProperties("primary_from");
	        
	        SendGrid sendgrid = new SendGrid(Gdbp.getMyProperties("SendGridApikey"));
	        SendGrid.Email email = new SendGrid.Email();
	        Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	        // TO="prabhakar.g@isharemail.in;anand.kumar@isharemail.in;raviteja.k@isharemail.in";
	        //bcc="prabhakar.g@isharemail.in;anand.kumar@isharemail.in";
	        //cc="raviteja.k@isharemail.in;anand.kumar@isharemail.in";
	        String toarray[] = TO.split(";");
            ArrayList toemailidarr=new ArrayList();
            ArrayList ccemailidarr= null;
            ArrayList bccemailidarr= null;
            String tempEmailId="";
            for(int i = 0; i < toarray.length; i++)
            {
	            	String emailId="";
	            	if(toarray[i]!=null && !toarray[i].equals("NA") && !toarray[i].equals("0") && !toarray[i].equals("")){
	            		emailId=toarray[i];
	            	}
	            	if(!tempEmailId.equals(emailId)){
		         		Matcher g=p.matcher(toarray[i]);
		         		boolean b=g.matches();
		         		if(b==true)
		         		{
		         			StringTokenizer stto = new StringTokenizer(toarray[i],",");
		      				while (stto.hasMoreTokens()) {
		      					toemailidarr.add(stto.nextToken());
		      				}//while
		            	}
		         		tempEmailId=toarray[i];
	            	}
            }
    	 	//CC*******************
            String toarraycc[] = cc.split(";");
            ccemailidarr=new ArrayList();
            String tempccEmailId="";
            for(int l = 0; l < toarraycc.length; l++)
            {
            	
            	String emailIdCC="";
            	if(toarraycc[l]!=null && !toarraycc[l].equals("NA") && !toarraycc[l].equals("0") && !toarraycc[l].equals("")){
            		emailIdCC=toarraycc[l];
            	}
            	if(!tempccEmailId.equals(emailIdCC)){
	        		Matcher v=p.matcher(toarraycc[l]);
	        		boolean h=v.matches();
	        		if(h==true)
	        		{
	        			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	      				while (st.hasMoreTokens()) {
	      					ccemailidarr.add(st.nextToken());
	      				}//while
	        		}//if h
	        		tempccEmailId=toarraycc[l];
            	}// if tempccEmailId 
    		}//for CC
	           
	      //CC END
		                 
		    //BCC*******************
	         String toarraybcc[] = bcc.split(";");
	          bccemailidarr=new ArrayList();
	          String tempbccEmailId="";
	         for(int l = 0; l < toarraybcc.length; l++)
	         {
	        	 String emailIdBCC="";
	            	if(toarraybcc[l]!=null && !toarraybcc[l].equals("NA") && !toarraybcc[l].equals("0") && !toarraybcc[l].equals("")){
	            		emailIdBCC=toarraybcc[l];
	            	}
	            	if(!tempbccEmailId.equals(emailIdBCC)){
			        	 Matcher vh=p.matcher(toarraybcc[l]);
			        	 boolean hh=vh.matches();
			     			if(hh==true)
			     			{
			     				StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
			     				while (st.hasMoreTokens()){
			     					bccemailidarr.add(st.nextToken());
			     				}//while
			     			}//if h
			     			tempbccEmailId=toarraybcc[l];
	            	}
	         
	 		}//for BCC
          
            int sizeto = toemailidarr.size(); 
            // System.out.println("SIZE CC"+sizecc);
             String toAddress[] = new String[sizeto];
             if(sizeto>0){
	                 for(int m = 0; m < sizeto; m++){
	                	 toAddress[m] = new String(toemailidarr.get(m).toString());
	                 }
             }
	           String ccAddress[]=null;
	           if(ccemailidarr!=null){
	        	   	int sizecc = ccemailidarr.size();
	        	   	 ccAddress = new String[sizecc];
	        	   	 if(sizecc>0){
		        	   	 for(int m = 0; m < sizecc; m++){
			            	 ccAddress[m] = new String(ccemailidarr.get(m).toString());
			             }
	        	   	 }
	           }
	           String bccAddress[]= null;
	           if(bccemailidarr!=null){
	        	   int sizebcc = bccemailidarr.size();  
	              bccAddress = new String[sizebcc];
	              if(sizebcc>0){
		             for(int m = 0; m < sizebcc; m++){
		            	 bccAddress[m] = new String(bccemailidarr.get(m).toString());
		             }
	              }
	           }
	        email.addTo(toAddress); // dev configuration   
	        if(bccAddress!=null){
	        email.addBcc(bccAddress);
	        }
	        if(ccAddress!=null){
	        	email.addCc(ccAddress);
	        }
	        email.setFrom(mail_from);
	        email.setSubject(subject);
	        email.setHtml(messageText);
	        
	        //ATTCHMENTS OF DOCS UPLOADED FROM ARRAY
			int index=0;
			String ftp_http_FileName;
			String docName;

			while(index < arr.size())
			{
				HashMap data=(HashMap) arr.get(index);

				logger.info("arr.size():"+arr.size());

				//messageBodyPart = new MimeBodyPart();

				//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
				ftp_http_FileName="";
				fileName="";
				ftp_http_FileName=data.get("id"+index).toString();

				String MyFileNameArr[]=ftp_http_FileName.split("~");
				System.out.println("MyFileNameArr[] : "+MyFileNameArr);


				if("1".equals(MyFileNameArr[1]))
				{
					docName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());
					url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
					System.out.println("ftp url : "+url);
				}//IF FTP FILE
				else
				{
					url = new URL(docpath_http+""+MyFileNameArr[0]);
					fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
					System.out.println("fileName : "+fileName);
					String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());
					
				}
				 System.out.println("filename HTTP:"+MyFileNameArr[2]);
				 logger.info("url ::  "+url);
				 URLDataSource udsource = new URLDataSource(url);
				 //udsource.getURL().getPath()
				 if(udsource.getInputStream()!=null){
				 InputStream is = udsource.getInputStream();
				 email.addAttachment(MyFileNameArr[2],is);
				}
				
				index++;
			}//while

			logger.info("After While");
			//message.setContent(multipart);

			//END DOCS ATTACHMENT
	       
	      
	        email.addUniqueArg("MYSUBJECT", subject);
	        email.addUniqueArg("MYSENDER", mail_from);
	        email.addUniqueArg("MODID", iAuthMailBean.getModuleId()+"");
	        email.addUniqueArg("MODLETID", iAuthMailBean.getModuleLetterId()+"");
	        email.addUniqueArg("LET_TY_ID", iAuthMailBean.getLetterTypeId()+"");
	        email.addUniqueArg("CLAIMID", iAuthMailBean.getClaimId()+"");
	        email.addUniqueArg("REAUTHFLAG", iAuthMailBean.getReAuthFlag()+"");
	        email.addUniqueArg("SUPPCNT", iAuthMailBean.getSuppCounter()+"");
	        email.addUniqueArg("POLICYID", iAuthMailBean.getPolicyID()+"");
	        email.addUniqueArg("GHPL_CARDID", iAuthMailBean.getCardId());
	        email.addUniqueArg("INSUREDID", iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details start-------------------");
	        logger.info("addUniqueArg MYSUBJECT  "+subject);
	        logger.info("addUniqueArg MYSENDER   "+mail_from);
	        logger.info("addUniqueArg MODID "+iAuthMailBean.getModuleId());
	        logger.info("addUniqueArg MODLETID "+ iAuthMailBean.getModuleLetterId()+"");
	        logger.info("addUniqueArg LET_TY_ID "+ iAuthMailBean.getLetterTypeId()+"");
	        logger.info("addUniqueArg CLAIMID "+ iAuthMailBean.getClaimId()+"");
	        logger.info("addUniqueArg REAUTHFLAG "+iAuthMailBean.getReAuthFlag()+"");
	        logger.info("addUniqueArg SUPPCNT "+ iAuthMailBean.getSuppCounter()+"");
	        logger.info("addUniqueArg POLICYID "+ iAuthMailBean.getPolicyID()+"");
	        logger.info("addUniqueArg GHPL_CARDID "+ iAuthMailBean.getCardId());
	        logger.info("addUniqueArg INSUREDID "+iAuthMailBean.getInsuredId()+"");
	        logger.info("-------------------before calling send grid details end---------------");
	        
	        
	        
	        SendGrid.Response response = sendgrid.send(email);
			return response;
	    }
	
	
	public int preauth_InsurerMails(String subject, String messageText, String attachementname,String displayfilenme,String TO,String cc,ArrayList arr,int moduleId)
			throws MessagingException
			{

		int k=0;  
		URL url=null;


		MyProperties Gdbp = new MyProperties();
        
		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		preauthBcc = Gdbp.getMyProperties("preauth_bcc");
	
		switch(moduleId)
		{
		case 1:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_GHPL;
			ftpFolder = Gdbp.getMyProperties("ftp_folder");
			break;
		case 2:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
			break;	
		case 4:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
			break;
		}



		//ftpFolder = Gdbp.getMyProperties("ftp_folder");


		FTPHOST = Gdbp.getMyProperties("ftp_host");
		FTPUName = Gdbp.getMyProperties("ftp_username");
		FTPPwd = Gdbp.getMyProperties("ftp_password");
		docpath=Gdbp.getMyProperties("DOCPATH_pr");
		docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");

		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host",  mail_smtp_host);
			props.put("mail.smtp.port", mail_smtp_port);


			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});


			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mail_from)); 
			message.setText(messageText);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			//Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{

				InternetAddress fromAddress = new InternetAddress(mail_from);


				message.setFrom(fromAddress);

				//VALIDATING TO EMAILID


				//Validating HR MAIL ID
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();

				if(b==true)
				{

					logger.info("ATTACHMENT NAME:"+attachementname+"  - EMAILID:"+toarray[i]+"   -moduleId:"+moduleId);

					//END

					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);


					/* CC MAIL  */
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();



					for(int l = 0; l < toarraycc.length; l++)
					{

						// System.out.println((new StringBuilder("ccmail=")).append(toarraycc[l]).toString());

						//VALIDATING TO EMAILID


						//Validating HR MAIL ID
						Matcher v=p.matcher(toarraycc[l]);
						//Matcher m=p.matcher(args[0]);
						boolean h=v.matches();

						if(h==true)
						{

							logger.info("ccmail:"+toarraycc[l]);

							//END


							StringTokenizer st = new StringTokenizer(toarraycc[l],",");

							while (st.hasMoreTokens()) {

								ccemailidarr.add(st.nextToken());

							}//while


						}//if h

					}//for CC

					/* END CC EMAIL */	 


					int sizecc = ccemailidarr.size(); 

					// System.out.println("SIZE CC"+sizecc);
					logger.info("SIZE CC:"+sizecc);

					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++)
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());


					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);

					message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(preauthBcc));


					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					messageBodyPart = new MimeBodyPart();
					// DataSource source = new FileDataSource(attachementPath);
					//

					//url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+ftpFolder+"/"+attachementname); 
					//url = new URL(ftpFolder+"/"+attachementname);
					//System.out.println("FTP PATH:"+"ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+ftpFolder+"/"+attachementname);
					//URLDataSource source = new URLDataSource(url);
					 String filepath=ftpFolder+"CORP"+File.separator+attachementname;
					 FileDataSource source = new FileDataSource(filepath);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(displayfilenme);
					multipart.addBodyPart(messageBodyPart);


					// message.setContent(multipart);



					//ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


					int index=0;
					String ftp_http_FileName;
					String fileName;

					while(index < arr.size())
					{
						HashMap data=(HashMap) arr.get(index);

						logger.info("arr.size():"+arr.size());

						messageBodyPart = new MimeBodyPart();

						//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
						ftp_http_FileName="";
						fileName="";
						ftp_http_FileName=data.get("id"+index).toString();

						String MyFileNameArr[]=ftp_http_FileName.split("~");


						if("1".equals(MyFileNameArr[1]))
						{

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

							// System.out.println("filename FTP:"+fileName);
							// System.out.println("extension FTP:"+extension); 
							// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

							url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
							// System.out.println("FTPurl:"+url);
						}//IF FTP FILE
						else
						{

							url = new URL(docpath_http+""+MyFileNameArr[0]);

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());


							// System.out.println("filename HTTP:"+fileName);
							// System.out.println("extension HTTP:"+extension); 
							//System.out.println("HTTPurl:"+url);


						}


						logger.info("FINAL URL:"+url);

						URLDataSource sourcedocs = new URLDataSource(url);
						messageBodyPart.setDataHandler(new DataHandler(sourcedocs));
						//messageBodyPart.setFileName(fileName);
						messageBodyPart.setFileName(MyFileNameArr[2]);

						multipart.addBodyPart(messageBodyPart);

						//END


						// message.setContent(multipart);
						index++;
					}//while

					logger.info("After While");
					message.setContent(multipart);

					//END DOCS ATTACHMENT


					logger.info("BEFORE sending mail to Insurer approvals");

					Transport.send(message); 

					/* //DELETING XLS FILE
		                FTPClient client = new FTPClient();
		                client.connect(FTPHOST);

		                	client.login(FTPUName, FTPPwd);

		                	// Set a string with the file you want to delete

		               	String filename = ftpFolder+"/"+attachementname;

		                	// Delete file

		                	boolean exist = client.deleteFile(filename);

		                	// Notify user for deletion
		                	if (exist) {
		                    System.out.println("File '"+ filename + "' deleted...");
		               	}
		                	// Notify user that file doesn't exist
		                	else
		                	    System.out.println("File '"+ filename + "' doesn't exist...");
		                	client.disconnect();	  

		                //DELETING FILE END
					 */	                  
					k=0;

				}//if b is true
			}//for



			//DELETING XLS FILE
			FTPClient client = new FTPClient();
			client.connect(FTPHOST);

			client.login(FTPUName, FTPPwd);

			// Set a string with the file you want to delete

			String filename = ftpFolder+attachementname;

			// Delete file

			boolean exist = client.deleteFile(filename);

			// Notify user for deletion
			if (exist) {
				logger.info("File '"+ filename + "' deleted...");
			}
			// Notify user that file doesn't exist
			else
				logger.info("File '"+ filename + "' doesn't exist...");
			client.disconnect();	  

			//DELETING FILE END

		}
		catch(Exception ex)
		{
			logger.error("Error in sending mail to Insurer approvals ::"+ex);

			k=1;
		}
		return k;
			} 




	public int preauth_InsurerMails_Denial(String subject, String messageText, String attachementname,String displayfilenme,String TO,String cc,ArrayList arr,int moduleId)
			throws MessagingException
			{

		int k=0;  
		URL url=null;

		MyProperties Gdbp = new MyProperties();
		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		preauthBcc = Gdbp.getMyProperties("preauth_bcc");


		//FTP_IAUTH_PATH = Gdbp.getMyProperties("FTP_IAUTH_PATH");
		switch(moduleId)
		{
		case 1:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_GHPL;
			break;
		case 2:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_AB;
			break;	
		case 3:
			FTP_IAUTH_PATH = Constants.FTP_IAUTH_PATH_VENUS;
			break;
		}




		ftpFolder = Gdbp.getMyProperties("ftp_folder");


		FTPHOST = Gdbp.getMyProperties("ftp_host");
		FTPUName = Gdbp.getMyProperties("ftp_username");
		FTPPwd = Gdbp.getMyProperties("ftp_password");
		docpath=Gdbp.getMyProperties("DOCPATH_pr");
		docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");

		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host",  mail_smtp_host);
			props.put("mail.smtp.port", mail_smtp_port);


			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});




			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mail_from)); 
			message.setText(messageText);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			//Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{

				InternetAddress fromAddress = new InternetAddress(mail_from);


				message.setFrom(fromAddress);




				//VALIDATING TO EMAILID


				//Validating HR MAIL ID
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();

				if(b==true)
				{

					logger.info("EMAILID:"+toarray[i]);

					//END

					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);


					/* CC MAIL  */
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();



					for(int l = 0; l < toarraycc.length; l++)
					{

						logger.info("ccmail="+toarraycc[l]);
						//VALIDATING TO EMAILID


						//Validating HR MAIL ID
						Matcher v=p.matcher(toarraycc[l]);
						//Matcher m=p.matcher(args[0]);
						boolean h=v.matches();

						if(h==true)
						{

							//System.out.println("CC EMAILID:"+toarraycc[l]);

							//END


							StringTokenizer st = new StringTokenizer(toarraycc[l],",");

							while (st.hasMoreTokens()) {

								ccemailidarr.add(st.nextToken());

							}//while


						}//if h

					}//for CC

					/* END CC EMAIL */	 


					int sizecc = ccemailidarr.size(); 

					logger.info("SIZE CC"+sizecc);

					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++)
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());


					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);

					message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(preauthBcc));


					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					messageBodyPart = new MimeBodyPart();
					// DataSource source = new FileDataSource(attachementPath);
					//



					//ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


					int index=0;
					String ftp_http_FileName;
					String fileName;

					while(index < arr.size())
					{
						HashMap data=(HashMap) arr.get(index);

						messageBodyPart = new MimeBodyPart();

						//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
						ftp_http_FileName="";
						fileName="";
						ftp_http_FileName=data.get("id"+index).toString();

						String MyFileNameArr[]=ftp_http_FileName.split("~");


						if("1".equals(MyFileNameArr[1]))
						{

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

							// System.out.println("filename FTP:"+fileName);
							// System.out.println("extension FTP:"+extension); 
							// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

							url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
							// System.out.println("FTPurl:"+url);
						}//IF FTP FILE
						else
						{

							url = new URL(docpath_http+""+MyFileNameArr[0]);

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());


							// System.out.println("filename HTTP:"+fileName);
							// System.out.println("extension HTTP:"+extension); 
							//System.out.println("HTTPurl:"+url);


						}

						logger.info("url:"+url); 


						URLDataSource sourcedocs = new URLDataSource(url);
						messageBodyPart.setDataHandler(new DataHandler(sourcedocs));
						//messageBodyPart.setFileName(fileName);
						messageBodyPart.setFileName(MyFileNameArr[2]);

						multipart.addBodyPart(messageBodyPart);

						//END

						// message.setContent(multipart);

						index++;
					}//while


					message.setContent(multipart);

					//END DOCS ATTACHMENT


					 System.out.println("BEFORE SENDING MSG");

					Transport.send(message); 
					 System.out.println("AFTER SENDING MSG");
					k=0;
				

				}//if b is true
			}//for

		}
		catch(Exception ex)
		{
			// System.out.println((new StringBuilder("Error in PreAuth Denial Mail to insurer ::")).append(ex).toString());
			logger.error((new StringBuilder("Error in sendmail from SendtoInsurer for DENIAL ::")).append(ex).toString());
			k=1;
		}
		return k;
			} 


	/**
	 * The Class SMTPAuthenticator.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

	/**
	 * The Class Sec_SMTPAuthenticator.
	 */
	private class Sec_SMTPAuthenticator extends javax.mail.Authenticator {

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(sec_username, sec_password);
		}
	}



	public int preauth_InsurerMailsRet(String subject, String messageText, String attachementname,String displayfilenme,String TO,String cc,ArrayList arr,int moduleId)
			throws MessagingException
			{

		int k=0;  
		URL url=null;


		MyProperties Gdbp = new MyProperties();

		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		preauthBcc = Gdbp.getMyProperties("preauth_bcc");

		switch(moduleId)
		{
		case 4:
			FTP_IAUTH_PATH = Constants.UIA_FTP_DOC_PATH;
			break;
		case 5:
			FTP_IAUTH_PATH = Constants.NIA_FTP_DOC_PATH;
			break;	
		case 6:
			FTP_IAUTH_PATH = Constants.ORH_FTP_DOC_PATH;
			break;
		case 7:
			FTP_IAUTH_PATH = Constants.NIC_FTP_DOC_PATH;
			break;
		case 8:
			FTP_IAUTH_PATH = Constants.NAV_FTP_DOC_PATH;
			break;	
		}



		ftpFolder = Gdbp.getMyProperties("ftp_folder");


		FTPHOST = Gdbp.getMyProperties("ftp_host");
		FTPUName = Gdbp.getMyProperties("ftp_username");
		FTPPwd = Gdbp.getMyProperties("ftp_password");
		docpath=Gdbp.getMyProperties("DOCPATH_pr");
		docpath_http=Gdbp.getMyProperties("DOCPATH_frmcollection");

		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host",  mail_smtp_host);
			props.put("mail.smtp.port", mail_smtp_port);


			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});


			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mail_from)); 
			message.setText(messageText);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			//Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{

				InternetAddress fromAddress = new InternetAddress(mail_from);


				message.setFrom(fromAddress);

				//VALIDATING TO EMAILID


				//Validating HR MAIL ID
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();

				if(b==true)
				{

					logger.info("attachementname:"+attachementname+"   -EMAILID:"+toarray[i]);

					//END

					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);


					/* CC MAIL  */
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();



					for(int l = 0; l < toarraycc.length; l++)
					{

						logger.info("ccmail="+toarraycc[l]);
						//VALIDATING TO EMAILID


						//Validating HR MAIL ID
						Matcher v=p.matcher(toarraycc[l]);
						//Matcher m=p.matcher(args[0]);
						boolean h=v.matches();

						if(h==true)
						{

							logger.info("CC EMAILID:"+toarraycc[l]);

							//END


							StringTokenizer st = new StringTokenizer(toarraycc[l],",");

							while (st.hasMoreTokens()) {

								ccemailidarr.add(st.nextToken());

							}//while


						}//if h

					}//for CC

					/* END CC EMAIL */	 


					int sizecc = ccemailidarr.size(); 

					logger.info("SIZE CC"+sizecc);

					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++)
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());


					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);


					message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(preauthBcc));


					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					messageBodyPart = new MimeBodyPart();
					// DataSource source = new FileDataSource(attachementPath);
					//

					//url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+ftpFolder+"/"+attachementname); 
					/*url = new URL(ftpFolderName+"RET"+File.separator+attachementname);
					
					URLDataSource source = new URLDataSource(url);*/
					
					
					 String filepath=ftpFolder+"RET"+File.separator+attachementname;
					 FileDataSource source = new FileDataSource(filepath);
					

					//
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(displayfilenme);
					multipart.addBodyPart(messageBodyPart);
					// message.setContent(multipart);

					logger.info("FINAL URL XLS RETAIL:"+url); 

					//ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


					int index=0;
					String ftp_http_FileName;
					String fileName;

					while(index < arr.size())
					{
						HashMap data=(HashMap) arr.get(index);

						logger.info("arr.size():"+arr.size());

						messageBodyPart = new MimeBodyPart();

						//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
						ftp_http_FileName="";
						fileName="";
						ftp_http_FileName=data.get("id"+index).toString();

						String MyFileNameArr[]=ftp_http_FileName.split("~");


						if("1".equals(MyFileNameArr[1]))
						{

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

							// System.out.println("filename FTP:"+fileName);
							// System.out.println("extension FTP:"+extension); 
							// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

							url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
							// System.out.println("FTPurl:"+url);
						}//IF FTP FILE

						logger.info("FINAL URL RETAIL:"+url);

						URLDataSource sourcedocs = new URLDataSource(url);
						messageBodyPart.setDataHandler(new DataHandler(sourcedocs));
						//messageBodyPart.setFileName(fileName);
						messageBodyPart.setFileName(MyFileNameArr[2]);

						multipart.addBodyPart(messageBodyPart);

						//END


						// message.setContent(multipart);
						index++;
					}//while

					logger.info("After While");
					message.setContent(multipart);

					//END DOCS ATTACHMENT


					logger.info("BEFORE sending mail to Insurer approvals");

					Transport.send(message); 

					k=0;

				}//if b is true
			}//for



			//DELETING XLS FILE
			FTPClient client = new FTPClient();
			client.connect(FTPHOST);

			client.login(FTPUName, FTPPwd);

			// Set a string with the file you want to delete

			String filename = ftpFolder+"/"+attachementname;

			// Delete file

			boolean exist = client.deleteFile(filename);

			// Notify user for deletion
			if (exist) {
				logger.info("File '"+ filename + "' deleted...");
			}
			// Notify user that file doesn't exist
			else
				logger.info("File '"+ filename + "' doesn't exist...");
			client.disconnect();	  

			//DELETING FILE END


		}
		catch(Exception ex)
		{
			// System.out.println((new StringBuilder("Error in sending mail to Insurer approvals ::")).append(ex).toString());
			logger.error((new StringBuilder("Error in sendmail from SendtoInsurer for APPROVAL RETAIL ::")).append(ex).toString());
			k=1;
			System.out.println(ex);
		}
		return k;
			}

	/* public static void main(String z[])
    {
        try
        {
            String str[] = {
                "E:\\icare_1\\web\\Invoice_out_PDF\\Ack_Letter_596.html"
            };
            System.out.println("mail sent");
        }
        catch(Exception ex)
        {
            System.out.println((new StringBuilder("Error  ::")).append(ex).toString());
            ex.printStackTrace();
        }
    }*/
}