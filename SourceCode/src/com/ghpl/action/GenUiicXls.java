package com.ghpl.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;

/**
 * Servlet implementation class GenUiicXls
 */
@WebServlet("/GenUiicXls")
public class GenUiicXls extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger logger =Logger.getLogger("GenUiicXls");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenUiicXls() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
		String clmid="0";
		int rauthflg=0;
		String insurerFlag=null;
		int moduleId=0;
		  
		try{  
			//RETRIEVE POST FORM DATA
			clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
			rauthflg=Integer.parseInt(request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0");
			insurerFlag=request.getParameter("INSURER_FLAG")!=null?request.getParameter("INSURER_FLAG"):"0";
			moduleId=Integer.parseInt(request.getParameter("moduleId")!=null?request.getParameter("moduleId"):"0");
			
			if( ("UIIC".equals(insurerFlag)))
			{
				String insuredname = "";
				String ptname = "";
				String age = "";
				String sex = "";
				String relation = "";
				String balsi = "";
				String policyno = "";
				String claimid = "";
				String si = "";
				String poltypegmc = "";
				String polinceptiondt = "";
				String hospname = "";
				String city = "";
				String state = "";
				String diagnosis = "";
				String mgmtsrplemr = "";
				String doa = "";
				String totaldurofstay = "";
				String roomtype = "";
				String roomrenticyprday = "";
				String roomrentelg = "";
				String noofdaysinicuroom = "";
				String gipsappnhosp = "";
				String gipsappntpa = "";
				String gipsappnreson = "";
				String roomrenthosp = "";
				String roomrenttpa = "";
				String roomrentded = "";
				String profchghosp = "";
				String profchgtpa = "";
				String profchgded = "";
				String phrhosp = "";
				String phrtpa = "";
				String phrded = "";
				String othosp = "";
				String ottpa = "";
				String otdedu = "";
				String labinvhosp = "";
				String labinvtpa = "";
				String labinvded = "";
				String conchrhosp = "";
				String conchrtpa = "";
				String conchrded = "";
				String othershosp = "";
				String otherstpa = "";
				String othersdedu = "";
				String totalhosp = "";
				String totaltpa = "";
				String totaldedu = "";
				String disessel = "";
				String expnsel = "";
				String headerssel = "";
				String diagcostsel = "";
				//String userid="";
				String drname="";
				String tpa="";
				String ppnntwrkhosp="";
				String cathosptertplus="";
				String policydesc="";
			 
			 insuredname=request.getParameter("insuredname")!=null?request.getParameter("insuredname"):"NA";
			 ptname=request.getParameter("ptname")!=null?request.getParameter("ptname"):"NA";
			 age=request.getParameter("age")!=null?request.getParameter("age"):"NA";
			 sex=request.getParameter("sex")!=null?request.getParameter("sex"):"NA";
			 relation=request.getParameter("relation")!=null?request.getParameter("relation"):"NA";
			 balsi=request.getParameter("balsi")!=null?request.getParameter("balsi"):"NA";
			 policyno=request.getParameter("policyno")!=null?request.getParameter("policyno"):"NA";
			 claimid=request.getParameter("claimid")!=null?request.getParameter("claimid"):"NA";
			 si=request.getParameter("si")!=null?request.getParameter("si"):"NA";
			 poltypegmc=request.getParameter("poltypegmc")!=null?request.getParameter("poltypegmc"):"NA";
			 polinceptiondt=request.getParameter("polinceptiondt")!=null?request.getParameter("polinceptiondt"):"NA";
			 hospname=request.getParameter("hospname")!=null?request.getParameter("hospname"):"NA";
			 city=request.getParameter("city")!=null?request.getParameter("city"):"NA";
			 state=request.getParameter("state")!=null?request.getParameter("state"):"NA";
			 diagnosis=request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
			 mgmtsrplemr=request.getParameter("mgmtsrplemr")!=null?request.getParameter("mgmtsrplemr"):"NA";
			 doa=request.getParameter("doa")!=null?request.getParameter("doa"):"NA";
			 totaldurofstay=request.getParameter("totaldurofstay")!=null?request.getParameter("totaldurofstay"):"NA";
			 roomtype=request.getParameter("roomtype")!=null?request.getParameter("roomtype"):"NA";
			 roomrenticyprday=request.getParameter("roomrenticyprday")!=null?request.getParameter("roomrenticyprday"):"NA";
			 roomrentelg=request.getParameter("roomrentelg")!=null?request.getParameter("roomrentelg"):"NA";
			 noofdaysinicuroom=request.getParameter("noofdaysinicuroom")!=null?request.getParameter("noofdaysinicuroom"):"NA";
			 gipsappnhosp=request.getParameter("gipsappnhosp")!=null?request.getParameter("gipsappnhosp"):"NA";
			 gipsappntpa=request.getParameter("gipsappntpa")!=null?request.getParameter("gipsappntpa"):"NA";
			 gipsappnreson=request.getParameter("gipsappnreson")!=null?request.getParameter("gipsappnreson"):"NA";
			 roomrenthosp=request.getParameter("roomrenthosp")!=null?request.getParameter("roomrenthosp"):"NA";
			 roomrenttpa=request.getParameter("roomrenttpa")!=null?request.getParameter("roomrenttpa"):"NA";
			 roomrentded=request.getParameter("roomrentded")!=null?request.getParameter("roomrentded"):"NA";
			 profchghosp=request.getParameter("profchghosp")!=null?request.getParameter("profchghosp"):"NA";
			 profchgtpa=request.getParameter("profchgtpa")!=null?request.getParameter("profchgtpa"):"NA";
			 profchgded=request.getParameter("profchgded")!=null?request.getParameter("profchgded"):"NA";
			 phrhosp=request.getParameter("phrhosp")!=null?request.getParameter("phrhosp"):"NA";
			 phrtpa=request.getParameter("phrtpa")!=null?request.getParameter("phrtpa"):"NA";
			 phrded=request.getParameter("phrded")!=null?request.getParameter("phrded"):"NA";
			 othosp=request.getParameter("othosp")!=null?request.getParameter("othosp"):"NA";
			 ottpa=request.getParameter("ottpa")!=null?request.getParameter("ottpa"):"NA";
			 otdedu=request.getParameter("otdedu")!=null?request.getParameter("otdedu"):"NA";
			 labinvhosp=request.getParameter("labinvhosp")!=null?request.getParameter("labinvhosp"):"NA";
			 labinvtpa=request.getParameter("labinvtpa")!=null?request.getParameter("labinvtpa"):"NA";
			 labinvded=request.getParameter("labinvded")!=null?request.getParameter("labinvded"):"NA";
			 conchrhosp=request.getParameter("conchrhosp")!=null?request.getParameter("conchrhosp"):"NA";
			 conchrtpa=request.getParameter("conchrtpa")!=null?request.getParameter("conchrtpa"):"NA";
			 conchrded=request.getParameter("conchrded")!=null?request.getParameter("conchrded"):"NA";
			 othershosp=request.getParameter("othershosp")!=null?request.getParameter("othershosp"):"NA";
			 otherstpa=request.getParameter("otherstpa")!=null?request.getParameter("otherstpa"):"NA";
			 othersdedu=request.getParameter("othersdedu")!=null?request.getParameter("othersdedu"):"NA";
			 totalhosp=request.getParameter("totalhosp")!=null?request.getParameter("totalhosp"):"NA";
			 totaltpa=request.getParameter("totaltpa")!=null?request.getParameter("totaltpa"):"NA";
			 totaldedu=request.getParameter("totaldedu")!=null?request.getParameter("totaldedu"):"NA";
			 disessel=request.getParameter("disessel")!=null?request.getParameter("disessel"):"NA";
			 expnsel=request.getParameter("expnsel")!=null?request.getParameter("expnsel"):"NA";
			 headerssel=request.getParameter("headerssel")!=null?request.getParameter("headerssel"):"NA";
			 diagcostsel=request.getParameter("diagcostsel")!=null?request.getParameter("diagcostsel"):"NA";
			 drname=request.getParameter("drname")!=null?request.getParameter("drname"):"NA";
			 tpa=request.getParameter("tpa")!=null?request.getParameter("tpa"):"NA";
			 //userid=request.getParameter("userid")!=null?request.getParameter("userid"):"NA";
			 policydesc=request.getParameter("poldesc")!=null?request.getParameter("poldesc"):"NA";
			 
			 ppnntwrkhosp=request.getParameter("ppnntwrkhosp")!=null?request.getParameter("ppnntwrkhosp"):"NA";
			 cathosptertplus=request.getParameter("cathosptertplus")!=null?request.getParameter("cathosptertplus"):"NA";
			 
			 
			 FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/UIIC.xls"));

             HSSFWorkbook workbook = new HSSFWorkbook(fis);
             
        	  CellStyle style = workbook.createCellStyle();
        	    style.setAlignment(CellStyle.ALIGN_JUSTIFY);
     	     
                HSSFSheet sheet = workbook.getSheetAt(0);
                
               
                Cell cell = null;
              
            //Update the value of cell
              cell = sheet.getRow(5).getCell(5);
              cell.setCellValue(insuredname);//Insured name
              cell = sheet.getRow(5).getCell(9);
              cell.setCellValue(ptname);//patient name
              cell = sheet.getRow(5).getCell(13);
              cell.setCellValue(age+"/"+sex);//AGE/SEX
              cell = sheet.getRow(5).getCell(16);
              cell.setCellValue(relation);//Relation with insured
              cell = sheet.getRow(5).getCell(20);
              cell.setCellValue(balsi);//balance sum insured
              
              cell = sheet.getRow(9).getCell(5);
              cell.setCellValue(policyno);//Policy no
              cell = sheet.getRow(9).getCell(9);
              cell.setCellValue(claimid);//CLAIM ID
              cell = sheet.getRow(9).getCell(13);
              cell.setCellValue(si);//Sum Insured
              cell = sheet.getRow(9).getCell(16);
              cell.setCellValue(poltypegmc);//POLICY TYPE/INDIVIDUAL OR GMC
              cell = sheet.getRow(9).getCell(20);
              cell.setCellValue(polinceptiondt);//DATE OF INCEPTION OF POLICY FIRST TAKEN
            
              cell = sheet.getRow(11).getCell(5);
              cell.setCellValue(policydesc);//policydesc
              cell.setCellStyle(style);

              cell = sheet.getRow(18).getCell(5);
              cell.setCellValue(hospname);//Hospital name
              cell = sheet.getRow(18).getCell(10);
              cell.setCellValue(city+"/"+state);//City/state
              cell = sheet.getRow(18).getCell(14);
              cell.setCellValue(ppnntwrkhosp);//PPN/NETWORK HOSPITALS
              cell = sheet.getRow(18).getCell(19);
              cell.setCellValue(cathosptertplus);//CATEGORY OF HOSPITAL /TERTIARY OR TERT PLUS
           		   
           		   
              cell = sheet.getRow(21).getCell(5);
              cell.setCellValue(diagnosis);//Hospital name
              cell = sheet.getRow(21).getCell(12);
              cell.setCellValue(mgmtsrplemr);//MANAGEMENT SURGICAL OR MEDICAL/PLANNED OR EMERGENCY
              cell = sheet.getRow(21).getCell(18);
              cell.setCellValue(doa);//DATE OF ADMISSION
              cell = sheet.getRow(21).getCell(21);
              cell.setCellValue(totaldurofstay);//EXPECTED DURATION OF STAY
           		   
              		   
              cell = sheet.getRow(26).getCell(5);
              cell.setCellValue(roomtype);//ROOM TYPE/ SINGLE,SEMIPRIVATE, GENERAL ETC
              cell = sheet.getRow(26).getCell(12);
              cell.setCellValue(roomrenticyprday);//ROOM RENT/ ICU RENT PER DAY
              cell = sheet.getRow(26).getCell(18);
              cell.setCellValue(roomrentelg);//ROOM RENT ELIGIBILITY
              cell = sheet.getRow(26).getCell(21);
              cell.setCellValue(noofdaysinicuroom);//NO OF DAYS STAYED IN ROOM AND IN ICU
              
              cell = sheet.getRow(31).getCell(11);
              cell.setCellValue(gipsappnhosp);//GIPSA AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(31).getCell(16);
              cell.setCellValue(gipsappntpa);//GIPSA AMOUNT RECOMMENDED BY TPA
              cell = sheet.getRow(31).getCell(21);
              cell.setCellValue(gipsappnreson);//GIPSA REASONS FOR DEDUCTION
            
              
              cell = sheet.getRow(33).getCell(11);
              cell.setCellValue(roomrenthosp); //ROOM RENT/ICU RENT AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(33).getCell(15);
              cell.setCellValue(roomrenttpa);//ROOM RENT/ICU RENT  AMOUNT RECOMMENDED BY TPA
              cell=sheet.getRow(33).getCell(20);
              cell.setCellValue(roomrentded);//ROOM RENT/ICU RENT  REASONS FOR DEDUCTION
            
              
              cell = sheet.getRow(35).getCell(11);
              cell.setCellValue(profchghosp); //PROCEDURE(PROFESSIONAL) CHARGES AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(35).getCell(15);
              cell.setCellValue(profchgtpa);//PROCEDURE(PROFESSIONAL) CHARGES AMOUNT RECOMMENDED BY TPA
              cell=sheet.getRow(35).getCell(20);
              cell.setCellValue(profchgded);//PROCEDURE(PROFESSIONAL) CHARGES REASONS FOR DEDUCTION 
            
               
              cell = sheet.getRow(37).getCell(11);
              cell.setCellValue(phrhosp); //PHARMACY AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(37).getCell(15);
              cell.setCellValue(phrtpa);//PHARMACY AMOUNT RECOMMENDED BY TPA
              cell=sheet.getRow(37).getCell(20);
              cell.setCellValue(phrded);//PHARMACY REASONS FOR DEDUCTION
            
              
              cell = sheet.getRow(39).getCell(11);
              cell.setCellValue(othosp); //OT ANATHESIA AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(39).getCell(15);
              cell.setCellValue(ottpa);//OT ANATHESIA AMOUNT AMOUNT RECOMMENDED BY TPA
              cell=sheet.getRow(39).getCell(20);
              cell.setCellValue(otdedu);//OT ANATHESIA AMOUNT REASONS FOR DEDUCTION
            
              
              cell = sheet.getRow(41).getCell(11);
              cell.setCellValue(labinvhosp); //LAB INVST AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(41).getCell(15);
              cell.setCellValue(labinvtpa);//LAB INVST AMOUNT RECOMMENDED BY TPA
              cell=sheet.getRow(41).getCell(20);
              cell.setCellValue(labinvded);//LAB INVST AMOUNT REASONS FOR DEDUCTION
            
              
              cell = sheet.getRow(43).getCell(11);
              cell.setCellValue(conchrhosp); //CONSULTATION CHARGES AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(43).getCell(16);
              cell.setCellValue(conchrtpa);//CONSULTATION CHARGES AMOUNT RECOMMENDED BY TPA
              cell = sheet.getRow(43).getCell(21);
              cell.setCellValue(conchrded);//CONSULTATION REASONS FOR DEDUCTION
                
            
              cell = sheet.getRow(45).getCell(11);
              cell.setCellValue(othershosp); //OTHERS AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(45).getCell(15);
              cell.setCellValue(otherstpa);//OTHERS AMOUNT RECOMMENDED BY TPA
              cell = sheet.getRow(45).getCell(19);
              cell.setCellValue(othersdedu);//OTHERS REASONS FOR DEDUCTION
                   
              cell = sheet.getRow(47).getCell(11);
              cell.setCellValue(totalhosp); //TOTAL AMOUNT REQUESTED BY HOSPITAL
              cell = sheet.getRow(47).getCell(15);
              cell.setCellValue(totaltpa); //TOTAL AMOUNT RECOMMENDED BY TPA
              cell = sheet.getRow(47).getCell(19);
              cell.setCellValue(totaldedu); //TOTAL REASONS FOR DEDUCTION
                 
              cell = sheet.getRow(50).getCell(20);
              cell.setCellValue(disessel); 
             
              cell = sheet.getRow(52).getCell(20);
              cell.setCellValue(expnsel); 
              
              cell = sheet.getRow(54).getCell(20);
              cell.setCellValue(headerssel); 
              
              cell = sheet.getRow(56).getCell(20);
              cell.setCellValue(diagcostsel); 
              cell = sheet.getRow(58).getCell(20);
              cell.setCellValue(drname+"/"+tpa); 
              
              response.setContentType("application/vnd.ms-excel");
              response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
              // ...
              // Now populate workbook the usual way.
              // ...
              workbook.write(response.getOutputStream()); // Write workbook to response.
             // workbook.close();
              workbook=null;
			} //IF UIIC 
			else if( ("BHAXA".equals(insurerFlag)))
			{

				 String patientName = "";
				 String empname = "";
				 String empid = "";
				 String corpname = "";
				 String sumInsured = "";
				 String reqamt = "";
				 String apramt = "";
				 String poldesc = "";
				 
				 String policyNumber="";
				 String balanceSi="";
				 String claimType="";
				 String policyPeriod="";
				 String initialInterimFinal="";
				 String hospitalName="";
				 String doaDod="";
				 String diagnosis="";
				 String treatment=""; 
				 String tariff="";
				 String anyNegotiations="";
				 String tpaRecommendations="";
				 
				//RETRIEVE POST FORM DATA
				clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
				rauthflg=Integer.parseInt(request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0");
				insurerFlag=request.getParameter("INSURER_FLAG")!=null?request.getParameter("INSURER_FLAG"):"0";
				moduleId=Integer.parseInt(request.getParameter("moduleId")!=null?request.getParameter("moduleId"):"0");
				
				patientName=request.getParameter("patientname")!=null?request.getParameter("patientname"):"NA";
				sumInsured=request.getParameter("suminsured")!=null?request.getParameter("suminsured"):"0";
				reqamt=request.getParameter("estimate")!=null?request.getParameter("estimate"):"0";
				
				policyNumber=request.getParameter("policynumber")!=null?request.getParameter("policynumber"):"NA";
				balanceSi=request.getParameter("balancesi")!=null?request.getParameter("balancesi"):"0";
				claimType=request.getParameter("claimtype")!=null?request.getParameter("claimtype"):"NA";
				policyPeriod=request.getParameter("policyperiod")!=null?request.getParameter("policyperiod"):"NA";
				initialInterimFinal=request.getParameter("initial")!=null?request.getParameter("initial"):"NA";
				hospitalName=request.getParameter("hospital")!=null?request.getParameter("hospital"):"NA";
				doaDod=request.getParameter("doadod")!=null?request.getParameter("doadod"):"NA";
				diagnosis=request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
				treatment=request.getParameter("treatment")!=null?request.getParameter("treatment"):"NA";
				tariff=request.getParameter("tariff")!=null?request.getParameter("tariff"):"NA";
				anyNegotiations=request.getParameter("negotiations")!=null?request.getParameter("negotiations"):"NA";
				tpaRecommendations=request.getParameter("tparecomm")!=null?request.getParameter("tparecomm"):"NA";
				
				 
				 FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/BHARATIAXA.xls"));
                 
			        HSSFWorkbook workbook = new HSSFWorkbook(fis);
			 	      CellStyle style = workbook.createCellStyle();
				      style.setAlignment(CellStyle.ALIGN_JUSTIFY);
				    
				     
			         HSSFSheet sheet = workbook.getSheetAt(0);
			     
			        
			         Cell cell = null;
			       //Update the value of cell
			      
			         cell = sheet.getRow(0).getCell(0);
			         cell.setCellValue("Patient Name");
			         cell = sheet.getRow(1).getCell(0);
			         cell.setCellValue("Corporate /policy number");
			         cell = sheet.getRow(2).getCell(0);
			         cell.setCellValue("Claim No/Preauth NO");
			         cell = sheet.getRow(3).getCell(0);
			         cell.setCellValue("Sum insured");
			         cell = sheet.getRow(4).getCell(0);
			         cell.setCellValue("Balance SI");
			         cell = sheet.getRow(5).getCell(0);
			         cell.setCellValue("Claim type");
			         cell = sheet.getRow(6).getCell(0);
			         cell.setCellValue("Policy period");
			         cell = sheet.getRow(7).getCell(0);
			         cell.setCellValue("Initial/interim/final");
			         cell = sheet.getRow(8).getCell(0);
			         cell.setCellValue("Hospital");
			         cell = sheet.getRow(9).getCell(0);
			         cell.setCellValue("DOA & DOD(probable)");
			         cell = sheet.getRow(10).getCell(0);
			         cell.setCellValue("Diagnosis");
			         cell = sheet.getRow(11).getCell(0);
			         cell.setCellValue("Treatment");
			         cell = sheet.getRow(12).getCell(0);
			         cell.setCellValue("Tariff- open or agreed");
			         cell = sheet.getRow(13).getCell(0);
			         cell.setCellValue("Estimate");
			         cell = sheet.getRow(14).getCell(0);
			         cell.setCellValue("Any negotiation/case management");
			         cell = sheet.getRow(15).getCell(0);
			         cell.setCellValue("TPA recommendations");
			        
			        cell = sheet.getRow(0).getCell(1);
			        cell.setCellValue(patientName);//CARDID
			        cell = sheet.getRow(1).getCell(1);
			        cell.setCellValue(policyNumber);//CARDID
			        cell = sheet.getRow(2).getCell(1);
			        cell.setCellValue(clmid);//CARDID

			        cell = sheet.getRow(3).getCell(1);
			        cell.setCellValue(sumInsured);//CARDID

			        cell = sheet.getRow(4).getCell(1);
			        cell.setCellValue(balanceSi);//CARDID

			        cell = sheet.getRow(5).getCell(1);
			        cell.setCellValue(claimType);//CARDID

			        cell = sheet.getRow(6).getCell(1);
			        cell.setCellValue(policyPeriod);//CARDID
			        
			        cell = sheet.getRow(7).getCell(1);
			        cell.setCellValue(initialInterimFinal);
			        cell.setCellStyle(style);
			        
			        cell = sheet.getRow(8).getCell(1);
			        cell.setCellValue(hospitalName);//CARDID
			         
			        cell = sheet.getRow(9).getCell(1);
			        cell.setCellValue(doaDod);//CARDID
			        
			        cell = sheet.getRow(10).getCell(1);
			        cell.setCellValue(diagnosis);//CARDID

			        cell = sheet.getRow(11).getCell(1);
			        cell.setCellValue(treatment);//CARDID

			        cell = sheet.getRow(12).getCell(1);
			        cell.setCellValue(tariff);//CARDID

			        cell = sheet.getRow(13).getCell(1);
			        cell.setCellValue(reqamt);//CARDID

			        cell = sheet.getRow(14).getCell(1);
			        cell.setCellValue(anyNegotiations);//CARDID 
			        
			        cell = sheet.getRow(15).getCell(1);
			        cell.setCellValue(tpaRecommendations);
			       cell.setCellStyle(style);
			       
			       response.setContentType("application/vnd.ms-excel");
		              response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
		              // ...
		              // Now populate workbook the usual way.
		              // ...
		              workbook.write(response.getOutputStream()); // Write workbook to response.
		             // workbook.close();
		              workbook=null;
				
			}//BHAXA
			else if( ("RGIL".equals(insurerFlag)))
			{

				 String CASE_TYPE = "";
				 String CLAIM_TYPE = "";
				 String PREAUTH_CASE_ID = "";
				 String CLAIM_NO_URL_NO = "";
				 String PATIENT_NAME = "";
				 String AGE = "";
				 String HOSPITALNAME = "";
				 String HOSPITAL_TYPE = "";
				 String DIAGNOSIS = "";
				 String CORPORATE_NAME = "";
				 String POLICY_NO = "";
				 String SUM_INSURED = "";
				 String BALANCESUMINSURED = "";
				 String LINE_OF_TREATMENT = "";
				 String REMARKS = "";
				 String REQUESTED_AMOUNT = "";
				 String INITIAL_AMOUNT_PAID = "";
				 String PAYABLE_AMOUNT = "";
				 String CORPORATE_BUFFER = "";
				 
				//RETRIEVE POST FORM DATA
				clmid=request.getParameter("clmid")!=null?request.getParameter("clmid"):"0";
				rauthflg=Integer.parseInt(request.getParameter("rauthflg")!=null?request.getParameter("rauthflg"):"0");
				insurerFlag=request.getParameter("INSURER_FLAG")!=null?request.getParameter("INSURER_FLAG"):"0";
				moduleId=Integer.parseInt(request.getParameter("moduleId")!=null?request.getParameter("moduleId"):"0");
				
				
				 CASE_TYPE = request.getParameter("casetype")!=null?request.getParameter("casetype"):"NA";
				 CLAIM_TYPE = request.getParameter("claimtype")!=null?request.getParameter("claimtype"):"NA";
				 PREAUTH_CASE_ID = request.getParameter("caseid")!=null?request.getParameter("caseid"):"0";
				 CLAIM_NO_URL_NO = request.getParameter("claimno")!=null?request.getParameter("claimno"):"0";
				 PATIENT_NAME = request.getParameter("patientname")!=null?request.getParameter("patientname"):"NA";
				 AGE = request.getParameter("age")!=null?request.getParameter("age"):"0";
				 HOSPITALNAME = request.getParameter("hospname")!=null?request.getParameter("hospname"):"NA";
				 HOSPITAL_TYPE = request.getParameter("hosptype")!=null?request.getParameter("hosptype"):"NA";
				 DIAGNOSIS = request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
				 CORPORATE_NAME = request.getParameter("corpname")!=null?request.getParameter("corpname"):"NA";
				 POLICY_NO = request.getParameter("policyno")!=null?request.getParameter("policyno"):"NA";
				 SUM_INSURED = request.getParameter("suminsured")!=null?request.getParameter("suminsured"):"0";
				 BALANCESUMINSURED = request.getParameter("balsuminsured")!=null?request.getParameter("balsuminsured"):"0";
				 LINE_OF_TREATMENT = request.getParameter("lineoftretment")!=null?request.getParameter("lineoftretment"):"NA";
				 REMARKS = request.getParameter("remarks")!=null?request.getParameter("remarks"):"NA";
				 REQUESTED_AMOUNT = request.getParameter("claimedamt")!=null?request.getParameter("claimedamt"):"0";
				 INITIAL_AMOUNT_PAID = request.getParameter("iniappramt")!=null?request.getParameter("iniappramt"):"0";
				 PAYABLE_AMOUNT = request.getParameter("payableamt")!=null?request.getParameter("payableamt"):"0";
				 CORPORATE_BUFFER = request.getParameter("corpbufferamt")!=null?request.getParameter("corpbufferamt"):"0";
				
				 
				 FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/RGIL.xls"));

		          HSSFWorkbook workbook = new HSSFWorkbook(fis);
		   	    CellStyle style = workbook.createCellStyle();
		 	    
		 	        style.setAlignment(CellStyle.ALIGN_LEFT);
			     
		          HSSFSheet sheet = workbook.getSheetAt(0);
		          
		          
		           Cell cell = null;
		           
		           cell = sheet.getRow(0).getCell(0);
		           cell.setCellValue("Case Type");
		           
		           cell = sheet.getRow(1).getCell(0);
		           cell.setCellValue("claim type");
		           
		           cell = sheet.getRow(2).getCell(0);
		           cell.setCellValue("Pre-auth/ Case ID");
		           
		           cell = sheet.getRow(3).getCell(0);
		           cell.setCellValue("Claim No/URL No");
		          
		           cell = sheet.getRow(4).getCell(0);
		           cell.setCellValue("Patient Name");
		           
		           cell = sheet.getRow(5).getCell(0);
		           cell.setCellValue(" Age");
		           
		           cell = sheet.getRow(6).getCell(0);
		           cell.setCellValue("Hospital Name");
		           
		           cell = sheet.getRow(7).getCell(0);
		           cell.setCellValue(" Hospital type");
		           
		           cell = sheet.getRow(8).getCell(0);
		           cell.setCellValue("Diagnosis");
		           
		           cell = sheet.getRow(9).getCell(0);
		           cell.setCellValue("Corporate Name");
		           
		           cell = sheet.getRow(10).getCell(0);
		           cell.setCellValue("Policy No");

		           cell = sheet.getRow(11).getCell(0);
		           cell.setCellValue("Sum Insured");
		           
		           cell = sheet.getRow(12).getCell(0);
		           cell.setCellValue("Bal Sum Insured");
		           
		           cell = sheet.getRow(13).getCell(0);
		           cell.setCellValue("Line of treatment");

		           cell = sheet.getRow(14).getCell(0);
		           cell.setCellValue("Estimated Amount/Claimed Amount");
		           
		           cell = sheet.getRow(15).getCell(0);
		           cell.setCellValue("initial amount approved");
		          
		           cell = sheet.getRow(16).getCell(0);
		           cell.setCellValue("Payable Amount");
		           
		           cell = sheet.getRow(17).getCell(0);
		           cell.setCellValue("Corporate Buffer Amount");
		            
		           cell = sheet.getRow(18).getCell(0);
		           cell.setCellValue("Remarks");

		           
		        //Update the value of cell
		         cell = sheet.getRow(0).getCell(1);
		         cell.setCellValue(CASE_TYPE); 
		      
		         cell = sheet.getRow(1).getCell(1);
		         cell.setCellValue(CLAIM_TYPE);
		        

		         cell = sheet.getRow(2).getCell(1);
		         cell.setCellValue(PREAUTH_CASE_ID);

		         cell = sheet.getRow(3).getCell(1);
		         cell.setCellValue(CLAIM_NO_URL_NO);

		         cell = sheet.getRow(4).getCell(1);
		         cell.setCellValue(PATIENT_NAME);

		         cell = sheet.getRow(5).getCell(1);
		         cell.setCellValue(AGE);

		         
		         cell = sheet.getRow(6).getCell(1);
		         cell.setCellValue(HOSPITALNAME);
		        
		         cell = sheet.getRow(7).getCell(1);
		         cell.setCellValue(HOSPITAL_TYPE);
		         
		         cell = sheet.getRow(8).getCell(1);
		         cell.setCellValue(DIAGNOSIS);
		         
		         cell = sheet.getRow(9).getCell(1);
		         cell.setCellValue(CORPORATE_NAME);
		          
		         cell = sheet.getRow(10).getCell(1);
		         cell.setCellValue(POLICY_NO);
		         
		         cell = sheet.getRow(11).getCell(1);
		         cell.setCellValue(SUM_INSURED);
		         
		         cell = sheet.getRow(12).getCell(1);
		         cell.setCellValue(BALANCESUMINSURED);
		         
		         cell = sheet.getRow(13).getCell(1);
		         cell.setCellValue(LINE_OF_TREATMENT);
		         
		         cell = sheet.getRow(14).getCell(1);
		         cell.setCellValue(REQUESTED_AMOUNT);
		         
		         cell = sheet.getRow(15).getCell(1);
		         cell.setCellValue(INITIAL_AMOUNT_PAID);

		         
		         cell = sheet.getRow(16).getCell(1);
		         cell.setCellValue(PAYABLE_AMOUNT);
		         
		         cell = sheet.getRow(17).getCell(1);
		         cell.setCellValue(CORPORATE_BUFFER);
		         
		         cell = sheet.getRow(18).getCell(1);
		         cell.setCellValue(REMARKS); 
			       
			       response.setContentType("application/vnd.ms-excel");
		              response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
		              // ...
		              // Now populate workbook the usual way.
		              // ...
		              workbook.write(response.getOutputStream()); // Write workbook to response.
		             // workbook.close();
		              workbook=null;
				
			}//RGIL 
			else if( ("ITGI".equals(insurerFlag)))
			{
				String cardid = "";
				String hospname = "";
				String hospmobileno = "";
				String benfname = "";
				String admdate = "";
				String medsurgdaycare = "";
				String roomtype = "";
				String grname = "";
				String city = "";
				String drmobileno = "";
				String telebenf = "";
				String icuadmnroom = "";
				String lenofstay = "";
				String roomicutariff = "";
				String consultchrgs = "";
				String surgerychrgs = "";
				String otchrgs = "";
				String implntscosts = "";
				String alamtaskdbynsp = "";
				String prenegotedtariff = "";
				String rmonursing = "";
				String medcost = "";
				String invstcost = "";
				String anastchrgs = "";
				String anyotherexp = "";
				String recmalamtbytpa = "";
				String tpafinalbillestm = "";
				String complntswithdurn = "";
				String clinicalfindngs = "";
				String testdone = "";
				String diagnosis = "";
				String meddetails = "";
				String surgeryrx = "";
				
				cardid=request.getParameter("cardid")!=null?request.getParameter("cardid"):"NA";
				 hospname=request.getParameter("hospname")!=null?request.getParameter("hospname"):"NA";
				 hospmobileno=request.getParameter("hospmobileno")!=null?request.getParameter("hospmobileno"):"NA";
				 benfname=request.getParameter("benfname")!=null?request.getParameter("benfname"):"NA";
				 admdate=request.getParameter("admdate")!=null?request.getParameter("admdate"):"NA";
				 medsurgdaycare=request.getParameter("medsurgdaycare")!=null?request.getParameter("medsurgdaycare"):"NA";
				 roomtype=request.getParameter("roomtype")!=null?request.getParameter("roomtype"):"NA";
				 grname=request.getParameter("grname")!=null?request.getParameter("grname"):"NA";
				 city=request.getParameter("city")!=null?request.getParameter("city"):"NA";
				 drmobileno=request.getParameter("drmobileno")!=null?request.getParameter("drmobileno"):"NA";
				 telebenf=request.getParameter("telebenf")!=null?request.getParameter("telebenf"):"NA";
				 icuadmnroom=request.getParameter("icuadmnroom")!=null?request.getParameter("icuadmnroom"):"NA";
				 lenofstay=request.getParameter("lenofstay")!=null?request.getParameter("lenofstay"):"NA";
				 roomicutariff=request.getParameter("roomicutariff")!=null?request.getParameter("roomicutariff"):"NA";
				 consultchrgs=request.getParameter("consultchrgs")!=null?request.getParameter("consultchrgs"):"NA";
				 surgerychrgs=request.getParameter("surgerychrgs")!=null?request.getParameter("surgerychrgs"):"NA";
				 otchrgs=request.getParameter("otchrgs")!=null?request.getParameter("otchrgs"):"NA";
				 implntscosts=request.getParameter("implntscosts")!=null?request.getParameter("implntscosts"):"NA";
				 alamtaskdbynsp=request.getParameter("alamtaskdbynsp")!=null?request.getParameter("alamtaskdbynsp"):"NA";
				 prenegotedtariff=request.getParameter("prenegotedtariff")!=null?request.getParameter("prenegotedtariff"):"NA";
				 rmonursing=request.getParameter("rmonursing")!=null?request.getParameter("rmonursing"):"NA";
				 medcost=request.getParameter("medcost")!=null?request.getParameter("medcost"):"NA";
				 invstcost=request.getParameter("invstcost")!=null?request.getParameter("invstcost"):"NA";
				 anastchrgs=request.getParameter("anastchrgs")!=null?request.getParameter("anastchrgs"):"NA";
				 anyotherexp=request.getParameter("anyotherexp")!=null?request.getParameter("anyotherexp"):"NA";
				 recmalamtbytpa=request.getParameter("recmalamtbytpa")!=null?request.getParameter("recmalamtbytpa"):"NA";
				 tpafinalbillestm=request.getParameter("tpafinalbillestm")!=null?request.getParameter("tpafinalbillestm"):"NA";
				 complntswithdurn=request.getParameter("complntswithdurn")!=null?request.getParameter("complntswithdurn"):"NA";
				 clinicalfindngs=request.getParameter("clinicalfindngs")!=null?request.getParameter("clinicalfindngs"):"NA";
				 testdone=request.getParameter("testdone")!=null?request.getParameter("testdone"):"NA";
				 diagnosis=request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
				 meddetails=request.getParameter("meddetails")!=null?request.getParameter("meddetails"):"NA";
				 surgeryrx=request.getParameter("surgeryrx")!=null?request.getParameter("surgeryrx"):"NA";
				 
				 FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/ITGI.xls"));
                 
		            HSSFWorkbook workbook = new HSSFWorkbook(fis);
		     	      CellStyle style = workbook.createCellStyle();
		   	      style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		   	      style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		   	      style.setBorderLeft(CellStyle.BORDER_THIN);
		   	      style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		   	      style.setBorderRight(CellStyle.BORDER_THIN);
		   	      style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		   	      style.setBorderTop(CellStyle.BORDER_THIN);
		   	      style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		   	      style.setBorderBottom(CellStyle.BORDER_THIN);
		   	      style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		  	     
		             HSSFSheet sheet = workbook.getSheetAt(0);
		             
		            
		             Cell cell = null;
		         //Update the value of cell
		           cell = sheet.getRow(1).getCell(1);
		           cell.setCellValue(cardid);//CARDID
		           
		           cell = sheet.getRow(2).getCell(1);
		           cell.setCellValue(hospname);//NAME_OF_HOSPITAL
		        
		           cell = sheet.getRow(3).getCell(1);
		           cell.setCellValue(hospmobileno);//MOBILE HOSP REP.
		        
		           cell = sheet.getRow(4).getCell(1);
		           cell.setCellValue(benfname);//BENEFICIARY_NAME
		        
		           cell = sheet.getRow(5).getCell(1);
		           cell.setCellValue(admdate);//ADM_DATE
		        
		           cell = sheet.getRow(6).getCell(1);
		           cell.setCellValue(medsurgdaycare);//MEDICAL / SURG / DAY CARE
		        
		           cell = sheet.getRow(7).getCell(1);
		           cell.setCellValue(roomtype);//ROOM TYPE
		        
		           cell = sheet.getRow(1).getCell(3);
		           cell.setCellValue(grname);//GR. NAME
		           
		           cell = sheet.getRow(2).getCell(3);
		           cell.setCellValue(city);//CITY
		        
		           cell = sheet.getRow(3).getCell(3);
		           cell.setCellValue(drmobileno);//MOBILE DOCTOR
		        
		           cell = sheet.getRow(4).getCell(3);
		           cell.setCellValue(telebenf);//TELE -BENEFICIARY
		        
		           cell = sheet.getRow(5).getCell(3);
		           cell.setCellValue(icuadmnroom);//ICU ADMISSION / ROOM
		        
		           cell = sheet.getRow(6).getCell(3);
		           cell.setCellValue(lenofstay);// LENGTH OF STAY
		        
		           
		           cell = sheet.getRow(9).getCell(1);
		           cell.setCellValue(roomicutariff);//ROOM / ICU TARIFF
		           
		           cell = sheet.getRow(10).getCell(1);
		           cell.setCellValue(consultchrgs);//CONSULTANT CHARGES
		           
		           cell = sheet.getRow(11).getCell(1);
		           cell.setCellValue(surgerychrgs);//SURGERY CHARGES
		           
		           cell = sheet.getRow(12).getCell(1);
		           cell.setCellValue(otchrgs);//OT CHARGES
		           
		           cell = sheet.getRow(13).getCell(1);
		           cell.setCellValue(implntscosts);//IMPLANTS COST, IF ANY
		           
		           cell = sheet.getRow(14).getCell(1);
		           cell.setCellValue(alamtaskdbynsp);//AL_AMOUNT ASKED BY NSP
		           
		           cell = sheet.getRow(15).getCell(1);
		           cell.setCellValue(prenegotedtariff);//PRE-NEGOTIATED TARIFF
		           
		           
		           cell = sheet.getRow(9).getCell(3);
		           cell.setCellValue(rmonursing);//RMO/ NURSING
		           
		           cell = sheet.getRow(10).getCell(3);
		           cell.setCellValue(medcost);//MEDICINES COST
		           
		           cell = sheet.getRow(11).getCell(3);
		           cell.setCellValue(invstcost);//INVST. COSTS
		           
		           cell = sheet.getRow(12).getCell(3);
		           cell.setCellValue(anastchrgs);//ANAESTHESIA CHARGES
		           
		           cell = sheet.getRow(13).getCell(3);
		           cell.setCellValue(anyotherexp);//ANY OTHER EXPENSES
		           
		           cell = sheet.getRow(14).getCell(3);
		           cell.setCellValue(recmalamtbytpa);//RECOMMENDED AL AMT  BY TPA
		           
		           cell = sheet.getRow(15).getCell(3);
		           cell.setCellValue(tpafinalbillestm);//TPA FINAL BILL ESTIMATE
		           
		           
		           cell = sheet.getRow(17).getCell(0);
		           cell.setCellValue("COMPLAINTS WITH DURATION: "+complntswithdurn);//COMPLAINTS WITH DURATION

		           cell = sheet.getRow(18).getCell(0);
		           cell.setCellValue("CLINICAL FINDINGS: "+clinicalfindngs);//CLINICAL FINDINGS

		           cell = sheet.getRow(19).getCell(0);
		           cell.setCellValue("TESTS DONE SO FAR: "+testdone);//TESTS DONE SO FAR

		           cell = sheet.getRow(20).getCell(0);
		           cell.setCellValue("DIAGNOSIS: "+diagnosis);//DIAGNOSIS

		           cell = sheet.getRow(21).getCell(0);
		           cell.setCellValue("MEDICAL DETAILS: "+meddetails);//MEDICAL DETAILS

		           cell = sheet.getRow(22).getCell(0);
		           cell.setCellValue("SURGERY Rx: "+surgeryrx);//SURGERY Rx
		           
		           response.setContentType("application/vnd.ms-excel");
		              response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
		              // ...
		              // Now populate workbook the usual way.
		              // ...
		              workbook.write(response.getOutputStream()); // Write workbook to response.
		             // workbook.close();
		              workbook=null;
			}
			else if( ("RE".equals(insurerFlag)))
			{
				String patientname = "";
				String suminsured = "";
				String age = "";
				String gender = "";
				String relationship = "";
				String employee = "";
				String city = "";
				String packag = "";
				String doa = "";
				String dod = "";
				String durationofstay = "";
				String roomtype = "";
				String roomrent = "";
				String diagnosis = "";
				String treatment = "";
				String claimedamt = "";
				String payableamt = "";
				String copay = "";
				String drremarks = "";
				String hospital = "";
				
				 patientname=request.getParameter("patientname")!=null?request.getParameter("patientname"):"NA";
				 age=request.getParameter("age")!=null?request.getParameter("age"):"NA";
				 gender=request.getParameter("gender")!=null?request.getParameter("gender"):"NA";
				 relationship=request.getParameter("relationship")!=null?request.getParameter("relationship"):"NA";
				 employee=request.getParameter("employee")!=null?request.getParameter("employee"):"NA";
				 suminsured=request.getParameter("suminsured")!=null?request.getParameter("suminsured"):"0";
				 hospital=request.getParameter("hospital")!=null?request.getParameter("hospital"):"NA";
				 city=request.getParameter("city")!=null?request.getParameter("city"):"NA";
				 packag=request.getParameter("package")!=null?request.getParameter("package"):"NA";
				 doa=request.getParameter("doa")!=null?request.getParameter("doa"):"NA";
				 dod=request.getParameter("dod")!=null?request.getParameter("dod"):"NA";
				 durationofstay=request.getParameter("durationofstay")!=null?request.getParameter("durationofstay"):"NA";
				 roomtype=request.getParameter("roomtype")!=null?request.getParameter("roomtype"):"NA";
				 roomrent=request.getParameter("roomrent")!=null?request.getParameter("roomrent"):"NA";
				 diagnosis=request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
				 treatment=request.getParameter("treatment")!=null?request.getParameter("treatment"):"NA";
				 claimedamt=request.getParameter("claimedamt")!=null?request.getParameter("claimedamt"):"0";
				 payableamt=request.getParameter("payableamt")!=null?request.getParameter("payableamt"):"0";
				 copay=request.getParameter("copay")!=null?request.getParameter("copay"):"0";
				 drremarks=request.getParameter("drremarks")!=null?request.getParameter("drremarks"):"NA";
				 
				 FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/RELIGARE.xls"));
                 
			        HSSFWorkbook workbook = new HSSFWorkbook(fis);
			 	      CellStyle style = workbook.createCellStyle();
				      style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
				      style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				      style.setBorderLeft(CellStyle.BORDER_THIN);
				      style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
				      style.setBorderRight(CellStyle.BORDER_THIN);
				      style.setRightBorderColor(IndexedColors.BLACK.getIndex());
				      style.setBorderTop(CellStyle.BORDER_THIN);
				      style.setTopBorderColor(IndexedColors.BLACK.getIndex());
				      style.setBorderBottom(CellStyle.BORDER_THIN);
				      style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				     
			         HSSFSheet sheet = workbook.getSheetAt(0);
			     
			        
			       Cell cell = null;
			       //Update the value of cell
			       cell = sheet.getRow(0).getCell(1);
			       cell.setCellValue(patientname);
			        
			       cell = sheet.getRow(1).getCell(1);
			       cell.setCellValue(age);
			       
			       cell = sheet.getRow(2).getCell(1);
			       cell.setCellValue(gender);

			       cell = sheet.getRow(3).getCell(1);
			       cell.setCellValue(relationship);

			       cell = sheet.getRow(4).getCell(1);
			       cell.setCellValue(employee);

			       cell = sheet.getRow(5).getCell(1);
			       cell.setCellValue(suminsured);

			       cell = sheet.getRow(6).getCell(1);
			       cell.setCellValue(hospital);
			      
			       cell = sheet.getRow(7).getCell(1);
			       cell.setCellValue(city);
			       
			       cell = sheet.getRow(8).getCell(1);
			       cell.setCellValue(packag);
			     
			       cell = sheet.getRow(9).getCell(1);
			       cell.setCellValue(doa);
			       
			       cell = sheet.getRow(10).getCell(1);
			       cell.setCellValue(dod);
			       
			       cell = sheet.getRow(11).getCell(1);
			       cell.setCellValue(durationofstay);
			       
			       cell = sheet.getRow(12).getCell(1);
			       cell.setCellValue(roomtype);
			       
			       cell = sheet.getRow(13).getCell(1);
			       cell.setCellValue(roomrent);
			       
			       cell = sheet.getRow(14).getCell(1);
			       cell.setCellValue(diagnosis);
			       
			       cell = sheet.getRow(15).getCell(1);
			       cell.setCellValue(treatment);
			       
			       cell = sheet.getRow(16).getCell(1);
			       cell.setCellValue(claimedamt);
			        
			       cell = sheet.getRow(17).getCell(1);
			       cell.setCellValue(payableamt);
			      
			       cell = sheet.getRow(18).getCell(1);
			       cell.setCellValue(copay);
			     
			       cell = sheet.getRow(19).getCell(1);
			       cell.setCellValue(drremarks);
			       
			       response.setContentType("application/vnd.ms-excel");
		           response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
		              // ...
		              // Now populate workbook the usual way.
		              // ...
		              workbook.write(response.getOutputStream()); // Write workbook to response.
		             // workbook.close();
		              workbook=null;
				
			}
			//santhosh added start
			//FOR NIA
			else if( ("NA".equals(insurerFlag)))
			{

			String claimno = "";
			String policyno = "";
			String policyperiod = "";
			String insured = "";
			String patient = "";
			String policytype = "";
			String incase = "";
			String copayment = "";
			String preauthtype = "";
			String dateofinception = "";
			String balsuminsured = "";
			String relation = "";
			String gender = "";
			String age = "";

			String suminsured = "";
			String policyno1 = "";
			String validity = "";
			String hospname = "";
			String city = "";
			String state = "";  
			String disease = "";
			String diagnosis = "";
			String lineoftreatment = "";
			String procedureincRefrencerate = "";
			String averagecost = "";
			String packagerate = "";
			String dateofAdmission = "";
			String dateofDischarge = "";
			String los = "";
			String detailsofidproof = "";
			String roomtype = "";


			String disease1 = "";
			String procedure = "";
			String amount = "";
			String ppn = "";
			String roomrent = "";
			String icdcode = "";
			String applicablePPN = "";
			String packageRate = "";
			
			String noofdaysRoom = "";
			String noOfdaysICU = "";
			String ICUperday = "";
			String roomrenteligibility = "";

			String gipsappnhospaphb = "";
			String gipsappntpa = "";
			String gipsappnresondod = "";
			String roomrenthospaphb = "";
			String roomrenttpa = "";
			String roomrentdod = "";
			String profchghospaphb = "";
			String profchgtpa = "";
			String profchgdod = "";

			String phrhospaphb = "";
			String phrtpa = "";
			String phrdod = "";

			String othospaphb = "";
			String ottpa = "";
			String otdod = "";

			String labinvhospaphb="";
			String labinvtpa="";
			String labinvdod="";

			String conchrhospaphb="";
			String conchrtpa="";
			String conchrdod="";

			String othershospaphb="";
			String otherstpa="";
			String othersdod="";

			String totalhospaphb="";
			String totaltpa="";
			String totaldod="";

			String specification="";
			String nameandqual="";
			String dateandtime="";

			String diseasetreatment="";
			String ppnpackage="";
			String expencesallowed="";
			String anyamountallowedinexcessbyTPA="";

				//RETRIEVE POST FORM DATA
				/* FORMAT FOR CASHLESS APPROVAL Rs. 1 LAKH AND ABOVE */
				 claimno=request.getParameter("claimno")!=null?request.getParameter("claimno"):"NA";
				 policyno=request.getParameter("policyno")!=""?request.getParameter("policyno"):"NA";
				 policyperiod=request.getParameter("policyperiod")!=""?request.getParameter("policyperiod"):"NA";
				 insured=request.getParameter("insured")!=""?request.getParameter("insured"):"NA";
				 patient=request.getParameter("patient")!=""?request.getParameter("patient"):"NA";	
				 policytype=request.getParameter("policytype")!=""?request.getParameter("policytype"):"NA";
				 incase=request.getParameter("incase")!=""?request.getParameter("incase"):"NA";
				 copayment=request.getParameter("copayment")!=""?request.getParameter("copayment"):"NA";
				 preauthtype=request.getParameter("preauthtype")!=""?request.getParameter("preauthtype"):"NA";
				 dateofinception=request.getParameter("dateofinception")!=""?request.getParameter("dateofinception"):"NA";
				 balsuminsured=request.getParameter("balsuminsured")!=""?request.getParameter("balsuminsured"):"NA";
				 relation=request.getParameter("relation")!=""?request.getParameter("relation"):"NA";
				 gender=request.getParameter("gender")!=""?request.getParameter("gender"):"NA";
				 age=request.getParameter("age")!=""?request.getParameter("age"):"NA";
				
				 /*Underwriting history of Patient */
				 
				 suminsured=request.getParameter("suminsured")!=""?request.getParameter("suminsured"):"NA";
				 policyno1=request.getParameter("policyno1")!=""?request.getParameter("policyno1"):"NA";
				 validity=request.getParameter("validity")!=""?request.getParameter("validity"):"NA";
				 hospname=request.getParameter("hospname")!=""?request.getParameter("hospname"):"NA";
				 city=request.getParameter("city")!=""?request.getParameter("city"):"NA";
				 state=request.getParameter("state")!=""?request.getParameter("state"):"NA";
				 disease=request.getParameter("disease")!=""?request.getParameter("disease"):"NA";
				 diagnosis=request.getParameter("diagnosis")!=""?request.getParameter("diagnosis"):"NA";
				 lineoftreatment=request.getParameter("lineoftreatment")!=""?request.getParameter("lineoftreatment"):"NA";
				 procedureincRefrencerate=request.getParameter("procedureincRefrencerate")!=""?request.getParameter("procedureincRefrencerate"):"NA";
				 averagecost=request.getParameter("averagecost")!=""?request.getParameter("averagecost"):"NA";
				 dateofAdmission=request.getParameter("dateofAdmission")!=""?request.getParameter("dateofAdmission"):"NA";
				 dateofDischarge=request.getParameter("dateofDischarge")!=""?request.getParameter("dateofDischarge"):"NA";
				 los=request.getParameter("los")!=""?request.getParameter("los"):"NA";
				 detailsofidproof=request.getParameter("detailsofidproof")!=""?request.getParameter("detailsofidproof"):"NA";
				 roomtype=request.getParameter("roomtype")!=""?request.getParameter("roomtype"):"NA";

				 /*Claim history of Patient */
				 disease1=request.getParameter("disease1")!=""?request.getParameter("disease1"):"NA";
				 procedure=request.getParameter("procedure")!=""?request.getParameter("procedure"):"NA";
				 amount=request.getParameter("amount")!=""?request.getParameter("amount"):"NA";
				 ppn=request.getParameter("ppn")!=""?request.getParameter("ppn"):"NA";
				 roomrent=request.getParameter("roomrent")!=""?request.getParameter("roomrent"):"NA";
				 icdcode=request.getParameter("icdcode")!=""?request.getParameter("icdcode"):"NA";
				 applicablePPN=request.getParameter("applicablePPN")!=""?request.getParameter("applicablePPN"):"NA";
				 packageRate=request.getParameter("packageRate")!=""?request.getParameter("packageRate"):"NA";
				 noofdaysRoom=request.getParameter("noofdaysRoom")!=""?request.getParameter("noofdaysRoom"):"NA";
				 noOfdaysICU=request.getParameter("noOfdaysICU")!=""?request.getParameter("noOfdaysICU"):"NA";
				 ICUperday=request.getParameter("ICUperday")!=""?request.getParameter("ICUperday"):"NA";
				 roomrenteligibility=request.getParameter("roomrenteligibility")!=""?request.getParameter("roomrenteligibility"):"NA";


				 gipsappnhospaphb=request.getParameter("gipsappnhospaphb")!=""?request.getParameter("gipsappnhospaphb"):"0";
				 gipsappntpa=request.getParameter("gipsappntpa")!=""?request.getParameter("gipsappntpa"):"0";
				 gipsappnresondod=request.getParameter("gipsappnresondod")!=""?request.getParameter("gipsappnresondod"):"0";
				 
				 roomrenthospaphb=request.getParameter("roomrenthospaphb")!=""?request.getParameter("roomrenthospaphb"):"0";
				 roomrenttpa=request.getParameter("roomrenttpa")!=""?request.getParameter("roomrenttpa"):"0";
				 roomrentdod=request.getParameter("roomrentdod")!=""?request.getParameter("roomrentdod"):"0";
				 
				 profchghospaphb=request.getParameter("profchghospaphb")!=""?request.getParameter("profchghospaphb"):"0";
				 profchgtpa=request.getParameter("profchgtpa")!=""?request.getParameter("profchgtpa"):"0";
				 profchgdod=request.getParameter("profchgdod")!=""?request.getParameter("profchgdod"):"0";

				 phrhospaphb=request.getParameter("phrhospaphb")!=""?request.getParameter("phrhospaphb"):"NA";
				 phrtpa=request.getParameter("phrtpa")!=""?request.getParameter("phrtpa"):"NA";
				 phrdod=request.getParameter("phrdod")!=""?request.getParameter("phrdod"):"NA";

				
				 othospaphb=request.getParameter("othospaphb")!=""?request.getParameter("othospaphb"):"NA";
				 ottpa=request.getParameter("ottpa")!=""?request.getParameter("ottpa"):"NA";
				 otdod=request.getParameter("otdod")!=""?request.getParameter("otdod"):"NA";
				 
				 labinvhospaphb=request.getParameter("labinvhospaphb")!=""?request.getParameter("labinvhospaphb"):"NA";
				 labinvtpa=request.getParameter("labinvtpa")!=""?request.getParameter("labinvtpa"):"NA";
				 labinvdod=request.getParameter("labinvdod")!=""?request.getParameter("labinvdod"):"NA";

				 conchrhospaphb=request.getParameter("conchrhospaphb")!=""?request.getParameter("conchrhospaphb"):"NA";
				 conchrtpa=request.getParameter("conchrtpa")!=""?request.getParameter("conchrtpa"):"NA";
				 conchrdod=request.getParameter("conchrdod")!=""?request.getParameter("conchrdod"):"NA";
				
				 
				 othershospaphb=request.getParameter("othershospaphb")!=""?request.getParameter("othershospaphb"):"NA";
				 otherstpa=request.getParameter("otherstpa")!=""?request.getParameter("otherstpa"):"NA";
				 othersdod=request.getParameter("othersdod")!=""?request.getParameter("othersdod"):"NA";
				 
				 totalhospaphb=request.getParameter("totalhospaphb")!=""?request.getParameter("totalhospaphb"):"NA";
				 totaltpa=request.getParameter("totaltpa")!=""?request.getParameter("totaltpa"):"NA";
				 totaldod=request.getParameter("totaldod")!=""?request.getParameter("totaldod"):"NA";

				 specification=request.getParameter("specification")!=""?request.getParameter("specification"):"NA";
				 nameandqual=request.getParameter("nameandqual")!=""?request.getParameter("nameandqual"):"NA";
				 dateandtime=request.getParameter("dateandtime")!=""?request.getParameter("dateandtime"):"NA";
				 
				 diseasetreatment=request.getParameter("diseasetreatment")!=null?request.getParameter("diseasetreatment"):"NA";
				 ppnpackage=request.getParameter("ppnpackage")!=null?request.getParameter("ppnpackage"):"NA";
				 expencesallowed=request.getParameter("expencesallowed")!=null?request.getParameter("expencesallowed"):"NA";
				 anyamountallowedinexcessbyTPA=request.getParameter("anyamountallowedinexcessbyTPA")!=null?request.getParameter("anyamountallowedinexcessbyTPA"):"NA";

			//XLS GENERATION
			

			             // FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/jspnew/preAuth/Templates/UIIC.xls"));
			//FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/UIIC.xls"));           
			FileInputStream fis = new FileInputStream(new File(getServletContext().getRealPath("/")+"/Templates/NIA.xls"));
			
			  //FileInputStream fis = new FileInputStream(new File("/WebContent/Templates/NIA.xls"));
			          HSSFWorkbook workbook = new HSSFWorkbook(fis);
			     	  CellStyle style = workbook.createCellStyle();
			     	    style.setAlignment(CellStyle.ALIGN_JUSTIFY);
			             HSSFSheet sheet = workbook.getSheetAt(0);
			              Cell cell = null;
			         //Update the value of cell
			         //FORMAT FOR CASHLESS APPROVAL Rs. 1 LAKH AND ABOVE
			         
			          cell = sheet.getRow(3).getCell(0);
			           cell.setCellValue(claimno);//claimno
			           cell = sheet.getRow(3).getCell(1);
			           cell.setCellValue(policyno+" & "+policyperiod);//policyno , policyperiod
			            cell = sheet.getRow(3).getCell(2);
			           cell.setCellValue(insured);//insured
			            cell = sheet.getRow(3).getCell(3);
			           cell.setCellValue(patient);//patient  
			           
			           cell = sheet.getRow(5).getCell(0);
			           cell.setCellValue(policytype);//policytype
			           cell = sheet.getRow(5).getCell(1);
			           cell.setCellValue(incase);//incase
			           cell = sheet.getRow(5).getCell(2);
			           cell.setCellValue(copayment);//copayment
			          
			           cell = sheet.getRow(5).getCell(3);
			           cell.setCellValue(preauthtype);//preauthtype  
			           
			           cell = sheet.getRow(7).getCell(0);
			           cell.setCellValue(dateofinception);//dateofinception
			           cell = sheet.getRow(7).getCell(1);
			           cell.setCellValue(suminsured+" / "+balsuminsured);//balsuminsured
			           cell = sheet.getRow(7).getCell(2);
			           cell.setCellValue(relation);//relation
			           cell = sheet.getRow(7).getCell(3);
			           cell.setCellValue(gender+" & "+age);//gender,age
			           
			          //Underwriting history of Patient
			           cell = sheet.getRow(10).getCell(0);
			           cell.setCellValue(suminsured);//suminsured
			           cell = sheet.getRow(10).getCell(1);
			           cell.setCellValue(policyno+" & "+validity);//policyno,validity
			           cell = sheet.getRow(12).getCell(0);
			           cell.setCellValue(hospname);//hospname
			           cell = sheet.getRow(12).getCell(1);
			           cell.setCellValue(city+" / "+state);//city,state
			           
			           cell = sheet.getRow(14).getCell(0);
			           cell.setCellValue(disease+" / "+diagnosis);//disease,diagnosis
			           cell = sheet.getRow(14).getCell(1);
			           cell.setCellValue(lineoftreatment+" / "+procedureincRefrencerate+" / "+averagecost);//lineoftreatment,procedureincRefrencerate,averagecost
			           cell = sheet.getRow(16).getCell(0);
			           cell.setCellValue(dateofAdmission);//dateofAdmission
			           cell = sheet.getRow(16).getCell(1);
			           cell.setCellValue(dateofDischarge+" / "+los);//dateofDischarge
			           
			           cell = sheet.getRow(18).getCell(0);
			           cell.setCellValue(detailsofidproof);//detailsofidproof
			           cell = sheet.getRow(18).getCell(1);
			           cell.setCellValue(roomtype);//roomtype
			           
			         //Claim history of Patient
			           cell = sheet.getRow(10).getCell(2);
			          cell.setCellValue(disease1+" / "+procedure);//disease
			          cell = sheet.getRow(10).getCell(3);
			          cell.setCellValue(amount);//amount
			          cell = sheet.getRow(12).getCell(2);
			          cell.setCellValue(ppn);//ppn
			          cell = sheet.getRow(12).getCell(3);
			          cell.setCellValue(roomrent);//roomrent
			          
			          cell = sheet.getRow(14).getCell(2);
			          cell.setCellValue(icdcode);//icdcode
			          cell = sheet.getRow(14).getCell(3);
			          cell.setCellValue(applicablePPN+" / "+packageRate);//applicablePPN
			          cell = sheet.getRow(16).getCell(2);
			          cell.setCellValue(noofdaysRoom);//noofdaysRoom
			          cell = sheet.getRow(16).getCell(3);
			          cell.setCellValue(noOfdaysICU);//noOfdaysICU
			          
			          cell = sheet.getRow(18).getCell(2);
			          cell.setCellValue(ICUperday);//ICUperday
			          cell = sheet.getRow(18).getCell(3);
			          cell.setCellValue(roomrenteligibility);//roomrenteligibility 
			           
			          ///////////////////////////////////////////////////////////////
			          
			          cell = sheet.getRow(20).getCell(1);
			          cell.setCellValue(gipsappnhospaphb);//gipsappnhospaphb
			          cell = sheet.getRow(20).getCell(2);
			          cell.setCellValue(gipsappntpa);//gipsappntpa
			          cell = sheet.getRow(20).getCell(3);
			          cell.setCellValue(gipsappnresondod);//gipsappnresondod
			          
			          
			          cell = sheet.getRow(21).getCell(1);
			          cell.setCellValue(roomrenthospaphb);//roomrenthospaphb
			          cell = sheet.getRow(21).getCell(2);
			          cell.setCellValue(roomrenttpa);//roomrenttpa
			          cell = sheet.getRow(21).getCell(3);
			          cell.setCellValue(roomrentdod);//roomrentdod
			          
			          cell = sheet.getRow(22).getCell(1);
			          cell.setCellValue(profchghospaphb);//profchghospaphb
			          cell = sheet.getRow(22).getCell(2);
			          cell.setCellValue(profchgtpa);//profchgtpa
			          cell = sheet.getRow(22).getCell(3);
			          cell.setCellValue(profchgdod);//profchgdod
			          
			          cell = sheet.getRow(23).getCell(1);
			          cell.setCellValue(phrhospaphb);//phrhospaphb
			          cell = sheet.getRow(23).getCell(2);
			          cell.setCellValue(phrtpa);//phrtpa
			          cell = sheet.getRow(23).getCell(3);
			          cell.setCellValue(phrdod);//phrdod
			          
			          cell = sheet.getRow(24).getCell(1);
			          cell.setCellValue(othospaphb);//othospaphb
			          cell = sheet.getRow(24).getCell(2);
			          cell.setCellValue(ottpa);//ottpa
			          cell = sheet.getRow(24).getCell(3);
			          cell.setCellValue(otdod);//otdod
			          
			          cell = sheet.getRow(25).getCell(1);
			          cell.setCellValue(labinvhospaphb);//labinvhospaphb
			          cell = sheet.getRow(25).getCell(2);
			          cell.setCellValue(labinvtpa);//labinvtpa
			          cell = sheet.getRow(25).getCell(3);
			          cell.setCellValue(labinvdod);//labinvdod
			          
			          cell = sheet.getRow(26).getCell(1);
			          cell.setCellValue(conchrhospaphb);//conchrhospaphb
			          cell = sheet.getRow(26).getCell(2);
			          cell.setCellValue(conchrtpa);//conchrtpa
			          cell = sheet.getRow(26).getCell(3);
			          cell.setCellValue(conchrdod);//conchrdod
			          
			          
			          cell = sheet.getRow(27).getCell(1);
			          cell.setCellValue(othershospaphb);//othershospaphb
			          
			          cell = sheet.getRow(27).getCell(2);
			          cell.setCellValue(otherstpa);//otherstpa
			          cell = sheet.getRow(27).getCell(3);
			          cell.setCellValue(othersdod);//othersdod
			          
			          cell = sheet.getRow(28).getCell(1);
			          cell.setCellValue(totalhospaphb);//totalhospaphb
			          cell = sheet.getRow(28).getCell(2);
			          cell.setCellValue(totaltpa);//totaltpa
			          cell = sheet.getRow(28).getCell(3);
			          cell.setCellValue(totaldod);//totaldedu
			          
			          cell = sheet.getRow(30).getCell(0);
			          cell.setCellValue(specification);//specification
			          cell = sheet.getRow(30).getCell(1);
			          cell.setCellValue(nameandqual);//nameandqual
			          cell = sheet.getRow(30).getCell(2);
			          cell.setCellValue(dateandtime);//dateandtime  
			          
			          cell = sheet.getRow(32).getCell(3);
			          cell.setCellValue(diseasetreatment);//diseasetreatment
			          cell = sheet.getRow(33).getCell(3);
			          cell.setCellValue(ppnpackage);//ppnpackage
			          cell = sheet.getRow(34).getCell(3);
			          cell.setCellValue(expencesallowed);//dateandtime 
			          cell = sheet.getRow(35).getCell(3);
			          cell.setCellValue(anyamountallowedinexcessbyTPA);//anyamountallowedinexcessbyTPA  
			         
					
			           response.setContentType("application/vnd.ms-excel");
			           response.setHeader("Content-Disposition", "attachment; filename="+clmid+"-"+rauthflg+".xls");
			              // ...
			              // Now populate workbook the usual way.
			              // ...
			              workbook.write(response.getOutputStream()); // Write workbook to response.
			             // workbook.close();
			              workbook=null;
			}//NIA END
			//santhosh added end
              
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
