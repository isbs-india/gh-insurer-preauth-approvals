package com.ghpl.action;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.ghpl.model.iauth.IAuthBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;

/**
 * Servlet implementation class IAuthSearchRetActionClaimId
 */
@WebServlet("/IAuthSearchRetActionClaimId")
public class IAuthSearchRetActionClaimId extends HttpServlet {
	Logger logger =Logger.getLogger("IAuthSearchRetActionClaimId");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IAuthSearchRetActionClaimId() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				System.out.println("do get method IAuthCCNAction");
				String ccnNumber = null;
				int moduleId =0;
				if(request.getParameter("claimid")!=null){
					ccnNumber = request.getParameter("claimid");
				}
				if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
					//session.setAttribute("moduleId", moduleId);
				}
				List<IAuthBean> empList=SendToInsurerManager.getIAuthSearchRetClaimid(ccnNumber, moduleId);
				request.setAttribute("empList", empList);
				RequestDispatcher rd = request.getRequestDispatcher("./claimemployeedetails.jsp");
				rd.forward(request,response);
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			//System.out.println("JSESSIONID="+cookie.getValue());
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
		    		session.invalidate();
		    	}
				 response.sendRedirect("./login.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.error(e);
			}
	}

}
