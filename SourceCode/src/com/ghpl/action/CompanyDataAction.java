package com.ghpl.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


/**
 * Servlet implementation class CompanyDataAction
 */
@WebServlet("/CompanyDataAction")
public class CompanyDataAction extends HttpServlet {
	static Logger logger =Logger.getLogger("CompanyDataAction");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompanyDataAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
		
		String module=null;
		if(request.getParameter("moduleId")!=null){
			module=	request.getParameter("moduleId");
		}
		int moduleId=0;
		if(module!=null){
			if(module.equals("Corp")){
				moduleId=1;
			}else if(module.equals("Bank Assurance")){
				moduleId=2;
			}else if(module.equals("Individual")){
				moduleId=4;
			}
		}
		session.setAttribute("moduleId", moduleId);
		response.sendRedirect("./preauthsearch.jsp");
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			//System.out.println("JSESSIONID="+cookie.getValue());
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
					session.setAttribute("moduleId", 0);
		    		session.invalidate();
		    	}
				 response.sendRedirect("./index.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				logger.error(e);
			}
		
	}

	
}
