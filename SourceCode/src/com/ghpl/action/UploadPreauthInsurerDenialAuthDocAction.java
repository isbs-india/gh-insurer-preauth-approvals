package com.ghpl.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ghpl.model.iauth.LoginBean;
import com.ghpl.model.iauth.PreAuthBean;
import com.ghpl.model.iauth.PreAuthXMLBean;
import com.ghpl.persistence.iauth.SendToInsurerManager;
import com.ghpl.util.Constants;
import com.ghpl.util.MyProperties;
import com.ghpl.util.Util;

/**
 * Servlet implementation class UploadPreauthInsurerDenialAuthDocAction
 */
@WebServlet("/UploadPreauthInsurerDenialAuthDocAction")
@MultipartConfig(fileSizeThreshold=1024*1024*2,	// 2MB 
maxFileSize=1024*1024*2		// 2MB
)
public class UploadPreauthInsurerDenialAuthDocAction extends HttpServlet {
	static Logger logger =Logger.getLogger("UploadPreauthInsurerDenialAuthDocAction");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadPreauthInsurerDenialAuthDocAction() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
					if (!org.apache.commons.fileupload.servlet.ServletFileUpload.isMultipartContent(request)) {
						return;
					}
					int userId= 0;
					//int role=0;
					String userName="";
					if(session.getAttribute("user")!=null){
						LoginBean loginBean=(LoginBean)session.getAttribute("user");
						userId =loginBean.getUid();
						//role=loginBean.getRoleId();
						userName =loginBean.getLoginUserName();
					}
					String subdocid=Constants.subdocid;
					String maindocid=Constants.maindocid;
					String FTPHOST=null;
					String FTPUName=null;
					String FTPPwd=null;
					String preauthAppNo=null;
					String ccnUnique = null;
					String claimId=null;
					String typeofdoc1=null;
					String typeofdoc2=null;
					String typeofdoc3=null;
					String typeofdoc4=null;
					String typeofdoc5=null;
					
					String policyNo=null;
					String cardid=null;
					String cardHolderName=null;
					String selfName=null;
					String srcofclaim=null;
					String policyHolderName=null;
					int insuredId=0;
					String patAddress=null;
					String patLandLineNo=null;
					
					String fileformat=null;
					String patEmail=null;
					String kayakoId=null;
					String policyId=null;
					String status=null;
					int reauthFlag=0;
					String insurerRemarks=null;
	            	String insurerReply=null;
					 PreAuthBean preAuthBean =new PreAuthBean();
					  MyProperties myResources = new MyProperties();
					FTPHOST = myResources.getMyProperties("ftp_host");
					FTPUName = myResources.getMyProperties("ftp_username");
					FTPPwd = myResources.getMyProperties("ftp_password");
			
				    FTPClient client = new FTPClient();
				   
				        try {
			                client.connect(FTPHOST);
				            client.login(FTPUName, FTPPwd);
				            client.setFileType(FTP.BINARY_FILE_TYPE);
				            client.enterLocalPassiveMode();
				           
				            int moduleId=0;
							if(request.getParameter("moduleId")!=null){
								moduleId=Integer.parseInt(request.getParameter("moduleId"));
							} 
							
							int moduleUserId= 0;
							if(session.getAttribute("user")!=null){
								LoginBean loginBean=(LoginBean)session.getAttribute("user");
								userId =loginBean.getUid();
								//role=loginBean.getRoleId();
								userName =loginBean.getLoginUserName();
								moduleUserId =loginBean.getModuleUserId(moduleId);
								if(moduleUserId==0){
									request.setAttribute("userName", loginBean.getLoginUserName());
									request.setAttribute("moduleId", moduleId);
									request.setAttribute("moduleName", Util.getModuleName(moduleId));
				            		getServletContext().getRequestDispatcher("/loginerror.jsp").forward(request, response);
				            		return;
								}
							}
							
							DateFormat dfm = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				            PreAuthXMLBean preAuthXMLBean= null;
				            File uploadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir");  
		            		   if(request.getParameter("insreply")!=null && !request.getParameter("insreply").equals("")) {
		            			   insurerReply = request.getParameter("insreply");
		                        }
		            		   if(request.getParameter("insurerremarks")!=null && !request.getParameter("insurerremarks").equals("")) {
		            			   insurerRemarks = request.getParameter("insurerremarks");
		                        }
					            if(request.getParameter("reqType")!=null && !request.getParameter("reqType").equals("")) {
					            	srcofclaim = request.getParameter("reqType");
		                        }
					            if(request.getParameter("preauthNo")!=null && !request.getParameter("preauthNo").equals("")) {
					            	preauthAppNo = request.getParameter("preauthNo");
					            }
					            if(request.getParameter("reauthFlag")!=null && !request.getParameter("reauthFlag").equals("")) {
					            	reauthFlag = Integer.parseInt(request.getParameter("reauthFlag"));
		                        }
					            if(request.getParameter("status")!=null && !request.getParameter("status").equals("")) {
					            	status = request.getParameter("status");
		                        }
					            if(request.getParameter("ccnUnique")!=null && !request.getParameter("ccnUnique").equals("")) {
					            	ccnUnique = request.getParameter("ccnUnique").trim();
		                        }
					            if(request.getParameter("claimId")!=null && !request.getParameter("claimId").equals("")) {
					            	claimId = request.getParameter("claimId");
		                        }
					            
					            if(request.getParameter("policyId")!=null && !request.getParameter("policyId").equals("")) {
					            	policyId = request.getParameter("policyId");
		                        }
					            if(request.getParameter("cardid")!=null && !request.getParameter("cardid").equals("")) {
					            	cardid = request.getParameter("cardid");
		                        }
					            if(request.getParameter("cardHolderName")!=null && !request.getParameter("cardHolderName").equals("")) {
					            	cardHolderName = request.getParameter("cardHolderName");
		                        }
					            if(request.getParameter("selfName")!=null && !request.getParameter("selfName").equals("")) {
					            	selfName = request.getParameter("selfName");
		                        }
					            if(request.getParameter("policyNo")!=null && !request.getParameter("policyNo").equals("")) {
					            	policyNo = request.getParameter("policyNo");
					            }
					            if(request.getParameter("patAddress")!=null && !request.getParameter("patAddress").equals("")) {
					            	patAddress = request.getParameter("patAddress");
					            }
					            if(request.getParameter("patEmail")!=null && !request.getParameter("patEmail").equals("")) {
					            	patEmail = request.getParameter("patEmail");
					            }
					            if(request.getParameter("patMobile")!=null && !request.getParameter("patMobile").equals("")) {
					            	patLandLineNo = request.getParameter("patMobile");
		                        }
					            if(request.getParameter("insuredId")!=null && !request.getParameter("insuredId").equals("")) {
					            	insuredId = Integer.parseInt(request.getParameter("insuredId"));
		                        }
					            if(request.getParameter("policyHolderName")!=null && !request.getParameter("policyHolderName").equals("")) {
					            	policyHolderName = request.getParameter("policyHolderName");
		                        }
					            if(request.getParameter("kayakoId")!=null && !request.getParameter("kayakoId").equals("")) {
					            	kayakoId = request.getParameter("kayakoId");
		                        }
					            if(request.getParameter("typeofdoc1")!=null && !request.getParameter("typeofdoc1").equals("0")) {
					            	typeofdoc1 = request.getParameter("typeofdoc1");
		                        }
					            if(request.getParameter("typeofdoc2")!=null && !request.getParameter("typeofdoc2").equals("0")) {
					            	typeofdoc2 = request.getParameter("typeofdoc2");
		                        }
					            if(request.getParameter("typeofdoc3")!=null && !request.getParameter("typeofdoc3").equals("0")) {
					            	typeofdoc3 = request.getParameter("typeofdoc3");
		                        }
					            if(request.getParameter("typeofdoc4")!=null && !request.getParameter("typeofdoc4").equals("0")) {
					            	typeofdoc4 = request.getParameter("typeofdoc4");
		                        }
					            if(request.getParameter("typeofdoc5")!=null && !request.getParameter("typeofdoc5").equals("0")) {
					            	typeofdoc5 = request.getParameter("typeofdoc5");
		                        }
					            
					            SimpleDateFormat dateFormatCur=new SimpleDateFormat("dd-MM-yyyy");
								Date curDate = new Date();
								String curDateString = dateFormatCur.format(curDate);
								String ftpUploadPath=null;
								String pathcurrent=null;
								String constantspath=null;
								String preauthflag=null;
								if(moduleId==1){
									constantspath=Constants.uploadGHPL;
									pathcurrent=constantspath+curDateString;
									File uploadDir = new File(pathcurrent);
								    if (!uploadDir.exists()){
										String str[] =pathcurrent.split("/");
										for(int i=0;i<str.length;i++){
											client.makeDirectory(str[i]);
											client.changeWorkingDirectory(str[i]);
										}
									}
									 ftpUploadPath=pathcurrent;
						    	 }else if(moduleId==2 || moduleId==4){
						    		 constantspath=Constants.uploadAB;
						    		 pathcurrent=constantspath+curDateString;
						    		 File uploadDir = new File(pathcurrent);
						    		 if (!uploadDir.exists()){
										String str[] =pathcurrent.split("/");
										for(int i=0;i<str.length;i++){
											client.makeDirectory(str[i]);
											client.changeWorkingDirectory(str[i]);
										}
						    		 }
									 ftpUploadPath=pathcurrent;
						    	 }
								
								
								List <PreAuthXMLBean> preAuthList = new ArrayList<PreAuthXMLBean>();
					            final Part filePart1 = request.getPart("filedoc1"); 
					            if(filePart1 !=null){
								String fileName = Util.extractFileName(filePart1);
								if(fileName!=null && !fileName.equals("")){
									String filePath = uploadPath + File.separator + fileName;
									Date date = new Date();                                                 
						   		    String dateFormat = dfm.format(date);
			                		int fileSerialno=1;
			                		String extension = Util.fileExtenssion(fileName);
					                fileformat=dateFormat+"_"+claimId+"_"+fileSerialno+"."+extension;
									  if((dateFormat!=null) && (claimId!=null) && (fileSerialno != 0) && (extension!=null)){
										  filePart1.write(filePath);
										  BufferedInputStream bis=null;
					                		try{
					                			File uploadedFile = new File(filePath);
						                          bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
						                          if(moduleId==1 || moduleId==2 || moduleId==4){
						                        	  client.storeFile(ftpUploadPath+"/"+fileformat, bis);
						                          }
					                         }finally { 
					                        	 if (bis != null){ 
					                        		 try { 
					                        			 bis.close(); 
					                        		 }catch(IOException ioe) {
					                        			 ioe.printStackTrace(); 
					                        		 } 
					                        	 }
					                         }
					                		preAuthXMLBean = new PreAuthXMLBean();
					                		preAuthXMLBean.setFilename(fileformat);
					                		preAuthXMLBean.setClaimID(claimId);
					                		preAuthXMLBean.setPreappno(preauthAppNo);
					                		preAuthXMLBean.setTypeOfDoc(typeofdoc1);
					                		preAuthXMLBean.setMaindocid(maindocid);
					                		preAuthXMLBean.setSubdocid(subdocid);
					                		preAuthXMLBean.setActualfilename(fileName);
					                		preAuthXMLBean.setPreauthflag(preauthflag);
					                		preAuthList.add(preAuthXMLBean); 
					                	}
									}
					            }
					            
					            final Part filePart2 = request.getPart("filedoc2"); 
					            if(filePart2 !=null){
								String fileName = Util.extractFileName(filePart2);
								if(fileName!=null && !fileName.equals("")){
									String filePath = uploadPath + File.separator + fileName;
									Date date = new Date();                                                 
						   		    String dateFormat = dfm.format(date);
			                		int fileSerialno=2;
			                		String extension = Util.fileExtenssion(fileName);
					                fileformat=dateFormat+"_"+claimId+"_"+fileSerialno+"."+extension;
									  if((dateFormat!=null) && (claimId!=null) && (fileSerialno != 0) && (extension!=null)){
										  filePart2.write(filePath);
										  BufferedInputStream bis=null;
					                		try{
					                			  File uploadedFile = new File(filePath);
						                          bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
						                          if(moduleId==1 || moduleId==2 || moduleId==4){
						                        	  client.storeFile(ftpUploadPath+"/"+fileformat, bis);
						                        	  logger.info("ftp client reply code : "+client.getReplyCode());
						                          }
					                         }finally { 
					                        	 if (bis != null){ 
					                        		 try { 
					                        			 bis.close(); 
					                        		 }catch(IOException ioe) {
					                        			 ioe.printStackTrace(); 
					                        		 } 
					                        	 }
					                         }
					                		preAuthXMLBean = new PreAuthXMLBean();
					                		preAuthXMLBean.setFilename(fileformat);
					                		preAuthXMLBean.setClaimID(claimId);
					                		preAuthXMLBean.setPreappno(preauthAppNo);
					                		preAuthXMLBean.setTypeOfDoc(typeofdoc2);
					                		preAuthXMLBean.setMaindocid(maindocid);
					                		preAuthXMLBean.setSubdocid(subdocid);
					                		preAuthXMLBean.setActualfilename(fileName);
					                		preAuthXMLBean.setPreauthflag(preauthflag);
					                		preAuthList.add(preAuthXMLBean); 
					                	}
									}
					            }
					            
					            final Part filePart3 = request.getPart("filedoc3"); 
					            if(filePart3 !=null){
								String fileName = Util.extractFileName(filePart3);
								if(fileName!=null && !fileName.equals("")){
									String filePath = uploadPath + File.separator + fileName;
									Date date = new Date();                                                 
						   		    String dateFormat = dfm.format(date);
			                		int fileSerialno=3;
			                		String extension = Util.fileExtenssion(fileName);
					                fileformat=dateFormat+"_"+claimId+"_"+fileSerialno+"."+extension;
									  if((dateFormat!=null) && (claimId!=null) && (fileSerialno != 0) && (extension!=null)){
										  filePart3.write(filePath);
										  BufferedInputStream bis=null;
					                		try{
					                			  File uploadedFile = new File(filePath);
						                          bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
						                          if(moduleId==1 || moduleId==2 || moduleId==4){
						                        	  client.storeFile(ftpUploadPath+"/"+fileformat, bis);
						                          }
					                         }finally { 
					                        	 if (bis != null){ 
					                        		 try { 
					                        			 bis.close(); 
					                        		 }catch(IOException ioe) {
					                        			 ioe.printStackTrace(); 
					                        		 } 
					                        	 }
					                         }
					                		preAuthXMLBean = new PreAuthXMLBean();
					                		preAuthXMLBean.setFilename(fileformat);
					                		preAuthXMLBean.setClaimID(claimId);
					                		preAuthXMLBean.setPreappno(preauthAppNo);
					                		preAuthXMLBean.setTypeOfDoc(typeofdoc3);
					                		preAuthXMLBean.setMaindocid(maindocid);
					                		preAuthXMLBean.setSubdocid(subdocid);
					                		preAuthXMLBean.setActualfilename(fileName);
					                		preAuthXMLBean.setPreauthflag(preauthflag);
					                		preAuthList.add(preAuthXMLBean); 
					                	}
									}
					            }
					            
					            final Part filePart4 = request.getPart("filedoc4"); 
					            if(filePart4 !=null){
								String fileName = Util.extractFileName(filePart4);
								if(fileName!=null && !fileName.equals("")){
									String filePath = uploadPath + File.separator + fileName;
									Date date = new Date();                                                 
						   		    String dateFormat = dfm.format(date);
			                		int fileSerialno=4;
			                		String extension = Util.fileExtenssion(fileName);
					                fileformat=dateFormat+"_"+claimId+"_"+fileSerialno+"."+extension;
									  if((dateFormat!=null) && (claimId!=null) && (fileSerialno != 0) && (extension!=null)){
										  filePart4.write(filePath);
										  BufferedInputStream bis=null;
					                		try{
					                			  File uploadedFile = new File(filePath);
						                          bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
						                          if(moduleId==1 || moduleId==2 || moduleId==4){
						                        	  client.storeFile(ftpUploadPath+"/"+fileformat, bis);
						                          }
					                         }finally { 
					                        	 if (bis != null){ 
					                        		 try { 
					                        			 bis.close(); 
					                        		 }catch(IOException ioe) {
					                        			 ioe.printStackTrace(); 
					                        		 } 
					                        	 }
					                         }
					                		preAuthXMLBean = new PreAuthXMLBean();
					                		preAuthXMLBean.setFilename(fileformat);
					                		preAuthXMLBean.setClaimID(claimId);
					                		preAuthXMLBean.setPreappno(preauthAppNo);
					                		preAuthXMLBean.setTypeOfDoc(typeofdoc4);
					                		preAuthXMLBean.setMaindocid(maindocid);
					                		preAuthXMLBean.setSubdocid(subdocid);
					                		preAuthXMLBean.setActualfilename(fileName);
					                		preAuthXMLBean.setPreauthflag(preauthflag);
					                		preAuthList.add(preAuthXMLBean); 
					                	}
									}
									
					            }
					            
					            final Part filePart5 = request.getPart("filedoc5"); 
					            if(filePart5 !=null){
								String fileName = Util.extractFileName(filePart5);
								if(fileName!=null && !fileName.equals("")){
									String filePath = uploadPath + File.separator + fileName;
									Date date = new Date();                                                 
						   		    String dateFormat = dfm.format(date);
			                		int fileSerialno=5;
			                		String extension = Util.fileExtenssion(fileName);
					                fileformat=dateFormat+"_"+claimId+"_"+fileSerialno+"."+extension;
									  if((dateFormat!=null) && (claimId!=null) && (fileSerialno != 0) && (extension!=null)){
										  filePart5.write(filePath);
										  BufferedInputStream bis=null;
					                		try{
					                			  File uploadedFile = new File(filePath);
						                          bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
						                          if(moduleId==1 || moduleId==2 || moduleId==4){
						                        	  client.storeFile(ftpUploadPath+"/"+fileformat, bis);
						                          }else{
						                        	  client.storeFile(ftpUploadPath+fileformat, bis);
						                          }
					                         }finally { 
					                        	 if (bis != null){ 
					                        		 try { 
					                        			 bis.close(); 
					                        		 }catch(IOException ioe) {
					                        			 ioe.printStackTrace(); 
					                        		 } 
					                        	 }
					                         }
					                		preAuthXMLBean = new PreAuthXMLBean();
					                		preAuthXMLBean.setFilename(fileformat);
					                		preAuthXMLBean.setClaimID(claimId);
					                		preAuthXMLBean.setPreappno(preauthAppNo);
					                		preAuthXMLBean.setTypeOfDoc(typeofdoc5);
					                		preAuthXMLBean.setMaindocid(maindocid);
					                		preAuthXMLBean.setSubdocid(subdocid);
					                		preAuthXMLBean.setActualfilename(fileName);
					                		preAuthXMLBean.setPreauthflag(preauthflag);
					                		preAuthList.add(preAuthXMLBean);
					                	}
									}
									 
					            }
						          
				          client.logout();
				          String xmlStringCorp=null;
				          String randomVal=null;
				          
				          
				          //22/NOV/2014 to integrate flow for INSURER COLLECTION
				         /* String xmlStringRet=null;
				            if(moduleId==4){
				            	if(preAuthList.size()>0){
				            		xmlStringRet=sendFaxDataResonseRet(preAuthList);
				            	}
				            	preAuthBean.setXmlString(xmlStringRet);
				            	preAuthBean.setKayakoId(kayakoId);
				            	preAuthBean.setUserId(userId);
				            	preAuthBean.setModuleUserId(moduleUserId);
				            	preAuthBean.setClaimFromSource(srcofclaim);
				            	preAuthBean.setReAuthFlag(reauthFlag);
				            	preAuthBean.setInsurerRemarks(insurerRemarks);
				            	preAuthBean.setInsurerReply(insurerReply);
				            	randomVal=Util.generateSessionKey(11);
				            	preAuthBean.setRandomVal(randomVal);
				            	preAuthBean= SendToInsurerManager.insertIAuthDocumentTypesInsurerRetDenial(preAuthBean, moduleId);
				            	if(preAuthBean.getNumberUpdates()>0){
				            		preAuthBean.setUserId(userId);
				            		preAuthBean.setClaimId(Integer.parseInt(claimId));
				            		
				            		if(Integer.parseInt(insurerReply)==1 && reauthFlag==0){
				            			preAuthBean.setClaimStatusId(Constants.insurerokreauthflagzerostatusDen);
				            		}else if(Integer.parseInt(insurerReply)==1 && reauthFlag>0){
				            			preAuthBean.setClaimStatusId(Constants.insurerokreauthflagngtzerostatusDen);
				            		}else if((Integer.parseInt(insurerReply)==2 || Integer.parseInt(insurerReply)==3) && reauthFlag==0){
				            			preAuthBean.setClaimStatusId(Constants.insurernotokreauthflagzerostatusDen);
				            		}else if((Integer.parseInt(insurerReply)==2 || Integer.parseInt(insurerReply)==3) && reauthFlag>0){
				            			preAuthBean.setClaimStatusId(Constants.insurernotokreauthflagngtzerostatusDen);
				            		}
				            		preAuthBean.setModuleId(moduleId);
				            		preAuthBean=SendToInsurerManager.updateIAuthPreAuthClaims(preAuthBean,1);
				            		request.setAttribute("claimId", claimId);
				            		request.setAttribute("ccnUnique", ccnUnique);
				            		preAuthBean=SendToInsurerManager.deleteSesionIdInsurerAuthRet(randomVal,moduleId);
				            		getServletContext().getRequestDispatcher("/docuploadsuccess.jsp").forward(request, response);
				            	}else{
				            		request.setAttribute("claimId", claimId);
				            		request.setAttribute("ccnUnique", ccnUnique);
				            		getServletContext().getRequestDispatcher("/docuploaderror.jsp").forward(request, response);
				            	}
				            }*/
				            
				            //END
				            if(moduleId==1 || moduleId==2 || moduleId==4){
				            	if(preAuthList.size()>0){
				            		xmlStringCorp=sendFaxDataResonseCorp(preAuthList);
				            		preAuthBean.setTotalNoDocs(preAuthList.size());
				            	}
				            	preAuthBean.setXmlString(xmlStringCorp);
				            	preAuthBean.setUserName(userName);
					            preAuthBean.setClaimId(Integer.parseInt(claimId));
					            preAuthBean.setPolicyId(Integer.parseInt(policyId));
					            preAuthBean.setCardid(cardid);
					            preAuthBean.setCardHolderName(cardHolderName);
					            preAuthBean.setSelfName(selfName);
					            preAuthBean.setPolicyNo(policyNo);
					            preAuthBean.setClaimFromSource(srcofclaim);
					            preAuthBean.setReAuthFlag(reauthFlag);
					            preAuthBean.setPatAddress(patAddress);
					            preAuthBean.setPatEmail(patEmail);
					            preAuthBean.setPatLandLineNo(patLandLineNo);
					            preAuthBean.setPolicyHolderName(policyHolderName);
					            preAuthBean.setInsuredId(insuredId);
					            preAuthBean.setKayakoId(kayakoId);
					            randomVal=Util.generateSessionKey(11);
				            	preAuthBean.setRandomVal(randomVal);
				            	preAuthBean.setInsurerReply(insurerReply);
				            	preAuthBean.setInsurerRemarks(insurerRemarks);
				            	SimpleDateFormat dateformat=new SimpleDateFormat("dd-MMM-yyyy");
								Date curentDate = new Date();
								String collectionDate=dateformat.format(curentDate);
				            	preAuthBean.setCollectionDate(collectionDate);
				            	preAuthBean= SendToInsurerManager.insertIAuthDocumentTypesInsurerDenialCorp(preAuthBean, moduleId);
				            	if(preAuthBean.getClaimUpdateCNT()>0){
				            		preAuthBean.setUserId(userId);
				            		preAuthBean=SendToInsurerManager.updateIAuthPreAuthClaims(preAuthBean,1);
				            		request.setAttribute("claimId", claimId);
				            		request.setAttribute("ccnUnique", ccnUnique);
				            		getServletContext().getRequestDispatcher("/docuploadsuccess.jsp").forward(request, response);
				            	}else{
				            		request.setAttribute("claimId", claimId);
				            		request.setAttribute("ccnUnique", ccnUnique);
				            		getServletContext().getRequestDispatcher("/docuploaderror.jsp").forward(request, response);
				            	}
				            }
			
				        }catch (Exception e) {
				            e.printStackTrace();
				            logger.error(e,e);
				        }finally{
				        	if(client!=null){
				        		client=null;
				        	}
				        }
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			//System.out.println("JSESSIONID="+cookie.getValue());
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
		    		session.invalidate();
		    	}
				 response.sendRedirect("./index.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				logger.error(e);
			}
	
	}
	
	
	public String sendFaxDataResonseRet(List<PreAuthXMLBean> fmb) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	breakupElement = doc.createElement("docsupload");
        	doc.appendChild(breakupElement);
        	
		if(fmb!=null && fmb.size()>0){
			    for(int i=0;i<fmb.size();i++){
			    	Element nodeElement = doc.createElement("node");
		        	breakupElement.appendChild(nodeElement);
					// CLAIMBREAKUP
					Element  preappnoElement= doc.createElement("preappno");
					preappnoElement.appendChild(doc.createTextNode(fmb.get(i).getPreappno()));
					nodeElement.appendChild(preappnoElement);
					
					Element claimIDElement = doc.createElement("claimID");
					claimIDElement.appendChild(doc.createTextNode(String.valueOf(fmb.get(i).getClaimID())));
					nodeElement.appendChild(claimIDElement);
					
					Element preauthflagElement = doc.createElement("preauthflag");
					preauthflagElement.appendChild(doc.createTextNode("P"));
					nodeElement.appendChild(preauthflagElement);
					
					Element filenameElement = doc.createElement("filename");
					filenameElement.appendChild(doc.createTextNode(fmb.get(i).getFilename()));
					nodeElement.appendChild(filenameElement);
					
					Element typeOfDocElement = doc.createElement("typeOfDoc");
					typeOfDocElement.appendChild(doc.createTextNode(fmb.get(i).getTypeOfDoc()));
					nodeElement.appendChild(typeOfDocElement);
					
					Element moduleElement = doc.createElement("module");
					moduleElement.appendChild(doc.createTextNode("Preauth"));
					nodeElement.appendChild(moduleElement);
				}
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(resultAsString);
		transformer.transform(source, result);
        }catch(Exception e){
        	 logger.error(e);
        }finally{
        	 docFactory = null;
    		 docBuilder = null;
    		 doc = null;
    		 breakupElement = null;
        }
        return resultAsString.toString();
	}
	
	public String sendFaxDataResonseCorp(List<PreAuthXMLBean> fmb) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	breakupElement = doc.createElement("docsupload");
        	doc.appendChild(breakupElement);
        	
		if(fmb!=null && fmb.size()>0){
		    for(int i=0;i<fmb.size();i++){
		    	Element nodeElement = doc.createElement("node");
	        	breakupElement.appendChild(nodeElement);
				
	        	Element  actualfilenameElement= doc.createElement("actualfilename");
	        	actualfilenameElement.appendChild(doc.createTextNode(fmb.get(i).getActualfilename()));
				nodeElement.appendChild(actualfilenameElement);
				
				Element filenameElement = doc.createElement("filename");
				filenameElement.appendChild(doc.createTextNode(fmb.get(i).getFilename()));
				nodeElement.appendChild(filenameElement);
	        	
				Element  maindocidElement= doc.createElement("maindocid");
				maindocidElement.appendChild(doc.createTextNode(fmb.get(i).getMaindocid()));
				nodeElement.appendChild(maindocidElement);
				
				Element subdocidElement = doc.createElement("subdocid");
				subdocidElement.appendChild(doc.createTextNode(String.valueOf(fmb.get(i).getSubdocid())));
				nodeElement.appendChild(subdocidElement);
				
				Element typeOfDocElement = doc.createElement("doctypeid");
				typeOfDocElement.appendChild(doc.createTextNode(fmb.get(i).getTypeOfDoc()));
				nodeElement.appendChild(typeOfDocElement);
				
				
			}
	
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(resultAsString);
		transformer.transform(source, result);
        }catch(Exception e){
        	 logger.error(e);
        }finally{
        	 docFactory = null;
    		 docBuilder = null;
    		 doc = null;
    		 breakupElement = null;
        }
        return resultAsString.toString();
	}

}
