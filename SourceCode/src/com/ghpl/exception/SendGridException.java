package com.ghpl.exception;

public class SendGridException extends Exception {
    public SendGridException(Exception e) {
        super(e);
    }
}
