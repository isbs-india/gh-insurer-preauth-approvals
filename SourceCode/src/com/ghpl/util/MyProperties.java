/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghpl.util;

import java.util.Properties;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 *
 * @author Surya Kiran
 */
public class MyProperties {
    Logger logger =Logger.getLogger("MyProperties");
    public String getMyProperties(String PropName)
    {
        Properties prop = new Properties();
        try {
               logger.info("to Get "+PropName+" Properties");
              // System.out.println("======    "+MyProperties.class.getName());
                prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));          
                return prop.getProperty(PropName);
 
    	} catch (IOException ex) {
    		logger.error("Error DBConn - getting "+PropName+" Properties Set IO Exception"); 
    		logger.error("IN " + ex.getMessage());
                return "";
        }
    }     
    
   /* public void putMyProperties()
    {
        Properties prop = new Properties();
        try {
        		logger.info("Error DBConn - getting Properties Set IO Exception"); 
                prop.setProperty("DBIP", "192.168.100.71");
                prop.setProperty("SID", "orcl");
                prop.setProperty("DBUser", "marble");
                prop.setProperty("DBPwd", "Ma3eMa3e$");
 
                //save properties to project root folder
                prop.store(new FileOutputStream("config.properties"), null);                                        
                
    	} catch (IOException ex) {
    		logger.error("Error DBConn - getting Properties Set IO Exception"); 
    		logger.error("OUT " + ex.getMessage());
        }
    }*/
}
