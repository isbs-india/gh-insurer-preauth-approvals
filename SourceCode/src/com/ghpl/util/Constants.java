package com.ghpl.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;

public class Constants {
	public static final String JNDI_KEY="java:comp/env/jdbc/testDB";
	//letter names
	public static final String HEADER_IMAGE="/images/ClaimLettersHeader2.jpg";
	public static final Rectangle pagesizeA4 =PageSize.A4;
	public static final Rectangle pagelandscape=PageSize.A4_LANDSCAPE;
	public static final String NewLine = "\n";
	public static final Font fontUnderlineBoldParaText1 = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final Font fontDateText= new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontGeneralText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontPintText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontTableCellText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontNoteText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontItalicParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
	public static final Font fontItalicBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC, BaseColor.BLACK);
	public static final Font fontItalicUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontItalicBoldUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final String RET="RET";
	public static final int statusId =43;
	public static final int medicoAuthriseRetstatusId =44;
	public static final int medicoQueryRetstatusId =45;
	public static final int medicoCancelRetstatusId =117;
	public static final int medicoCloseRetstatusId =116;
	public static final int medicoDenyRetstatusId =46;
	public static final int ghplpreAuthNo=0;
	public static final int IAUTH_Approve_INsurer = 1;
	public static final int IAUTH_DENY_INsurer = 2;
	public static final String IAUTH_1 = "GHPL";
	public static final String IAUTH_2 = "AB";
	public static final String IAUTH_3 = "VENUS";
	public static final String IAUTH_4 = "UNITED";
	public static final String IAUTH_5 = "NEW INDIA";
	public static final String IAUTH_6 = "ORIENTAL";
	public static final String IAUTH_7 = "NATIONAL";
	public static final String IAUTH_8 = "NIC NAV";
	
	//LIVE documents path 
	/*public static final String uploadGHPL = "/LIVE/CORP/GHPL/collectiondoc/2/7/";
	public static final String uploadAB = "/LIVE/CORP/AB/collectiondoc/2/7/";
	public static final String uploadVENUS = "/LIVE/CORP/VENUS/collectiondoc/2/7/";
	public static final String uploadGHPLDischarge = "/LIVE/CORP/GHPL/collectiondoc/2/3/";
	public static final String uploadABDischarge = "/LIVE/CORP/AB/collectiondoc/2/3/";
	public static final String uploadVENUSDischarge = "/LIVE/CORP/VENUS/collectiondoc/2/3/";
	public static final String uploadUIA_P = "/LIVE/RETAIL/UIA/P/";
	public static final String uploadUIA_Q = "/LIVE/RETAIL/UIA/Q/";
	public static final String uploadUIA_K = "/LIVE/RETAIL/UIA/K/";
	public static final String uploadUIA_EQ = "/LIVE/RETAIL/UIA/EQ/";
	public static final String uploadUIA_InsurerLetters = "/LIVE/RETAIL/UIA/InsurerLetters/";
	public static final String uploadUIA_E = "/LIVE/RETAIL/UIA/E/";
	public static final String uploadUIA_UPDOC = "/LIVE/RETAIL/UIA/UPDOC/";
	public static final String uploadNIC_P = "/LIVE/RETAIL/NIC/P/";
	public static final String uploadNIC_Q = "/LIVE/RETAIL/NIC/Q/";
	public static final String uploadNIC_K = "/LIVE/RETAIL/NIC/K/";
	public static final String uploadNIC_EQ = "/LIVE/RETAIL/NIC/EQ/";
	public static final String uploadNIC_InsurerLetters = "/LIVE/RETAIL/NIC/InsurerLetters/";
	public static final String uploadNIC_E = "/LIVE/RETAIL/NIC/E/";
	public static final String uploadNIC_UPDOC = "/LIVE/RETAIL/NIC/UPDOC/";
	public static final String uploadNIA_P = "/LIVE/RETAIL/NIA/P/";
	public static final String uploadNIA_Q = "/LIVE/RETAIL/NIA/Q/";
	public static final String uploadNIA_K = "/LIVE/RETAIL/NIA/K/";
	public static final String uploadNIA_EQ = "/LIVE/RETAIL/NIA/EQ/";
	public static final String uploadNIA_InsurerLetters = "/LIVE/RETAIL/NIA/InsurerLetters/";
	public static final String uploadNIA_E = "/LIVE/RETAIL/NIA/E/";
	public static final String uploadNIA_UPDOC = "/LIVE/RETAIL/NIA/UPDOC/";
	
	public static final String UIA_FTP_DOC_PATH = "/LIVE/RETAIL/UIA/";
    public static final String NIC_FTP_DOC_PATH = "/LIVE/RETAIL/NIC/";
    public static final String NAV_FTP_DOC_PATH = "/LIVE/RETAIL/NAV/";
    public static final String ORH_FTP_DOC_PATH = "/LIVE/RETAIL/ORH/";
    public static final String NIA_FTP_DOC_PATH = "/LIVE/RETAIL/NIA/";
	
	public static final String uploadNAV_P = "/LIVE/RETAIL/NAV/P/";
	public static final String uploadNAV_Q = "/LIVE/RETAIL/NAV/Q/";
	public static final String uploadNAV_K = "/LIVE/RETAIL/NAV/K/";
	public static final String uploadNAV_EQ = "/LIVE/RETAIL/NAV/EQ/";
	
	public static final String uploadNAV_InsurerLetters = "/LIVE/RETAIL/NAV/InsurerLetters/";
	public static final String uploadNAV_E = "/LIVE/RETAIL/NAV/E/";
	public static final String uploadNAV_UPDOC = "/LIVE/RETAIL/NAV/UPDOC/";
	public static final String uploadORH_P = "/LIVE/RETAIL/ORH/P/";
	public static final String uploadORH_Q = "/LIVE/RETAIL/ORH/Q/";
	public static final String uploadORH_K = "/LIVE/RETAIL/ORH/K/";
	public static final String uploadORH_EQ = "/LIVE/RETAIL/ORH/EQ/";
	public static final String uploadORH_InsurerLetters = "/LIVE/RETAIL/ORH/InsurerLetters/";
	public static final String uploadORH_E = "/LIVE/RETAIL/ORH/E/";
	public static final String uploadORH_UPDOC = "/LIVE/RETAIL/ORH/UPDOC/";
	
	
	public static final String FTP_IAUTH_PATH_GHPL="/LIVE/CORP/GHPL/collectiondoc/";
	public static final String FTP_IAUTH_PATH_AB="/LIVE/CORP/AB/collectiondoc/";
	public static final String FTP_IAUTH_PATH_VENUS="/LIVE/CORP/VENUS/collectiondoc/";*/
	
	
	    //deve documents path 
		/*public static final String uploadGHPL = "/TEST/CORP/GHPL/collectiondoc/2/7/";
		public static final String uploadAB = "/TEST/CORP/AB/collectiondoc/2/7/";
		public static final String uploadVENUS = "/TEST/CORP/VENUS/collectiondoc/2/7/";
		
		public static final String uploadGHPLDischarge = "/TEST/CORP/GHPL/collectiondoc/2/3/";
		public static final String uploadABDischarge = "/TEST/CORP/AB/collectiondoc/2/3/";
		public static final String uploadVENUSDischarge = "/TEST/CORP/VENUS/collectiondoc/2/3/";
		
		public static final String uploadUIA_P = "/TEST/RETAIL/UIA/P/";
		public static final String uploadUIA_Q = "/TEST/RETAIL/UIA/Q/";
		public static final String uploadUIA_EQ = "/TEST/RETAIL/UIA/EQ/";
		public static final String uploadUIA_InsurerLetters = "/TEST/RETAIL/UIA/InsurerLetters/";
		public static final String uploadUIA_E = "/TEST/RETAIL/UIA/E/";
		public static final String uploadUIA_UPDOC = "/TEST/RETAIL/UIA/UPDOC/";
		
		public static final String uploadNIC_P = "/TEST/RETAIL/NIC/P/";
		public static final String uploadNIC_Q = "/TEST/RETAIL/NIC/Q/";
		public static final String uploadNIC_EQ = "/TEST/RETAIL/NIC/EQ/";
		public static final String uploadNIC_InsurerLetters = "/TEST/RETAIL/NIC/InsurerLetters/";
		public static final String uploadNIC_E = "/TEST/RETAIL/NIC/E/";
		public static final String uploadNIC_UPDOC = "/TEST/RETAIL/NIC/UPDOC/";
		
		public static final String uploadNIA_P = "/TEST/RETAIL/NIA/P/";
		public static final String uploadNIA_Q = "/TEST/RETAIL/NIA/Q/";
		public static final String uploadNIA_EQ = "/TEST/RETAIL/NIA/EQ/";
		public static final String uploadNIA_InsurerLetters = "/TEST/RETAIL/NIA/InsurerLetters/";
		public static final String uploadNIA_E = "/TEST/RETAIL/NIA/E/";
		public static final String uploadNIA_UPDOC = "/TEST/RETAIL/NIA/UPDOC/";
		
		public static final String uploadNAV_P = "/TEST/RETAIL/NAV/P/";
		public static final String uploadNAV_Q = "/TEST/RETAIL/NAV/Q/";
		public static final String uploadNAV_EQ = "/TEST/RETAIL/NAV/EQ/";
		public static final String uploadNAV_InsurerLetters = "/TEST/RETAIL/NAV/InsurerLetters/";
		public static final String uploadNAV_E = "/TEST/RETAIL/NAV/E/";
		public static final String uploadNAV_UPDOC = "/TEST/RETAIL/NAV/UPDOC/";
		
		public static final String uploadORH_P = "/TEST/RETAIL/ORH/P/";
		public static final String uploadORH_Q = "/TEST/RETAIL/ORH/Q/";
		public static final String uploadORH_EQ = "/TEST/RETAIL/ORH/EQ/";
		public static final String uploadORH_InsurerLetters = "/TEST/RETAIL/ORH/InsurerLetters/";
		public static final String uploadORH_E = "/TEST/RETAIL/ORH/E/";
		public static final String uploadORH_UPDOC = "/TEST/RETAIL/ORH/UPDOC/";
		
		public static final String UIA_FTP_DOC_PATH = "/TEST/RETAIL/UIA/";
		public static final String NIC_FTP_DOC_PATH = "/TEST/RETAIL/NIC/";
		public static final String NAV_FTP_DOC_PATH = "/TEST/RETAIL/NAV/";
		public static final String ORH_FTP_DOC_PATH = "/TEST/RETAIL/ORH/";
		public static final String NIA_FTP_DOC_PATH = "/TEST/RETAIL/NIA/";
		
		public static final String FTP_IAUTH_PATH_GHPL="TEST/CORP/GHPL/collectiondoc/";
		public static final String FTP_IAUTH_PATH_AB="TEST/CORP/AB/collectiondoc/";
		public static final String FTP_IAUTH_PATH_VENUS="TEST/CORP/VENUS/collectiondoc/"; */
		
		
		
		/*public static final String uploadGHPL = "/homes/docproxy/IAUTHDOCS/CORP/GHPL/collectiondoc/2/7/";
		public static final String uploadAB = "/homes/docproxy/IAUTHDOCS/CORP/AB/collectiondoc/2/7/";
	    public static final String uploadVENUS = "/homes/docproxy/IAUTHDOCS/CORP/VENUS/collectiondoc/2/7/";

	    public static final String uploadGHPLDischarge = "/homes/docproxy/IAUTHDOCS/CORP/GHPL/collectiondoc/2/3/";
	    public static final String uploadABDischarge = "/homes/docproxy/IAUTHDOCS/CORP/AB/collectiondoc/2/3/";
	    public static final String uploadVENUSDischarge = "/homes/docproxy/IAUTHDOCS/CORP/VENUS/collectiondoc/2/3/";

        public static final String uploadUIA_P = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/P/";
		public static final String uploadUIA_Q = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/Q/";
		public static final String uploadUIA_K = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/K/";
		public static final String uploadUIA_EQ = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/EQ/";
		public static final String uploadUIA_InsurerLetters = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/InsurerLetters/";
		public static final String uploadUIA_E = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/E/";
		public static final String uploadUIA_UPDOC = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/UPDOC/";

        public static final String uploadNIC_P = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/P/";
		public static final String uploadNIC_Q = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/Q/";
		public static final String uploadNIC_K = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/K/";
		public static final String uploadNIC_EQ = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/EQ/";
		public static final String uploadNIC_InsurerLetters = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/InsurerLetters/";
		public static final String uploadNIC_E = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/E/";
		public static final String uploadNIC_UPDOC = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/UPDOC/";

		public static final String uploadNIA_P = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/P/";
		public static final String uploadNIA_Q = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/Q/";
		public static final String uploadNIA_K = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/K/";
		public static final String uploadNIA_EQ = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/EQ/";
		public static final String uploadNIA_InsurerLetters = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/InsurerLetters/";
		public static final String uploadNIA_E = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/E/";
	    public static final String uploadNIA_UPDOC = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/UPDOC/";

		public static final String uploadNAV_P = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/P/";
		public static final String uploadNAV_Q = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/Q/";
		public static final String uploadNAV_K = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/K/";
		public static final String uploadNAV_EQ = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/EQ/";
		public static final String uploadNAV_InsurerLetters = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/InsurerLetters/";
		public static final String uploadNAV_E = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/E/";
		public static final String uploadNAV_UPDOC = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/UPDOC/";

		public static final String uploadORH_P = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/P/";
		public static final String uploadORH_Q = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/Q/";
		public static final String uploadORH_K = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/K/";
		public static final String uploadORH_EQ = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/EQ/";
		public static final String uploadORH_InsurerLetters = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/InsurerLetters/";
		public static final String uploadORH_E = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/E/";
		public static final String uploadORH_UPDOC = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/UPDOC/";
		
		
		
		
		public static final String UIA_FTP_DOC_PATH = "/homes/docproxy/IAUTHDOCS/RETAIL/UIA/";
		public static final String NIC_FTP_DOC_PATH = "/homes/docproxy/IAUTHDOCS/RETAIL/NIC/";
		public static final String NAV_FTP_DOC_PATH = "/homes/docproxy/IAUTHDOCS/RETAIL/NAV/";
		public static final String ORH_FTP_DOC_PATH = "/homes/docproxy/IAUTHDOCS/RETAIL/ORH/";
		public static final String NIA_FTP_DOC_PATH = "/homes/docproxy/IAUTHDOCS/RETAIL/NIA/";
		
		public static final String FTP_IAUTH_PATH_GHPL="homes/docproxy/IAUTHDOCS/CORP/GHPL/collectiondoc/";
		public static final String FTP_IAUTH_PATH_AB="homes/docproxy/IAUTHDOCS/CORP/AB/collectiondoc/";
		public static final String FTP_IAUTH_PATH_VENUS="homes/docproxy/IAUTHDOCS/CORP/VENUS/collectiondoc/";*/
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		public static final String uploadGHPL = "/LIVE/CORP/GHPL/collectiondoc/2/7/";
		public static final String uploadAB = "/LIVE/CORP/AB/collectiondoc/2/7/";
	    public static final String uploadVENUS = "/LIVE/CORP/VENUS/collectiondoc/2/7/";

	    public static final String uploadGHPLDischarge = "/LIVE/CORP/GHPL/collectiondoc/2/3/";
	    public static final String uploadABDischarge = "/LIVE/CORP/AB/collectiondoc/2/3/";
	    public static final String uploadVENUSDischarge = "/LIVE/CORP/VENUS/collectiondoc/2/3/";
	  

        public static final String uploadUIA_P = "/LIVE/RETAIL/UIA/P/";
		public static final String uploadUIA_Q = "/LIVE/RETAIL/UIA/Q/";
		public static final String uploadUIA_K = "/LIVE/RETAIL/UIA/K/";
		public static final String uploadUIA_EQ = "/LIVE/RETAIL/UIA/EQ/";
		public static final String uploadUIA_InsurerLetters = "/LIVE/RETAIL/UIA/InsurerLetters/";
		public static final String uploadUIA_E = "/LIVE/RETAIL/UIA/E/";
		public static final String uploadUIA_UPDOC = "/LIVE/RETAIL/UIA/UPDOC/";
		
        public static final String uploadNIC_P = "/LIVE/RETAIL/NIC/P/";
		public static final String uploadNIC_Q = "/LIVE/RETAIL/NIC/Q/";
		public static final String uploadNIC_K = "/LIVE/RETAIL/NIC/K/";
		public static final String uploadNIC_EQ = "/LIVE/RETAIL/NIC/EQ/";
		public static final String uploadNIC_InsurerLetters = "/LIVE/RETAIL/NIC/InsurerLetters/";
		public static final String uploadNIC_E = "/LIVE/RETAIL/NIC/E/";
		public static final String uploadNIC_UPDOC = "/LIVE/RETAIL/NIC/UPDOC/";
		

		public static final String uploadNIA_P = "/LIVE/RETAIL/NIA/P/";
		public static final String uploadNIA_Q = "/LIVE/RETAIL/NIA/Q/";
		public static final String uploadNIA_K = "/LIVE/RETAIL/NIA/K/";
		public static final String uploadNIA_EQ = "/LIVE/RETAIL/NIA/EQ/";
		public static final String uploadNIA_InsurerLetters = "/LIVE/RETAIL/NIA/InsurerLetters/";
		public static final String uploadNIA_E = "/LIVE/RETAIL/NIA/E/";
	    public static final String uploadNIA_UPDOC = "/LIVE/RETAIL/NIA/UPDOC/";

		public static final String uploadNAV_P = "/LIVE/RETAIL/NAV/P/";
		public static final String uploadNAV_Q = "/LIVE/RETAIL/NAV/Q/";
		public static final String uploadNAV_K = "/LIVE/RETAIL/NAV/K/";
		public static final String uploadNAV_EQ = "/LIVE/RETAIL/NAV/EQ/";
		public static final String uploadNAV_InsurerLetters = "/LIVE/RETAIL/NAV/InsurerLetters/";
		public static final String uploadNAV_E = "/LIVE/RETAIL/NAV/E/";
		public static final String uploadNAV_UPDOC = "/LIVE/RETAIL/NAV/UPDOC/";

		public static final String uploadORH_P = "/LIVE/RETAIL/ORH/P/";
		public static final String uploadORH_Q = "/LIVE/RETAIL/ORH/Q/";
		public static final String uploadORH_K = "/LIVE/RETAIL/ORH/K/";
		public static final String uploadORH_EQ = "/LIVE/RETAIL/ORH/EQ/";
		public static final String uploadORH_InsurerLetters = "/LIVE/RETAIL/ORH/InsurerLetters/";
		public static final String uploadORH_E = "/LIVE/RETAIL/ORH/E/";
		public static final String uploadORH_UPDOC = "/LIVE/RETAIL/ORH/UPDOC/";
		
		public static final String UIA_FTP_DOC_PATH = "/LIVE/RETAIL/UIA/";
		public static final String NIC_FTP_DOC_PATH = "/LIVE/RETAIL/NIC/";
		public static final String NAV_FTP_DOC_PATH = "/LIVE/RETAIL/NAV/";
		public static final String ORH_FTP_DOC_PATH = "/LIVE/RETAIL/ORH/";
		public static final String NIA_FTP_DOC_PATH = "/LIVE/RETAIL/NIA/";
		
		public static final String FTP_IAUTH_PATH_GHPL="LIVE/CORP/GHPL/collectiondoc/";
		public static final String FTP_IAUTH_PATH_AB="LIVE/CORP/AB/collectiondoc/";
		public static final String FTP_IAUTH_PATH_VENUS="LIVE/CORP/VENUS/collectiondoc/";
				
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*public static final String FTP_IAUTH_PATH_GHPL="LIVE/CORP/GHPL/collectiondoc/";
		public static final String FTP_IAUTH_PATH_AB="LIVE/CORP/AB/collectiondoc/";
		public static final String FTP_IAUTH_PATH_VENUS="LIVE/CORP/VENUS/collectiondoc/";*/
	
	
	
	/*public static final String FTP_IAUTH_PATH_UIA="/TEST/RETAIL/UIA/";
	public static final String FTP_IAUTH_PATH_NIC="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_NAV="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_ORH="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_NIA="TEST/CORP/VENUS/collectiondoc/";*/
	
	public static final String MAIL_INSURER_SUBJECT="REQUEST FOR AUTHORIZATION APPROVAL CLAIMID: ";
	public static final String MAIL_INSURER_TEXT="Dear Sir / Madam, <br>Greetings!<br><br>This is an intimation for the approval of the request for Preauthorization that are awaiting your<br> Approval.<br><br>You can to view the complete details of the preauthorization request for your reference by <br>logging on to the Insurer portal with the Username and Password provided to you.<br><br>URL:<a href=\"http://web.ghpltpa.com:8080/GHWebLogin/index.php\">http://web.ghpltpa.com:8080/GHWebLogin/index.php</a> <br><br>Please write back to us in case of any clarification. <br> <br> Thanks and Regards, <br>Good Health Preauthorization Team  <br> Ph: 1800 425 3232 / 1860 425 3232 <br>Preauth email: <a href=\"mailto:preauth@ghpltpa.com\">preauth@ghpltpa.com</a> ";

	public static final String MAIL_INSURER_SUBJECT_DENIAL="PREAUTH DENIAL APPROVAL FOR CLAIMID: ";
	public static final String MAIL_INSURER_TEXT_DENIAL="Dear Sir / Madam, <br><br>Please find the supporting documents for your kind perusal and confirmation for further processing of the claim. <br><br> Medico Remarks: <medicoRemarks> <br> <br> Regards, <br> Preauth Team <br>GHPL";
	
	public static final String DOWNLOAD_REDIRECT_JSP_UIIC="UIIC_INSPREAUTH_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_BHAXA="BharatiAxa_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_RGIL="RelInsPreAuth_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_ITGI="ITGI_INSPREAUTH_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_RELIGARE="Religaredownloadxls.jsp";
	//santhosh added start
	public static final String DOWNLOAD_REDIRECT_JSP_NIA="NIA_INSPREAUTH_Downloadxls.jsp";
	//santhosh added end
	public static final String DOWNLOAD_REDIRECT_JSP_DEFAULT="default_ins.jsp";
	
	public static final String documentnotfound="file not found";
	
	public static final String maindocid = "2";
	public static final String subdocid = "7";
	public static final String maindocidDischarge = "2";
	public static final String subdocidDischarge = "3";
	
	//STATUS FOR INSURER APPROVAL COLLECTION
	public static final int insurerokreauthflagzerostatus=174;
	public static final int insurerokreauthflagngtzerostatus=175;
	public static final int insurernotokreauthflagzerostatus=163;
	public static final int insurernotokreauthflagngtzerostatus=164;
	
	//STATUS FOR INSURER DENIAL COLLECTION
	public static final int insurerokreauthflagzerostatusDen=176;
	public static final int insurerokreauthflagngtzerostatusDen=177;
	public static final int insurernotokreauthflagzerostatusDen=170;
	public static final int insurernotokreauthflagngtzerostatusDen=171;
	
	//StatusID for sending back to medico from SenttoInsurer
	public static final int backToMedicoStatusId=182;
	
}
