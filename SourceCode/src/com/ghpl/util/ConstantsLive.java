package com.ghpl.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;

public class ConstantsLive {
	public static final String JNDI_KEY="java:comp/env/jdbc/testDB";
	//letter names
	public static final String HEADER_IMAGE="/images/ClaimLettersHeader2.jpg";
	public static final Rectangle pagesizeA4 =PageSize.A4;
	public static final Rectangle pagelandscape=PageSize.A4_LANDSCAPE;
	public static final String NewLine = "\n";
	public static final Font fontUnderlineBoldParaText1 = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final Font fontDateText= new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontGeneralText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontPintText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontTableCellText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontNoteText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontItalicParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
	public static final Font fontItalicBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC, BaseColor.BLACK);
	public static final Font fontItalicUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontItalicBoldUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final String RET="RET";
	public static final int statusId =43;
	public static final int medicoAuthriseRetstatusId =44;
	public static final int medicoQueryRetstatusId =45;
	public static final int medicoCancelRetstatusId =117;
	public static final int medicoCloseRetstatusId =116;
	public static final int medicoDenyRetstatusId =46;
	public static final int ghplpreAuthNo=0;
	public static final int IAUTH_Approve_INsurer = 1;
	public static final int IAUTH_DENY_INsurer = 2;
	public static final String IAUTH_1 = "GHPL";
	public static final String IAUTH_2 = "AB";
	public static final String IAUTH_3 = "VENUS";
	public static final String IAUTH_4 = "UNITED";
	public static final String IAUTH_5 = "NEW INDIA";
	public static final String IAUTH_6 = "ORIENTAL";
	public static final String IAUTH_7 = "NATIONAL";
	public static final String IAUTH_8 = "NIC NAV";
	
	//deve docments path 
	public static final String uploadGHPL = "/TEST/LIVE/CORP/GHPL/collectiondoc/2/7/";
	public static final String uploadAB = "/TEST/LIVE/CORP/AB/collectiondoc/2/7/";
	public static final String uploadVENUS = "/TEST/LIVE/CORP/VENUS/collectiondoc/2/7/";
	
	public static final String uploadGHPLDischarge = "/TEST/LIVE/CORP/GHPL/collectiondoc/2/3/";
	public static final String uploadABDischarge = "/TEST/LIVE/CORP/AB/collectiondoc/2/3/";
	public static final String uploadVENUSDischarge = "/TEST/LIVE/CORP/VENUS/collectiondoc/2/3/";
	
	public static final String uploadUIA_P = "/TEST/LIVE/RETAIL/UIA/P/";
	public static final String uploadUIA_Q = "/TEST/LIVE/RETAIL/UIA/Q/";
	public static final String uploadUIA_EQ = "/TEST/LIVE/RETAIL/UIA/EQ/";
	public static final String uploadUIA_InsurerLetters = "/TEST/LIVE/RETAIL/UIA/InsurerLetters/";
	public static final String uploadUIA_E = "/TEST/LIVE/RETAIL/UIA/E/";
	public static final String uploadUIA_UPDOC = "/TEST/LIVE/RETAIL/UIA/UPDOC/";
	
	public static final String uploadNIC_P = "/TEST/LIVE/RETAIL/NIC/P/";
	public static final String uploadNIC_Q = "/TEST/LIVE/RETAIL/NIC/Q/";
	public static final String uploadNIC_EQ = "/TEST/LIVE/RETAIL/NIC/EQ/";
	public static final String uploadNIC_InsurerLetters = "/TEST/LIVE/RETAIL/NIC/InsurerLetters/";
	public static final String uploadNIC_E = "/TEST/LIVE/RETAIL/NIC/E/";
	public static final String uploadNIC_UPDOC = "/TEST/LIVE/RETAIL/NIC/UPDOC/";
	
	public static final String uploadNIA_P = "/TEST/LIVE/RETAIL/NIA/P/";
	public static final String uploadNIA_Q = "/TEST/LIVE/RETAIL/NIA/Q/";
	public static final String uploadNIA_EQ = "/TEST/LIVE/RETAIL/NIA/EQ/";
	public static final String uploadNIA_InsurerLetters = "/TEST/LIVE/RETAIL/NIA/InsurerLetters/";
	public static final String uploadNIA_E = "/TEST/LIVE/RETAIL/NIA/E/";
	public static final String uploadNIA_UPDOC = "/TEST/LIVE/RETAIL/NIA/UPDOC/";
	
	public static final String uploadNAV_P = "/TEST/LIVE/RETAIL/NAV/P/";
	public static final String uploadNAV_Q = "/TEST/LIVE/RETAIL/NAV/Q/";
	public static final String uploadNAV_EQ = "/TEST/LIVE/RETAIL/NAV/EQ/";
	public static final String uploadNAV_InsurerLetters = "/TEST/LIVE/RETAIL/NAV/InsurerLetters/";
	public static final String uploadNAV_E = "/TEST/LIVE/RETAIL/NAV/E/";
	public static final String uploadNAV_UPDOC = "/TEST/LIVE/RETAIL/NAV/UPDOC/";
	
	public static final String uploadORH_P = "/TEST/LIVE/RETAIL/ORH/P/";
	public static final String uploadORH_Q = "/TEST/LIVE/RETAIL/ORH/Q/";
	public static final String uploadORH_EQ = "/TEST/LIVE/RETAIL/ORH/EQ/";
	public static final String uploadORH_InsurerLetters = "/TEST/LIVE/RETAIL/ORH/InsurerLetters/";
	public static final String uploadORH_E = "/TEST/LIVE/RETAIL/ORH/E/";
	public static final String uploadORH_UPDOC = "/TEST/LIVE/RETAIL/ORH/UPDOC/";
	
	public static final String UIA_FTP_DOC_PATH = "/TEST/LIVE/RETAIL/UIA/";
	public static final String NIC_FTP_DOC_PATH = "/TEST/LIVE/RETAIL/NIC/";
	public static final String NAV_FTP_DOC_PATH = "/TEST/LIVE/RETAIL/NAV/";
	public static final String ORH_FTP_DOC_PATH = "/TEST/LIVE/RETAIL/ORH/";
	public static final String NIA_FTP_DOC_PATH = "/TEST/LIVE/RETAIL/NIA/";
	
	public static final String FTP_IAUTH_PATH_GHPL="TEST/LIVE/CORP/GHPL/collectiondoc/";
	public static final String FTP_IAUTH_PATH_AB="TEST/LIVE/CORP/AB/collectiondoc/";
	public static final String FTP_IAUTH_PATH_VENUS="TEST/LIVE/CORP/VENUS/collectiondoc/";
	
	/*public static final String FTP_IAUTH_PATH_UIA="/TEST/RETAIL/UIA/";
	public static final String FTP_IAUTH_PATH_NIC="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_NAV="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_ORH="TEST/CORP/VENUS/collectiondoc/";
	public static final String FTP_IAUTH_PATH_NIA="TEST/CORP/VENUS/collectiondoc/";*/
	
	public static final String MAIL_INSURER_SUBJECT="AUTHORIZATION APPROVAL REQUEST FOR CLAIMID: ";
	public static final String MAIL_INSURER_TEXT="Dear Sir / Madam, <br><br>Please find the attached information sheet and supporting documents for your kind perusal and confirmation for further processing of the claim. <br> <br> Regards, <br> Preauth Team <br>GOOD HEALTH INSURANCE TPA LIMITED";
	
	public static final String MAIL_INSURER_SUBJECT_DENIAL="PREAUTH DENIAL APPROVAL FOR CLAIMID: ";
	public static final String MAIL_INSURER_TEXT_DENIAL="Dear Sir / Madam, <br><br>Please find the attached information sheet and supporting documents for your kind perusal and confirmation for further processing of the claim. <br><br> Medico Remarks: <medicoRemarks> <br> <br> Regards, <br> Preauth Team <br>GHPL";
	
	public static final String DOWNLOAD_REDIRECT_JSP_UIIC="UIIC_INSPREAUTH_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_BHAXA="BharatiAxa_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_RGIL="RelInsPreAuth_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_ITGI="ITGI_INSPREAUTH_Downloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_RELIGARE="Religaredownloadxls.jsp";
	public static final String DOWNLOAD_REDIRECT_JSP_DEFAULT="default_ins.jsp";
	
	public static final String documentnotfound="file not found";
	
	public static final String maindocid = "2";
	public static final String subdocid = "7";
	public static final String maindocidDischarge = "2";
	public static final String subdocidDischarge = "3";
	
	public static final int insurerokreauthflagzerostatus=174;
	public static final int insurerokreauthflagngtzerostatus=175;
	public static final int insurernotokreauthflagzerostatus=163;
	public static final int insurernotokreauthflagngtzerostatus=164;
	
}
