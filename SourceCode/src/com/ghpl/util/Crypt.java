/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ghpl.util;

public class Crypt
{

    private static Object lock = new Object();
    private static long ePerm32Tab[][][] = new long[4][256][2];
    private static int pc1_1;
    private static int pc1_2;
    private static int pc1_3;
    private static int pc2_1;
    private static int pc2_2;
    private static long doPc1[];
    private static long doPc2[];
    private static long efp[][][] = new long[16][64][2];
    private static int byteMask[] = {
        128, 64, 32, 16, 8, 4, 2, 1
    };
    private static int pc1[] = {
        57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 
        42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 
        27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 
        47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 
        30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 
        13, 5, 28, 20, 12, 4
    };
    private static int rots[] = {
        1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 
        2, 2, 2, 2, 2, 1
    };
    private static int pc2[] = {
        14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 
        21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 
        27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 
        30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 
        34, 53, 46, 42, 50, 36, 29, 32
    };
    private static int esel[] = {
        32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 
        8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 
        14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 
        20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 
        28, 29, 28, 29, 30, 31, 32, 1
    };
    private static int eInverse[] = new int[64];
    private static int perm32[] = {
        16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 
        23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 
        32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 
        4, 25
    };
    private static int sbox[][][] = {
        {
            {
                14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 
                6, 12, 5, 9, 0, 7
            }, {
                0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 
                12, 11, 9, 5, 3, 8
            }, {
                4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 
                9, 7, 3, 10, 5, 0
            }, {
                15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 
                3, 14, 10, 0, 6, 13
            }
        }, {
            {
                15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 
                2, 13, 12, 0, 5, 10
            }, {
                3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 
                1, 10, 6, 9, 11, 5
            }, {
                0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 
                12, 6, 9, 3, 2, 15
            }, {
                13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 
                7, 12, 0, 5, 14, 9
            }
        }, {
            {
                10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 
                12, 7, 11, 4, 2, 8
            }, {
                13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 
                5, 14, 12, 11, 15, 1
            }, {
                13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 
                2, 12, 5, 10, 14, 7
            }, {
                1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 
                14, 3, 11, 5, 2, 12
            }
        }, {
            {
                7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 
                8, 5, 11, 12, 4, 15
            }, {
                13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 
                2, 12, 1, 10, 14, 9
            }, {
                10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 
                3, 14, 5, 2, 8, 4
            }, {
                3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 
                5, 11, 12, 7, 2, 14
            }
        }, {
            {
                2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 
                3, 15, 13, 0, 14, 9
            }, {
                14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 
                15, 10, 3, 9, 8, 6
            }, {
                4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 
                12, 5, 6, 3, 0, 14
            }, {
                11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 
                0, 9, 10, 4, 5, 3
            }
        }, {
            {
                12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 
                3, 4, 14, 7, 5, 11
            }, {
                10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 
                13, 14, 0, 11, 3, 8
            }, {
                9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 
                4, 10, 1, 13, 11, 6
            }, {
                4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 
                1, 7, 6, 0, 8, 13
            }
        }, {
            {
                4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 
                9, 7, 5, 10, 6, 1
            }, {
                13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 
                5, 12, 2, 15, 8, 6
            }, {
                1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 
                6, 8, 0, 5, 9, 2
            }, {
                6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 
                0, 15, 14, 2, 3, 12
            }
        }, {
            {
                13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 
                3, 14, 5, 0, 12, 7
            }, {
                1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 
                6, 11, 0, 14, 9, 2
            }, {
                7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 
                10, 13, 15, 3, 5, 8
            }, {
                2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 
                9, 0, 3, 5, 6, 11
            }
        }
    };
    private static int initialPerm[] = {
        58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 
        44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 
        30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 
        16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 
        59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 
        45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 
        31, 23, 15, 7
    };
    private static int finalPerm[] = {
        40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 
        47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 
        54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 
        61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 
        35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 
        42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 
        49, 17, 57, 25
    };
    private static long longMask[] = {
        0xffffffff80000000L, 0x40000000L, 0x20000000L, 0x10000000L, 0x8000000L, 0x4000000L, 0x2000000L, 0x1000000L, 0x800000L, 0x400000L, 
        0x200000L, 0x100000L, 0x80000L, 0x40000L, 0x20000L, 0x10000L, 32768L, 16384L, 8192L, 4096L, 
        2048L, 1024L, 512L, 256L, 128L, 64L, 32L, 16L, 8L, 4L, 
        2L, 1L
    };
    private static long ufcKeytab[] = new long[16];
    private static long sb[][] = new long[4][4096];
    private static boolean inited = false;
    private static byte currentSalt[] = new byte[2];
    private static int currentSaltbits;
    private static int direction = 0;

    public Crypt()
    {
    }

    public static boolean selfTest(boolean flag)
    {
        String s = "arlEKn0OzVJn.";
        if(inited)
        {
            if(flag)
                System.err.println("Warning: initialization already done.");
        } else
        {
            initDes();
        }
        String s1 = crypt("ar", "foob");
        boolean flag1 = !s.equals(s1);
        if(flag1)
        {
            if(flag)
                System.err.println("TEST FAILED! Got:" + s1 + " Wanted" + s);
            return false;
        }
        if(flag)
            System.err.println("Test succeeded");
        return true;
    }

    public static String crypt(byte abyte0[], byte abyte1[])
    {
        String s = new String(abyte1);
        return crypt(abyte0, s);
        
    }

    public static String crypt(String s, String s1)
    {
        return crypt(s.getBytes(), s1);
       
       
    }

    public static String crypt(byte abyte0[], String s)
    {
        if(!inited)
            initDes();
        String s1;
        synchronized(lock)
        {
            byte abyte1[] = new byte[9];
            for(int i = 0; i < 9; i++)
                abyte1[i] = 0;

            setupSalt(abyte0);
            int j = s.length();
            for(int k = 0; k < (j > 8 ? 9 : j); k++)
                abyte1[k] = (byte)s.charAt(k);

            ufcMkKeytab(abyte1);
            int ai[] = ufcDoit(0, 0, 0, 0, 25);
            s1 = outputConversion(ai[0], ai[1], abyte0);
        }
        return s1;
    }

    private static String outputConversion(int i, int j, byte abyte0[])
    {
        char ac[] = new char[13];
        ac[0] = (char)abyte0[0];
        ac[1] = (char)(abyte0[1] != 0 ? abyte0[1] : abyte0[0]);
        for(int k = 0; k < 5; k++)
            ac[k + 2] = binToAscii(i >>> 26 - 6 * k & 0x3f);

        int l = (j & 0xf) << 2;
        j = j >>> 2 | (i & 3) << 30;
        for(int i1 = 5; i1 < 10; i1++)
            ac[i1 + 2] = binToAscii(j >>> 56 - 6 * i1 & 0x3f);

        ac[12] = binToAscii(l);
        return new String(ac);
    }

    private static int sLookup(int i, int j)
    {
        return sbox[i][j >>> 4 & 2 | j & 1][j >>> 1 & 0xf];
    }

    private static long bitMask(long l)
    {
        long l1 = 1 << (int)((11L - l % 12L) + 3L);
        long l2 = l < 12L ? 16L : 0L;
        return l1 << (int)l2;
    }

    public static void initDes()
    {
        synchronized(lock)
        {
            inited = true;
            for(int i = 0; i < 56; i++)
            {
                int i2 = pc1[i] - 1;
                long l3 = byteMask[i2 % 8 + 1];
                long l6 = longMask[i % 28 + 4];
                for(int k7 = 0; k7 < 128; k7++)
                    if(((long)k7 & l3) != 0L)
                        doPc1[k7 + (i / 28) * pc1_1 + (i2 / 8) * pc1_1 * pc1_2] |= l6;

            }

            for(int j = 0; j < 48; j++)
            {
                int j2 = pc2[j] - 1;
                long l4 = byteMask[j2 % 7 + 1];
                long l7 = bitMask(j % 24);
                for(int i8 = 0; i8 < 128; i8++)
                    if(((long)i8 & l4) != 0L)
                        doPc2[i8 + (j2 / 7) * pc2_1] |= l7;

            }

            for(int k = 0; k < 4; k++)
            {
                for(int k2 = 0; k2 < 256; k2++)
                {
                    for(int i4 = 0; i4 < 2; i4++)
                        ePerm32Tab[k][k2][i4] = 0L;

                }

            }

            for(int l = 0; l < 48; l++)
            {
                int l2 = perm32[esel[l] - 1] - 1;
                int j4 = byteMask[l2 % 8];
                for(int k5 = 255; k5 >= 0; k5--)
                    if((k5 & j4) != 0)
                        ePerm32Tab[l2 / 8][k5][l / 24] |= bitMask(l % 24);

            }

            for(int i1 = 0; i1 < 4; i1++)
            {
                for(int i3 = 0; i3 < 64; i3++)
                {
                    int k4 = sLookup(2 * i1, i3);
                    for(int l5 = 0; l5 < 64; l5++)
                    {
                        int j6 = sLookup(2 * i1 + 1, l5);
                        int i7 = (k4 << 4 | j6) << 24 - 8 * i1;
                        int j8 = i3 << 6 | l5;
                        sb[i1][j8] = ePerm32Tab[0][i7 >>> 24 & 0xff][0] << 32 | ePerm32Tab[0][i7 >>> 24 & 0xff][1];
                        sb[i1][j8] |= ePerm32Tab[1][i7 >>> 16 & 0xff][0] << 32 | ePerm32Tab[1][i7 >>> 16 & 0xff][1];
                        sb[i1][j8] |= ePerm32Tab[2][i7 >>> 8 & 0xff][0] << 32 | ePerm32Tab[2][i7 >>> 8 & 0xff][1];
                        sb[i1][j8] |= ePerm32Tab[3][i7 & 0xff][0] << 32 | ePerm32Tab[3][i7 & 0xff][1];
                    }

                }

            }

            for(int j1 = 47; j1 >= 0; j1--)
            {
                eInverse[esel[j1] - 1] = j1;
                eInverse[(esel[j1] - 1) + 32] = j1 + 48;
            }

            for(int k1 = 0; k1 < 16; k1++)
            {
                for(int j3 = 0; j3 < 64; j3++)
                {
                    for(int i5 = 0; i5 < 2; i5++)
                        efp[k1][j3][i5] = 0L;

                }

            }

            for(int l1 = 0; l1 < 64; l1++)
            {
                int k3 = l1 / 32;
                int j5 = l1 % 32;
                int i6 = finalPerm[l1] - 1;
                int k6 = eInverse[i6];
                int j7 = k6 / 6;
                int k8 = k6 % 6;
                long l8 = longMask[k8 + 26];
                long l9 = longMask[j5];
                for(int i9 = 63; i9 >= 0; i9--)
                    if(((long)i9 & l8) != 0L)
                        efp[j7][i9][k3] |= l9;

            }

        }
    }

    private static void setupSalt(byte abyte0[])
    {
        if(abyte0[0] == currentSalt[0] && abyte0[1] == currentSalt[1])
            return;
        currentSalt[0] = abyte0[0];
        currentSalt[1] = abyte0[1];
        int i = 0;
        for(int j = 0; j < 2; j++)
        {
            long l = asciiToBin(abyte0[j]);
            if(l < 0L || l > 63L)
                l = 0L;
            for(int k = 0; k < 6; k++)
                if((l >>> k & 1L) != 0L)
                    i = (int)((long)i | bitMask(6 * j + k));

        }

        shuffleSb(sb[0], currentSaltbits ^ i);
        shuffleSb(sb[1], currentSaltbits ^ i);
        shuffleSb(sb[2], currentSaltbits ^ i);
        shuffleSb(sb[3], currentSaltbits ^ i);
        currentSaltbits = i;
    }

    private static void shuffleSb(long al[], int i)
    {
        int j = 0;
        for(int k = 4095; k >= 0; k--)
        {
            long l = (al[j] >>> 32 ^ al[j]) & (long)i;
            al[j++] ^= l << 32 | l;
        }

    }

    private static long asciiToBin(byte byte0)
    {
        return (long)(byte0 >= 97 ? byte0 - 59 : byte0 >= 65 ? byte0 - 53 : byte0 - 46);
    }

    private static char binToAscii(long l)
    {
        return (char)(int)(l >= 38L ? (l - 38L) + 97L : l >= 12L ? (l - 12L) + 65L : l + 46L);
    }

    private static void ufcMkKeytab(byte abyte0[])
    {
        int i = 0;
        int k = 0;
        int l = 0;
        long l1 = 0L;
        long l2 = 0L;
        for(int i1 = 7; i1 >= 0; i1--)
        {
            l1 |= doPc1[i + (abyte0[l] & 0x7f)];
            i += 128;
            l2 |= doPc1[i + (abyte0[l++] & 0x7f)];
            i += 128;
        }

        for(int j1 = 0; j1 < 16; j1++)
        {
            int j = 0;
            l1 = l1 << rots[j1] | l1 >>> 28 - rots[j1];
            long l3 = doPc2[j + (int)(l1 >>> 21 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l1 >>> 14 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l1 >>> 7 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l1 & 127L)];
            j += 128;
            l3 <<= 32;
            l2 = l2 << rots[j1] | l2 >>> 28 - rots[j1];
            l3 |= doPc2[j + (int)(l2 >>> 21 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l2 >>> 14 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l2 >>> 7 & 127L)];
            j += 128;
            l3 |= doPc2[j + (int)(l2 & 127L)];
            ufcKeytab[k] = l3;
            k++;
        }

        direction = 0;
    }

    private static long sba(long al[], long l)
    {
        if((l >>> 3) << 3 != l)
            (new Exception("FATAL : non aligned V:" + l)).printStackTrace();
        return al[(int)(l >>> 3)];
    }

    private static int[] ufcDoFinalPerm(int i, int j, int k, int l)
    {
        int ai[] = new int[2];
        int i1 = (i ^ j) & currentSaltbits;
        i ^= i1;
        j ^= i1;
        i1 = (k ^ l) & currentSaltbits;
        k ^= i1;
        l ^= i1;
        int j1 = 0;
        int k1 = 0;
        i >>>= 3;
        j >>>= 3;
        k >>>= 3;
        l >>>= 3;
        j1 = (int)((long)j1 | efp[15][l & 0x3f][0]);
        k1 = (int)((long)k1 | efp[15][l & 0x3f][1]);
        j1 = (int)((long)j1 | efp[14][(l >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[14][l & 0x3f][1]);
        j1 = (int)((long)j1 | efp[13][(l >>>= 10) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[13][l & 0x3f][1]);
        j1 = (int)((long)j1 | efp[12][(l >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[12][l & 0x3f][1]);
        j1 = (int)((long)j1 | efp[11][k & 0x3f][0]);
        k1 = (int)((long)k1 | efp[11][k & 0x3f][1]);
        j1 = (int)((long)j1 | efp[10][(k >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[10][k & 0x3f][1]);
        j1 = (int)((long)j1 | efp[9][(k >>>= 10) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[9][k & 0x3f][1]);
        j1 = (int)((long)j1 | efp[8][(k >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[8][k & 0x3f][1]);
        j1 = (int)((long)j1 | efp[7][j & 0x3f][0]);
        k1 = (int)((long)k1 | efp[7][j & 0x3f][1]);
        j1 = (int)((long)j1 | efp[6][(j >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[6][j & 0x3f][1]);
        j1 = (int)((long)j1 | efp[5][(j >>>= 10) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[5][j & 0x3f][1]);
        j1 = (int)((long)j1 | efp[4][(j >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[4][j & 0x3f][1]);
        j1 = (int)((long)j1 | efp[3][i & 0x3f][0]);
        k1 = (int)((long)k1 | efp[3][i & 0x3f][1]);
        j1 = (int)((long)j1 | efp[2][(i >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[2][i & 0x3f][1]);
        j1 = (int)((long)j1 | efp[1][(i >>>= 10) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[1][i & 0x3f][1]);
        j1 = (int)((long)j1 | efp[0][(i >>>= 6) & 0x3f][0]);
        k1 = (int)((long)k1 | efp[0][i & 0x3f][1]);
        ai[0] = j1;
        ai[1] = k1;
        return ai;
    }

    private static int[] ufcDoit(int i, int j, int k, int l, int i1)
    {
        long l1 = (long)i << 32 | (long)j;
        long l2;
        long l4;
        for(l2 = (long)k << 32 | (long)l; i1-- != 0; l2 = l4)
        {
            int j1 = 0;
            for(int k1 = 7; k1 >= 0; k1--)
            {
                long l3 = ufcKeytab[j1++] ^ l2;
                l1 ^= sba(sb[3], l3 >>> 0 & 65535L);
                l1 ^= sba(sb[2], l3 >>> 16 & 65535L);
                l1 ^= sba(sb[1], l3 >>> 32 & 65535L);
                l1 ^= sba(sb[0], l3 >>> 48 & 65535L);
                l3 = ufcKeytab[j1++] ^ l1;
                l2 ^= sba(sb[3], l3 >>> 0 & 65535L);
                l2 ^= sba(sb[2], l3 >>> 16 & 65535L);
                l2 ^= sba(sb[1], l3 >>> 32 & 65535L);
                l2 ^= sba(sb[0], l3 >>> 48 & 65535L);
            }

            l4 = l1;
            l1 = l2;
        }

        i = (int)(l1 >>> 32);
        j = (int)(l1 & -1L);
        k = (int)(l2 >>> 32);
        l = (int)(l2 & -1L);
        return ufcDoFinalPerm(i, j, k, l);
    }

    static 
    {
        pc1_1 = 128;
        pc1_2 = 2;
        pc1_3 = 8;
        pc2_1 = 128;
        pc2_2 = 8;
        doPc1 = new long[pc1_1 * pc1_2 * pc1_3];
        doPc2 = new long[pc2_1 * pc2_2];
    }
}