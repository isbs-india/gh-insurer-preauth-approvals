/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghpl.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 *
 * @author Surya Kiran
 * Module Definitions
 * 1 CORP
 * 2 AB
 * 3 VENUS
 * 4 RETAIL UIC
 * 5 RETAIL NIA
 * 6 RETAIL OIC
 * 7 RETAIL NIC
 */
public class DBConn {
	static Logger logger =Logger.getLogger("DBConn");
    
    public static Connection getMyConnection(int Module) {
        
        Connection myConnection = null;
        
        switch (Module) {
            case 1:
            case 2:
            case 4:myConnection = getOracleConnection(Module); break;
        }
        
        return myConnection;
    }
    public static Connection getOracleConnection(int Module) {
    	logger.info(Module);
		//System.out.println("-------- Trying to Set Oracle JDBC Connection for "+Module+" Module------");
		
                MyProperties Gdbp = new MyProperties();
                
                String jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("GHIP")+":1521:"+Gdbp.getMyProperties("GHSID");
                String dbUser = Gdbp.getMyProperties("GHUser");
                String dbPwd = Gdbp.getMyProperties("GHPwd");
                /*String jdbcURL = "jdbc:oracle:thin:@"+ "192.168.100.73"+":1521:"+"orcl";
                String dbUser = "marble";
                String dbPwd = "Ma3eMa3e$";*/
                
                /*
                 * GHIPLIVE=192.168.100.73
					GHSIDLIVE=orcl
					GHUserLIVE=marble
					GHPwdLIVE=Ma3eMa3e$
                 */
                switch (Module) {
                    case 2 :
                        jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("ABIP")+":1521:"+Gdbp.getMyProperties("ABSID");
                        dbUser = Gdbp.getMyProperties("ABUser");
                        dbPwd = Gdbp.getMyProperties("ABPwd");
                        break;
                    case 4 :
                        jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("WINIP")+":1521:"+Gdbp.getMyProperties("WINSID");
                        dbUser = Gdbp.getMyProperties("WINUser");
                        dbPwd = Gdbp.getMyProperties("WINPwd");
                        break;
                }                                
                
		Connection myConnection = null;
		 
		try { 
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
		} catch (ClassNotFoundException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found for "+Module+" Module");
			return null; 
		} catch ( InstantiationException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found Cannot Instantiate for "+Module+" Module");
			return null; 
		} catch (IllegalAccessException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found Illegal Access for "+Module+" Module" );
			return null; 
		} 
 
		//logger.info("Oracle JDBC Driver Registered! for "+Module+" Module");		
		
		try {
			 
			myConnection = DriverManager.getConnection(jdbcURL, dbUser, dbPwd);
                        myConnection.setAutoCommit(false);
 
		} catch (SQLException e) {
			logger.error("Error DBConn");
			logger.error("Connection Failed! Check output console for "+Module+" Module");
			e.printStackTrace();
			logger.error(e);
			return null; 
		}
		return myConnection;
		
	}
	
    public static Connection getSQLConnection(String DBId) {
		
		logger.info("-------- Trying to Set SQL JDBC Connection ------for Retail "+DBId+" Module");
		
                MyProperties Gdbp = new MyProperties();
            
                String jdbcURL = "jdbc:sqlserver://"+ Gdbp.getMyProperties("RETIP")+":1433;databaseName="+DBId
                        +";user="+Gdbp.getMyProperties("RETUser")+";password="+Gdbp.getMyProperties("RETPwd");
//		String dbUser = Gdbp.getMyProperties("RETUser");
//		String dbPwd = Gdbp.getMyProperties("RETPwd");
              //  System.out.println("JDBC Ret : got url ");
                
		Connection myConnection = null;
		 
		try { 
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
		} catch (ClassNotFoundException e) { 
			logger.error("Error DBConn1 for Retail "+DBId+" Module"); 
			logger.error("No MSSQL JDBC Driver1 Found for Retail "+DBId+" Module");
			return null; 
		} catch ( InstantiationException e) { 
			logger.error("Error DBConn1"); 
			logger.error("No MSSQL JDBC Driver2 Found Cannot Instantiate for Retail "+DBId+" Module");
			return null; 
		} catch (IllegalAccessException e) { 
			logger.error("Error DBConn"); 
			logger.error("No MSSQL JDBC Driver3 Found Illegal Access for Retail "+DBId+" Module");
			return null; 
		} 
 
		//System.out.println("MSSQL JDBC Driver Registered! for Retail "+DBId+" Module");		
		
		try {
			 
			myConnection = DriverManager.getConnection(jdbcURL);
 
		} catch (SQLException e) {
			logger.error("Error DBConn");
			logger.error("Connection Failed! Check output console for Retail "+DBId+" Module");
			e.printStackTrace();
			return null; 
		}
		
		if (myConnection != null) {
			logger.error(" MsSQL Connection Succuessfull.... for Retail "+DBId+" Module");
		} else {
			logger.error("Failed to make connection! for Retail "+DBId+" Module");
		}
			
		return myConnection;
		
	}
    
    public static Connection getMySQLConnection() {
	    //String jdbcURL = DBIP;
	    MyProperties Gdbp = new MyProperties();
	   // getPreAuthData("jdbc:mysql://192.168.116.175:3306/ghplkatako","root", "psf4FN8cae47YYe9");//LIVE
       /* String jdbcURL = "jdbc:mysql://"+ Gdbp.getMyProperties("MSIP")+":3306/"+Gdbp.getMyProperties("MSSID")
                +";user="+Gdbp.getMyProperties("MSUser")+";password="+Gdbp.getMyProperties("MSPwd");*/
	    				//jdbc:mysql://192.168.116.175:3306/ghplkatako;user=root;password=psf4FN8cae47YYe9
        String jdbcURL = "jdbc:mysql://"+ Gdbp.getMyProperties("MSIP")+":3306/"+Gdbp.getMyProperties("MSSID");
	    String user =Gdbp.getMyProperties("MSUser");
	    String pwd= Gdbp.getMyProperties("MSPwd");
			Connection myConnection = null;
			try { 
				Class.forName("com.mysql.jdbc.Driver").newInstance();
			} catch (ClassNotFoundException e) { 
				logger.error("Error DBConn "+Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found");
				return null; 
			} catch ( InstantiationException e) { 
				logger.error("Error DBConn   "+ Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found Cannot Instantiate");
				return null; 
			} catch (IllegalAccessException e) { 
				logger.error("Error DBConn "+Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found Illegal Access");
				return null; 
			} 
			
			//logger.info("MYSQL JDBC Driver Registered!");		
			
			try {
				 
				myConnection = DriverManager.getConnection(jdbcURL,user,pwd);
			
			} catch (SQLException e) {
				logger.error("Error DBConn  "+jdbcURL);
				logger.error("Connection Failed! Check output console");
				e.printStackTrace();
				return null; 
			}
			
			if (myConnection != null) {
				logger.info(" MYSQL Connection Succuessfull....");
			} else {
				logger.info("Failed to make connection!");
			}
			return myConnection;
	}
}
