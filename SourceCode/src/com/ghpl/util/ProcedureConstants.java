package com.ghpl.util;

public class ProcedureConstants {  

	public static final String PROC_FOR_GET_CHK_LOGIN_ORACLE ="{call PKG_USERLOGIN.CHK_LOGIN_TEMP(?,?,?,?)}";
	public static final String PROC_FOR_GET_CLM_TO_SENDTO_INSURER ="{call  PKG_SEND_TO_INSURER.GET_CLAIMS_TOBE_SEND_TO_INS(?)}";
	public static final String PROC_FOR_GET_CLM_DENY_TO_SENDTO_INSURER ="{call  PKG_SEND_TO_INSURER.GET_CLAIMS_DENIAL_SEND_TO_INS(?)}";
	
	public static final String PROC_FOR_GET_UIIC_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER_SEP2019.GET_PREAUTH_UII(?,?,?)}";
	
	public static final String PROC_FOR_GET_PREAUTH_NIA_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER_SEP2019.GET_PREAUTH_NIA(?,?,?)}";

	public static final String PROC_FOR_GET_BHAXA_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER_SEP2019.GET_PREAUTH_BHARTIAXA(?,?,?)}";
	public static final String PROC_FOR_GET_ITGI_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER_SEP2019.GET_PREAUTH_ITGI(?,?,?)}";
	public static final String PROC_FOR_GET_RELIGARE_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER.GET_PREAUTH_RELIGARE(?,?,?)}";
	public static final String PROC_FOR_GET_RGIL_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER.get_preauth_rel1(?,?,?)}";
	public static final String PROC_FOR_GET_DOCS_DETAILS_CLAIM_CORP ="{call PKG_SEND_TO_INSURER.GET_DOCS_TOBE_SEND_TO_INS(?,?,?)}";
	public static final String PROC_FOR_INSERT_UIIC_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_UIIC_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static final String PROC_FOR_INSERT_BHAXA_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_BHARATI_AXA_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static final String PROC_FOR_INSERT_ITGI_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_ITGI_FORMAT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static final String PROC_FOR_INSERT_RELIGARE_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_PREAUTH_RELIGARE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static final String PROC_FOR_INSERT_RGIL_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_RGIL_MAIL_FORMAT1(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	
	public static final String PROC_FOR_UPDATE_STATUS_IAUTHPREAUTH_INSURER_SENT ="{ call PKG_SEND_TO_INSURER.IAUTH_PREAUTH_CLM_UPDATE_INS(?,?,?,?) }";
	                                                                                                             
	
	public static final String PROC_FOR_GET_PREFILL_DENIAL_DTLS_CORP ="{call PKG_PREAUTH_INSURER.GET_PREAUTH_DENIAL(?,?,?)}";
	public static final String PROC_FOR_INSERT_DENIAL_CORP_TO_INSURER ="{call PKG_PREAUTH_INSURER.PROC_DENIAL_UPDATE_STATUS(?,?,?,?,?)}";
	public static final String PROC_FOR_UPDATE_STATUS_IAUTHPREAUTH_INSURER_SENT_DENIAL ="{ call PKG_SEND_TO_INSURER.IAUTH_PREAUTH_DENY_UPDT_INS(?,?,?,?) }";
	
	public static final String PROC_FOR_GET_CLM_SENT_TO_INS ="{ call PKG_SEND_TO_INSURER.GET_CLAIMS_SENT_TO_INS(?) }";
	public static final String PROC_FOR_GET_UIIC_PREFILL_DTLS_SQLSERVER  ="{call GET_MAIL_DETAILS_INSU(?)}";
	public static final String PROC_FOR_I_AUTH_PREOCESS_SHEET_DATA  ="{call I_AUTH_PREOCESS_SHEET_DATA(?)}";
	public static final String PROC_FOR_INSERT_UIIC_DTLS_SQLSERVER  ="{call SP_INSERT_INSURE_MAIL_DETAILS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	//Prefill UIIC & download xls
	public static final String PROC_FOR_GET_PREFILL_CLAIM_DTLS ="{call PKG_SEND_TO_INSURER.GET_UWCODE_CLAIM_ALL_MODULES(?,?,?,?)}";
	
	public static final String PROC_FOR_GET_UIIC_PREFILL_CORP ="{call PKG_SEND_TO_INSURER.GET_UIIC_PREFILL_DATA(?,?,?)}";
	public static final String PROC_FOR_GET_BHAXA_PREFILL_CORP ="{call PKG_SEND_TO_INSURER.GET_BHAXA_PREFILL_DATA(?,?,?)}";
	public static final String PROC_FOR_GET_RGIL_PREFILL_CORP ="{call PKG_SEND_TO_INSURER.GET_RGIL_PREFILL_DATA(?,?,?)}";
	public static final String PROC_FOR_GET_ITGI_PREFILL_CORP ="{call PKG_SEND_TO_INSURER.GET_ITGI_PREFILL_DATA(?,?,?)}";
	public static final String PROC_FOR_GET_RELIGARE_PREFILL_CORP ="{call PKG_SEND_TO_INSURER.GET_RELIGARE_PREFILL_DATA(?,?,?)}";
	public static final String PROC_FOR_GET_UIIC_PREFILL_SQLSERVER ="{call GET_UIC_PREFILLING_DATA(?)}";
	//santhosh added start
	public static final String PROC_FOR_GET_NIA_PREFILL_DATA ="{call PKG_SEND_TO_INSURER.GET_NIA_PREFILL_DATA(?,?,?)}";
	//santhosh added end
	
	public static final String PROC_FOR_GET_IAUTH_CLMS_LIST_FOR_DOC_UPLOAD  ="{call PKG_IAUTH.IAUTH_CLMS_LIST_FOR_DOC_UPLOAD(?,?)}";
	public static final String PROC_FOR_GET_IAUTH_PROCESSSHEET  ="{call PKG_IAUTH.IAUTH_PROCESSSHEET(?,?,?)}";
	public static final String PROC_FOR_GET_I_AUTH_SRCH_BY_CLAIMID  ="{call PKG_IAUTH.IAUTH_CLMS_LIST_FOR_DOC_UPLOAD(?,?)}";
	public static final String PROC_FOR_UPLOAD_I_AUTH_INSURER_BILLS_RECEIVE_SQLSERVER  ="{call I_AUTH_INSURER_BILLS_RECEIVE(?,?,?,?,?,?,?,?)}";
	public static final String PROC_FOR_UPDATE_IAUTH_PREAUTH_CLM_UPDATE="{call pkg_iauth.iauth_preauth_clm_update(?,?,?,?,?)}";
	public static final String PROC_FOR_DELETE_I_AUTH_INSURER_BILLS_RECEIVE_DELETE_SQLSERVER="{call I_AUTH_INSURER_BILLS_RECEIVE_DELETE(?)}";
	public static final String PROC_FOR_UPLOAD_IAUTH_INSURER_CONFIRMATION_ORACLE  ="{call PKG_IAUTH.IAUTH_INSURER_CONFIRMATION(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public static final String PROC_FOR_UPLOAD_IAUTH_INSURER_DENIAL_ORACLE  ="{call PKG_IAUTH.IAUTH_INSURER_DENIAL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	//PROCEDURE TO PREFILL RETAIL INSURER DENIAL FORM
	public static final String PROC_FOR_GET_PREFILL_PREAUTH_DENIAL_SQLSERVER ="{call IAUTH_GET_PREAUTH_DENIAL(?,?)}";
	
	//PROCEDURE TO SEND DENAIL AUTH TO INSURER
	public static final String PROC_FOR_INSERT_DENIAL_TO_INSURER_SQLSERVER ="{call IAUTH_DENIAL_UPDATE_STATUS(?,?,?,?)}";
	
	//PROCEDURE TO COLLECT DENAIL AUTH FROM INSURER
	public static final String PROC_FOR_COLLECT_DENIAL_TO_INSURER_SQLSERVER ="{call I_AUTH_INSURER_DENIAL_BILLS_RECEIVE(?,?,?,?,?,?,?,?)}";
	
	public static final String PROC_FOR_UPDATE_BACK_TO_MEDICO_SQLSERVER  ="{call IAUTH_BACK_TO_MEDICO_FROM_INSU(?,?,?,?,?,?)}";
	
	public static final String PROC_FOR_UPDATE_BACK_TO_MEDICO_CORP  ="{call PKG_SEND_TO_INSURER.IAUTH_BACK_TO_MEDICO_FROM_INSU(?,?,?,?,?)}";
	
	public static final String PROC_FOR_GET_INSURER_EMAILIDS  ="{call PKG_PREAUTH_INSURER.GET_INSURER_EMAILIDS(?,?,?,?)}";
	
	public static final String PROC_NIA_INSERT  ="{call PKG_PREAUTH_INSURER.PROC_NIA_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
    
	public static final String PROC_FOR_INSERT_NIA_DTLS_SQLSERVER  ="{call PROC_NIA_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	public static final String PROC_FOR_GET_NIA_PREFILL_DTLS_SQLSERVER  ="{call GET_MAIL_DETAILS_INSU(?)}";
	public static final String PROC_FOR_GET_NIA_PREFILL_SQLSERVER ="{call GET_NIA_PREFILLING_DATA(?)}";
	public static final String PROC_FOR_INSERT_DEFAULT_CORP_TO_INSURER ="{ call PKG_PREAUTH_INSURER.PROC_UIIC_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static final String PROC_FOR_GET_DEFAULT_PREFILL_DTLS_CORP ="{call PKG_PREAUTH_INSURER_SEP2019.GET_PREAUTH_UII(?,?,?)}";
}

