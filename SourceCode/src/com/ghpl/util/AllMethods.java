package com.ghpl.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;


public class AllMethods {
    class TableHeader extends PdfPageEventHelper {
        /** The header text. */
        String header;
        /** The template with the total number of pages. */
        PdfTemplate total;
        String imagePath;
        /**
         * Allows us to change the content of the header.
         * @param header The new header String
         */
        public void setHeader(String header) {
            this.header = header;
        }
        public void setImagePath(String imagePath){
        	this.imagePath=imagePath;
        }
 
        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onOpenDocument(PdfWriter writer, Document document) {
            total = writer.getDirectContent().createTemplate(30, 16);
        }
 
        /**
         * Adds a header to every page
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onEndPage(PdfWriter writer, Document document) {
            try {
            	//header text start
            	PdfPTable table = new PdfPTable(3);
            	table.setTotalWidth(527);
                table.setLockedWidth(true);
                table.getDefaultCell().setBorder(0);
               // System.out.println("images ------------"+imagePath+Constants.HEADER_IMAGE);
                Image image = Image.getInstance(imagePath+""+Constants.HEADER_IMAGE);
                PdfPCell cell1 = new PdfPCell(image,true); 
                cell1.setBorder(PdfPCell.NO_BORDER);
                cell1.setColspan(3);
                table.addCell(cell1);
                table.writeSelectedRows(0, -1, 34, document.top()+50, writer.getDirectContent()); 
               // System.out.println("top ------"+document.top());
	            //header text end
	                
                //footer text start
                PdfPTable table1 = new PdfPTable(3); 
                table1.setWidths(new int[]{34, 14, 2});
                table1.setTotalWidth(527);
                table1.setLockedWidth(true);
                table1.getDefaultCell().setFixedHeight(20);
                table1.getDefaultCell().setBorder(Rectangle.TOP);
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.addCell(new Phrase(String.format("Created on  "+new Date()+"        printed by "+header),Constants.fontGeneralText));
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table1.addCell(new Phrase(String.format("Page %d of", writer.getPageNumber()),Constants.fontGeneralText));
                PdfPCell cellfooter = new PdfPCell(Image.getInstance(total));
                cellfooter.setBorder(Rectangle.TOP);
                table1.addCell(cellfooter);
                //System.out.println("bottom ------"+document.bottom());
                table1.writeSelectedRows(0, -1, 34, document.bottom()-18, writer.getDirectContent());
              //footer text end
            }
            catch(Exception de) {
                throw new ExceptionConverter(de);
            }
        }
 
        /**
         * Fills out the total number of pages before the document is closed.
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onCloseDocument(PdfWriter writer, Document document) {
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber() - 1),Constants.fontGeneralText), 2, 6, 0);
        }
    }

    public void getPageNumber(PdfWriter writer,String printName,String imagePath){
    	//System.out.println("print Name   "+printName);
		TableHeader th= new TableHeader();
		th.setHeader(printName);
		th.setImagePath(imagePath);
		 writer.setPageEvent(th);
	}
    
    //claims.getCheckinFavour(),claims.getInsurerComName(),claims.getHospitalName(),claims.getAddress()
    public Paragraph closurLetterLeftAddress(int claimType,String selforhospName,String data)
    {
        Paragraph address = new Paragraph();
        address.setLeading(10);
        address.setSpacingAfter(0.0f);
        address.setAlignment(Element.ALIGN_LEFT);
        address.add(new Phrase("To"+Constants.NewLine, Constants.fontParaText));
        //if(claimType==0){
        	if(selforhospName!=null){
        		address.add(new Phrase(selforhospName+Constants.NewLine, Constants.fontParaText));
        	}else{
        		address.add(new Phrase(""+Constants.NewLine, Constants.fontParaText));
        	}
        //}
        
        if(data!=null){
        	int i=1;
        	String[] values = data.split(","); 
        	for (String retval: values){
        		//System.out.println(values.length +"  --   "+i+"    "+retval);
        		
	        		if(values.length==i){
	        			if(!retval.equals("")){
	        				address.add(new Phrase(retval.toUpperCase().trim() +".", Constants.fontParaText));
	        			}
	        		}else{
	        			if(!retval.equals("")){
	        				address.add(new Phrase(retval.toUpperCase().trim() +","+Constants.NewLine, Constants.fontParaText));
	        			}
	        			i++;
	        		}
        		}
             
        }
        return address;
    }

    public Paragraph closurLetterRightAddress(String line1, String line2)
    {
        Paragraph address = new Paragraph();
        address.setLeading(10);
        address.setSpacingAfter(0.0f);
        address.setAlignment(Element.ALIGN_RIGHT);
        address.add(new Phrase("",Constants.fontParaText));
        if (!line1.equals("") && line1 != null)
        {
        	address.add(new Phrase(line1.toUpperCase()+Constants.NewLine, Constants.fontParaText));
            //address.add(paraLine(line1.toUpperCase() + Constants.NewLine));
        }
        if (!line2.equals("") && line2 != null)
        {
        	address.add(new Phrase(line2.toUpperCase(), Constants.fontParaText));
            //address.add(paraLine(line2.toUpperCase() ));
        }
        return address;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
public  PdfPTable getFooter(int x, int y) {
    PdfPTable table = new PdfPTable(2);
    table.setTotalWidth(527);
    table.setLockedWidth(true);
    table.getDefaultCell().setFixedHeight(20);
    table.getDefaultCell().setBorder(Rectangle.TOP);
    table.addCell(new Phrase(String.format("Created on  "+new Date()),Constants.fontGeneralText));
    table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
    table.addCell(new Phrase(String.format("Page %d of %d", x, y),Constants.fontGeneralText));
    return table;
}

	
	
	

	
	public Image addLogo(String filepath) throws BadElementException, MalformedURLException, IOException
    {
        Image image = Image.getInstance(filepath);
        image.scaleToFit(500, 500);
        image.setAlignment(Image.MIDDLE | Image.ALIGN_JUSTIFIED);
        image.setBorder(Image.BOX);
        image.setBorderWidth(0);  
        return image;
    }
	public Paragraph addHeading(String text1, String text2, boolean bold, boolean underline)
    {
        Paragraph header = new Paragraph();
        header.setAlignment(1);
        if (bold == true && underline == true)
        {
            header.add(new Phrase(text1, Constants.fontUnderlineBoldParaText));
            if (!text2.equals("") && text2 != null)
            {
                header.add(new Phrase(text2, Constants.fontBoldParaText));
            }
        }
        else if (bold == true && underline == false)
        {
            header.add(new Phrase(text1, Constants.fontBoldParaText));
            if (!text2.equals("") && text2 != null)
            {
                header.add(new Phrase(text2, Constants.fontBoldParaText));
            }
        }
        else if (bold == false && underline == true)
        {
            header.add(new Phrase(text1, Constants.fontBoldParaText));
            if (!text2.equals("") && text2 != null)
            {
                header.add(new Phrase(text2, Constants.fontBoldParaText));
            }
        }
        else
        {
            header.add(new Phrase(text1, Constants.fontBoldParaText));
            if (!text2.equals("") && text2 != null)
            {
                header.add(new Phrase(text2, Constants.fontBoldParaText));
            }
        }
        return header;
    }
	public PdfPCell addCellData(String text,boolean bold){
    	PdfPCell pdfPCell = null;
    	if(bold){
	    	if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontBoldParaText));
	    		pdfPCell.setBorder(Rectangle.NO_BORDER);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_TOP);
	    	}
    	}
    	else{
    		if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.NO_BORDER);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_TOP);
    		}else{
    			pdfPCell =new PdfPCell(new Phrase("", Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.NO_BORDER);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_TOP);
    		}
    	}
    	return pdfPCell;
    }
	
	public PdfPCell addCellDataWithBorder(String text,boolean bold){
    	PdfPCell pdfPCell = null;
    	if(bold){
	    	if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontBoldParaText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	}
    	}
    	else{
    		if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		}else{
    			pdfPCell =new PdfPCell(new Phrase("", Constants.fontGeneralText));
    			pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		}
    	}
    	return pdfPCell;
    }
	
	public PdfPCell addCellDataWithBorderRight(String text,boolean bold){
    	PdfPCell pdfPCell = null;
    	if(bold){
	    	if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontBoldParaText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_RIGHT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    	}
    	}
    	else{
    		if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_RIGHT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    		}else{
    			pdfPCell =new PdfPCell(new Phrase("", Constants.fontGeneralText));
    			pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_RIGHT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    		}
    	}
    	return pdfPCell;
    }
	
	public PdfPCell addCellDataWithBorderLeft(String text,boolean bold){
    	PdfPCell pdfPCell = null;
    	if(bold){
	    	if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontBoldParaText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	    	}
    	}
    	else{
    		if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		}else{
    			pdfPCell =new PdfPCell(new Phrase("", Constants.fontGeneralText));
    			pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_LEFT);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		}
    	}
    	return pdfPCell;
    }
	
	public PdfPCell addCellDataUBoldBorder(String text,boolean underbold){
    	PdfPCell pdfPCell = null;
    	if(underbold){
	    	if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontUnderlineBoldParaText1));
	    		pdfPCell.setFixedHeight(15f);
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
	    	}
    	}
    	else{
    		if(text!=null && !text.equals("")){
	    		pdfPCell =new PdfPCell(new Phrase(text, Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_TOP);
    		}else{
    			pdfPCell =new PdfPCell(new Phrase("", Constants.fontGeneralText));
	    		pdfPCell.setBorder(Rectangle.BOX);
	    		pdfPCell.setVerticalAlignment(Element.ALIGN_TOP);
    		}
    	}
    	return pdfPCell;
    }
	 public PdfPTable addTableWithoutBorder(String first, String second, String third) throws DocumentException
	    {
	        PdfPTable table = new PdfPTable(1);
	        table.setTotalWidth(500);
	        float[] widths = new float[] { 15f };
	        table.setWidths(widths);
	        table.setSpacingAfter(10.0f);
	        table.setHorizontalAlignment(0);
	        PdfPCell cell;
	        cell = new PdfPCell(cellData(first));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(cellData(second));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(cellData(third));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        return table;
	    }
	    
	    public PdfPTable addTableWithoutBorder(String first, String second, String third,String four,String fifth,String six,String seven,String eight) throws DocumentException
	    {
	        PdfPTable table = new PdfPTable(1);
	        table.setTotalWidth(500);
	        float[] widths = new float[] { 15f };
	        table.setWidths(widths);
	        table.setSpacingAfter(10.0f);
	        table.setHorizontalAlignment(0);
	        PdfPCell cell;
	        cell = new PdfPCell(cellData(first));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(cellData(second));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(cellData(third));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(cellData(four));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        cell = new PdfPCell(cellData(fifth));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        cell = new PdfPCell(cellData(six));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        cell = new PdfPCell(cellData(seven));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        cell = new PdfPCell(cellData(eight));
	        cell.setBorder(Rectangle.NO_BORDER);
	        cell.setVerticalAlignment(Element.ALIGN_TOP);
	        table.addCell(cell);
	        
	        return table;
	    }
	    public Paragraph addEmptyLine(Paragraph paragraph, int number) {
		    for (int i = 0; i < number; i++) {
		      paragraph.add(new Paragraph(" "));
		    }
		    return paragraph;
		  }
	   
	    public Phrase paraLine(String Text)
	    {
	        return new Phrase(Text, Constants.fontBoldParaText);
	    }
	    
	   
	    
	    public  Paragraph  addAddress(String line1, String line2, String line3, String line4, String line5, String line6, String line7)
	    {
	        Paragraph address = new Paragraph();
	        address.setLeading(10);
	        address.setSpacingAfter(0.0f);
	        address.setAlignment(Element.ALIGN_LEFT);
	        address.add(new Phrase("To," + Constants.NewLine, Constants.fontParaText));
	        if (!line1.equals("") && line1 != null)
	        {
	            address.add(paraLine(line1.toUpperCase() + Constants.NewLine));
	        }
	        if (!line2.equals("") && line2 != null)
	        {
	            address.add(paraLine(line2.toUpperCase() + ","+Constants.NewLine));
	        }
	        if (!line3.equals("") && line3 != null)
	        {
	            address.add(paraLine(line3.toUpperCase() + Constants.NewLine));
	        }
	        if (!line4.equals("") && line4 != null)
	        {
	            address.add(paraLine(line4.toUpperCase() + Constants.NewLine));
	        }
	        if (!line5.equals("") && line5 != null)
	        {
	            address.add(paraLine(line5.toUpperCase() + Constants.NewLine));
	        }
	        if (!line6.equals("") && line6 != null)
	        {
	            address.add(paraLine(line6.toUpperCase() + "," + line7.toUpperCase() + Constants.NewLine));
	        }

	        //if (line7 != String.Empty)
	        //{
	        //    address.add(ParaLine(line7.ToUpper() + NewLine));
	        //}

	        return address;
	    }
	    public PdfPTable addTable(String first, String second, String third) throws DocumentException
	    {
	        PdfPTable table = new PdfPTable(1);
	        float[] widths = new float[] { 15f };
	        table.setWidths(widths);
	        //table.setSpacingAfter(10.0f);
	        table.setHorizontalAlignment(1);
	        table.addCell(cellData(first));
	        table.addCell(cellData(second));
	        table.addCell(cellData(third));

	        return table;
	    }
	    public Image addSignature(String filepath) throws BadElementException, MalformedURLException, IOException
	    {
	        Image logo = Image.getInstance(filepath);
	        logo.setAbsolutePosition(60f, 800f);
	        logo.scaleToFit(50, 50);
	        logo.setAlignment(Image.ALIGN_LEFT);
	        return logo;
	    }
	    public PdfPCell cellData(String text)
	    {
	        Paragraph curphrase = new Paragraph(text, Constants.fontGeneralText);
	        PdfPCell x = new PdfPCell(curphrase);
	        x.setBackgroundColor(BaseColor.WHITE);
	        return x;
	    }
		
		public Paragraph addBoldParagraph(String text)
	    {
	        Paragraph p = new Paragraph();
	        p.setSpacingAfter(10.0f);
	        p.setAlignment(Element.ALIGN_JUSTIFIED);
	        p.add(new Phrase(text, Constants.fontBoldParaText));
	        return p;
	    }
}
