package com.ghpl.util;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import com.ghpl.util.MyProperties;

/**
 * Servlet implementation class DownloadFtpFile
 */
@WebServlet("/DownloadFtpFile")
public class DownloadFtpFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadFtpFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fileName=null;
		String actualFileName=null;
		String scanningDate=null;
		String scanningGTS=null;
		String FTPHOST=null;
		String FTPUName=null;
		String FTPPwd=null;
		String folderName=null;
		String status= null;
		String constantspath=null;
		String pathcurrent=null;
		String maindocid="0";
		String subdocid="0";
		String moduleName=null;
		int moduleid=0;
		
		String claimId="0";
		String reauthFlag="0";
		String keyFlag="";
		//String extension="";
		
		String uploadGHPL="";
		String ftpFolderName="";
		
		keyFlag=request.getParameter("keyFlag");
		//moduleid=Integer.parseInt(request.getParameter("moduleid"));
			
		MyProperties Gdbp = new MyProperties();

		FTPHOST = Gdbp.getMyProperties("ftp_host");
		FTPUName = Gdbp.getMyProperties("ftp_username");
		FTPPwd = Gdbp.getMyProperties("ftp_password");
		ftpFolderName=Gdbp.getMyProperties("ftp_folder");
		
		if((!"".equals(keyFlag)) && (keyFlag!=null))
		{
			claimId = request.getParameter("ccn");
			reauthFlag = request.getParameter("reauthflag");
			fileName = claimId+"_"+reauthFlag+".xls";
			uploadGHPL = ftpFolderName;
		}
		else
		{
			moduleName=request.getParameter("modulename");
			fileName=request.getParameter("filenme");
			scanningDate=request.getParameter("scanningdate");
			actualFileName=request.getParameter("actfilename");
			maindocid=request.getParameter("maindocid");
			subdocid=request.getParameter("subdocid");
//ftp://192.168.100.99/LIVE/CORP/GHPL/collectiondoc/2/7/
			//uploadGHPL = "/TEST/CORP/"+moduleName+"/collectiondoc/"+maindocid+"/"+subdocid+"/";
			//uploadGHPL = "/TEST/LIVE/CORP/"+moduleName+"/collectiondoc/"+maindocid+"/"+subdocid+"/";	
			if(moduleName.equals("Corp")){
				uploadGHPL = "/LIVE/CORP/GHPL/collectiondoc/"+maindocid+"/"+subdocid+"/";	
			}else{
				uploadGHPL = "/LIVE/CORP/AB/collectiondoc/"+maindocid+"/"+subdocid+"/";	
			}
			
			
		}

		FTPClient client = new FTPClient();
		//System.out.println("CONNECTING");

		boolean success=false;
		try
		{
			
			String curDateString = scanningDate;
			constantspath=uploadGHPL;
			
			if((!"".equals(keyFlag)) && (keyFlag!=null))
			{
				pathcurrent=constantspath;
			}
			else
			{
				pathcurrent=constantspath+curDateString;
			}
			
			File uploadDir = new File(pathcurrent);
			
			System.out.println("pathcurrent:"+pathcurrent);
			
			
				   client.connect(FTPHOST);
		           client.login(FTPUName, FTPPwd);
		           client.setFileType(FTP.BINARY_FILE_TYPE);
		           client.enterLocalPassiveMode();
		           FileOutputStream fos = null;  
		           //File downloadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir"); 
		          
		           
		           ServletContext sc = getServletConfig().getServletContext();
		           
		           File downloadPath = (File)sc.getAttribute("javax.servlet.context.tempdir"); 
		           
		           //System.out.println("downloadPath:"+downloadPath);
					
					String ftpDownloadPath=null;
					/* String pathcurrent=null;
					String constantspath=null;
					String curDateString = scanningDate; */
					 ftpDownloadPath=pathcurrent;
					
					String filePath = ftpDownloadPath + "/" + fileName;
					if(actualFileName==null){
						actualFileName=fileName;
					}
					String downloadTempdir= downloadPath+"/"+actualFileName;
					 File downloadFile2 = new File(downloadTempdir);
					 OutputStream outputStream2=null;
					 InputStream inputStream=null;
				         outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
				         System.out.println("filePath:"+filePath);
				         inputStream = client.retrieveFileStream(filePath);
				        byte[] bytesArray = new byte[4096];
				        int bytesRead = -1;
				        
				        System.out.println("INPUTSTREAM:"+inputStream);
				        
				        if(inputStream!=null){
					        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
					        	outputStream2.write(bytesArray, 0, bytesRead);
					        }
					        success = client.completePendingCommand();
					      if (success) {
					        	System.out.println("File #2 has been downloaded successfully.");
					        }
					     
					        outputStream2.close();
					        inputStream.close();
					        outputStream2.flush();
				        }
				        
						
						 if (success) {
							 
							
						 //File downloadFile = new File(filePath);
					        FileInputStream inStream = new FileInputStream(downloadFile2);
					        // if you want to use a relative path to context root:
					        // obtains ServletContext
					        ServletContext context = getServletContext();
					         
					        // gets MIME type of the file
					        String mimeType = context.getMimeType(filePath);
					        if (mimeType == null) {        
					            // set to binary type if MIME mapping not found
					            mimeType = "application/octet-stream";
					        }
					       // System.out.println("MIME type: " + mimeType);
					         
					        // modifies response
					        response.setContentType(mimeType);
					        response.setContentLength((int) downloadFile2.length());
					         
					        // forces download
					        String headerKey = "Content-Disposition";
					        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile2.getName());
					        response.setHeader(headerKey, headerValue);
					         
					        // obtains response's output stream
					        OutputStream outStream = response.getOutputStream();
					         
					        byte[] buffer = new byte[8912];
					        int bytesRead1 = -1;
					         
					        while ((bytesRead1 = inStream.read(buffer)) != -1) {
					            outStream.write(buffer, 0, bytesRead1);
					        }
					         
					        inStream.close();
					        outStream.close();
					        outStream.flush();
					        downloadFile2.delete();
					        
					        //return;
		}
						 else
						 {
							    PrintWriter out = response.getWriter();
								out.println("<html>");
								out.println("<body>");
								out.println("<h3><div align='center'><a href='#' onclick='window.close()'>Document Not Found. Click here to close the window</a></div></h3>");
								out.println("</body>");
								out.println("</html>");	
							 
							 
							 downloadFile2.delete();
							
						 }
						// System.out.println("AFTER ELSE");
						
						 return;
		}
		catch(Exception e)
		{
			System.out.println("Error"+e);
		}
	}

}
