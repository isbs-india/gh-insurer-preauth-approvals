package com.ghpl.persistence.login;

import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.LoginBean;
import com.ghpl.persistence.DAOFactory;

public class LoginDAOManager {
	
	
	
	public static LoginBean getUserDetails(String userName,String pwd, int serverIp,String hostName)throws DAOException {
		try {
			return DAOFactory.getInstance().getLoginDAO().getUserDetails(userName,pwd,serverIp,hostName);
		} catch (DAOException e) {

		}
		return null;
	}

}
