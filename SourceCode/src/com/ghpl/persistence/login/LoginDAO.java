package com.ghpl.persistence.login;

import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.LoginBean;

public interface LoginDAO {
	public  LoginBean getUserDetails(String uname,String pwd,int serverip,String hostName) throws DAOException;
}
