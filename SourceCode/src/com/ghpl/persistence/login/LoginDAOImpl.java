package com.ghpl.persistence.login;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.ghpl.action.SendMail_preAuth;
import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.LoginBean;
import com.ghpl.util.Crypt;
import com.ghpl.util.DBConn;
import com.ghpl.util.ProcedureConstants;

public class LoginDAOImpl extends DBConn implements LoginDAO {

	Logger logger = Logger.getLogger("LoginDAOImpl");
	public LoginBean getUserDetails(String uname, String pwd,int serverip, String hostName) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		LoginBean user=null;
		ResultSet rs=null;
		int indexpos=0;
		
		try {
			
			con =getMyConnection(1);
			//con.setAutoCommit(false);
			//if(moduleId==1 || moduleId==2 || moduleId==3){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_CHK_LOGIN_ORACLE);
				cstmt.setString(++indexpos, uname);
				cstmt.setString(++indexpos, pwd);
				
				//cstmt.setInt(++indexpos, serverip);
				//cstmt.setString(++indexpos, hostName);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos-1);
			//}
			if (rs.next()) {
				user=new LoginBean();
				//ID, ROLE, USERNAME, NAME, (SELECT HOMEPAGE FROM ROLES WHERE ID =USERS.ROLE) HOMEPAGE
				user.setUid(rs.getInt("ID"));
				user.setRoleId(rs.getInt("ROLE"));
				user.setLoginUserName(rs.getString("USERNAME"));
				user.setHomePage(rs.getString("HOMEPAGE"));
				user.setAssignedMedicoName(rs.getString("ASSIGNED_MEDICO"));
				user.setLoginId(rs.getString("NAME"));
				user.setAbUid(rs.getInt("ABID"));
				user.setVeniusUid(rs.getInt("SAID"));
				user.setRetUid(rs.getInt("RETID"));
			}
			//logger.info(underWritersList);
			//System.out.println("under writers :: "+moduleId +"       "+underWritersList.size());
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();//pbs.getStaticMailId()
     		try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}		
		return user;
	}
}
