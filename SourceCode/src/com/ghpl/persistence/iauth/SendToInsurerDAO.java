package com.ghpl.persistence.iauth;

import java.util.List;

import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.BackToAuditBean;
import com.ghpl.model.iauth.GetInsurerPrefillDataListBean;
import com.ghpl.model.iauth.IAuthAuditBean;
import com.ghpl.model.iauth.IAuthBean;
import com.ghpl.model.iauth.OSTicketBean;
import com.ghpl.model.iauth.PreAuthBean;
import com.ghpl.model.iauth.ProcessSheetBean;
import com.ghpl.model.iauth.UiicDataBean;


public interface SendToInsurerDAO {
	
	public List<IAuthAuditBean> getAuditListDetails(int moduleId) throws DAOException;
	public List<IAuthAuditBean> getAuditListDenialDetails(int moduleId) throws DAOException;
	public List<IAuthAuditBean> getAuditListInsSent(int moduleId) throws DAOException;
	public GetInsurerPrefillDataListBean getUiicDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getNIADataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getbhaxaDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getrelianceDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getitgiDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getreligareDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getUiicDataPrefillRetail(int moduleId,String claimid,String ccnUnique,int reauthFlag,int preAuthno) throws DAOException;
	public GetInsurerPrefillDataListBean getDenialDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException;
	public GetInsurerPrefillDataListBean getClaimDownloadDocData(String claimid,int reauthFlag,int moduleId) throws DAOException;
	
	public List<IAuthBean> getEmployeeDetailsCCN(String insuredorempcode,int moduleId) throws DAOException;
	public ProcessSheetBean getProcessSheetDetailsByCCN(String claimId,int moduleId) throws DAOException;
	public ProcessSheetBean getProcessSheetDetailsByCCNRET(String claimId,int moduleId) throws DAOException;
	public List<IAuthBean> getIAuthSearchRetClaimid(String claimid,int moduleId) throws DAOException;
	public List<OSTicketBean> getKayakoDataDropdown()throws DAOException;
	public PreAuthBean insertIAuthDocumentTypesInsurerRet(PreAuthBean preAuthBean, int moduleId) throws DAOException;
	public PreAuthBean updateIAuthPreAuthClaims(PreAuthBean preAuthBean,int moduleId)throws DAOException;
	public PreAuthBean deleteSesionIdInsurerAuthRet(String sessionid,int moduleId)throws DAOException;
	public PreAuthBean insertIAuthDocumentTypesInsurerCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException;
	public PreAuthBean insertIAuthDocumentTypesInsurerDenialCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException;
	public PreAuthBean insertIAuthDocumentTypesInsurerRetDenial(PreAuthBean preAuthBean, int moduleId) throws DAOException;
	public BackToAuditBean updateClaimStatusBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException;
	public BackToAuditBean updateIAuthPreAuthBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException;
	public GetInsurerPrefillDataListBean getNIADataPrefillRetail(int moduleId,String claimid,int reauthFlag,int preAuthno) throws DAOException;
	public GetInsurerPrefillDataListBean getDefaultDataPrefill(int moduleId, String claimid, String ccnUnique,int reauthflag)throws DAOException;
	public GetInsurerPrefillDataListBean getDefaultDataPrefillRetail(int moduleId, String claimid, String ccnUnique,int reauthflag, int preAuthno)throws DAOException;


}

