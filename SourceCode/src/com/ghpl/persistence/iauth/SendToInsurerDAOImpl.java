package com.ghpl.persistence.iauth;

//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.ObjectOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.ghpl.action.SendMail_preAuth;
import com.ghpl.exception.DAOException;
import com.ghpl.model.iauth.BackToAuditBean;
import com.ghpl.model.iauth.BhratiAxaDataBean;
import com.ghpl.model.iauth.ClaimDenialBean;
import com.ghpl.model.iauth.Claims;
import com.ghpl.model.iauth.DocsDetailsCorpBean;
import com.ghpl.model.iauth.DocumentBean;
import com.ghpl.model.iauth.EmailDetailsBean;
import com.ghpl.model.iauth.IAuthAuditBean;
import com.ghpl.model.iauth.IAuthBean;
import com.ghpl.model.iauth.ItgiDataBean;
import com.ghpl.model.iauth.NIADataBean;
import com.ghpl.model.iauth.OSTicketBean;
import com.ghpl.model.iauth.PreAuthBean;
import com.ghpl.model.iauth.ProcessSheetBean;
import com.ghpl.model.iauth.RelianceDatabean;
import com.ghpl.model.iauth.ReligareDataBean;
import com.ghpl.model.iauth.UiicDataBean;
import com.ghpl.model.iauth.GetInsurerPrefillDataListBean;
import com.ghpl.util.Constants;
import com.ghpl.util.DBConn;
import com.ghpl.util.ProcedureConstants;
import com.ghpl.util.Util;
//import com.itextpdf.text.log.SysoLogger;


public class SendToInsurerDAOImpl extends DBConn implements SendToInsurerDAO{
	Logger logger =(Logger) Logger.getInstance("IAuthDAOImpl");

	public List<IAuthAuditBean> getAuditListDetails(int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		IAuthAuditBean iAuthAuditBean=null;
		ResultSet rs=null;
		int indexpos=0;
		int serialNo=0;
		List<IAuthAuditBean> auditList = new ArrayList<IAuthAuditBean>();
		try {
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_CLM_TO_SENDTO_INSURER);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);

			while (rs.next()) {
				iAuthAuditBean=new IAuthAuditBean();
				iAuthAuditBean.setSerialNo(++serialNo);
				String moduleName="";
				if(rs.getInt("MODULEID")==1){
					moduleName="Corp";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==2){
					moduleName="Bank Assurance";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==4){
					moduleName="Individual";
					moduleId=rs.getInt("MODULEID");
				}
				iAuthAuditBean.setModuleName(moduleName);
				iAuthAuditBean.setModuleId(moduleId);
				iAuthAuditBean.setClaimId(rs.getString("CLAIMID"));
				iAuthAuditBean.setCcnUnique(rs.getString("CCNO"));
				iAuthAuditBean.setReAuthFlag(rs.getInt("REAUTHFLAG"));
				iAuthAuditBean.setPreAuthNo(rs.getInt("PREAUTH_APPNO"));
				iAuthAuditBean.setStatusId(rs.getString("STATUS_ID"));
				iAuthAuditBean.setProcedureName(rs.getString("PROCEDURE_NAME"));
				iAuthAuditBean.setCardId(rs.getString("MEMBER_CARDID"));
				iAuthAuditBean.setMemberName(rs.getString("MEMBER_NAME"));
				iAuthAuditBean.setUwCode(rs.getString("UWCODE"));
				iAuthAuditBean.setModuleId(rs.getInt("MODULEID"));
				iAuthAuditBean.setCreateddate(rs.getString("CREATEDDATE"));
				iAuthAuditBean.setUwId(rs.getString("UWID"));
				auditList.add(iAuthAuditBean);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return auditList;
	}



	public List<IAuthAuditBean> getAuditListDenialDetails(int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		IAuthAuditBean iAuthAuditBean=null;
		ResultSet rs=null;
		int indexpos=0;
		int serialNo=0;
		List<IAuthAuditBean> auditList = new ArrayList<IAuthAuditBean>();
		try {
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_CLM_DENY_TO_SENDTO_INSURER);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);

			while (rs.next()) {
				iAuthAuditBean=new IAuthAuditBean();
				iAuthAuditBean.setSerialNo(++serialNo);
				String moduleName="";
				if(rs.getInt("MODULEID")==1){
					moduleName="Corp";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==2){
					moduleName="Bank Assurance";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==4){
					moduleName="Individual";
					moduleId=rs.getInt("MODULEID");
				}

				iAuthAuditBean.setModuleName(moduleName);
				iAuthAuditBean.setModuleId(moduleId);
				iAuthAuditBean.setCcnUnique(rs.getString("CCNO"));  // open in wind module
				iAuthAuditBean.setClaimId(rs.getString("CLAIMID"));
				iAuthAuditBean.setReAuthFlag(rs.getInt("REAUTHFLAG"));
				iAuthAuditBean.setPreAuthNo(rs.getInt("PREAUTH_APPNO"));
				iAuthAuditBean.setStatusId(rs.getString("STATUS_ID"));
				iAuthAuditBean.setProcedureName(rs.getString("PROCEDURE_NAME"));
				iAuthAuditBean.setCardId(rs.getString("MEMBER_CARDID"));
				iAuthAuditBean.setMemberName(rs.getString("MEMBER_NAME"));
				iAuthAuditBean.setUwCode(rs.getString("UWCODE"));
				iAuthAuditBean.setModuleId(rs.getInt("MODULEID"));
				iAuthAuditBean.setCreateddate(rs.getString("CREATEDDATE"));
				iAuthAuditBean.setUwId(rs.getString("UWID"));
				auditList.add(iAuthAuditBean);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return auditList;
	}



	public List<IAuthAuditBean> getAuditListInsSent(int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		IAuthAuditBean iAuthAuditBean=null;
		ResultSet rs=null;
		int indexpos=0;
		int serialNo=0;
		List<IAuthAuditBean> auditList = new ArrayList<IAuthAuditBean>();
		try {
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_CLM_SENT_TO_INS);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);

			while (rs.next()) {
				iAuthAuditBean=new IAuthAuditBean();
				iAuthAuditBean.setSerialNo(++serialNo);
				String moduleName="";
				if(rs.getInt("MODULEID")==1){
					moduleName="Corp";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==2){
					moduleName="Bank Assurance";
					moduleId=rs.getInt("MODULEID");
				}else if(rs.getInt("MODULEID")==4){
					moduleName="Individual";
					moduleId=rs.getInt("MODULEID");
				}

				iAuthAuditBean.setModuleName(moduleName);
				iAuthAuditBean.setModuleId(moduleId);
				iAuthAuditBean.setCcnUnique(rs.getString("CCNO"));
				iAuthAuditBean.setClaimId(rs.getString("CLAIMID"));
				iAuthAuditBean.setReAuthFlag(rs.getInt("REAUTHFLAG"));
				iAuthAuditBean.setPreAuthNo(rs.getInt("PREAUTH_APPNO"));
				iAuthAuditBean.setStatusId(rs.getString("STATUS_ID"));
				iAuthAuditBean.setProcedureName(rs.getString("PROCEDURE_NAME"));
				iAuthAuditBean.setCardId(rs.getString("MEMBER_CARDID"));
				iAuthAuditBean.setMemberName(rs.getString("MEMBER_NAME"));
				iAuthAuditBean.setUwCode(rs.getString("UWCODE"));
				iAuthAuditBean.setModuleId(rs.getInt("MODULEID"));
				iAuthAuditBean.setCreateddate(rs.getString("CREATEDDATE"));
				iAuthAuditBean.setApprovalType(rs.getString("APPROVALTYPE"));
				auditList.add(iAuthAuditBean);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return auditList;
	}


	//GET UIIC DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getUiicDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_UIIC_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				uiicDataBeanObj=new UiicDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				uiicDataBeanObj.setCcnUnique(ccnUnique);
				uiicDataBeanObj.setInsuredName(rs.getString("INSURED_NAME"));
				uiicDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				uiicDataBeanObj.setAge(rs.getString("AGE"));
				uiicDataBeanObj.setSex(rs.getString("SEX"));
				uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
				uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NO"));
				uiicDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
				uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICYTYPE"));
				uiicDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				uiicDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				uiicDataBeanObj.setCity(rs.getString("CITY"));
				uiicDataBeanObj.setState(rs.getString("STATE"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				uiicDataBeanObj.setDateOfAdmission(rs.getString("DATE_OF_ADMISSION"));
				uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				uiicDataBeanObj.setRoomRentIcuRentAmountRequested(rs.getString("ROOM_RENT_REQUESTED"));
				uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOM_RENT_PAID"));
				uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROFESSIONAL_CHARGES_REQ"));
				uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROFESSIONAL_CHARGES_PAID"));
				uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHARMACY_CHARGES_REQ"));
				uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHARMACY_CHARGES_PAID"));
				uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OT_CHARGES_REQ"));
				uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OT_CHARGES_PAID"));
				uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LAB_INVES_CHARGES_REQ"));
				uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LAB_INVES_CHARGES_PAID"));
				uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTHER_CHARGES_REQ"));
				uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTHER_CHARGES_PAID"));
				uiicDataBeanObj.setProcessingDoctorsNameandtpa(rs.getString("PROCESSING_DOCTOR"));
				uiicDataBeanObj.setTpaName(rs.getString("TPANAME"));
				uiicDataBeanObj.setEmailId(rs.getString("EMAILID"));
				uiicDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				uiicDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));
                
				uiicDataBeanObj.setClaimid(claimid);
				uiicDataBeanObj.setReauthFlag(reauthFlag);
				uiicDataBeanObj.setModuleId(moduleId);

				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				uiicDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				uiicDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				uiicDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				uiicDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				uiicDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));
				
				//INCLUDE CODE HERE FROM DB NEW COLUMN ON 31/AUG/2019
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("TREATMENT_TYPE"));
				uiicDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				uiicDataBeanObj.setRoomRentIcuRent(rs.getString("ROOMRENT_PERDAY"));
				uiicDataBeanObj.setRoomRentEligibility(rs.getString("ELIGIBLE_ROOM_TYPE"));
				uiicDataBeanObj.setNoOfDaysStayed(rs.getString("EXPECTED_DURATION_OF_STAY"));
				/*
				 * uiicDataBeanObj.setGipsaPpaAmountRequested("");
				 * uiicDataBeanObj.setGipsaPpnAmountRecomm("");
				 * uiicDataBeanObj.setGipsaPpnReasonsForDeduction("");
				 * uiicDataBeanObj.setConsultationAmountRecomm("");
				 * uiicDataBeanObj.setConsultationAmountRequested("");
				 * uiicDataBeanObj.setConsultationReasonsForDeduc("");
				 */
				//INCLUDE CODE HERE FROM DB NEW COLUMN
				finalBeanObj.setUiicBean(uiicDataBeanObj);
				
				
				
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


			

			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getUiicBean().getModuleId(),finalBeanObj.getUiicBean().getUnderWriterId(),finalBeanObj.getUiicBean().getIssuingOfficePrimaryKey()));


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}
	
	//GET NIA DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getNIADataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException 
	{
		Connection con = null;
		CallableStatement cstmt = null;
		NIADataBean niaDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		//String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_PREAUTH_NIA_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				niaDataBeanObj=new NIADataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				
				niaDataBeanObj.setClaimId(rs.getString("CLAIMID"));	
				niaDataBeanObj.setInsured(rs.getString("INSURED_NAME"));	
				niaDataBeanObj.setPatient(rs.getString("PATIENT_NAME"));	
				niaDataBeanObj.setAge(rs.getString("AGE"));
				niaDataBeanObj.setSex(rs.getString("SEX"));	
				niaDataBeanObj.setRelation(rs.getString("RELATION_WITH_INSURED"));
				niaDataBeanObj.setBalSuminsured(Integer.parseInt(rs.getString("BALANCESUMINSURED")));
				niaDataBeanObj.setPolicyNo(rs.getString("POLICY_NO"));
				niaDataBeanObj.setSumInsured(Integer.parseInt(rs.getString("SUMINSURED")));
				//niaDataBeanObj.setPolicyType(rs.getString("POLICYTYPE"));
				niaDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				niaDataBeanObj.setHospName(rs.getString("HOSPITAL_NAME"));
				niaDataBeanObj.setCity(rs.getString("CITY"));
				niaDataBeanObj.setState(rs.getString("STATE"));	
				niaDataBeanObj.setNetworks(rs.getString("NETWORKS"));
				niaDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				niaDataBeanObj.setDateofAdmission(rs.getString("DATE_OF_ADMISSION"));
				niaDataBeanObj.setDateofDischarge(rs.getString("DATE_OF_DISCHARGE"));
				niaDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				niaDataBeanObj.setRoomRentRequest(rs.getString("ROOM_RENT_REQUESTED"));
				niaDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_PAID"));	
				niaDataBeanObj.setRoomRentDeducted(rs.getString("ROOM_RENT_DEDUCTED"));		
				niaDataBeanObj.setProfChgHospRequest(rs.getString("PROFESSIONAL_CHARGES_REQ"));		
				niaDataBeanObj.setProfChgPaid(rs.getString("PROFESSIONAL_CHARGES_PAID"));		
				niaDataBeanObj.setProfChgDeducted(rs.getString("PROFESSIONAL_CHARGES_DEDUCTED"));	
				niaDataBeanObj.setPhrHospRequest(rs.getString("PHARMACY_CHARGES_REQ"));	
				niaDataBeanObj.setPhrPaid(rs.getString("PHARMACY_CHARGES_PAID"));	
				niaDataBeanObj.setPhrDeducted(rs.getString("PHARMACY_CHARGES_DEDUCTED"));
				niaDataBeanObj.setOtHospRequest(rs.getString("OT_CHARGES_REQ"));
				niaDataBeanObj.setOtPaid(rs.getString("OT_CHARGES_PAID"));
				niaDataBeanObj.setOtDeducted(rs.getString("OT_CHARGES_DEDUCTED"));
				niaDataBeanObj.setLabInvHospRequest(rs.getString("LAB_INVES_CHARGES_REQ"));
				niaDataBeanObj.setLabInvPaid(rs.getString("LAB_INVES_CHARGES_PAID"));
				niaDataBeanObj.setLabInvDeducted(rs.getString("LAB_INVES_CHARGES_DEDUCTED"));
				niaDataBeanObj.setOthersHospRequest(rs.getString("OTHER_CHARGES_REQ"));
				niaDataBeanObj.setOthersPaid(rs.getString("OTHER_CHARGES_PAID"));
				niaDataBeanObj.setOthersDeducted(rs.getString("OTHER_CHARGES_DEDUCTED"));	
				niaDataBeanObj.setProcessingDoctor(rs.getString("PROCESSING_DOCTOR"));	
				niaDataBeanObj.setTpaName(rs.getString("TPANAME"));	
				niaDataBeanObj.setEmailId(rs.getString("EMAILID"));
				niaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
				niaDataBeanObj.setPolicyDiscription(rs.getString("POLICY_DESCRIPTION"));
				niaDataBeanObj.setCode(rs.getString("CODE"));
				niaDataBeanObj.setClaimId(claimid);
				niaDataBeanObj.setReauthFlag(reauthFlag);
				niaDataBeanObj.setModuleId(moduleId);
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				niaDataBeanObj.setUwId(rs.getInt("UWID"));
				niaDataBeanObj.setIssuingOfficeKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				//INCLUDE CODE HERE FROM DB NEW COLUMN
				niaDataBeanObj.setDisease(rs.getString("DISEASE"));
				niaDataBeanObj.setProcedure(rs.getString("PROCEDURECODE"));
				niaDataBeanObj.setAmount(rs.getString("AMOUNT"));
				niaDataBeanObj.setIcdCode(rs.getString("ICD_CODE"));
				niaDataBeanObj.setRoomType(rs.getString("ROOMTYPE"));
				niaDataBeanObj.setPreauthType(rs.getString("PRE_AUTH_TYPE"));
				niaDataBeanObj.setRoomRentEligibility(rs.getString("ROOM_RENT_ELIGIBILITY"));
				niaDataBeanObj.setDateAndTime(rs.getString("DATE_AND_TIME"));
				
				niaDataBeanObj.setPolicyType(rs.getString("POLICY_TYPE"));
				niaDataBeanObj.setPolicyPeriod(rs.getString("POLICY_PERIOD"));
				niaDataBeanObj.setLetterRemarks(rs.getString("LETTER_REMARKS"));
				
				niaDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				niaDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				niaDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));
				
				
				
				  niaDataBeanObj.setCopayment(rs.getString("Copayment"));
				  niaDataBeanObj.setLineOfTreatment(rs.getString("Line_of_treatment"));
				  niaDataBeanObj.setProcedureincRefrencerate(rs.getString("PROCEDURE_INC_REFRENCE_RATE"));
				  niaDataBeanObj.setAverageCost(rs.getString("AVERAGE_COST"));
				  niaDataBeanObj.setDetailsOfIdProof(rs.getString("DETAILS_ID_PROOF_OBTAINED"));
				  niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_ELIGIBILITY"));
				  niaDataBeanObj.setApplicablePPN(rs.getString("Applicable_PPN"));
				  niaDataBeanObj.setPackageRate(rs.getString("Package_Rate"));
				  niaDataBeanObj.setNoOfDaysRoom(rs.getString("No_days_Room"));
				  niaDataBeanObj.setNoOfDaysICU(rs.getString("No_days_ICU"));
				  niaDataBeanObj.setICUPerDay(rs.getString("ROOMRENT_ICURENT_PER_DAY")); 
				  niaDataBeanObj.setSpecification(rs.getString("Specification_Implants_USED"));

				/*
				 * niaDataBeanObj.setGipsAppnHospAphb(""); //GIPSA_PPN_PACKAGE
				 * niaDataBeanObj.setGipsAppnResonDod(""); niaDataBeanObj.setGipsAppnTpa("");
				 * niaDataBeanObj.setConChrDeducted("");
				 * niaDataBeanObj.setConChrHospRequest(""); niaDataBeanObj.setConChrPaid("");
				 */
			
				finalBeanObj.setNIABean(niaDataBeanObj);
			}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			if(finalBeanObj.getNIABean()!=null){
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getNIABean().getModuleId(),finalBeanObj.getNIABean().getUwId(),finalBeanObj.getNIABean().getIssuingOfficeKey()));
			}else{
				finalBeanObj = null;
			}


		}
		catch (Exception ex) {
			ex.printStackTrace();
			/*SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}*/
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}



	//GET UIIC DATA PREFILL RETAIL
	public GetInsurerPrefillDataListBean getUiicDataPrefillRetail(int moduleId,String claimid,String ccnUnique,int reauthFlag,int preAuthno) throws DAOException {

		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_UIIC_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				uiicDataBeanObj=new UiicDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				uiicDataBeanObj.setCcnUnique(ccnUnique);
				uiicDataBeanObj.setInsuredName(rs.getString("INSURED_NAME"));
				uiicDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				uiicDataBeanObj.setAge(rs.getString("AGE"));
				uiicDataBeanObj.setSex(rs.getString("SEX"));
				uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
				uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NO"));
				uiicDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
				uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICYTYPE"));
				uiicDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				uiicDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				uiicDataBeanObj.setCity(rs.getString("CITY"));
				uiicDataBeanObj.setState(rs.getString("STATE"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				uiicDataBeanObj.setDateOfAdmission(rs.getString("DATE_OF_ADMISSION"));
				uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				uiicDataBeanObj.setRoomRentIcuRentAmountRequested(rs.getString("ROOM_RENT_REQUESTED"));
				uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOM_RENT_PAID"));
				uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROFESSIONAL_CHARGES_REQ"));
				uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROFESSIONAL_CHARGES_PAID"));
				uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHARMACY_CHARGES_REQ"));
				uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHARMACY_CHARGES_PAID"));
				uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OT_CHARGES_REQ"));
				uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OT_CHARGES_PAID"));
				uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LAB_INVES_CHARGES_REQ"));
				uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LAB_INVES_CHARGES_PAID"));
				uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTHER_CHARGES_REQ"));
				uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTHER_CHARGES_PAID"));
				uiicDataBeanObj.setProcessingDoctorsNameandtpa(rs.getString("PROCESSING_DOCTOR"));
				uiicDataBeanObj.setTpaName(rs.getString("TPANAME"));
				uiicDataBeanObj.setEmailId(rs.getString("EMAILID"));
				uiicDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				uiicDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));

				uiicDataBeanObj.setClaimid(claimid);
				uiicDataBeanObj.setReauthFlag(reauthFlag);
				uiicDataBeanObj.setModuleId(moduleId);

				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				uiicDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				uiicDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				uiicDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				uiicDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				uiicDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));

				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("TREATMENT_TYPE"));
				uiicDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				uiicDataBeanObj.setRoomRentIcuRent(rs.getString("ROOMRENT_PERDAY"));
				uiicDataBeanObj.setRoomRentEligibility(rs.getString("ELIGIBLE_ROOM_TYPE"));
				uiicDataBeanObj.setNoOfDaysStayed(rs.getString("EXPECTED_DURATION_OF_STAY"));
				/*
				 * uiicDataBeanObj.setGipsaPpaAmountRequested("");
				 * uiicDataBeanObj.setGipsaPpnAmountRecomm("");
				 * uiicDataBeanObj.setGipsaPpnReasonsForDeduction("");
				 * uiicDataBeanObj.setConsultationAmountRecomm("");
				 * uiicDataBeanObj.setConsultationAmountRequested("");
				 * uiicDataBeanObj.setConsultationReasonsForDeduc("");
				 */
				//INCLUDE CODE HERE FROM DB NEW COLUMN

				finalBeanObj.setUiicBean(uiicDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getUiicBean().getModuleId(),finalBeanObj.getUiicBean().getUnderWriterId(),finalBeanObj.getUiicBean().getIssuingOfficePrimaryKey()));


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	
	}


	//GET BHARATI AXA DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getbhaxaDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		BhratiAxaDataBean bhaxaDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;                    
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;
		//int indexpos=0;
		//int serialNo=0;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_BHAXA_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				bhaxaDataBeanObj=new BhratiAxaDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				bhaxaDataBeanObj.setClaimid(claimid);
				bhaxaDataBeanObj.setReauthFlag(reauthFlag);
				bhaxaDataBeanObj.setModuleId(moduleId);
				/*bhaxaDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				bhaxaDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
				bhaxaDataBeanObj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
				bhaxaDataBeanObj.setCorporateName(rs.getString("CORPORATE_NAME"));
				bhaxaDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				bhaxaDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
				bhaxaDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
				bhaxaDataBeanObj.setEmailId(rs.getString("EMAILID"));
				bhaxaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
				bhaxaDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));*/
				
				bhaxaDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				bhaxaDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				
				bhaxaDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
				bhaxaDataBeanObj.setPolicyNumber(rs.getString("POLICYNO"));
				bhaxaDataBeanObj.setBalanceSi(rs.getString("BALANCESUMINSURED"));
				bhaxaDataBeanObj.setClaimType(rs.getString("Claim_type"));
				bhaxaDataBeanObj.setPolicyPeriod(rs.getString("Policy_period"));
				bhaxaDataBeanObj.setInitialInterimFinal(rs.getString("INITIALSET"));
				bhaxaDataBeanObj.setHospitalName(rs.getString("HOSPITALNAME"));
				bhaxaDataBeanObj.setDoaDod(rs.getString("DOA_DOD"));
				bhaxaDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				bhaxaDataBeanObj.setTreatment(rs.getString("TREATMENT"));
				bhaxaDataBeanObj.setTariff(rs.getString("TARIFF"));
				bhaxaDataBeanObj.setAnyNegotiations(rs.getString("NEGOTIATIONS"));
				bhaxaDataBeanObj.setTpaRecommendations(rs.getString("TPA_RECOMMENDATIONS"));
				
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				bhaxaDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				bhaxaDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				bhaxaDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				bhaxaDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				bhaxaDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));

				finalBeanObj.setBhaxaBean(bhaxaDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getBhaxaBean().getModuleId(),finalBeanObj.getBhaxaBean().getUnderWriterId(),finalBeanObj.getBhaxaBean().getIssuingOfficePrimaryKey()));



		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				StringBuilder sb=new StringBuilder();
				sb.append("METHOD: getbhaxaDataPrefill"+"\n");
				sb.append("CLAIMID:"+claimid+"\n");
				sb.append("REAUTHFLAG:"+reauthFlag+"\n");
				sb.append("MODULEID:"+moduleId+"\n");
				
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION","ERROR"+ex.getMessage()+"\n"+sb.toString(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	//GET Reliance DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getrelianceDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		RelianceDatabean relianceDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;
		//int indexpos=0;
		//int serialNo=0;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_RGIL_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				relianceDataBeanObj=new RelianceDatabean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				relianceDataBeanObj.setClaimid(claimid);
				relianceDataBeanObj.setReauthFlag(reauthFlag);
				relianceDataBeanObj.setModuleId(moduleId);

				relianceDataBeanObj.setCaseType(rs.getString("CASE_TYPE"));
				relianceDataBeanObj.setClaimType(rs.getString("CLAIM_TYPE"));
				relianceDataBeanObj.setPreauthCaseId(rs.getString("PRE_AUTH_CASE_ID"));
				relianceDataBeanObj.setClaimNoUrlNo(rs.getString("CLAIM_NO_URL_NO"));
				relianceDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				relianceDataBeanObj.setAge(rs.getString("AGE"));
				relianceDataBeanObj.setHospitalName(rs.getString("HOSPITALNAME"));
				relianceDataBeanObj.setHospitalType(rs.getString("HOSPITAL_TYPE"));
				relianceDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				relianceDataBeanObj.setCorporateName(rs.getString("CORPORATE_NAME"));
				relianceDataBeanObj.setPolicyNo(rs.getString("POLICY_NO"));
				relianceDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				relianceDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				relianceDataBeanObj.setLineOfTreatment(rs.getString("LINE_OF_TREATMENT"));
				relianceDataBeanObj.setRemarks(rs.getString("REMARKS"));
				relianceDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
				relianceDataBeanObj.setInitialAmountPaid(rs.getString("INITIAL_AMOUNT_PAID"));
				relianceDataBeanObj.setPayableAmount(rs.getString("PAYABLE_AMOUNT"));
				relianceDataBeanObj.setCorporateBuffer(rs.getString("CORPORATE_BUFFER"));
				relianceDataBeanObj.setEmailId(rs.getString("EMAILID"));
				relianceDataBeanObj.setCcemailId(rs.getString("CC_EMAILID"));
				
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				relianceDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				relianceDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				relianceDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				relianceDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				relianceDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));


				finalBeanObj.setRelianceBean(relianceDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

			
			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getRelianceBean().getModuleId(),finalBeanObj.getRelianceBean().getUnderWriterId(),finalBeanObj.getRelianceBean().getIssuingOfficePrimaryKey()));


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}



	//GET ITGI DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getitgiDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ItgiDataBean itgiDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;
		//int indexpos=0;
		//int serialNo=0;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_ITGI_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				itgiDataBeanObj=new ItgiDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				itgiDataBeanObj.setClaimid(claimid);
				itgiDataBeanObj.setReauthFlag(reauthFlag);
				itgiDataBeanObj.setModuleId(moduleId);
				itgiDataBeanObj.setCardId(rs.getString("CARDID"));
				itgiDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				itgiDataBeanObj.setAdmDate(rs.getString("ADM_DATE"));
				itgiDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				itgiDataBeanObj.setLengthOfStay(rs.getString("LENGTH_OF_STAY"));
				itgiDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				itgiDataBeanObj.setTpaFinalBillEstimate(rs.getString("TPA_FINAL_BILL_ESTIMATE"));
				itgiDataBeanObj.setEmailId(rs.getString("EMAILID"));
				itgiDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
				
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				itgiDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				itgiDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));

				itgiDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				itgiDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				itgiDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));
				
				
				  itgiDataBeanObj.setMobileHospRep(rs.getString("MOBILE_HOSP_REP"));
				  itgiDataBeanObj.setBenificiaryName(rs.getString("BENEFICIARY_NAME"));
				  itgiDataBeanObj.setMedicalSurgDayCare(rs.getString("TREATMENT_TYPE"));
				  itgiDataBeanObj.setGrName(rs.getString("DR_NAME"));
				  itgiDataBeanObj.setCity(rs.getString("CITY"));
				  itgiDataBeanObj.setMobileDoctor(rs.getString("MOBILE_DOCTOR"));
				  itgiDataBeanObj.setTeleBeneficiary(rs.getString("TELE_BENEFICIARY2"));
				  itgiDataBeanObj.setIcuAdmissionRoom(rs.getString("ROOM_TYPE"));
				//  itgiDataBeanObj.setRoomIcuTariff(rs.getString(""));
				  itgiDataBeanObj.setConsultantCharges(rs.getString("CONSULTANT_CHARGES"));
				  itgiDataBeanObj.setSurgeryCharges(rs.getString("SURGERY_CHARGES"));
				  itgiDataBeanObj.setOtCharges(rs.getString("OT_CHARGES"));
				  itgiDataBeanObj.setImplantsCostIf_any(rs.getString("IMPLANTS_COST"));
				  itgiDataBeanObj.setAlAmountAskedByNsp(rs.getString("AL_AMOUNT"));
				  itgiDataBeanObj.setPreNegotiatedTariff(rs.getString("PRE_NEGOTIATED_TARIFF"));
				  itgiDataBeanObj.setRmoNursing(rs.getString("RMO_NURSING"));
				  itgiDataBeanObj.setMedicineCost(rs.getString("MEDICINES_COST"));
				  itgiDataBeanObj.setInvstCosts(rs.getString("INVST_COSTS"));
				  itgiDataBeanObj.setAnaesthesiaCharges(rs.getString("ANAESTHESIA_CHARGES"));
				  itgiDataBeanObj.setAnyOtherExpenses(rs.getString("ANY_OTHER_EXPENSES"));
				  itgiDataBeanObj.setRecommendedAlamtBytpa(rs.getString("RECOMMENDED_AL_AMT"));
				/// itgiDataBeanObj.setComplaintsWithDuration(rs.getString(""));
				  itgiDataBeanObj.setClinicalFindings(rs.getString("CLINICAL_FINDINGS"));
				  itgiDataBeanObj.setTestsDoneSoFar(rs.getString("TESTS_DONE"));
				  itgiDataBeanObj.setMedicalDetails(rs.getString("MEDICAL_DETAILS"));
				  itgiDataBeanObj.setSurgeryRx(rs.getString("SURGERY_RX"));
				 
				
				finalBeanObj.setItgiBean(itgiDataBeanObj);
			}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getItgiBean().getModuleId(),finalBeanObj.getItgiBean().getUnderWriterId(),finalBeanObj.getItgiBean().getIssuingOfficePrimaryKey()));

			

		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}



	//GET RELIGARE DATA PREFILL CORP
	public GetInsurerPrefillDataListBean getreligareDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ReligareDataBean religareDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;
		//int indexpos=0;
		//int serialNo=0;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_RELIGARE_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				religareDataBeanObj=new ReligareDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				religareDataBeanObj.setClaimid(claimid);
				religareDataBeanObj.setReauthFlag(reauthFlag);
				religareDataBeanObj.setModuleId(moduleId);
				religareDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				religareDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
				religareDataBeanObj.setAge(rs.getString("AGE"));
				religareDataBeanObj.setGender(rs.getString("GENDER"));
				religareDataBeanObj.setRelationShip(rs.getString("RELATIONSHIP"));
				religareDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				religareDataBeanObj.setPackagee(rs.getString("PACKAGE"));
				religareDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				religareDataBeanObj.setCity(rs.getString("CITY"));
				religareDataBeanObj.setDoa(rs.getString("DOA"));
				religareDataBeanObj.setDod(rs.getString("DOD"));
				religareDataBeanObj.setDurationOfStay(rs.getString("DURATION_OF_STAY"));
				religareDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				religareDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				religareDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_PAID"));
				religareDataBeanObj.setAmtPaid(rs.getString("AMTPAID"));
				religareDataBeanObj.setAmtClaimed(rs.getString("AMTCLAIMED"));
				religareDataBeanObj.setApprovedAmount(rs.getString("AMTPAID"));
				religareDataBeanObj.setRequestedAmount(rs.getString("AMTCLAIMED"));
				religareDataBeanObj.setCopayPer(rs.getString("COPAYMENT_PER"));
				religareDataBeanObj.setPlanOfTreatment(rs.getString("PLANOFTREATMENT"));
				religareDataBeanObj.setDrRemarks(rs.getString("DR_REAMRKS"));
				religareDataBeanObj.setEmailId(rs.getString("EMAILID"));
				religareDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				religareDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				religareDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				religareDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				religareDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				religareDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));

				finalBeanObj.setReligareBean(religareDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

		
			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getReligareBean().getModuleId(),finalBeanObj.getReligareBean().getUnderWriterId(),finalBeanObj.getReligareBean().getIssuingOfficePrimaryKey()));



		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}



	//GET DENIAL DATA PREFILL CORP (MODIFIED TO INCLUDE RETAIL ALSO)
	public GetInsurerPrefillDataListBean getDenialDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ClaimDenialBean claimDenialDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;
		//int indexpos=0;
		//int serialNo=0;

		try {
			con =getMyConnection(moduleId);
			//if(moduleId==1){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_PREFILL_DENIAL_DTLS_CORP);
				cstmt.setString(1, claimid);
				cstmt.setInt(2, reauthFlag);
				cstmt.registerOutParameter(3, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(3);

				finalBeanObj=new GetInsurerPrefillDataListBean();
				if(rs.next()) {
					claimDenialDataBeanObj=new ClaimDenialBean();

					claimDenialDataBeanObj.setClaimid(claimid);
					claimDenialDataBeanObj.setReauthFlag(reauthFlag);
					claimDenialDataBeanObj.setModuleId(moduleId);
					claimDenialDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
					claimDenialDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
					claimDenialDataBeanObj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
					claimDenialDataBeanObj.setCorporatename(rs.getString("CORPORATE_NAME"));
					claimDenialDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
					claimDenialDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
					claimDenialDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
					claimDenialDataBeanObj.setPolicydesc(rs.getString("POLICY_DESCRIPTION"));
					claimDenialDataBeanObj.setEmailId(rs.getString("EMAILID"));
					claimDenialDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
					claimDenialDataBeanObj.setMedicoRemarks(rs.getString("MEDICOREMARKS"));

					//New columns which needs to be added in the procedure for Unified UW related data from GHPL
					claimDenialDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
					claimDenialDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
					//POLICYID, PAT_INSUREDID, PAT_CARDID
					
					claimDenialDataBeanObj.setPolicyId(rs.getString("POLICYID"));
					claimDenialDataBeanObj.setInsurerId(rs.getString("PAT_INSUREDID"));
					claimDenialDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
					claimDenialDataBeanObj.setCcnUnique(ccnUnique);
					
					
					finalBeanObj.setClmDenialBean(claimDenialDataBeanObj);
				}

				cstmt.close();
				cstmt=null;
				rs.close();
				rs=null;

			
			//}//if CORP MODULES
			/*else if(moduleId==4 || moduleId==5 || moduleId==6 || moduleId==7 || moduleId==8){


				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_PREFILL_PREAUTH_DENIAL_SQLSERVER);
				cstmt.setString(1, claimid);
				cstmt.setInt(2, reauthFlag);
				cstmt.execute();
				rs = cstmt.getResultSet();
				if(rs != null){

					if(rs.next()) {
						claimDenialDataBeanObj=new ClaimDenialBean();
						finalBeanObj=new GetInsurerPrefillDataListBean();

						claimDenialDataBeanObj.setClaimid(claimid);
						claimDenialDataBeanObj.setReauthFlag(reauthFlag);
						claimDenialDataBeanObj.setModuleId(moduleId);
						claimDenialDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
						claimDenialDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
						claimDenialDataBeanObj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
						claimDenialDataBeanObj.setCorporatename(rs.getString("CORPORATE_NAME"));
						claimDenialDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
						claimDenialDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
						claimDenialDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
						claimDenialDataBeanObj.setPolicydesc(rs.getString("POLICY_DESCRIPTION"));
						claimDenialDataBeanObj.setEmailId(rs.getString("EMAILID"));
						claimDenialDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
						claimDenialDataBeanObj.setMedicoRemarks(rs.getString("MEDICOREMARKS"));
						
						//New columns which needs to be added in the procedure for Unified UW related data from GHPL
						claimDenialDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
						claimDenialDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
						
						//santhosh
						claimDenialDataBeanObj.setPolicyId(rs.getString("PolicyID"));
						claimDenialDataBeanObj.setInsurerId(rs.getString("INSUREDID"));
						claimDenialDataBeanObj.setCardId(rs.getString("CardId"));

						finalBeanObj.setClmDenialBean(claimDenialDataBeanObj);


					}
				}

				cstmt.close();
				cstmt=null;
				rs.close();
				rs=null;


				//DOCS DISPLAY IN DENIAL PREFILL FORM FOR RETAIL

				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_I_AUTH_PREOCESS_SHEET_DATA);
				cstmt.setString(1, claimid);
				cstmt.execute();
				rsDocsInfo=cstmt.getResultSet();
				//
				if(rsDocsInfo != null)
				{

					if(rsDocsInfo.next()){
						//DO NOTHING
					}

				}

				boolean result = cstmt.getMoreResults();
				if(result){
					rsDocsInfo = cstmt.getResultSet();


					while(rsDocsInfo.next())
					{
						//finalBeanObj=new GetInsurerPrefillDataListBean();
						docsDetailsCorpBeanObj = new DocsDetailsCorpBean();
						docsDetailsCorpBeanObj.setFileName(rsDocsInfo.getString("FILE_NAME"));
						docsDetailsCorpBeanObj.setFolder(rsDocsInfo.getString("FOLDER"));
						docsDetailsCorpList.add(docsDetailsCorpBeanObj);
						finalBeanObj.setDocsDetailsCorpList(docsDetailsCorpList);

					}

				}


				//DOCS END



			}*///RETAIL MODULES

			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			if(finalBeanObj.getClmDenialBean()!=null){
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getClmDenialBean().getModuleId(),finalBeanObj.getClmDenialBean().getUnderWriterId(),finalBeanObj.getClmDenialBean().getIssuingOfficePrimaryKey()));
			}else{
				finalBeanObj= null;
			}

			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}




	//GET CLAIM DETAILS FOR DOWNLOAD DOCS
	public GetInsurerPrefillDataListBean getClaimDownloadDocData(String claimid,int reauthFlag,int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		String uwCode=null;
		String UwId=null;
		//int moduleId=0;
		GetInsurerPrefillDataListBean finalBeanObj=null;

		try {
			con =getMyConnection(1);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_PREFILL_CLAIM_DTLS);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.setInt(3, moduleId);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(4);

			if(rs.next()) 
			{
				uwCode=rs.getString("UWCODE").trim();
				UwId=rs.getString("UWID").trim();
				//moduleId=rs.getInt("MODULEID");
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


			if("BX".equals(uwCode) )
			{
				finalBeanObj=getbhaxaDataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_BHAXA);
				}
			}
			else if("UI".equals(uwCode) )
			{
				finalBeanObj=getUiicDataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_UIIC);
				}
			}
			else if("UIK".equals(UwId) )
			{
				finalBeanObj=getUiicDataPrefillShowDocRetail(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_UIIC);
				}
			}
			else if("RL".equals(uwCode) )
			{

				finalBeanObj=getRgilDataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_RGIL);
				}
			}
			else if("IT".equals(uwCode) )
			{

				finalBeanObj=getItgiDataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_ITGI);
				}
			}
			else if("RE".equals(uwCode) )
			{
				finalBeanObj=getReligareDataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_RELIGARE);
				}
			}
			else if("NA".equals(uwCode) )
			{
				finalBeanObj=getNIADataPrefillShowDoc(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_NIA);
				}
			}else if("NIA".equals(UwId))
			{
				finalBeanObj=getNIADataPrefillShowDocRetail(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_NIA);
				}
			}else if("NCM".equals(UwId))
			{
				finalBeanObj=getNIADataPrefillShowDocRetail(moduleId,claimid,reauthFlag);

				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_NIA);
				}
			}
			else if(UwId!=null)
			{
				if(UwId.contains("UI") || uwCode.contains("UI")){
				finalBeanObj=getUiicDataPrefillShowDocRetail(moduleId,claimid,reauthFlag);
				
				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_UIIC);
				   }
			    }

				if(UwId.contains("NI") || uwCode.contains("NI")){
					finalBeanObj=getNIADataPrefillShowDocRetail(moduleId,claimid,reauthFlag);
					
				if(finalBeanObj != null){
					finalBeanObj.setFileName(Constants.DOWNLOAD_REDIRECT_JSP_NIA);
				    }
				}
			}
			


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	
	//GET NIA DATA PREFILL RETAIL SHOW DOC
	public GetInsurerPrefillDataListBean getNIADataPrefillShowDocRetail(int moduleId,String claimid,int reauthFlag) throws DAOException {

		Connection con = null;
		CallableStatement cstmt = null;
		NIADataBean niaDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_NIA_PREFILL_DATA);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);
		
				if(rs.next()) {
					niaDataBeanObj=new NIADataBean();
					finalBeanObj=new GetInsurerPrefillDataListBean();
				
					niaDataBeanObj.setClaimNo(rs.getString("CLAIM_NO"));
					niaDataBeanObj.setPolicynoAndPolicyperiad(rs.getString("POLICYNO_POLICYPERIOD"));
					niaDataBeanObj.setInsured(rs.getString("INSURED_NAME"));
					niaDataBeanObj.setPatient(rs.getString("PATIENT_NAME"));
					niaDataBeanObj.setIncase(rs.getString("DEVIATIONS"));
					niaDataBeanObj.setCopayment(rs.getString("COPAYMENT"));
					niaDataBeanObj.setPreauthType(rs.getString("PREAUTH_TYPE"));
					niaDataBeanObj.setBalSuminsured(rs.getLong("BALANCESUMINSURED"));
					niaDataBeanObj.setRelation(rs.getString("RELATION"));
					niaDataBeanObj.setGenderAndAge(rs.getString("GENDER_AGE"));
					niaDataBeanObj.setSumInsured(rs.getLong("SUM_INSURED1"));
					niaDataBeanObj.setPolicyNoAndValidity(rs.getString("POLICYNO_VALIDITY"));
					niaDataBeanObj.setDiseaseAndProcedure(rs.getString("DISEASE_PROCEDURE"));
					niaDataBeanObj.setAmount(rs.getString("AMOUNT"));
					niaDataBeanObj.setHospName(rs.getString("HOSPITAL_NAME"));
					niaDataBeanObj.setCityState(rs.getString("CITY_STATE"));
					niaDataBeanObj.setPpnAndCategory(rs.getString("PPN_CATEGORY"));
					niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_FOR_SINGLE_ROOM"));
					niaDataBeanObj.setDiseaseAndDiagnosis(rs.getString("DIASEASE_DIAGNOSIS"));
					niaDataBeanObj.setLineOfTreatment(rs.getString("LINE_OF_TREATMENT"));
					niaDataBeanObj.setIcdCode(rs.getString("ICD_CODE"));
					niaDataBeanObj.setApplicablePPN(rs.getString("APPLICABLE_PPN"));
					niaDataBeanObj.setDateofAdmission(rs.getString("DOA"));
					niaDataBeanObj.setDateofDischarge(rs.getString("DOD"));
					niaDataBeanObj.setNoOfDaysRoom(rs.getString("NO_OF_DAYS_ROOM"));
					niaDataBeanObj.setNoOfDaysICU(rs.getString("NO_OF_DAYS_ICU"));
					niaDataBeanObj.setDetailsOfIdProof(rs.getString("DETAILSOFIDPROOF"));
					niaDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
					niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_PER_DAY"));
					niaDataBeanObj.setRoomRentEligibility(rs.getString("ROOM_RENT_ELIGIBILITY"));
					niaDataBeanObj.setGipsAppnHospAphb(rs.getString("GIPSA_PPN_PACKAGE_REQ"));
					niaDataBeanObj.setRoomRentRequest(rs.getString("ROOM_RENT_REQ"));
					niaDataBeanObj.setProfChgHospRequest(rs.getString("PROFESSIONAL_CHARGES_REQ"));
					niaDataBeanObj.setPhrHospRequest(rs.getString("PHARMACY_REQ"));
					niaDataBeanObj.setOtHospRequest(rs.getString("OT_CHARGES_REQ"));
					niaDataBeanObj.setLabInvHospRequest(rs.getString("LAB_INVESTIGATIONS_REQ"));
					niaDataBeanObj.setConChrHospRequest(rs.getString("CONSULTATION_CHARGES_REQ"));
					niaDataBeanObj.setOthersHospRequest(rs.getString("OTHERS_REQ"));
					niaDataBeanObj.setTotalHospRequest(rs.getString("TOTAL_REQ"));
					
					niaDataBeanObj.setGipsAppnTpa(rs.getString("GIPSA_PPN_PACKAGE_RECOMM"));
					niaDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_RECOMM"));
					niaDataBeanObj.setProfChgPaid(rs.getString("PROFESSIONAL_CHARGES_RECOMM"));
					niaDataBeanObj.setPhrPaid(rs.getString("PHARMACY_RECOMM"));
					niaDataBeanObj.setOtPaid(rs.getString("OT_CHARGES_RECOMM"));
					niaDataBeanObj.setLabInvPaid(rs.getString("LAB_INVESTIGATIONS_RECOMM"));
					niaDataBeanObj.setConChrPaid(rs.getString("CONSULTATION_CHARGES_RECOMM"));
					niaDataBeanObj.setOthersPaid(rs.getString("OTHERS_RECOMM"));
					niaDataBeanObj.setTotalPaid(rs.getString("TOTAL_RECOMM"));
					
					niaDataBeanObj.setGipsAppnResonDod(rs.getString("GIPSA_PPN_PACKAGE_DED"));									
					niaDataBeanObj.setRoomRentDeducted(rs.getString("ROOM_RENT_DED"));
					niaDataBeanObj.setProfChgDeducted(rs.getString("PROFESSIONAL_CHARGES_DED"));
					niaDataBeanObj.setPhrDeducted(rs.getString("PHARMACY_DED"));
					niaDataBeanObj.setOtDeducted(rs.getString("OT_CHARGES_DED"));
					niaDataBeanObj.setLabInvDeducted(rs.getString("LAB_INVESTIGATIONS_DED"));
					niaDataBeanObj.setConChrDeducted(rs.getString("CONSULTATION_CHARGES_DED"));
					niaDataBeanObj.setOthersDeducted(rs.getString("OTHERS_DED"));
					niaDataBeanObj.setTotalDeducted(rs.getString("TOTAL_DED"));
					
					niaDataBeanObj.setClaimId(rs.getString("CLAIMID"));
					niaDataBeanObj.setReauthFlag(Integer.parseInt(rs.getString("REAUTHFLAG")));
					niaDataBeanObj.setEmailId(rs.getString("EMAILID"));
					niaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID")); 
					
					niaDataBeanObj.setSpecification(rs.getString("SPEC_IMPLANTS_USED"));
					niaDataBeanObj.setNameAndQual(rs.getString("PROCESSINGDOCTORSNAMEANDTPA"));
					niaDataBeanObj.setDateAndTime(rs.getString("DT_TIME_FORMAT_SENT"));
					
					niaDataBeanObj.setPolicyType(rs.getString("POLICY_TYPE"));
					niaDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
					
					niaDataBeanObj.setDiseaseTreatment(rs.getString("DISEASE_TREATMENT_ARE_COVERED"));
					niaDataBeanObj.setPpnPackage(rs.getString("CHRGS_ALLOW_PPN_PKG"));
					niaDataBeanObj.setExpencesAllowed(rs.getString("EXP_REASONABLE"));
					
					niaDataBeanObj.setId(rs.getString("ID"));
					niaDataBeanObj.setCreatedDate(rs.getString("CREATEDDATE"));

					finalBeanObj.setNIABean(niaDataBeanObj);
				}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	
	}
	
	//GET UIIC DATA PREFILL CORP SHOW DOC
	public GetInsurerPrefillDataListBean getUiicDataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_UIIC_PREFILL_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				uiicDataBeanObj=new UiicDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				uiicDataBeanObj.setInsuredName(rs.getString("INSURED_NAME"));
				uiicDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				uiicDataBeanObj.setAge(rs.getString("AGE"));
				uiicDataBeanObj.setSex(rs.getString("SEX"));
				uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
				uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCE_SUM_INSURED"));
				uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NUMBER"));
				uiicDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICY_TYPE_INDIVIDUAL_OR_GMC"));
				uiicDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				uiicDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				uiicDataBeanObj.setCity(rs.getString("CITY"));
				uiicDataBeanObj.setState(rs.getString("STATE"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("PPN_NETWORK_HOSPITALS"));
				uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				uiicDataBeanObj.setDateOfAdmission(rs.getString("DATEOFADMISSION"));
				uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTEDDURATIONOFSTAY"));
				uiicDataBeanObj.setRoomRentIcuRentAmountRequested(rs.getString("ROOMRENTICURENTAMOUNTREQUESTED"));
				uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOMRENT_AMOUNT_RECOMM"));
				uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROCEDUREAMOUNTREQUESTED"));
				uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROCEDURE_AMOUNT_RECOMM"));
				uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHARMACYAMOUNTREQUESTED"));
				uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHARMACY_AMOUNT_RECOMM"));
				uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OTANESTHESIAAMOUNTREQUESTED"));
				uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OTANESTHESIA_AMOUNT_RECOMM"));
				uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LABINVESTIGATIONSAMTREQUESTED"));
				uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LABINVESTIGATIONSAMTRECOMM"));
				uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTHERS_AMOUNTREQUESTED"));
				uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTHERS_AMOUNT_RECOMM"));
				uiicDataBeanObj.setProcessingDoctorsNameandtpa(rs.getString("PROCESSINGDOCTORSNAMEANDTPA"));
				uiicDataBeanObj.setTpaName(rs.getString("TPA_NAME"));
				uiicDataBeanObj.setEmailId(rs.getString("EMAILID"));
				uiicDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				uiicDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));


				uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("MANAGEMENTORMEDICALOREMERGENCY"));
				uiicDataBeanObj.setRoomType(rs.getString("ROOMTYPE"));
				uiicDataBeanObj.setRoomRentIcuRent(rs.getString("ROOMRENT_ICURENT"));
				uiicDataBeanObj.setRoomRentEligibility(rs.getString("ROOMRENT_ELIGIBILITY"));
				uiicDataBeanObj.setGipsaPpaAmountRequested(rs.getString("GIPSAPPNAMOUNTREQUESTED"));
				uiicDataBeanObj.setGipsaPpnAmountRecomm(rs.getString("GIPSAPPN_AMOUNT_RECOMM"));
				uiicDataBeanObj.setGipsaPpnReasonsForDeduction(rs.getString("GIPSAPPN_REASONSFORDEDUCTION"));
				uiicDataBeanObj.setRoomRentReasonsForDeduction(rs.getString("ROOMRENT_REASONSFORDEDUCTION"));
				uiicDataBeanObj.setProcedureReasonsForDeduction(rs.getString("PROCEDURE_REASONSFORDEDUCTION"));
				uiicDataBeanObj.setPharmacyReasonsForDeduction(rs.getString("PHARMACY_REASONSFORDEDUCTION"));
				uiicDataBeanObj.setOthersReasonsForDeduction(rs.getString("OTANESTHESIAREASONSDEDUCTION"));
				uiicDataBeanObj.setLabInvestigationrsnDeduction(rs.getString("LABINVESTIGATIONRSNDEDUCTION"));
				uiicDataBeanObj.setConsultationAmountRequested(rs.getString("CONSULTATIONAMOUNTREQUESTED"));
				uiicDataBeanObj.setConsultationAmountRecomm(rs.getString("CONSULTATION_AMOUNT_RECOMM"));
				uiicDataBeanObj.setConsultationReasonsForDeduc(rs.getString("CONSULTATIONREASONSFORDEDUC"));
				uiicDataBeanObj.setOthersReasonsForDeduction(rs.getString("OTHERS_REASONSFORDEDUCTION"));
				uiicDataBeanObj.setTotalAmountRequested(rs.getString("TOTAL_AMOUNTREQUESTED"));
				uiicDataBeanObj.setTotalAmountRecomm(rs.getString("TOTAL_AMOUNT_RECOMM"));
				uiicDataBeanObj.setTotalAmountRecomm(rs.getString("TOTAL__REASONSFORDEDUCTION"));
				uiicDataBeanObj.setDiseaseTreatmentAreCovered(rs.getString("DISEASE_TREATMENT_ARE_COVERED"));
				uiicDataBeanObj.setExpensesPaid(rs.getString("EXPENSESPAID"));
				uiicDataBeanObj.setAllHeadersInBreakDown(rs.getString("ALLHEADERSINBREAKDOWN"));
				uiicDataBeanObj.setTheCostOfDiagnosticsExceed(rs.getString("THECOSTOFDIAGNOSTICSEXCEED"));
				uiicDataBeanObj.setCreatedDate(rs.getString("CREATEDDATE"));
				uiicDataBeanObj.setClaimid(rs.getString("CLAIMID"));
				uiicDataBeanObj.setReauthFlag(rs.getInt("REAUTHFLAG"));
				uiicDataBeanObj.setTpaName(rs.getString("TPA_NAME"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("PPN_NETWORK_HOSPITALS"));
				uiicDataBeanObj.setCategoryHospitaltertyPlus(rs.getString("CATEGORY_HOSPITALTERTYPLUS"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("PPN_NETWORK_HOSPITALS"));
				uiicDataBeanObj.setTpaName(rs.getString("TPA_NAME"));
				uiicDataBeanObj.setClaimid(claimid);
				uiicDataBeanObj.setReauthFlag(reauthFlag);
				uiicDataBeanObj.setModuleId(moduleId);


				finalBeanObj.setUiicBean(uiicDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	//GET BHAXA DATA PREFILL CORP SHOW DOC
	public GetInsurerPrefillDataListBean getbhaxaDataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		BhratiAxaDataBean bhaxaDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_BHAXA_PREFILL_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				bhaxaDataBeanObj=new BhratiAxaDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				bhaxaDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				bhaxaDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
				bhaxaDataBeanObj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
				bhaxaDataBeanObj.setCorporateName(rs.getString("CORPORATE_NAME"));
				bhaxaDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
				bhaxaDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
				bhaxaDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
				bhaxaDataBeanObj.setEmailId(rs.getString("EMAILID"));
				bhaxaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
				bhaxaDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));
				bhaxaDataBeanObj.setPolicyNumber(rs.getString("POLICYNO"));
				bhaxaDataBeanObj.setBalanceSi(rs.getString("BALANCE_SI"));
				bhaxaDataBeanObj.setClaimType(rs.getString("CLAIMTYPE"));
				bhaxaDataBeanObj.setPolicyPeriod(rs.getString("POLICYPERIOD"));
				bhaxaDataBeanObj.setInitialInterimFinal(rs.getString("INITIAL_INTERIM_FINAL"));
				bhaxaDataBeanObj.setHospitalName(rs.getString("HOSPITAL"));
				bhaxaDataBeanObj.setDoaDod(rs.getString("DOA_DOD"));
				bhaxaDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				bhaxaDataBeanObj.setTreatment(rs.getString("TREATMENT"));
				bhaxaDataBeanObj.setTariff(rs.getString("TARIFF"));
				bhaxaDataBeanObj.setAnyNegotiations(rs.getString("NEGOTIATIONS"));
				bhaxaDataBeanObj.setTpaRecommendations(rs.getString("TPA_RECOMMENDATIONS"));
				
				bhaxaDataBeanObj.setClaimid(claimid);
				bhaxaDataBeanObj.setReauthFlag(reauthFlag);
				bhaxaDataBeanObj.setModuleId(moduleId);

				finalBeanObj.setBhaxaBean(bhaxaDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	//GET RELIANCE DATA PREFILL CORP SHOW DOC
	public GetInsurerPrefillDataListBean getRgilDataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		RelianceDatabean relianceDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_RGIL_PREFILL_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				relianceDataBeanObj=new RelianceDatabean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				relianceDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				relianceDataBeanObj.setPatientName(rs.getString("EMPLOYEE_NAME"));
				relianceDataBeanObj.setEmployeeId(rs.getString("EMPLOYEE_ID"));
				relianceDataBeanObj.setCorporateName(rs.getString("CORPORATE_NAME"));
				relianceDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				relianceDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
				relianceDataBeanObj.setPayableAmount(rs.getString("RECOMMENDED_AMOUNT"));
				relianceDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));
				relianceDataBeanObj.setEmailId(rs.getString("EMAILID"));
				relianceDataBeanObj.setCcemailId(rs.getString("CC_EMAILID"));
				relianceDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				relianceDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				relianceDataBeanObj.setPolicyNo(rs.getString("POLICY_NUMBER"));
				relianceDataBeanObj.setCaseType(rs.getString("CASE_TYPE"));
				relianceDataBeanObj.setClaimType(rs.getString("CLAIM_TYPE"));
				relianceDataBeanObj.setPreauthCaseId(rs.getString("PREAUTH_CASE_ID"));
				relianceDataBeanObj.setClaimNoUrlNo(rs.getString("CLAIM_NO_URL_NO"));
				relianceDataBeanObj.setAge(rs.getString("AGE"));
				relianceDataBeanObj.setHospitalType(rs.getString("HOSPITAL_TYPE"));
				relianceDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				relianceDataBeanObj.setLineOfTreatment(rs.getString("LINE_OF_TREATMENT"));
				relianceDataBeanObj.setRemarks(rs.getString("REMARKS"));
				relianceDataBeanObj.setInitialAmountPaid(rs.getString("INITIAL_AMOUNT_PAID"));
				relianceDataBeanObj.setCorporateBuffer(rs.getString("CORPORATE_BUFFER"));

				relianceDataBeanObj.setClaimid(claimid);
				relianceDataBeanObj.setReauthFlag(reauthFlag);
				relianceDataBeanObj.setModuleId(moduleId);
				
				relianceDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				relianceDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				relianceDataBeanObj.setInsuredId("PAT_INSUREDID");

				finalBeanObj.setRelianceBean(relianceDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	//GET ITGI DATA PREFILL CORP SHOW DOC
	public GetInsurerPrefillDataListBean getItgiDataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ItgiDataBean itgiDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_ITGI_PREFILL_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				itgiDataBeanObj=new ItgiDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				itgiDataBeanObj.setCardId(rs.getString("TPA_EMP_ID_CARD_NO"));
				itgiDataBeanObj.setHospitalName(rs.getString("NAME_OF_HOSPITAL"));
				itgiDataBeanObj.setMobileHospRep(rs.getString("MOBILE_HOSP_REP"));
				itgiDataBeanObj.setBenificiaryName(rs.getString("BENEFICIARY_NAME"));
				itgiDataBeanObj.setAdmDate(rs.getString("ADM_DATE"));
				itgiDataBeanObj.setMedicalSurgDayCare(rs.getString("MEDICAL_SURG_DAY_CARE"));
				itgiDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				itgiDataBeanObj.setGrName(rs.getString("GR_NAME"));
				itgiDataBeanObj.setCity(rs.getString("CITY"));
				itgiDataBeanObj.setMobileDoctor(rs.getString("MOBILE_DOCTOR"));
				itgiDataBeanObj.setTeleBeneficiary(rs.getString("TELE_BENEFICIARY"));
				itgiDataBeanObj.setIcuAdmissionRoom(rs.getString("ICU_ADMISSION_ROOM"));
				itgiDataBeanObj.setLengthOfStay(rs.getString("LENGTH_OF_STAY"));
				itgiDataBeanObj.setRoomIcuTariff(rs.getString("ROOM_ICU_TARIFF"));
				itgiDataBeanObj.setConsultantCharges(rs.getString("CONSULTANTCHARGES"));
				itgiDataBeanObj.setSurgeryCharges(rs.getString("SURGERYCHARGES"));
				itgiDataBeanObj.setOtCharges(rs.getString("OTCHARGES"));
				itgiDataBeanObj.setImplantsCostIf_any(rs.getString("IMPLANTS_COST_IF_ANY"));
				itgiDataBeanObj.setAlAmountAskedByNsp(rs.getString("AL_AMOUNT_ASKED_BY_NSP"));
				itgiDataBeanObj.setPreNegotiatedTariff(rs.getString("PRE_NEGOTIATED_TARIFF"));
				itgiDataBeanObj.setRmoNursing(rs.getString("RMO_NURSING"));
				itgiDataBeanObj.setMedicineCost(rs.getString("MEDICINES_COST"));
				itgiDataBeanObj.setInvstCosts(rs.getString("INVST_COSTS"));
				itgiDataBeanObj.setAnaesthesiaCharges(rs.getString("ANAESTHESIA_CHARGES"));
				itgiDataBeanObj.setAnyOtherExpenses(rs.getString("ANY_OTHER_EXPENSES"));
				itgiDataBeanObj.setRecommendedAlamtBytpa(rs.getString("RECOMMENDED_ALAMT_BYTPA"));
				itgiDataBeanObj.setTpaFinalBillEstimate(rs.getString("TPA_FINAL_BILL_ESTIMATE"));
				itgiDataBeanObj.setComplaintsWithDuration(rs.getString("COMPLAINTS_WITH_DURATION"));
				itgiDataBeanObj.setClinicalFindings(rs.getString("CLINICAL_FINDINGS"));
				itgiDataBeanObj.setTestsDoneSoFar(rs.getString("TESTS_DONE_SO_FAR"));
				itgiDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				itgiDataBeanObj.setMedicalDetails(rs.getString("MEDICAL_DETAILS"));
				itgiDataBeanObj.setSurgeryRx(rs.getString("SURGERY_RX"));
				itgiDataBeanObj.setEmailId(rs.getString("EMAILID"));
				itgiDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));


				itgiDataBeanObj.setClaimid(claimid);
				itgiDataBeanObj.setReauthFlag(reauthFlag);
				itgiDataBeanObj.setModuleId(moduleId);


				finalBeanObj.setItgiBean(itgiDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}


	//GET RELIGARE DATA PREFILL CORP SHOW DOC
	public GetInsurerPrefillDataListBean getReligareDataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ReligareDataBean religareDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_RELIGARE_PREFILL_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				religareDataBeanObj=new ReligareDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();

				religareDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				religareDataBeanObj.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
				religareDataBeanObj.setAge(rs.getString("AGE"));
				religareDataBeanObj.setGender(rs.getString("GENDER"));
				religareDataBeanObj.setRelationShip(rs.getString("RELATIONSHIP"));
				religareDataBeanObj.setSumInsured(rs.getString("SUM_INSURED"));
				religareDataBeanObj.setPackagee(rs.getString("PACKAGE1"));
				religareDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				religareDataBeanObj.setCity(rs.getString("CITY"));
				religareDataBeanObj.setDoa(rs.getString("DOA"));
				religareDataBeanObj.setDod(rs.getString("DOD"));
				religareDataBeanObj.setDurationOfStay(rs.getString("DURATION_OF_STAY"));
				religareDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				religareDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				religareDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_PAID"));
				religareDataBeanObj.setApprovedAmount(rs.getString("APPROVED_AMOUNT"));
				religareDataBeanObj.setRequestedAmount(rs.getString("REQUESTED_AMOUNT"));
				religareDataBeanObj.setCopayPer(rs.getString("COPAYMENTAMT"));
				religareDataBeanObj.setEmailId(rs.getString("EMAILID"));
				religareDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				religareDataBeanObj.setPlanOfTreatment(rs.getString("PLANOFTREATMENT"));
				religareDataBeanObj.setDrRemarks(rs.getString("DR_REAMRKS"));

				religareDataBeanObj.setClaimid(claimid);
				religareDataBeanObj.setReauthFlag(reauthFlag);
				religareDataBeanObj.setModuleId(moduleId);
				
				religareDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				religareDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				religareDataBeanObj.setInsuredId("PAT_INSUREDID");

				finalBeanObj.setReligareBean(religareDataBeanObj);
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}



	//GET UIIC DATA PREFILL RETAIL SHOW DOC
	public GetInsurerPrefillDataListBean getUiicDataPrefillShowDocRetail(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_UIIC_PREFILL_SQLSERVER);
			cstmt.setString(1, claimid);
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs != null){	


				if(rs.next()) {
					uiicDataBeanObj=new UiicDataBean();
					finalBeanObj=new GetInsurerPrefillDataListBean();

					uiicDataBeanObj.setInsuredName(rs.getString("NAME_OF_THE_INSURED"));
					uiicDataBeanObj.setPatientName(rs.getString("NAME_OF_THE_PATIENT"));
					uiicDataBeanObj.setAge(rs.getString("AGE_SEX"));
					uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
					uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCE_SUMINSURED"));
					uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NUMBER"));
					uiicDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
					uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICY_TYPE"));
					uiicDataBeanObj.setDateOfInception(rs.getString("DATEOFINCEPTION"));
					//uiicDataBeanObj.set(rs.getString("SPECIFIC_EXCLUSIONS");
					uiicDataBeanObj.setHospitalName(rs.getString("HOSPITALNAME"));
					uiicDataBeanObj.setCity(rs.getString("CITY_STATE"));
					uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("PPN"));
					uiicDataBeanObj.setCategoryHospitaltertyPlus(rs.getString("CATEGORYOFHOSPITAL"));
					uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
					uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("MANAGEMENTSURGICAL"));
					uiicDataBeanObj.setDateOfAdmission(rs.getString("DATEOFADMISSION"));
					uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION"));
					uiicDataBeanObj.setRoomType(rs.getString("ROOMTYPE"));
					uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOMRENT_PER_DAY"));
					uiicDataBeanObj.setRoomRentEligibility(rs.getString("ROOMRENTELIGIBILITY"));
					uiicDataBeanObj.setNoOfDaysStayed(rs.getString("NOOFDAYS"));
					uiicDataBeanObj.setGipsaPpaAmountRequested(rs.getString("GIPSAREQ_AMT"));
					uiicDataBeanObj.setGipsaPpnAmountRecomm(rs.getString("GIPSAAPP_AMT"));
					uiicDataBeanObj.setGipsaPpnReasonsForDeduction(rs.getString("GIPSA_REMARKS"));
					uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOMRENT_REQ_AMT"));
					uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOMRENT_APP_AMT"));
					uiicDataBeanObj.setRoomRentReasonsForDeduction(rs.getString("ROOMRENT_REMARKS"));
					uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROF_REQ_AMT"));
					uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROF_APP_AMT"));
					uiicDataBeanObj.setProcedureReasonsForDeduction(rs.getString("PROF_REMARKS"));
					uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHAR_REQ_AMT"));
					uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHAR_APP_AMT"));
					uiicDataBeanObj.setPharmacyReasonsForDeduction(rs.getString("PHAR_REMARKS"));
					uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OT_REQ_AMT"));
					uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OT_APP_AMT"));
					uiicDataBeanObj.setOtanesthesiaReasonsDeduction(rs.getString("OT_REMARKS"));
					uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LAB_REQ_AMT"));
					uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LAB_APP_AMT"));
					uiicDataBeanObj.setLabInvestigationrsnDeduction(rs.getString("LAB_REMARKS"));
					uiicDataBeanObj.setConsultationAmountRequested(rs.getString("CONS_REQ_AMT"));
					uiicDataBeanObj.setConsultationAmountRecomm(rs.getString("CONS_APP_AMT"));
					uiicDataBeanObj.setConsultationReasonsForDeduc(rs.getString("CONS_REMARKS"));
					uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTH__REQ_AMT"));
					uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTH_APP_AMT"));
					uiicDataBeanObj.setOthersReasonsForDeduction(rs.getString("OTH_REMARKS"));
					uiicDataBeanObj.setTotalAmountRequested(rs.getString("TOT_REQ_AMT"));
					uiicDataBeanObj.setTotalAmountRecomm(rs.getString("TOT_APP_AMT"));
					uiicDataBeanObj.setTotalReasonsForDeduction(rs.getString("TOT_REMARKS"));
					uiicDataBeanObj.setDiseaseTreatmentAreCovered(rs.getString("DISEASE_TREAT_CON"));
					uiicDataBeanObj.setExpensesPaid(rs.getString("EXPENS_PAID"));
					uiicDataBeanObj.setAllHeadersInBreakDown(rs.getString("ALL_HEAD_BREAK"));
					//uiicDataBeanObj.setc(rs.getString("IF_THE_COST");
					//uiicDataBeanObj.setu(rs.getString("USE_NAME");


					uiicDataBeanObj.setClaimid(claimid);
					uiicDataBeanObj.setReauthFlag(reauthFlag);
					uiicDataBeanObj.setModuleId(moduleId);
					
					uiicDataBeanObj.setPolicyID(rs.getString("POLICYID"));
					uiicDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
					uiicDataBeanObj.setInsuredId("PAT_INSUREDID");


					finalBeanObj.setUiicBean(uiicDataBeanObj);
				}

			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}

	
	//GET UIIC DATA PREFILL RETAIL SHOW DOC
	public GetInsurerPrefillDataListBean getNIADataPrefillShowDoc(int moduleId,String claimid,int reauthFlag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		NIADataBean niaDataBeanObj=null;
		GetInsurerPrefillDataListBean finalBeanObj=null;
		ResultSet rs=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_NIA_PREFILL_DATA);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);
		
				if(rs.next()) {
					niaDataBeanObj=new NIADataBean();
					finalBeanObj=new GetInsurerPrefillDataListBean();
				
					niaDataBeanObj.setClaimNo(rs.getString("CLAIM_NO"));
					niaDataBeanObj.setPolicynoAndPolicyperiad(rs.getString("POLICYNO_POLICYPERIOD"));
					niaDataBeanObj.setInsured(rs.getString("INSURED_NAME"));
					niaDataBeanObj.setPatient(rs.getString("PATIENT_NAME"));
					niaDataBeanObj.setIncase(rs.getString("DEVIATIONS"));
					niaDataBeanObj.setCopayment(rs.getString("COPAYMENT"));
					niaDataBeanObj.setPreauthType(rs.getString("PREAUTH_TYPE"));
					niaDataBeanObj.setBalSuminsured(rs.getLong("BALANCESUMINSURED"));
					niaDataBeanObj.setRelation(rs.getString("RELATION"));
					niaDataBeanObj.setGenderAndAge(rs.getString("GENDER_AGE"));
					niaDataBeanObj.setSumInsured(rs.getLong("SUM_INSURED1"));
					niaDataBeanObj.setPolicyNoAndValidity(rs.getString("POLICYNO_VALIDITY"));
					niaDataBeanObj.setDiseaseAndProcedure(rs.getString("DISEASE_PROCEDURE"));
					niaDataBeanObj.setAmount(rs.getString("AMOUNT"));
					niaDataBeanObj.setHospName(rs.getString("HOSPITAL_NAME"));
					niaDataBeanObj.setCityState(rs.getString("CITY_STATE"));
					niaDataBeanObj.setPpnAndCategory(rs.getString("PPN_CATEGORY"));
					niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_FOR_SINGLE_ROOM"));
					niaDataBeanObj.setDiseaseAndDiagnosis(rs.getString("DIASEASE_DIAGNOSIS"));
					niaDataBeanObj.setLineOfTreatment(rs.getString("LINE_OF_TREATMENT"));
					niaDataBeanObj.setIcdCode(rs.getString("ICD_CODE"));
					niaDataBeanObj.setApplicablePPN(rs.getString("APPLICABLE_PPN"));
					niaDataBeanObj.setDateofAdmission(rs.getString("DOA"));
					niaDataBeanObj.setDateofDischarge(rs.getString("DOD"));
					niaDataBeanObj.setNoOfDaysRoom(rs.getString("NO_OF_DAYS_ROOM"));
					niaDataBeanObj.setNoOfDaysICU(rs.getString("NO_OF_DAYS_ICU"));
					niaDataBeanObj.setDetailsOfIdProof(rs.getString("DETAILSOFIDPROOF"));
					niaDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
					niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_PER_DAY"));
					niaDataBeanObj.setRoomRentEligibility(rs.getString("ROOM_RENT_ELIGIBILITY"));
					niaDataBeanObj.setGipsAppnHospAphb(rs.getString("GIPSA_PPN_PACKAGE_REQ"));
					niaDataBeanObj.setRoomRentRequest(rs.getString("ROOM_RENT_REQ"));
					niaDataBeanObj.setProfChgHospRequest(rs.getString("PROFESSIONAL_CHARGES_REQ"));
					niaDataBeanObj.setPhrHospRequest(rs.getString("PHARMACY_REQ"));
					niaDataBeanObj.setOtHospRequest(rs.getString("OT_CHARGES_REQ"));
					niaDataBeanObj.setLabInvHospRequest(rs.getString("LAB_INVESTIGATIONS_REQ"));
					niaDataBeanObj.setConChrHospRequest(rs.getString("CONSULTATION_CHARGES_REQ"));
					niaDataBeanObj.setOthersHospRequest(rs.getString("OTHERS_REQ"));
					niaDataBeanObj.setTotalHospRequest(rs.getString("TOTAL_REQ"));
					
					niaDataBeanObj.setGipsAppnTpa(rs.getString("GIPSA_PPN_PACKAGE_RECOMM"));
					niaDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_RECOMM"));
					niaDataBeanObj.setProfChgPaid(rs.getString("PROFESSIONAL_CHARGES_RECOMM"));
					niaDataBeanObj.setPhrPaid(rs.getString("PHARMACY_RECOMM"));
					niaDataBeanObj.setOtPaid(rs.getString("OT_CHARGES_RECOMM"));
					niaDataBeanObj.setLabInvPaid(rs.getString("LAB_INVESTIGATIONS_RECOMM"));
					niaDataBeanObj.setConChrPaid(rs.getString("CONSULTATION_CHARGES_RECOMM"));
					niaDataBeanObj.setOthersPaid(rs.getString("OTHERS_RECOMM"));
					niaDataBeanObj.setTotalPaid(rs.getString("TOTAL_RECOMM"));
					
					niaDataBeanObj.setGipsAppnResonDod(rs.getString("GIPSA_PPN_PACKAGE_DED"));									
					niaDataBeanObj.setRoomRentDeducted(rs.getString("ROOM_RENT_DED"));
					niaDataBeanObj.setProfChgDeducted(rs.getString("PROFESSIONAL_CHARGES_DED"));
					niaDataBeanObj.setPhrDeducted(rs.getString("PHARMACY_DED"));
					niaDataBeanObj.setOtDeducted(rs.getString("OT_CHARGES_DED"));
					niaDataBeanObj.setLabInvDeducted(rs.getString("LAB_INVESTIGATIONS_DED"));
					niaDataBeanObj.setConChrDeducted(rs.getString("CONSULTATION_CHARGES_DED"));
					niaDataBeanObj.setOthersDeducted(rs.getString("OTHERS_DED"));
					niaDataBeanObj.setTotalDeducted(rs.getString("TOTAL_DED"));
					
					niaDataBeanObj.setClaimId(rs.getString("CLAIMID"));
					niaDataBeanObj.setReauthFlag(Integer.parseInt(rs.getString("REAUTHFLAG")));
					niaDataBeanObj.setEmailId(rs.getString("EMAILID"));
					niaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID")); 
					
					niaDataBeanObj.setSpecification(rs.getString("SPEC_IMPLANTS_USED"));
					niaDataBeanObj.setNameAndQual(rs.getString("PROCESSINGDOCTORSNAMEANDTPA"));
					niaDataBeanObj.setDateAndTime(rs.getString("DT_TIME_FORMAT_SENT"));
					
					niaDataBeanObj.setPolicyType(rs.getString("POLICY_TYPE"));
					niaDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
					
					niaDataBeanObj.setDiseaseTreatment(rs.getString("DISEASE_TREATMENT_ARE_COVERED"));
					niaDataBeanObj.setPpnPackage(rs.getString("CHRGS_ALLOW_PPN_PKG"));
					niaDataBeanObj.setExpencesAllowed(rs.getString("EXP_REASONABLE"));
					
					niaDataBeanObj.setId(rs.getString("ID"));
					niaDataBeanObj.setCreatedDate(rs.getString("CREATEDDATE"));

					finalBeanObj.setNIABean(niaDataBeanObj);
				}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}

	//FOR PREAUTH SEARCH LISTING
	public List<IAuthBean> getEmployeeDetailsCCN(String ccno,int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		IAuthBean iauthBean=null;
		ResultSet rs=null;
		int indexpos=0;
		int serialNo=0;
		List<IAuthBean> employeeList = new ArrayList<IAuthBean>();
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==1 || moduleId==2 ){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_CLMS_LIST_FOR_DOC_UPLOAD);
				cstmt.setString(++indexpos, ccno);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
			}
			while (rs.next()) {
				iauthBean=new IAuthBean();
				iauthBean.setModuleId(moduleId);
				iauthBean.setSerialNo(++serialNo);
				iauthBean.setCardHolderName(rs.getString("CARDHOLDERNAME"));
				iauthBean.setEmpid(rs.getString("EMPID"));
				if(rs.getString("SUMINSURED")!=null){
					iauthBean.setSumInsured(rs.getString("SUMINSURED"));
				}else{
					iauthBean.setSumInsured("0");
				}
				iauthBean.setPolicyNo(rs.getString("POLICYNO"));
				iauthBean.setPolicyId(rs.getString("POLICYID"));
				iauthBean.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				iauthBean.setRelation(rs.getString("RELATION"));
				iauthBean.setAge(rs.getInt("AGE"));
				iauthBean.setDob(rs.getString("DOB"));
				iauthBean.setPolicyFrom(rs.getString("POLICYFROM"));
				iauthBean.setPolicyTo(rs.getString("POLICYTO"));
				iauthBean.setDoj(rs.getString("DOJ"));
				iauthBean.setDol(rs.getString("DOL"));
				iauthBean.setEnrolmentExcep(rs.getString("ENROLMENT_EXCEPTION"));
				iauthBean.setBlocking(rs.getString("BLOCKFLAG"));
				iauthBean.setFamilyId(rs.getInt("FAMILYID"));
				iauthBean.setInsuredId(rs.getString("INSUREDID"));
				iauthBean.setCurrentDate(rs.getString("CURRENTDATE"));
				iauthBean.setRenewalStatus(rs.getString("RENEWALSTATUS"));
				iauthBean.setBano(rs.getString("BANO"));
				iauthBean.setUwCode(rs.getString("UWCODE"));
				iauthBean.setUwId(rs.getString("UWID"));
				iauthBean.setRid(rs.getString("RID"));
				iauthBean.setInceptionDate(rs.getString("INCEPTION_DATE"));
				iauthBean.setSelfName(rs.getString("SELFNAME"));
				iauthBean.setGender(rs.getString("gender"));
				iauthBean.setSelfcardid(rs.getString("selfcardid"));
				iauthBean.setCardid(rs.getString("CARDID"));
				iauthBean.setIssusingofficeid(rs.getString("ISSUINGOFFICEID"));
				iauthBean.setCcnUnique(rs.getString("CCNO"));
				iauthBean.setClaimId(rs.getString("CLAIMID"));
				iauthBean.setStatus(rs.getString("CLAIM_STATUSID"));
				iauthBean.setReauthFlag(rs.getInt("REAUTHFLAG"));
				iauthBean.setPatAddress(rs.getString("PATIENT_ADDRESS"));
				iauthBean.setPatEmail(rs.getString("PATIENT_EMAIL"));
				iauthBean.setPatMobile(rs.getString("PATIENT_MOBILE"));
				iauthBean.setSuppCounter(rs.getInt("SUPPCOUNTER"));
				//AmtReq, AmtRecomm, AmtDedu
				
				employeeList.add(iauthBean);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
     		try {
     			StringBuilder sb= new StringBuilder();
     			sb.append("moduleId for database connection : "+moduleId+"<br/>");
     			sb.append("package and procedurename : PKG_IAUTH.IAUTH_CLMS_LIST_FOR_DOC_UPLOAD"+"<br/>");
     			sb.append("ccno :"+ccno+"<br/>");
     			logger.error(sb.toString());
				mailsend.SendMail_preauthcron1("IAUTH_EXCEPTION DAO - getEmployeeDetailsCCN",sb.toString()+"<br/>"+ex.toString(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return employeeList;
	}


	public ProcessSheetBean getProcessSheetDetailsByCCN(String ccno,int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		Claims claims=null;
		DocumentBean documentBean= null;
		ResultSet rsClaimDetails=null;
		ResultSet rsClaimDoc=null;
		int indexpos=0;
		List<Claims> claimDetailsList = new ArrayList<Claims>();
		List<DocumentBean> documentList = new ArrayList<DocumentBean>();
		ProcessSheetBean processSheetBean= new ProcessSheetBean();
		String ccnUnique="";
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==1 ){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_PROCESSSHEET);
				cstmt.setString(++indexpos, ccno);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rsClaimDetails = (ResultSet) cstmt.getObject(indexpos-1);
				rsClaimDoc = (ResultSet) cstmt.getObject(indexpos);
			}
			//CLAIMID	SELFCARDID	PTCARDID	STATUS	PATIENTNAME	CLAIMTYPE	POLICYHOLDERNAME	DOA	DOD

			while (rsClaimDetails.next()) {
				claims= new Claims();
				ccnUnique =rsClaimDetails.getString("CCNO");
				claims.setCcnUnique(rsClaimDetails.getString("CCNO"));
				//claims.setModuleId(rsClaimDetails.getInt("moduleid"));
				claims.setClaimId(rsClaimDetails.getString("CLAIMID"));
				claims.setSelfCardId(rsClaimDetails.getString("SELFCARDID"));
				claims.setpCardId(rsClaimDetails.getString("PTCARDID"));
				claims.setStatus(rsClaimDetails.getString("STATUS"));
				claims.setPatientName(rsClaimDetails.getString("PATIENTNAME"));
				claims.setClaimTypevar(rsClaimDetails.getString("CLAIMTYPE"));
				claims.setPolicyHolderName(rsClaimDetails.getString("POLICYHOLDERNAME"));
				claims.setDoa(rsClaimDetails.getString("DOA"));
				claims.setDod(rsClaimDetails.getString("DOD"));
				claims.setHospitalName(rsClaimDetails.getString("hospitalname"));
				if(rsClaimDetails.getString("AmtReq")!=null){
					claims.setReqAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtReq")),2)));
				}
				if(rsClaimDetails.getString("AmtDedu")!=null){
					claims.setDeduAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtDedu")),2)));
				}
				if(rsClaimDetails.getString("AmtRecomm")!=null){
					claims.setRecomAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtRecomm")),2)));
				}
				
				String module=null;
				if(moduleId==1){
					module="Corp";
		    	 }else if(moduleId==2){
		    		 module="Bank Assurance";
		    	 }else if(moduleId==4){
		    		 module="Individual";
		    	 }
				claims.setModule(module);
				claimDetailsList.add(claims);
			}
			
			
			////CLAIMID	COLLECTION_MAIN_DOC_TYPE_ID	COLLECTION_SUB_DOC_TYPE_ID	FILENAME	ACTUAL_FILE_NAME	SCANNINGDATE	SCANNINGTS
			int i=0;
			while (rsClaimDoc.next()) {
				documentBean=new DocumentBean();
				documentBean.setSerialNo(i++);
				documentBean.setModuleId(moduleId);
				documentBean.setCcnUnique(ccnUnique);
				documentBean.setClaimId(rsClaimDoc.getString("CLAIMID"));
				documentBean.setCollectionMaindocTypeId(rsClaimDoc.getString("COLLECTION_MAIN_DOC_TYPE_ID"));
				documentBean.setCollectionSubdocTypeId(rsClaimDoc.getString("COLLECTION_SUB_DOC_TYPE_ID"));
				documentBean.setFileName(rsClaimDoc.getString("FILENAME"));
				documentBean.setActualFileName(rsClaimDoc.getString("ACTUAL_FILE_NAME"));
				documentBean.setScanningDate(rsClaimDoc.getString("SCANNINGDATE"));
				documentBean.setScanningGTS(rsClaimDoc.getString("SCANNINGTS"));
				documentList.add(documentBean);
			}
			processSheetBean.setClaimDetailsList(claimDetailsList);
			processSheetBean.setDocumentList(documentList);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
     		try {
     			StringBuilder sb= new StringBuilder();
     			sb.append("moduleId for database connection : "+moduleId+"<br/>");
     			sb.append("package and procedurename :  PKG_IAUTH.IAUTH_PROCESSSHEET"+"<br/>");
     			sb.append("ccno :"+ccno+"<br/>");
     			logger.error(sb.toString());
				mailsend.SendMail_preauthcron1("IAUTH_EXCEPTION DAO - getProcessSheetDetailsByCCN",sb.toString()+"<br/>"+ex.toString(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rsClaimDetails!=null){
					rsClaimDetails.close();
				}
				if(rsClaimDoc!=null){
					rsClaimDoc.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return processSheetBean;
	}

	public ProcessSheetBean getProcessSheetDetailsByCCNRET(String ccno,int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		Claims claims=null;
		DocumentBean documentBean= null;
		ResultSet rsClaimDetails=null;
		ResultSet rsClaimDoc=null;
		int indexpos=0;
		List<Claims> claimDetailsList = new ArrayList<Claims>();
		List<DocumentBean> documentList = new ArrayList<DocumentBean>();
		ProcessSheetBean processSheetBean= new ProcessSheetBean();
		String ccnUnique="";
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==2 || moduleId==4){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_PROCESSSHEET);
				cstmt.setString(++indexpos, ccno);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rsClaimDetails = (ResultSet) cstmt.getObject(indexpos-1);
				rsClaimDoc = (ResultSet) cstmt.getObject(indexpos);
			}
			//CLAIMID	SELFCARDID	PTCARDID	STATUS	PATIENTNAME	CLAIMTYPE	POLICYHOLDERNAME	DOA	DOD

			while (rsClaimDetails.next()) {
				claims= new Claims();
				ccnUnique =rsClaimDetails.getString("CCNO");
				claims.setCcnUnique(ccnUnique);
				//claims.setModuleId(rsClaimDetails.getInt("moduleid"));
				claims.setClaimId(rsClaimDetails.getString("CLAIMID"));
				claims.setSelfCardId(rsClaimDetails.getString("SELFCARDID"));
				claims.setpCardId(rsClaimDetails.getString("PTCARDID"));
				claims.setStatus(rsClaimDetails.getString("STATUS"));
				claims.setPatientName(rsClaimDetails.getString("PATIENTNAME"));
				claims.setClaimTypevar(rsClaimDetails.getString("CLAIMTYPE"));
				claims.setPolicyHolderName(rsClaimDetails.getString("POLICYHOLDERNAME"));
				claims.setDoa(rsClaimDetails.getString("DOA"));
				claims.setDod(rsClaimDetails.getString("DOD"));
				claims.setHospitalName(rsClaimDetails.getString("hospitalname"));
				if(rsClaimDetails.getString("AmtReq")!=null){
					claims.setReqAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtReq")),2)));
				}
				if(rsClaimDetails.getString("AmtDedu")!=null){
					claims.setDeduAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtDedu")),2)));
				}
				if(rsClaimDetails.getString("AmtRecomm")!=null){
					claims.setRecomAmt(Util.getCurrencyWithDecimalFormatByComma().format(Util.roundDouble(Double.parseDouble(rsClaimDetails.getString("AmtRecomm")),2)));
				}
				
				String module=null;
				if(moduleId==1){
					module="Corp";
		    	 }else if(moduleId==2){
		    		 module="Bank Assurance";
		    	 }else if(moduleId==4){
		    		 module="Individual";
		    	 }
				claims.setModule(module);
				claimDetailsList.add(claims);
			}
			
			
			////CLAIMID	COLLECTION_MAIN_DOC_TYPE_ID	COLLECTION_SUB_DOC_TYPE_ID	FILENAME	ACTUAL_FILE_NAME	SCANNINGDATE	SCANNINGTS
			int i=0;
			while (rsClaimDoc.next()) {
				documentBean=new DocumentBean();
				documentBean.setSerialNo(i++);
				documentBean.setModuleId(moduleId);
				documentBean.setCcnUnique(ccnUnique);
				documentBean.setClaimId(rsClaimDoc.getString("CLAIMID"));
				documentBean.setCollectionMaindocTypeId(rsClaimDoc.getString("COLLECTION_MAIN_DOC_TYPE_ID"));
				documentBean.setCollectionSubdocTypeId(rsClaimDoc.getString("COLLECTION_SUB_DOC_TYPE_ID"));
				documentBean.setFileName(rsClaimDoc.getString("FILENAME"));
				documentBean.setActualFileName(rsClaimDoc.getString("ACTUAL_FILE_NAME"));
				documentBean.setScanningDate(rsClaimDoc.getString("SCANNINGDATE"));
				documentBean.setScanningGTS(rsClaimDoc.getString("SCANNINGTS"));
				documentList.add(documentBean);
			}
			processSheetBean.setClaimDetailsList(claimDetailsList);
			processSheetBean.setDocumentList(documentList);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
     		try {
     			StringBuilder sb= new StringBuilder();
     			sb.append("moduleId for database connection : "+moduleId+"<br/>");
     			sb.append("package and procedurename :  PKG_IAUTH.IAUTH_PROCESSSHEET"+"<br/>");
     			sb.append("ccno :"+ccno+"<br/>");
     			logger.error(sb.toString());
				mailsend.SendMail_preauthcron1("IAUTH_EXCEPTION DAO - getProcessSheetDetailsByCCN",sb.toString()+"<br/>"+ex.toString(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rsClaimDetails!=null){
					rsClaimDetails.close();
				}
				if(rsClaimDoc!=null){
					rsClaimDoc.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return processSheetBean;
	
	}

	public List<IAuthBean> getIAuthSearchRetClaimid(String claimid,int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		IAuthBean iauthBean=null;
		ResultSet rs=null;
		int indexpos=0;
		int serialNo=0;
		
		List<IAuthBean> employeeList = new ArrayList<IAuthBean>();
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==4){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_I_AUTH_SRCH_BY_CLAIMID);
				cstmt.setString(++indexpos, claimid);
				 cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				 cstmt.execute();
				 rs = (ResultSet) cstmt.getObject(indexpos);
			}
			while (rs.next()) {
				iauthBean=new IAuthBean();
				iauthBean.setModuleId(moduleId);
				iauthBean.setSerialNo(++serialNo);
				iauthBean.setCardHolderName(rs.getString("CARDHOLDERNAME"));
				iauthBean.setEmpid(rs.getString("EMPID"));
				if(rs.getString("SUMINSURED")!=null){
					iauthBean.setSumInsured(rs.getString("SUMINSURED"));
				}else{
					iauthBean.setSumInsured("0");
				}
				iauthBean.setPolicyNo(rs.getString("POLICYNO"));
				iauthBean.setPolicyId(rs.getString("POLICYID"));
				iauthBean.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				iauthBean.setRelation(rs.getString("RELATION"));
				iauthBean.setAge(rs.getInt("AGE"));
				iauthBean.setDob(rs.getString("DOB"));
				iauthBean.setPolicyFrom(rs.getString("POLICYFROM"));
				iauthBean.setPolicyTo(rs.getString("POLICYTO"));
				iauthBean.setDoj(rs.getString("DOJ"));
				iauthBean.setDol(rs.getString("DOL"));
				iauthBean.setEnrolmentExcep(rs.getString("ENROLMENT_EXCEPTION"));
				iauthBean.setBlocking(rs.getString("BLOCKFLAG"));
				iauthBean.setFamilyId(rs.getInt("FAMILYID"));
				iauthBean.setInsuredId(rs.getString("INSUREDID"));
				iauthBean.setCurrentDate(rs.getString("CURRENTDATE"));
				iauthBean.setRenewalStatus(rs.getString("RENEWALSTATUS"));
				iauthBean.setBano(rs.getString("BANO"));
				iauthBean.setUwCode(rs.getString("UWCODE"));
				iauthBean.setUwId(rs.getString("UWID"));
				iauthBean.setRid(rs.getString("RID"));
				iauthBean.setInceptionDate(rs.getString("INCEPTION_DATE"));
				iauthBean.setSelfName(rs.getString("SELFNAME"));
				iauthBean.setGender(rs.getString("gender"));
				iauthBean.setSelfcardid(rs.getString("selfcardid"));
				iauthBean.setCardid(rs.getString("CARDID"));
				iauthBean.setIssusingofficeid(rs.getString("ISSUINGOFFICEID"));
				iauthBean.setCcnUnique(rs.getString("CCNO"));
				iauthBean.setClaimId(rs.getString("CLAIMID"));
				iauthBean.setStatus(rs.getString("CLAIM_STATUSID"));
				iauthBean.setReauthFlag(rs.getInt("REAUTHFLAG"));
				iauthBean.setPatAddress(rs.getString("PATIENT_ADDRESS"));
				iauthBean.setPatEmail(rs.getString("PATIENT_EMAIL"));
				iauthBean.setPatMobile(rs.getString("PATIENT_MOBILE"));
				iauthBean.setSuppCounter(rs.getInt("SUPPCOUNTER"));
				//AmtReq, AmtRecomm, AmtDedu
				
				employeeList.add(iauthBean);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
     		try {
     			StringBuilder sb= new StringBuilder();
     			sb.append("moduleId for database connection : "+moduleId+"<br/>");
     			sb.append("claimid :"+claimid+"<br/>");
     			sb.append("package and procedurename : PKG_IAUTH.I_AUTH_SRCH_BY_CLAIMID");
     			logger.error(sb.toString());
				mailsend.SendMail_preauthcron1("IAUTH_EXCEPTION DAO - getIAuthSearchRetClaimid",sb.toString()+"<br/>"+ex.toString(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return employeeList;
	}


	public List<OSTicketBean> getKayakoDataDropdown()throws DAOException {
		ResultSet rs= null;
		Connection con = null;
		OSTicketBean oSTicketBean= null;
		List<OSTicketBean> ticketList = new ArrayList<OSTicketBean>();

		String sql="Select * from getonlyopentickets";
		try
		{ 
			con = getMySQLConnection();
			//System.out.println(con);
			PreparedStatement pst = con.prepareStatement(sql);
			rs=pst.executeQuery();
			while(rs.next()){
				oSTicketBean= new OSTicketBean();
				oSTicketBean.setTicketId(rs.getInt("ticketID"));
				oSTicketBean.setTicketEmailId(rs.getString("nspostemailid"));
				ticketList.add(oSTicketBean);
			}
		}catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return ticketList;
	}


	public PreAuthBean insertIAuthDocumentTypesInsurerRet(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		//PreAuthBean preAuth=new PreAuthBean();
		int numberUpate=0;
		int indexpos=0;
		int reAuthFlag=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==4 || moduleId==5 || moduleId==6 || moduleId==7 || moduleId==8){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPLOAD_I_AUTH_INSURER_BILLS_RECEIVE_SQLSERVER);
				cstmt.setString(++indexpos, preAuthBean.getXmlString());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getKayakoId()));
				cstmt.setInt(++indexpos, preAuthBean.getReAuthFlag());
				cstmt.setString(++indexpos, preAuthBean.getInsurerReply());
				cstmt.setString(++indexpos, preAuthBean.getInsurerRemarks());
				cstmt.setInt(++indexpos, preAuthBean.getModuleUserId());
				cstmt.setString(++indexpos, preAuthBean.getRandomVal());


				System.out.println(preAuthBean.getXmlString());
				System.out.println(Integer.parseInt(preAuthBean.getKayakoId()));
				System.out.println(preAuthBean.getReAuthFlag());
				System.out.println(preAuthBean.getInsurerReply());
				System.out.println(preAuthBean.getInsurerRemarks());
				System.out.println(preAuthBean.getModuleUserId());
				System.out.println(preAuthBean.getRandomVal());


				cstmt.registerOutParameter(++indexpos, java.sql.Types.INTEGER); 
				cstmt.execute();
				cstmt.getInt(indexpos);
				con.commit();
				numberUpate=cstmt.getInt("Numupdatevar");
				System.out.println(numberUpate);
				preAuthBean.setNumberUpdates(numberUpate);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuthBean;
	}	

	//22/NOV/2014 to integrate flow for INSURER COLLECTION
	public PreAuthBean insertIAuthDocumentTypesInsurerRetDenial(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		//PreAuthBean preAuth=new PreAuthBean();
		int numberUpate=0;
		int indexpos=0;
		int reAuthFlag=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==4 || moduleId==5 || moduleId==6 || moduleId==7 || moduleId==8){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_COLLECT_DENIAL_TO_INSURER_SQLSERVER);
				cstmt.setString(++indexpos, preAuthBean.getXmlString());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getKayakoId()));
				cstmt.setInt(++indexpos, preAuthBean.getReAuthFlag());
				cstmt.setString(++indexpos, preAuthBean.getInsurerReply());
				cstmt.setString(++indexpos, preAuthBean.getInsurerRemarks());
				cstmt.setInt(++indexpos, preAuthBean.getModuleUserId());
				cstmt.setString(++indexpos, preAuthBean.getRandomVal());


				System.out.println(preAuthBean.getXmlString());
				System.out.println(Integer.parseInt(preAuthBean.getKayakoId()));
				System.out.println(preAuthBean.getReAuthFlag());
				System.out.println(preAuthBean.getInsurerReply());
				System.out.println(preAuthBean.getInsurerRemarks());
				System.out.println(preAuthBean.getModuleUserId());
				System.out.println(preAuthBean.getRandomVal());


				cstmt.registerOutParameter(++indexpos, java.sql.Types.INTEGER); 
				cstmt.execute();
				cstmt.getInt(indexpos);
				con.commit();
				numberUpate=cstmt.getInt("Numupdatevar");
				System.out.println(numberUpate);
				preAuthBean.setNumberUpdates(numberUpate);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuthBean;
	}	

	public PreAuthBean updateIAuthPreAuthClaims(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		//PreAuthBean preAuth=new PreAuthBean();
		int indexpos=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==1 || moduleId==2 || moduleId==3){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPDATE_IAUTH_PREAUTH_CLM_UPDATE);
				cstmt.setString(++indexpos, String.valueOf(preAuthBean.getClaimStatusId()));
				cstmt.setInt(++indexpos, preAuthBean.getUserId());
				cstmt.setInt(++indexpos, preAuthBean.getClaimId());
				cstmt.setInt(++indexpos, preAuthBean.getReAuthFlag());
				cstmt.setInt(++indexpos, preAuthBean.getModuleId());
				cstmt.execute();
				con.commit();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuthBean;
	}


	public PreAuthBean deleteSesionIdInsurerAuthRet(String sessionid,int moduleId)throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		PreAuthBean preAuth=new PreAuthBean();
		int indexpos=0;
		int reAuthFlag=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			//if(moduleId==4 || moduleId==5 || moduleId==6 || moduleId==7 || moduleId==8){
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_DELETE_I_AUTH_INSURER_BILLS_RECEIVE_DELETE_SQLSERVER);
			cstmt.setString(++indexpos, sessionid);
			cstmt.execute();
			con.commit();
			preAuth.setReAuthFlag(reAuthFlag);
			//}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuth;
	}


	public PreAuthBean insertIAuthDocumentTypesInsurerCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		//PreAuthBean preAuth=new PreAuthBean();
		int indexpos=0;
		int claimStatusId=0;
		int claimUpdateCNT=0;
		int numberUpdates=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==1 || moduleId==2 || moduleId==3){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPLOAD_IAUTH_INSURER_CONFIRMATION_ORACLE);
				cstmt.setString(++indexpos, preAuthBean.getXmlString());
				cstmt.setString(++indexpos,preAuthBean.getUserName());
				cstmt.setInt(++indexpos, preAuthBean.getClaimId());
				cstmt.setInt(++indexpos, preAuthBean.getPolicyId());
				cstmt.setInt(++indexpos, preAuthBean.getTotalNoDocs());
				cstmt.setString(++indexpos,preAuthBean.getCardid());
				cstmt.setString(++indexpos, preAuthBean.getCardHolderName());
				cstmt.setString(++indexpos, preAuthBean.getSelfName());
				cstmt.setString(++indexpos, preAuthBean.getPolicyNo());
				cstmt.setString(++indexpos, "1");
				cstmt.setString(++indexpos, preAuthBean.getClaimFromSource());
				cstmt.setString(++indexpos, preAuthBean.getPatAddress());
				cstmt.setInt(++indexpos, 0);
				cstmt.setString(++indexpos, preAuthBean.getPatEmail());
				cstmt.setString(++indexpos, preAuthBean.getPatEmail());
				cstmt.setString(++indexpos, preAuthBean.getPatLandLineNo());
				cstmt.setInt(++indexpos, preAuthBean.getInsuredId());
				cstmt.setString(++indexpos, "NA");
				cstmt.setInt(++indexpos, 0);
				cstmt.setString(++indexpos, preAuthBean.getCollectionDate());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getKayakoId()));
				cstmt.setString(++indexpos, preAuthBean.getRandomVal());
				cstmt.setInt(++indexpos, preAuthBean.getReAuthFlag());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getInsurerReply()));
				cstmt.setString(++indexpos,preAuthBean.getInsurerRemarks() );

				System.out.println( preAuthBean.getXmlString());
							System.out.println(preAuthBean.getUserName());
							System.out.println( preAuthBean.getClaimId());
							System.out.println( preAuthBean.getPolicyId());
							System.out.println( preAuthBean.getTotalNoDocs());
							System.out.println(preAuthBean.getCardid());
							System.out.println( preAuthBean.getCardHolderName());
							System.out.println( preAuthBean.getSelfName());
							System.out.println( preAuthBean.getPolicyNo());
							System.out.println( "1");
							System.out.println( preAuthBean.getClaimFromSource());
							System.out.println( preAuthBean.getPatAddress());
							System.out.println( 0);
							System.out.println( preAuthBean.getPatEmail());
							System.out.println( preAuthBean.getPatEmail());
							System.out.println( preAuthBean.getPatLandLineNo());
							System.out.println( preAuthBean.getInsuredId());
							System.out.println( "NA");
							System.out.println( 0);
							System.out.println( preAuthBean.getCollectionDate());
							System.out.println( Integer.parseInt(preAuthBean.getKayakoId()));
							System.out.println( preAuthBean.getRandomVal());
							System.out.println( preAuthBean.getReAuthFlag());
							System.out.println( Integer.parseInt(preAuthBean.getInsurerReply()));
							System.out.println(preAuthBean.getInsurerRemarks() );
				cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				claimStatusId=cstmt.getInt(indexpos-2);
				claimUpdateCNT=cstmt.getInt(indexpos-1);
				numberUpdates=cstmt.getInt(indexpos);
				preAuthBean.setClaimStatusId(claimStatusId);
				preAuthBean.setClaimUpdateCNT(claimUpdateCNT);
				preAuthBean.setNumberUpdates(numberUpdates);
				preAuthBean.setReAuthFlag(preAuthBean.getReAuthFlag());
				preAuthBean.setClaimId(preAuthBean.getClaimId());
				preAuthBean.setModuleId(moduleId);
				con.commit();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuthBean;
	}


	public PreAuthBean insertIAuthDocumentTypesInsurerDenialCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		//PreAuthBean preAuth=new PreAuthBean();
		int indexpos=0;
		int claimStatusId=0;
		int claimUpdateCNT=0;
		int numberUpdates=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			if(moduleId==1 || moduleId==2 || moduleId==3){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPLOAD_IAUTH_INSURER_DENIAL_ORACLE);
				cstmt.setString(++indexpos, preAuthBean.getXmlString());
				cstmt.setString(++indexpos,preAuthBean.getUserName());
				cstmt.setInt(++indexpos, preAuthBean.getClaimId());
				cstmt.setInt(++indexpos, preAuthBean.getPolicyId());
				cstmt.setInt(++indexpos, preAuthBean.getTotalNoDocs());
				cstmt.setString(++indexpos,preAuthBean.getCardid());
				cstmt.setString(++indexpos, preAuthBean.getCardHolderName());
				cstmt.setString(++indexpos, preAuthBean.getSelfName());
				cstmt.setString(++indexpos, preAuthBean.getPolicyNo());
				cstmt.setString(++indexpos, "1");
				cstmt.setString(++indexpos, preAuthBean.getClaimFromSource());
				cstmt.setString(++indexpos, preAuthBean.getPatAddress());
				cstmt.setInt(++indexpos, 0);
				cstmt.setString(++indexpos, preAuthBean.getPatEmail());
				cstmt.setString(++indexpos, preAuthBean.getPatEmail());
				cstmt.setString(++indexpos, preAuthBean.getPatLandLineNo());
				cstmt.setInt(++indexpos, preAuthBean.getInsuredId());
				cstmt.setString(++indexpos, "NA");
				cstmt.setInt(++indexpos, 0);
				cstmt.setString(++indexpos, preAuthBean.getCollectionDate());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getKayakoId()));
				cstmt.setString(++indexpos, preAuthBean.getRandomVal());
				cstmt.setInt(++indexpos, preAuthBean.getReAuthFlag());
				cstmt.setInt(++indexpos, Integer.parseInt(preAuthBean.getInsurerReply()));
				cstmt.setString(++indexpos,preAuthBean.getInsurerRemarks() );
				cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				claimStatusId=cstmt.getInt(indexpos-2);
				claimUpdateCNT=cstmt.getInt(indexpos-1);
				numberUpdates=cstmt.getInt(indexpos);
				preAuthBean.setClaimStatusId(claimStatusId);
				preAuthBean.setClaimUpdateCNT(claimUpdateCNT);
				preAuthBean.setNumberUpdates(numberUpdates);
				preAuthBean.setReAuthFlag(preAuthBean.getReAuthFlag());
				preAuthBean.setClaimId(preAuthBean.getClaimId());
				preAuthBean.setModuleId(moduleId);
				con.commit();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return preAuthBean;
	}



	public BackToAuditBean updateClaimStatusBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int claimStatusId=0;
		int claimUpdateCNT=0;
		int numberUpdates=0;
		try {
			con =getMyConnection(moduleId);
			//con.setAutoCommit(false);

			if(moduleId==1){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPDATE_BACK_TO_MEDICO_CORP);
				cstmt.setString(++indexpos, backToAuditbeanObj.getClaimId());
				cstmt.setInt(++indexpos,backToAuditbeanObj.getReauthFlag());
				cstmt.setInt(++indexpos, backToAuditbeanObj.getModuleUserId());
				cstmt.setString(++indexpos, backToAuditbeanObj.getRemarks());
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				numberUpdates=cstmt.getInt(indexpos);
				backToAuditbeanObj.setProcessingStatus(numberUpdates);
				//con.commit();
			}
			else if(moduleId==2 || moduleId==4 ){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPDATE_BACK_TO_MEDICO_CORP);
				cstmt.setString(++indexpos, backToAuditbeanObj.getClaimId());
				cstmt.setInt(++indexpos,backToAuditbeanObj.getReauthFlag());
				cstmt.setInt(++indexpos, backToAuditbeanObj.getModuleUserId());
				cstmt.setString(++indexpos, backToAuditbeanObj.getRemarks());
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				numberUpdates=cstmt.getInt(indexpos);
				backToAuditbeanObj.setProcessingStatus(numberUpdates);
			}

		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION - updateClaimStatusBackToAudit",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return backToAuditbeanObj;
	}


	public BackToAuditBean updateIAuthPreAuthBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException{
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_UPDATE_IAUTH_PREAUTH_CLM_UPDATE);
			cstmt.setString(++indexpos, String.valueOf(Constants.backToMedicoStatusId));
			cstmt.setInt(++indexpos, backToAuditbeanObj.getUserId());
			cstmt.setString(++indexpos, backToAuditbeanObj.getClaimId());
			cstmt.setInt(++indexpos, backToAuditbeanObj.getReauthFlag());
			cstmt.setInt(++indexpos, backToAuditbeanObj.getModuleId());
			cstmt.execute();
			con.commit();

		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION-  UPDATEIAUTHPREUTH IN BACK TO MEDICO FROM SENT TO INSURER",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return backToAuditbeanObj;
	}


	//Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
	public EmailDetailsBean getInsurerEmailIds(int moduleId,int uwId,int pkey) throws DAOException {
		Connection con_ghpl = null;
		CallableStatement cstmt = null;

		EmailDetailsBean emailDetBeanObj=null;
		ResultSet rsIssuingOfficeEmailInfo=null;

		try {

			con_ghpl = getMyConnection(1);

			cstmt = con_ghpl.prepareCall(ProcedureConstants.PROC_FOR_GET_INSURER_EMAILIDS);
			cstmt.setInt(1, moduleId);
			cstmt.setInt(2, uwId);
			cstmt.setInt(3, pkey);
			cstmt.registerOutParameter(4, OracleTypes.CURSOR);
			cstmt.execute();
			rsIssuingOfficeEmailInfo = (ResultSet) cstmt.getObject(4);

			while(rsIssuingOfficeEmailInfo.next()) {
				emailDetBeanObj=new EmailDetailsBean();
				emailDetBeanObj.setEmailId(rsIssuingOfficeEmailInfo.getString("EMAILID"));
				emailDetBeanObj.setCcEmailId(rsIssuingOfficeEmailInfo.getString("CCEMAILID"));
			}

			cstmt.close();
			cstmt=null;
			rsIssuingOfficeEmailInfo.close();
			rsIssuingOfficeEmailInfo=null;
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con_ghpl!=null){
					con_ghpl.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return emailDetBeanObj;
	}
	
	
	public GetInsurerPrefillDataListBean getNIADataPrefillRetail(int moduleId,String claimid,int reauthFlag,int preAuthno) throws DAOException 
	{

		Connection con = null;
		CallableStatement cstmt = null;
		NIADataBean niaDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		//String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_PREAUTH_NIA_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthFlag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				niaDataBeanObj=new NIADataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				
				niaDataBeanObj.setClaimId(rs.getString("CLAIMID"));	
				niaDataBeanObj.setInsured(rs.getString("INSURED_NAME"));	
				niaDataBeanObj.setPatient(rs.getString("PATIENT_NAME"));	
				niaDataBeanObj.setAge(rs.getString("AGE"));
				niaDataBeanObj.setSex(rs.getString("SEX"));	
				niaDataBeanObj.setRelation(rs.getString("RELATION_WITH_INSURED"));
				niaDataBeanObj.setBalSuminsured(Integer.parseInt(rs.getString("BALANCESUMINSURED")));
				niaDataBeanObj.setPolicyNo(rs.getString("POLICY_NO"));
				niaDataBeanObj.setSumInsured(Integer.parseInt(rs.getString("SUMINSURED")));
				//niaDataBeanObj.setPolicyType(rs.getString("POLICYTYPE"));
				niaDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				niaDataBeanObj.setHospName(rs.getString("HOSPITAL_NAME"));
				niaDataBeanObj.setCity(rs.getString("CITY"));
				niaDataBeanObj.setState(rs.getString("STATE"));	
				niaDataBeanObj.setNetworks(rs.getString("NETWORKS"));
				niaDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				niaDataBeanObj.setDateofAdmission(rs.getString("DATE_OF_ADMISSION"));
				niaDataBeanObj.setDateofDischarge(rs.getString("DATE_OF_DISCHARGE"));
				niaDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				niaDataBeanObj.setRoomRentRequest(rs.getString("ROOM_RENT_REQUESTED"));
				niaDataBeanObj.setRoomRentPaid(rs.getString("ROOM_RENT_PAID"));	
				niaDataBeanObj.setRoomRentDeducted(rs.getString("ROOM_RENT_DEDUCTED"));		
				niaDataBeanObj.setProfChgHospRequest(rs.getString("PROFESSIONAL_CHARGES_REQ"));		
				niaDataBeanObj.setProfChgPaid(rs.getString("PROFESSIONAL_CHARGES_PAID"));		
				niaDataBeanObj.setProfChgDeducted(rs.getString("PROFESSIONAL_CHARGES_DEDUCTED"));	
				niaDataBeanObj.setPhrHospRequest(rs.getString("PHARMACY_CHARGES_REQ"));	
				niaDataBeanObj.setPhrPaid(rs.getString("PHARMACY_CHARGES_PAID"));	
				niaDataBeanObj.setPhrDeducted(rs.getString("PHARMACY_CHARGES_DEDUCTED"));
				niaDataBeanObj.setOtHospRequest(rs.getString("OT_CHARGES_REQ"));
				niaDataBeanObj.setOtPaid(rs.getString("OT_CHARGES_PAID"));
				niaDataBeanObj.setOtDeducted(rs.getString("OT_CHARGES_DEDUCTED"));
				niaDataBeanObj.setLabInvHospRequest(rs.getString("LAB_INVES_CHARGES_REQ"));
				niaDataBeanObj.setLabInvPaid(rs.getString("LAB_INVES_CHARGES_PAID"));
				niaDataBeanObj.setLabInvDeducted(rs.getString("LAB_INVES_CHARGES_DEDUCTED"));
				niaDataBeanObj.setOthersHospRequest(rs.getString("OTHER_CHARGES_REQ"));
				niaDataBeanObj.setOthersPaid(rs.getString("OTHER_CHARGES_PAID"));
				niaDataBeanObj.setOthersDeducted(rs.getString("OTHER_CHARGES_DEDUCTED"));	
				niaDataBeanObj.setProcessingDoctor(rs.getString("PROCESSING_DOCTOR"));	
				niaDataBeanObj.setTpaName(rs.getString("TPANAME"));	
				niaDataBeanObj.setEmailId(rs.getString("EMAILID"));
				niaDataBeanObj.setCcEmailId(rs.getString("CC_EMAILID"));
				niaDataBeanObj.setPolicyDiscription(rs.getString("POLICY_DESCRIPTION"));
				niaDataBeanObj.setCode(rs.getString("CODE"));
				niaDataBeanObj.setClaimId(claimid);
				niaDataBeanObj.setReauthFlag(reauthFlag);
				niaDataBeanObj.setModuleId(moduleId);
				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				niaDataBeanObj.setUwId(rs.getInt("UWID"));
				niaDataBeanObj.setIssuingOfficeKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				//INCLUDE CODE HERE FROM DB NEW COLUMN
				niaDataBeanObj.setDisease(rs.getString("DISEASE"));
				niaDataBeanObj.setProcedure(rs.getString("PROCEDURECODE"));
				niaDataBeanObj.setAmount(rs.getString("AMOUNT"));
				niaDataBeanObj.setIcdCode(rs.getString("ICD_CODE"));
				niaDataBeanObj.setRoomType(rs.getString("ROOMTYPE"));
				niaDataBeanObj.setPreauthType(rs.getString("PRE_AUTH_TYPE"));
				niaDataBeanObj.setRoomRentEligibility(rs.getString("ROOM_RENT_ELIGIBILITY"));
				niaDataBeanObj.setDateAndTime(rs.getString("DATE_AND_TIME"));
				
				niaDataBeanObj.setPolicyType(rs.getString("POLICY_TYPE"));
				niaDataBeanObj.setPolicyPeriod(rs.getString("POLICY_PERIOD"));
				niaDataBeanObj.setLetterRemarks(rs.getString("LETTER_REMARKS"));
				
				niaDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				niaDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				niaDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));
				
				
				  niaDataBeanObj.setCopayment(rs.getString("Copayment"));
				  niaDataBeanObj.setLineOfTreatment(rs.getString("Line_of_treatment"));
				  niaDataBeanObj.setProcedureincRefrencerate(rs.getString("PROCEDURE_INC_REFRENCE_RATE"));
				  niaDataBeanObj.setAverageCost(rs.getString("AVERAGE_COST"));
				  niaDataBeanObj.setDetailsOfIdProof(rs.getString("DETAILS_ID_PROOF_OBTAINED"))
				  ; niaDataBeanObj.setRoomRent(rs.getString("ROOM_RENT_ELIGIBILITY"));
				  niaDataBeanObj.setApplicablePPN(rs.getString("Applicable_PPN"));
				  niaDataBeanObj.setPackageRate(rs.getString("Package_Rate"));
				  niaDataBeanObj.setNoOfDaysRoom(rs.getString("No_days_Room"));
				  niaDataBeanObj.setNoOfDaysICU(rs.getString("No_days_ICU"));
				  niaDataBeanObj.setICUPerDay(rs.getString("ROOMRENT_ICURENT_PER_DAY"));
				 
				/*
				 * niaDataBeanObj.setGipsAppnHospAphb(""); //GIPSA_PPN_PACKAGE
				 * niaDataBeanObj.setGipsAppnResonDod(""); niaDataBeanObj.setGipsAppnTpa("");
				 * niaDataBeanObj.setConChrDeducted("");
				 * niaDataBeanObj.setConChrHospRequest(""); niaDataBeanObj.setConChrPaid("");
				 */
				 
			
				
				finalBeanObj.setNIABean(niaDataBeanObj);
			}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;

			
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			if(finalBeanObj.getNIABean()!=null){
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getNIABean().getModuleId(),finalBeanObj.getNIABean().getUwId(),finalBeanObj.getNIABean().getIssuingOfficeKey()));
			}else{
				finalBeanObj= null;
			}


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	
	}
	
	@Override
	public GetInsurerPrefillDataListBean getDefaultDataPrefill(int moduleId, String claimid, String ccnUnique,
			int reauthflag) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_UIIC_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthflag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				uiicDataBeanObj=new UiicDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				uiicDataBeanObj.setCcnUnique(ccnUnique);
				uiicDataBeanObj.setInsuredName(rs.getString("INSURED_NAME"));
				uiicDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				uiicDataBeanObj.setAge(rs.getString("AGE"));
				uiicDataBeanObj.setSex(rs.getString("SEX"));
				uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
				uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NO"));
				uiicDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
				uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICYTYPE"));
				uiicDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				uiicDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				uiicDataBeanObj.setCity(rs.getString("CITY"));
				uiicDataBeanObj.setState(rs.getString("STATE"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				uiicDataBeanObj.setDateOfAdmission(rs.getString("DATE_OF_ADMISSION"));
				uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				uiicDataBeanObj.setRoomRentIcuRentAmountRequested(rs.getString("ROOM_RENT_REQUESTED"));
				uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOM_RENT_PAID"));
				uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROFESSIONAL_CHARGES_REQ"));
				uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROFESSIONAL_CHARGES_PAID"));
				uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHARMACY_CHARGES_REQ"));
				uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHARMACY_CHARGES_PAID"));
				uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OT_CHARGES_REQ"));
				uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OT_CHARGES_PAID"));
				uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LAB_INVES_CHARGES_REQ"));
				uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LAB_INVES_CHARGES_PAID"));
				uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTHER_CHARGES_REQ"));
				uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTHER_CHARGES_PAID"));
				uiicDataBeanObj.setProcessingDoctorsNameandtpa(rs.getString("PROCESSING_DOCTOR"));
				uiicDataBeanObj.setTpaName(rs.getString("TPANAME"));
				uiicDataBeanObj.setEmailId(rs.getString("EMAILID"));
				uiicDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				uiicDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));
                
				uiicDataBeanObj.setClaimid(claimid);
				uiicDataBeanObj.setReauthFlag(reauthflag);
				uiicDataBeanObj.setModuleId(moduleId);

				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				uiicDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				uiicDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				uiicDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				uiicDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				uiicDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));
				
				//INCLUDE CODE HERE FROM DB NEW COLUMN ON 31/AUG/2019
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("TREATMENT_TYPE"));
				uiicDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				uiicDataBeanObj.setRoomRentIcuRent(rs.getString("ROOMRENT_PERDAY"));
				uiicDataBeanObj.setRoomRentEligibility(rs.getString("ELIGIBLE_ROOM_TYPE"));
				uiicDataBeanObj.setNoOfDaysStayed(rs.getString("EXPECTED_DURATION_OF_STAY"));
				/*
				 * uiicDataBeanObj.setGipsaPpaAmountRequested("");
				 * uiicDataBeanObj.setGipsaPpnAmountRecomm("");
				 * uiicDataBeanObj.setGipsaPpnReasonsForDeduction("");
				 * uiicDataBeanObj.setConsultationAmountRecomm("");
				 * uiicDataBeanObj.setConsultationAmountRequested("");
				 * uiicDataBeanObj.setConsultationReasonsForDeduc("");
				 */
				//INCLUDE CODE HERE FROM DB NEW COLUMN
				finalBeanObj.setUiicBean(uiicDataBeanObj);
				
				
				
			}

			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;


			

			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)

			if(con != null)
			{
				con.close();
			}

			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getUiicBean().getModuleId(),finalBeanObj.getUiicBean().getUnderWriterId(),finalBeanObj.getUiicBean().getIssuingOfficePrimaryKey()));


		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	}
	public GetInsurerPrefillDataListBean getDefaultDataPrefillRetail(int moduleId, String claimid, String ccnUnique,
			int reauthflag, int preAuthno) throws DAOException {

		Connection con = null;
		CallableStatement cstmt = null;
		UiicDataBean uiicDataBeanObj=null;
		DocsDetailsCorpBean docsDetailsCorpBeanObj=null;
		List<DocsDetailsCorpBean> docsDetailsCorpList=new ArrayList<DocsDetailsCorpBean>();
		GetInsurerPrefillDataListBean finalBeanObj=null;

		ResultSet rs=null;
		ResultSet rsDocsInfo=null;
		ResultSet rsServerInfo=null;

		String code=null;

		try {
			con =getMyConnection(moduleId);

			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_DEFAULT_PREFILL_DTLS_CORP);
			cstmt.setString(1, claimid);
			cstmt.setInt(2, reauthflag);
			cstmt.registerOutParameter(3, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(3);

			if(rs.next()) {
				uiicDataBeanObj=new UiicDataBean();
				finalBeanObj=new GetInsurerPrefillDataListBean();
				uiicDataBeanObj.setCcnUnique(ccnUnique);
				uiicDataBeanObj.setInsuredName(rs.getString("INSURED_NAME"));
				uiicDataBeanObj.setPatientName(rs.getString("PATIENT_NAME"));
				uiicDataBeanObj.setAge(rs.getString("AGE"));
				uiicDataBeanObj.setSex(rs.getString("SEX"));
				uiicDataBeanObj.setRelationWithInsured(rs.getString("RELATION_WITH_INSURED"));
				uiicDataBeanObj.setBalanceSumInsured(rs.getString("BALANCESUMINSURED"));
				uiicDataBeanObj.setPolicyNumber(rs.getString("POLICY_NO"));
				uiicDataBeanObj.setSumInsured(rs.getString("SUMINSURED"));
				uiicDataBeanObj.setPolicyTypeIndividualOrGmc(rs.getString("POLICYTYPE"));
				uiicDataBeanObj.setDateOfInception(rs.getString("DATE_OF_INCEPTION"));
				uiicDataBeanObj.setHospitalName(rs.getString("HOSPITAL_NAME"));
				uiicDataBeanObj.setCity(rs.getString("CITY"));
				uiicDataBeanObj.setState(rs.getString("STATE"));
				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setDiagnosis(rs.getString("DIAGNOSIS"));
				uiicDataBeanObj.setDateOfAdmission(rs.getString("DATE_OF_ADMISSION"));
				uiicDataBeanObj.setExpectedDurationOfStay(rs.getString("EXPECTED_DURATION_OF_STAY"));
				uiicDataBeanObj.setRoomRentIcuRentAmountRequested(rs.getString("ROOM_RENT_REQUESTED"));
				uiicDataBeanObj.setRoomRentAmountRecomm(rs.getString("ROOM_RENT_PAID"));
				uiicDataBeanObj.setProcedureAmountRequested(rs.getString("PROFESSIONAL_CHARGES_REQ"));
				uiicDataBeanObj.setProcedureAmountRecomm(rs.getString("PROFESSIONAL_CHARGES_PAID"));
				uiicDataBeanObj.setPharmacyAmountRequested(rs.getString("PHARMACY_CHARGES_REQ"));
				uiicDataBeanObj.setPharmacyAmountRecomm(rs.getString("PHARMACY_CHARGES_PAID"));
				uiicDataBeanObj.setOtanesthesiaAmountRequested(rs.getString("OT_CHARGES_REQ"));
				uiicDataBeanObj.setOtanesthesiaAmountRecomm(rs.getString("OT_CHARGES_PAID"));
				uiicDataBeanObj.setLabInvestigationsAmtRequested(rs.getString("LAB_INVES_CHARGES_REQ"));
				uiicDataBeanObj.setLabInvestigationsAmtRecomm(rs.getString("LAB_INVES_CHARGES_PAID"));
				uiicDataBeanObj.setOthersAmountRequested(rs.getString("OTHER_CHARGES_REQ"));
				uiicDataBeanObj.setOthersAmountRecomm(rs.getString("OTHER_CHARGES_PAID"));
				uiicDataBeanObj.setProcessingDoctorsNameandtpa(rs.getString("PROCESSING_DOCTOR"));
				uiicDataBeanObj.setTpaName(rs.getString("TPANAME"));
				uiicDataBeanObj.setEmailId(rs.getString("EMAILID"));
				uiicDataBeanObj.setCcEmailid(rs.getString("CC_EMAILID"));
				uiicDataBeanObj.setPolicyDescription(rs.getString("POLICY_DESCRIPTION"));

				uiicDataBeanObj.setClaimid(claimid);
				uiicDataBeanObj.setReauthFlag(reauthflag);
				uiicDataBeanObj.setModuleId(moduleId);

				//New columns which needs to be added in the procedure for Unified UW related data from GHPL
				uiicDataBeanObj.setUnderWriterId(rs.getInt("UWID"));
				uiicDataBeanObj.setIssuingOfficePrimaryKey(rs.getInt("ISSUINGOFFICE_PKEY"));
				
				uiicDataBeanObj.setPolicyID(rs.getString("POLICYID"));
				uiicDataBeanObj.setCardId(rs.getString("PAT_CARDID"));
				uiicDataBeanObj.setInsuredId(rs.getString("PAT_INSUREDID"));

				uiicDataBeanObj.setPpnNetworkHospitals(rs.getString("NETWORKS"));
				uiicDataBeanObj.setManagementOrMedicalOrEmergency(rs.getString("TREATMENT_TYPE"));
				uiicDataBeanObj.setRoomType(rs.getString("ROOM_TYPE"));
				uiicDataBeanObj.setRoomRentIcuRent(rs.getString("ROOMRENT_PERDAY"));
				uiicDataBeanObj.setRoomRentEligibility(rs.getString("ELIGIBLE_ROOM_TYPE"));
				uiicDataBeanObj.setNoOfDaysStayed(rs.getString("EXPECTED_DURATION_OF_STAY"));
				finalBeanObj.setUiicBean(uiicDataBeanObj);
			}
			cstmt.close();
			cstmt=null;
			rs.close();
			rs=null;
			//Newly added on 17DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
			if(con != null)
			{
				con.close();
			}
			//Adding the insurer EmailIds to finalBeanObj
			finalBeanObj.setEmailCommonDetaisBean(getInsurerEmailIds(finalBeanObj.getUiicBean().getModuleId(),finalBeanObj.getUiicBean().getUnderWriterId(),finalBeanObj.getUiicBean().getIssuingOfficePrimaryKey()));
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
			try {
				mailsend.SendMail_preauthcron1("SENDTOINSURER_EXCEPTION",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {

				if(con!=null){
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}
		}
		return finalBeanObj;
	
	}

}
