package com.ghpl.persistence.iauth;

import java.util.List;

import org.apache.log4j.Logger;

import com.ghpl.exception.DAOException;

import com.ghpl.model.iauth.BackToAuditBean;
import com.ghpl.model.iauth.GetInsurerPrefillDataListBean;
import com.ghpl.model.iauth.IAuthAuditBean;
import com.ghpl.model.iauth.IAuthBean;
import com.ghpl.model.iauth.OSTicketBean;
import com.ghpl.model.iauth.PreAuthBean;
import com.ghpl.model.iauth.ProcessSheetBean;
import com.ghpl.model.iauth.UiicDataBean;
import com.ghpl.persistence.DAOFactory;

public class SendToInsurerManager {
	Logger logger = Logger.getLogger("IAuthManager");
	public static List<IAuthAuditBean> getAuditListDetails(int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getAuditListDetails(moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static List<IAuthAuditBean> getAuditListDenialDetails(int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getAuditListDenialDetails(moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static List<IAuthAuditBean> getAuditListInsSent(int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getAuditListInsSent(moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	public static GetInsurerPrefillDataListBean getUiicDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getUiicDataPrefill(moduleId,claimid,ccnUnique,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getNIADataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getNIADataPrefill(moduleId,claimid,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getbhaxaDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getbhaxaDataPrefill(moduleId,claimid,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getrelianceDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getrelianceDataPrefill(moduleId,claimid,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	public static GetInsurerPrefillDataListBean getitgiDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getitgiDataPrefill(moduleId,claimid,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	public static GetInsurerPrefillDataListBean getreligareDataPrefill(int moduleId,String claimid,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getreligareDataPrefill(moduleId,claimid,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getUiicDataPrefillRetail(int moduleId,String claimid,String ccnUnique,int reauthFlag, int preAuthno) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getUiicDataPrefillRetail(moduleId,claimid,ccnUnique,reauthFlag,preAuthno);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getDenialDataPrefill(int moduleId,String claimid,String ccnUnique,int reauthFlag) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getDenialDataPrefill(moduleId,claimid,ccnUnique,reauthFlag);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getClaimDownloadDocData(String claimid,int reauthFlag, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getClaimDownloadDocData(claimid,reauthFlag,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static List<IAuthBean> getEmployeeDetailsCCN(String insuredorempcode,int moduleId) {
		try {
			return DAOFactory.getInstance().getIAuthDAO().getEmployeeDetailsCCN(insuredorempcode,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static ProcessSheetBean getProcessSheetDetailsByCCNRET(String claimId,int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getProcessSheetDetailsByCCNRET(claimId,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static ProcessSheetBean getProcessSheetDetailsByCCN(String claimId,int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getProcessSheetDetailsByCCN(claimId,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static List<IAuthBean> getIAuthSearchRetClaimid(String claimid,int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getIAuthSearchRetClaimid(claimid,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static List<OSTicketBean> getKayakoDataDropdown()throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getKayakoDataDropdown();
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PreAuthBean insertIAuthDocumentTypesInsurerRet(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().insertIAuthDocumentTypesInsurerRet(preAuthBean,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PreAuthBean updateIAuthPreAuthClaims(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().updateIAuthPreAuthClaims(preAuthBean,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PreAuthBean deleteSesionIdInsurerAuthRet(String randomVal, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().deleteSesionIdInsurerAuthRet(randomVal,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PreAuthBean insertIAuthDocumentTypesInsurerCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().insertIAuthDocumentTypesInsurerCorp(preAuthBean,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PreAuthBean insertIAuthDocumentTypesInsurerDenialCorp(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().insertIAuthDocumentTypesInsurerDenialCorp(preAuthBean,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	public static PreAuthBean insertIAuthDocumentTypesInsurerRetDenial(PreAuthBean preAuthBean, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().insertIAuthDocumentTypesInsurerRetDenial(preAuthBean,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	public static BackToAuditBean updateClaimStatusBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().updateClaimStatusBackToAudit(backToAuditbeanObj,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static BackToAuditBean updateIAuthPreAuthBackToAudit(BackToAuditBean backToAuditbeanObj, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().updateIAuthPreAuthBackToAudit(backToAuditbeanObj,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getNIADataPrefillRetail(int moduleId,String claimid,int reauthFlag, int preAuthno) throws DAOException{
		try {
			return DAOFactory.getInstance().getIAuthDAO().getNIADataPrefillRetail(moduleId,claimid,reauthFlag,preAuthno);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static GetInsurerPrefillDataListBean getDefaultDataPrefill(int moduleId, String claimid, String ccnUnique,
			int reauthflag) {
		try {
			return DAOFactory.getInstance().getIAuthDAO().getDefaultDataPrefill(moduleId,claimid,ccnUnique,reauthflag);
		} catch (DAOException e) {

		}
		return null;
	}

	public static GetInsurerPrefillDataListBean getDefaultDataPrefillRetail(int moduleId, String claimid,
			String ccnUnique, int reauthflag, int preAuthno) {
		try {
			return DAOFactory.getInstance().getIAuthDAO().getDefaultDataPrefillRetail(moduleId,claimid,ccnUnique,reauthflag,preAuthno);
		} catch (DAOException e) {

		}
		return null;
	}
	
}
