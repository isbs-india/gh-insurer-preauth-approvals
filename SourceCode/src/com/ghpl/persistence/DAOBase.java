package com.ghpl.persistence;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.ghpl.exception.DAOException;
import com.ghpl.exception.ServiceLocatorException;
import com.ghpl.util.Constants;
import com.ghpl.util.ServiceLocator;

public class DAOBase {
	 
	public static Connection getConnection() throws DAOException {
		String jndiName = Constants.JNDI_KEY;
		try {
			DataSource source;
			source = ServiceLocator.getInstance().getDataSource(jndiName);
			if (source == null)
				return null;
			return source.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
		return null;
	}
}
