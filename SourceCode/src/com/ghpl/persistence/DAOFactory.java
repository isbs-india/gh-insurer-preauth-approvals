package com.ghpl.persistence;

import com.ghpl.persistence.iauth.SendToInsurerDAO;
import com.ghpl.persistence.iauth.SendToInsurerDAOImpl;
import com.ghpl.persistence.login.LoginDAO;
import com.ghpl.persistence.login.LoginDAOImpl;

public class DAOFactory {

	private static DAOFactory instance;

	static {
		instance = new DAOFactory();
	}

	private DAOFactory() {

	}

	public static DAOFactory getInstance() {
		return instance;
	}
	
	public SendToInsurerDAO getIAuthDAO() {
		return new SendToInsurerDAOImpl();
	}
	
	public LoginDAO getLoginDAO() {
		return new LoginDAOImpl();
	}
	
}
