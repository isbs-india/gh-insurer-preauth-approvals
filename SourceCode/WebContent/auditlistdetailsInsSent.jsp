<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>

<h3 align="center">CLAIMS LIST SENT TO INSURER FOR APPROVAL/DENIAL</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		   <c:if test="${fn:length(auditList) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   		<tr>
					<th>Index</th>
				    <th>CCN/PreAuth App No</th>
				    <th>CARDID</th>
				    <th>INSURED NAME</th>
				    <th>MODIFIED DATE</th>
                    <th>Module</th>
				    <th>APPROVAL TYPE</th>
				</tr>
				 <c:forEach items="${auditList}" var="auditData"> 
				 
				 
				 
                <tr>
					<td><c:out value="${auditData.serialNo}"/></td>
				    <td><c:out value="${auditData.ccnUnique}"/>/<c:out value="${auditData.preAuthNo}"/></th>
				    <td><c:out value="${auditData.cardId}"/></td>
				    <td><c:out value="${auditData.memberName}"/></td>
				    <td><c:out value="${auditData.createddate}"/></td>
				    <td><c:out value="${auditData.moduleName}"/></td>
				    <td><c:out value="${auditData.approvalType}"/></td>
				    </tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(auditList) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	   <tr>
					<th>Index</th>
				    <th>CCN/PreAuth App No</th>
				    <th>CARDID</th>
				    <th>INSURED NAME</th>
				    <th>MODIFIED DATE</th>
                    <th>Module</th>
				    <th>APPROVAL TYPE</th>
				  </tr>
			   	    <tr align="center">
			   			<td colspan="10" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>