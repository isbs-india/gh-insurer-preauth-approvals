<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.ClaimDenialBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearchRET.js"></script>




<%
		
	 int moduleId=0; 
	 String CLAIMID = "";
	 String PATIENT_NAME = "";
	 String EMPLOYEE_NAME = "";
	 String EMPLOYEE_ID = "";
	 String CORPORATE_NAME = "";
	 String SUM_INSURED = "";
	 String REQUESTED_AMOUNT = "";
	 String APPROVED_AMOUNT = "";
	 String POLICY_DESC = "";
	 String EMAILID = "";
	 String CCEMAILID = "";
	 String Medico_Comments="NA";
    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String policyId="";
      	 String cardId="";
      	 String insuredId="";
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 
    	 GetInsurerPrefillDataListBean finalBean = null;
 
try{  

	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	if(finalBean.getClmDenialBean() != null)
	{
    	  CLAIMID = String.valueOf(finalBean.getClmDenialBean().getClaimid());
    	  PATIENT_NAME = finalBean.getClmDenialBean().getPatientName();
    	  EMPLOYEE_NAME = finalBean.getClmDenialBean().getEmployeeName();
    	  EMPLOYEE_ID = finalBean.getClmDenialBean().getEmployeeId();
    	  CORPORATE_NAME = finalBean.getClmDenialBean().getCorporatename();
    	  SUM_INSURED = finalBean.getClmDenialBean().getSumInsured();
    	  REQUESTED_AMOUNT = finalBean.getClmDenialBean().getRequestedAmount();
    	  APPROVED_AMOUNT = finalBean.getClmDenialBean().getApprovedAmount();
    	  POLICY_DESC = finalBean.getClmDenialBean().getPolicydesc();
    	  //EMAILID = finalBean.getClmDenialBean().getEmailId();
    	  //CCEMAILID = finalBean.getClmDenialBean().getCcEmailId();
    	  Medico_Comments = finalBean.getClmDenialBean().getMedicoRemarks();
    	  moduleId=finalBean.getClmDenialBean().getModuleId();
    	  rauthflg=finalBean.getClmDenialBean().getReauthFlag();
    	  
    	  policyId = finalBean.getClmDenialBean().getPolicyId();
       	  cardId = finalBean.getClmDenialBean().getCardId();
       	  insuredId = finalBean.getClmDenialBean().getInsurerId();
      }//if
      
      
    //Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
    if (finalBean.getEmailCommonDetaisBean() != null) {
    	EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
    	CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
    }
      
      LoginBean lb=new LoginBean();

  	session= request.getSession();
  	if(session != null) {
  		
  		lb=(LoginBean)session.getAttribute("user");
  		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
  		 name_of_user_obj_1 =session.getAttribute("userName").toString();
  	     userid_of_user = lb.getModuleUserId(1);
  	     moduleUserId = lb.getModuleUserId(moduleId);
  	}

}
catch(Exception e)
{
	System.out.println("ERROR IN DENIAL PREFILL: "+e);
}
finally
{
	
}


%>




<h3 align="center">View DENIAL Details for Claim</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="Denial_sendmailRET.jsp" method="post">
                            
       <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">
       <input type="hidden" name="username" id="username" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="DENIAL">  
        <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		 <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">  
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		
		<input type="hidden" name="policyId" id="policyId" value="<%=policyId%>">                   
        <input type="hidden" name="cardId" id="cardId" value="<%=cardId%>">   
		<input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr >	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=CLAIMID%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">            </td>
            </tr>
 			 <tr>
            <td  class="text_heading"><strong>Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patientname" type="text"  id="patientname"  size="20"   value="<%=PATIENT_NAME%>" readonly="readonly"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Employee Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="empname" type="text"  id="empname"  size="20"    value="<%=EMPLOYEE_NAME%>"  readonly="readonly"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Employee ID</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="empid" type="text"  id="empid"  size="20"  value="<%=EMPLOYEE_ID%>"  readonly="readonly"></td>
            </tr>
            <tr>
            <td  class="text_heading"><strong>Corporate Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="corpname" type="text"  id="corpname"  size="20" MAXLENGTH="200"  value="<%=CORPORATE_NAME%>"  readonly="readonly"></td>
            </tr>	
			
			  <tr>
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20"   value="<%=SUM_INSURED%>"  readonly="readonly"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Requested Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="reqamt" type="text"  id="reqamt"  size="20"  value="<%=REQUESTED_AMOUNT%>"  readonly="readonly"></td>
            </tr>
			
			 
			<tr>
            <td  class="text_heading"><strong>Approved Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="apramt" type="text"  id="apramt"  size="20"   value="<%=APPROVED_AMOUNT%>"  readonly="readonly"></td>
            </tr>
            
            <tr>
            <td  class="text_heading"><strong>Medico Remarks</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="medicoremarks" type="text"  id="medicoremarks"  size="20"   value="<%=Medico_Comments%>"  readonly="readonly"></td>
            </tr>
		
		<tr>
		<td colspan="4">
<%
try
{
int j=0;
System.out.println("DOCS LIST SIZE:"+finalBean.getDocsDetailsCorpList().size());
for(j=0;j<finalBean.getDocsDetailsCorpList().size();j++){
	
	
	
%>
<input type="checkbox" name="doc" id="doc" value="<%=finalBean.getDocsDetailsCorpList().get(j).getFolder()%>/<%=finalBean.getDocsDetailsCorpList().get(j).getFileName()%>~1~<%=finalBean.getDocsDetailsCorpList().get(j).getFileName()%>">&nbsp;
<input type="hidden" name="filenme<%=j%>" id="filenme<%=j%>" value="<%=finalBean.getDocsDetailsCorpList().get(j).getFileName()%>">
<input type="hidden" name="folder<%=j%>" id="folder<%=j%>" value="<%=finalBean.getDocsDetailsCorpList().get(j).getFolder()%>">
<input type="hidden" name="actfilename<%=j%>" id="actfilename<%=j%>" value="<%=finalBean.getDocsDetailsCorpList().get(j).getFileName()%>">
<input type="hidden" name="moduleId<%=j%>" id="moduleId<%=j%>" value="<%=moduleId%>">
<input type="button" value="<%=finalBean.getDocsDetailsCorpList().get(j).getFileName()%>" OnClick="viewDocsFtp(<%=j%>);">
<br/>
<%
  			}
 			
        
	}
	catch(Exception e)
	{
		System.out.println("ERROR IN DOXCS UIIC DOCS"+e);
	}
	finally
	{
		//
	}

%> 
		</td>
		</tr>
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>
	




</form>  
		   
<form name="downloadodoc" action="./DownloadFtpFileRet" method="post" target="_blank">
<input type="hidden" name="filenme" id="filenme" value="">
<input type="hidden" name="folder" id="folder" value="">
<input type="hidden" name="actfilename" id="actfilename" value="">
<input type="hidden" name="moduleIdVal" id="moduleIdVal" value="">
</form>	 	
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>