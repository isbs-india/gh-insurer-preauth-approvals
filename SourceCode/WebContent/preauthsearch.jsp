<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>

<script type="text/javascript" >
	function validate(inputtxt){
		//alert(inputtxt);
		var numbers = /^[0-9]+$/;  
	      if(inputtxt.value.match(numbers))  
	      {  
	      	//alert('Your Registration number has accepted....');  
	      	//inputtxt.focus();  
	      	return true;  
	      }  
	      else  
	      {  
	      	alert('Please enter valid claim id only');  
	      	inputtxt.value="";
	      	inputtxt.focus();  
	      	return false;  
	      }  
	}
</script>

<script type="text/javascript">
	
function getCompanyData(){
	
	var moduleId = document.getElementById("moduleIdno");
	//alert("enter11    "+moduleId);
	var strUser = moduleId.options[moduleId.selectedIndex].value;
	//alert(strUser+"    "+moduleId);
	document.modForm.action="./CompanyDataAction?moduleId="+strUser; 
	document.modForm.submit();
	//return false;
	
}


	function ccNformSubmit() {  
		f1Submit();  
		f2Submit();  
	}  

	</script>  
<h3 align="center">I AUTH SEARCH </h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>

<%! int moduleId=0; %>
<%
if(session.getAttribute("moduleId")!=null){
	moduleId=(Integer)session.getAttribute("moduleId");
}else{
	moduleId=0;
}
%>


	<form name="modForm" method="post" action="./CompanyDataAction"> 
	<table width="20%"  border="1" cellpadding="0" cellspacing="0" class="tablebg">
		<tr>
			<td align="left" width="10%">Module: </td>
			<td width="10%">
				<select name="moduleIdno" id="moduleIdno" onchange="return getCompanyData();">
					<option id="0" <%if(moduleId==0){%>selected="selected"<%}%>>Select</option>
					<option id="1" <%if(moduleId==1){%>selected="selected"<%}%>>Corp</option>
					<option id="2" <%if(moduleId==2){%>selected="selected"<%}%>>Bank Assurance</option>
					<option id="3" <%if(moduleId==4){%>selected="selected"<%}%>>Individual</option>
					
				</select> 
		    </td>
		</tr>
		
	</table>
	</form>
<br/>

<%if(moduleId==1 || moduleId==2 || moduleId==3) {%>

<br/>
		 <form name="ccNform" method="post" action="./IAuthSearchClaimIdAction"> 
		<input type="hidden" name="moduleId" value='<%=moduleId %>' />
         	<table width="40%"  border="1" cellpadding="0" cellspacing="0" class="tablebg">
				<tr>
					<td align="left">ClaimID: </td>
					<td>
						<input type="text" size="30" name="ccnNumber" id="ccnNumber"/> 
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr><td>&nbsp;</td>
					<td colspan="2">
					<input type="submit" value="Submit" onclick="return ccNformSubmit();" class="button"/>
					</td>
				</tr>
			</table>
		</form> 
<%} %>
<%if(moduleId==4 || moduleId==5 || moduleId==6 || moduleId==7 || moduleId==8) {%>
		<form name="claimidRetForm" id="claimidRetForm" method="post" action="./IAuthSearchRetActionClaimId"> 
		<input type="hidden" name="moduleId" value='<%=moduleId %>' />
         	<table width="40%"  border="1" cellpadding="0" cellspacing="0" class="tablebg">
				<tr><td colspan="2">&nbsp; </td></tr>
				<tr>
					<td align="left">ClaimID: </td>
					<td>
						<input type="text" size="30" name="claimid" id="claimid"/> 
					</td>
				</tr>
               
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td>&nbsp;</td><td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" value="Submit"  class="button" />
					</td>
				</tr>
			</table>
		</form>
	
<%} %>

</td>
</tr>
</table>
<%@include file="footer.jsp" %>