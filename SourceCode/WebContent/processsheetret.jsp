<%@page import="com.ghpl.model.iauth.IAuthBean"%>
<%@page import="com.ghpl.util.Util"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>.:: Good Health on WEB ::.</title>
<link rel="stylesheet" href="css/1024.css">
<script type="text/javascript">
function noBack(){window.history.forward()}
noBack();
window.onload=noBack;
window.onpageshow=function(evt){if(evt.persisted)noBack()}
window.onunload=function(){void(0)}
</script>
</head>

<body>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td align="center" width="100%" rowspan="2">
		<c:if test="${fn:length(processList) gt 0 }">
		<c:forEach items="${processList}" var="processsheet"> 
    		<h3>CLAIM DETAILS</h3>
				<div id="ProcessSheetDiv" align="center">
				<table width="95%" border="1" align="center" cellpadding="1" cellspacing="1" class="tablebg">
						<tr>
							<th>CCN/PreAuth App No</th>
						    <th>Policy Holder Name</th>
						    <th>Claim Type</th>
						    <th>Current Status</th>
						    <th>Pt Card Id</th>
						    <th>Self CardID</th>
		                    <th>Pt Name</th>
		                    <th>DOA</th>
		                    <th>DOD</th>
		                    <th>Hospital Name</th>
						    <th>Module</th>
						    <th>ReqAMT</th>
						    <th>Deduction Amt</th>
						    <th>Recommand Amt</th>
					    </tr>
					    <c:if test="${fn:length(processsheet.claimDetailsList) gt 0 }">
					     <c:forEach items="${processsheet.claimDetailsList}" var="claimDetails"> 
		                <tr>
							<td><c:out value="${claimDetails.ccnUnique}"/></td>
						    <td><c:out value="${claimDetails.policyHolderName}"/></td>
						    <td><c:out value="${claimDetails.claimTypevar}"/></td>
						    <td><c:out value="${claimDetails.status}"/></td>
						    <td><c:out value="${claimDetails.pCardId}"/></td>
						    <td><c:out value="${claimDetails.selfCardId}"/></td>
		                    <td><c:out value="${claimDetails.patientName}"/></td>
		                    <td><c:out value="${claimDetails.doa}"/></td>
		                    <td><c:out value="${claimDetails.dod}"/></td>
		                    <td><c:out value="${claimDetails.hospitalName}"/></td>
						    <td><c:out value="${claimDetails.module}"/></td>
						    <td>Rs.&nbsp;<c:out value="${claimDetails.reqAmt}"/></td>
						    <td>Rs.&nbsp;<c:out value="${claimDetails.deduAmt}"/></td>
						    <td>Rs.&nbsp;<c:out value="${claimDetails.recomAmt}"/></td>
						    
			      		</tr>
			      		</c:forEach>
			      	</c:if>
			      	 <c:if test="${fn:length(processsheet.claimDetailsList) eq 0 }">
		                <tr>
							<td colspan="14">&nbsp;no data found.</td>
			      		</tr>
			      	</c:if>
			</table>
			<br/>
			</div><br/><br/><br/><br/><br/>
			<div id="DocsListDiv" align="center">
			
			<table width="50%" border="1" align="center" cellpadding="1" cellspacing="1" class="tablebg">
			 			
							<tr>
							    <th width="23%" align="left">Doc Uploaded date</th>
							    <th width="23%" align="center">Download</th>
						    </tr>
						    <c:forEach items="${processsheet.documentList}" var="documentDetails"> 
			                <tr>
							     <td align="center"><c:out value="${documentDetails.scanningGTS}"/></td>
							     <td align="center">
							     <form name="processsheetdownloadForm" action="./ProcessSheetDownloadAction" method="post" enctype="multipart/form-data">
							    
							        <input type="hidden" name="moduleId" value="<c:out value="${documentDetails.moduleId}"/>"/>
							         <input type="hidden" name="ccnUnique" value="<c:out value="${documentDetails.ccnUnique}"/>"/>
							        <input type="hidden" name="claimId" value="<c:out value="${documentDetails.claimId}"/>"/>
							        <input type="hidden" name="collectionMaindocTypeId" value="<c:out value="${documentDetails.collectionMaindocTypeId}"/>"/>
							        <input type="hidden" name="collectionSubdocTypeId" value="<c:out value="${documentDetails.collectionSubdocTypeId}"/>"/>
							        <input type="hidden" name="fileName" value="<c:out value="${documentDetails.fileName}"/>"/>
							        <input type="hidden" name="actualFileName" value="<c:out value="${documentDetails.actualFileName}"/>"/>
							         <input type="hidden" name="scanningDate" value="<c:out value="${documentDetails.scanningDate}"/>"/>
							     	<input type=submit value="<c:out value="${documentDetails.actualFileName}"/>" />
							     </form>
							     </td>
						    </tr>
						   </c:forEach>
						  
			</table>
			<br/>
			
			</div>
</c:forEach>
</c:if>
			</td>
	</tr>
	
	<!-- <tr>
	<td>&nbsp;</td>
 	<td valign="top" align="center"> 
    <h3>CLAIM DOCS</h3>


</td></tr> -->

</table>

<%@include file="footer.jsp" %>
