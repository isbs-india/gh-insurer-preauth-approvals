<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.RelianceDatabean"%>


<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};

</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
Connection con = null;
		
ResultSet rs=null;
		
	 int moduleId=0; 
	 String CASE_TYPE = "";
	 String CLAIM_TYPE = "";
	 String PREAUTH_CASE_ID = "";
	 String CLAIM_NO_URL_NO = "";
	 String PATIENT_NAME = "";
	 String AGE = "";
	 String HOSPITALNAME = "";
	 String HOSPITAL_TYPE = "";
	 String DIAGNOSIS = "";
	 String CORPORATE_NAME = "";
	 String POLICY_NO = "";
	 String SUM_INSURED = "";
	 String BALANCESUMINSURED = "";
	 String LINE_OF_TREATMENT = "";
	 String REMARKS = "";
	 String REQUESTED_AMOUNT = "";
	 String INITIAL_AMOUNT_PAID = "";
	 String PAYABLE_AMOUNT = "";
	 String CORPORATE_BUFFER = "";
	 String EMAILID = "";
	 String CCEMAILID = "";
   	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 
    	 GetInsurerPrefillDataListBean finalBean = null;
 
try{  

	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
   	 
	   if(finalBean.getRelianceBean() != null)
	   {
	
	      CASE_TYPE = finalBean.getRelianceBean().getCaseType();
	   	  CLAIM_TYPE = finalBean.getRelianceBean().getClaimType();
	   	  PREAUTH_CASE_ID = finalBean.getRelianceBean().getPreauthCaseId();
	   	  CLAIM_NO_URL_NO = finalBean.getRelianceBean().getClaimNoUrlNo();
	   	  PATIENT_NAME = finalBean.getRelianceBean().getPatientName();
	   	  AGE = finalBean.getRelianceBean().getAge();
	   	  HOSPITALNAME = finalBean.getRelianceBean().getHospitalName();
	   	  HOSPITAL_TYPE = finalBean.getRelianceBean().getHospitalType();
	   	  DIAGNOSIS = finalBean.getRelianceBean().getDiagnosis();
	   	  CORPORATE_NAME = finalBean.getRelianceBean().getCorporateName();
	   	  POLICY_NO = finalBean.getRelianceBean().getPolicyNo();
	   	  SUM_INSURED = finalBean.getRelianceBean().getSumInsured();
	   	  BALANCESUMINSURED = finalBean.getRelianceBean().getBalanceSumInsured();
	   	  LINE_OF_TREATMENT = finalBean.getRelianceBean().getLineOfTreatment();
	   	  REMARKS = finalBean.getRelianceBean().getRemarks();
	   	  REQUESTED_AMOUNT = finalBean.getRelianceBean().getRequestedAmount();
	   	  INITIAL_AMOUNT_PAID = finalBean.getRelianceBean().getInitialAmountPaid();
	   	  PAYABLE_AMOUNT = finalBean.getRelianceBean().getPayableAmount();
	   	  CORPORATE_BUFFER = finalBean.getRelianceBean().getCorporateBuffer();
	   	  EMAILID = finalBean.getRelianceBean().getEmailId();
	   	  CCEMAILID = finalBean.getRelianceBean().getCcemailId();
	      moduleId=finalBean.getRelianceBean().getModuleId();
	      clmid=Integer.parseInt(finalBean.getRelianceBean().getClaimid());
	      rauthflg=finalBean.getRelianceBean().getReauthFlag();
	   }
	   
	   
		LoginBean lb=new LoginBean();

		session= request.getSession();
		if(session != null) {
			
			lb=(LoginBean)session.getAttribute("user");
			//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
			 name_of_user_obj_1 =session.getAttribute("userName").toString();
		     userid_of_user = lb.getModuleUserId(1);
		     moduleUserId = lb.getModuleUserId(moduleId);
		}
	 
      

}
catch(Exception e)
{
	System.out.println("ERROR IN RELIANCE PREFILL: "+e);
}
finally
{
	//
}


%>




<h3 align="center">View reliance Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="./GenUiicXls" method="post">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
       <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="RGIL">  
       <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
	   <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">     
	   <input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
	   <input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
	   <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
       <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
              
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr >	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=clmid%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">            </td>
            </tr>
 			 <tr>
            <td  class="text_heading"><strong>Case Type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="casetype" type="text"  id="casetype"  size="20" MAXLENGTH="150"  readonly="readonly" value="<%=CASE_TYPE%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>
              claim type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimtype" type="text"  id="claimtype"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%=CLAIM_TYPE%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Pre-auth/ Case ID</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="caseid" type="text"  id="caseid"  size="20" MAXLENGTH="15" readonly="readonly"  value="<%=PREAUTH_CASE_ID%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Claim No/URL No</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimno" type="text"  id="claimno"  size="20" MAXLENGTH="15"  readonly="readonly" value="<%=CLAIM_NO_URL_NO%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patientname" type="text"  id="patientname"  size="20" MAXLENGTH="150"v  value="<%=PATIENT_NAME%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Age</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=AGE%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Hospital Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="600"v  value="<%=HOSPITALNAME%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Hospital type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hosptype" type="text"  id="hosptype"  size="20" MAXLENGTH="30" readonly="readonly"  value="<%=HOSPITAL_TYPE%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Diagnosis</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%=DIAGNOSIS%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Corporate Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="corpname" type="text"  id="corpname"  size="20" MAXLENGTH="500" readonly="readonly"  value="<%=CORPORATE_NAME%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Policy No</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno" type="text"  id="policyno"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%=POLICY_NO%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="25" readonly="readonly"  value="<%=SUM_INSURED%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Bal Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="balsuminsured" type="text"  id="balsuminsured"  size="20" MAXLENGTH="15" readonly="readonly"  value="<%=BALANCESUMINSURED%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Line of treatment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="lineoftretment" type="text"  id="lineoftretment"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%=LINE_OF_TREATMENT%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Estimated Amount/Claimed Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimedamt" type="text"  id="claimedamt"  size="20" MAXLENGTH="15"  readonly="readonly" value="<%=REQUESTED_AMOUNT%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>initial amount approved</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="iniappramt" type="text"  id="iniappramt"  size="20" MAXLENGTH="15"  readonly="readonly" value="<%=INITIAL_AMOUNT_PAID%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Payable Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="payableamt" type="text"  id="payableamt"  size="20" MAXLENGTH="15"  readonly="readonly" value="<%=PAYABLE_AMOUNT%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Corporate Buffer Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="corpbufferamt" type="text"  id="corpbufferamt"  size="20" MAXLENGTH="15" readonly="readonly"  value="<%=CORPORATE_BUFFER%>"></td>
            </tr>
            
             <tr>
            <td  class="text_heading"><strong>Remarks</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="remarks" type="text"  id="remarks"  size="20" MAXLENGTH="650"  readonly="readonly" value="<%=REMARKS%>"></td>
            </tr>
		
		
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"  ></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  


		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>