<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.BhratiAxaDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearch.js"></script>


<script>
function validateForm(){

	var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
	
	var initial= document.getElementById("initial").value;
	var tariff= document.getElementById("tariff").value;
	var negotiations= document.getElementById("negotiations").value;
	var tparecomm= document.getElementById("tparecomm").value;
	var treatment= document.getElementById("treatment").value;
	
	
	if(initial.trim()!=""){
 		for (var i = 0; i < initial.trim().length; i++) {
 			if(iChars.indexOf(initial.trim().charAt(i)) != -1){
 				alert ("Special Characters are not allowed in Initial/interim/final");
 				document.getElementById("initial").value="";
 				document.getElementById("initial").focus();
 			    return false;
 			}
 		}
 	}
	
	
	if(treatment.trim()!=""){
 		for (var i = 0; i < treatment.trim().length; i++) {
 			if(iChars.indexOf(treatment.trim().charAt(i)) != -1){
 				alert ("Special Characters are not allowed in Treatment");
 				document.getElementById("treatment").value="";
 				document.getElementById("treatment").focus();
 			    return false;
 			}
 		}
 	}
	
	if(tariff.trim()!=""){
 		for (var i = 0; i < tariff.trim().length; i++) {
 			if(iChars.indexOf(tariff.trim().charAt(i)) != -1){
 				alert ("Special Characters are not allowed in Tariff- open or agreed");
 				document.getElementById("tariff").value="";
 				document.getElementById("tariff").focus();
 			    return false;
 			}
 		}
 	}
	
	
	if(negotiations.trim()!=""){
 		for (var i = 0; i < negotiations.trim().length; i++) {
 			if(iChars.indexOf(negotiations.trim().charAt(i)) != -1){
 				alert ("Special Characters are not allowed in Any negotiation/case management");
 				document.getElementById("negotiations").value="";
 				document.getElementById("negotiations").focus();
 			    return false;
 			}
 		}
 	}
	
	
	if(tparecomm.trim()!=""){
 		for (var i = 0; i < tparecomm.trim().length; i++) {
 			if(iChars.indexOf(tparecomm.trim().charAt(i)) != -1){
 				alert ("Special Characters are not allowed in TPA recommendations");
 				document.getElementById("tparecomm").value="";
 				document.getElementById("tparecomm").focus();
 			    return false;
 			}
 		}
 	}
	
}

</script>

<%
	 int moduleId=0; 
	 String CLAIMID = "";
   	 String PATIENT_NAME = "";
   	 String EMPLOYEE_NAME = "";
   	 String EMPLOYEE_ID = "";
   	 String CORPORATE_NAME = "";
   	 String SUM_INSURED = "";
   	 String REQUESTED_AMOUNT = "";
   	 String APPROVED_AMOUNT = "";
   	 String EMAILID = "";
   	 String CCEMAILID = "";
   	 String policyDesc="";
   	 
   	 String policyNumber="";
   	 String balanceSi="";
   	 String claimType="";
   	 String policyPeriod="";
   	 String initial="";
   	 String hospitalName="";
   	 String doaDod="";
   	 String diagnosis="";
   	 String treatment="";
   	 String tariff="";
   	 String negotiations="";
   	 String tpaRecommendations="";
   	 
     String policyId="";
  	 String cardId="";
  	 String insuredId="";
  	 String ccnUnique="";
   	 
    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 GetInsurerPrefillDataListBean finalBean = null;
try{  

	
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	
	if(finalBean.getBhaxaBean() != null)
	{
	
      	  CLAIMID = String.valueOf(finalBean.getBhaxaBean().getClaimid());
    	  PATIENT_NAME = finalBean.getBhaxaBean().getPatientName();
    	  SUM_INSURED = finalBean.getBhaxaBean().getSumInsured();
       	  //APPROVED_AMOUNT = finalBean.getBhaxaBean().getApprovedAmount();
       	  moduleId=finalBean.getBhaxaBean().getModuleId();
   	      rauthflg=finalBean.getBhaxaBean().getReauthFlag();
   	
    	  //EMPLOYEE_NAME = finalBean.getBhaxaBean().getEmployeeName();
    	 // EMPLOYEE_ID = finalBean.getBhaxaBean().getEmployeeId();
    	 // CORPORATE_NAME = finalBean.getBhaxaBean().getCorporateName();
    	  REQUESTED_AMOUNT = finalBean.getBhaxaBean().getRequestedAmount();
     	  //EMAILID = finalBean.getBhaxaBean().getEmailId();
    	  //CCEMAILID = finalBean.getBhaxaBean().getCcEmailId();
    	  //policyDesc= finalBean.getBhaxaBean().getPolicyDescription();
   	     policyNumber= finalBean.getBhaxaBean().getPolicyNumber();
     	 balanceSi= finalBean.getBhaxaBean().getBalanceSi();
     	 claimType= finalBean.getBhaxaBean().getClaimType();
     	 policyPeriod= finalBean.getBhaxaBean().getPolicyPeriod();
     	 hospitalName= finalBean.getBhaxaBean().getHospitalName();
     	 doaDod= finalBean.getBhaxaBean().getDoaDod();
     	 diagnosis= finalBean.getBhaxaBean().getDiagnosis();
     	 treatment= finalBean.getBhaxaBean().getTreatment();
     	 
     	 
     	policyId = finalBean.getBhaxaBean().getPolicyID();
     	cardId = finalBean.getBhaxaBean().getCardId();
     	insuredId = finalBean.getBhaxaBean().getInsuredId();
     	ccnUnique = finalBean.getCcnUnique();
     	if(finalBean.getBhaxaBean().getInitialInterimFinal()!= null){
        	 initial= finalBean.getBhaxaBean().getInitialInterimFinal();
     	}
     	if(finalBean.getBhaxaBean().getTariff()!=null){
        	 tariff= finalBean.getBhaxaBean().getTariff();

     	}
     	if(finalBean.getBhaxaBean().getAnyNegotiations()!=null){
        	 negotiations= finalBean.getBhaxaBean().getAnyNegotiations();

     	}
     	if(finalBean.getBhaxaBean().getTpaRecommendations()!=null){
        	 tpaRecommendations= finalBean.getBhaxaBean().getTpaRecommendations();

     	}
	}
	
		//Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
		if (finalBean.getEmailCommonDetaisBean() != null) {
			EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
			CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
		}

		LoginBean lb = new LoginBean();

		session = request.getSession();
		if (session != null) {

			lb = (LoginBean) session.getAttribute("user");
			//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
			name_of_user_obj_1 = session.getAttribute("userName")
					.toString();
			userid_of_user = lb.getModuleUserId(1);
			moduleUserId = lb.getModuleUserId(moduleId);
		}

	} catch (Exception e) {
		System.out.println("ERROR IN BHARATI AXA PREFILL: " + e);
	} finally {

	}
%>




<h3 align="center">View Bharati Axa Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>

		 <form name="Form1" id="Form1" action="./GeneratexlsxAction" method="post" onSubmit="return validateForm();">
		 
		 <input type="hidden" name="policyId" id="policyId" value="<%=policyId%>">                   
        <input type="hidden" name="cardId" id="cardId" value="<%=cardId%>">   
		<input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="BHAXA">   
        <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		<input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr >	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=ccnUnique%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
              <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
            
            </td>
            </tr>
 			<tr>
            <td  class="text_heading"><strong>Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patientname" type="text"  id="patientname"  size="20" MAXLENGTH="150" value="<%=PATIENT_NAME%>" readonly="readonly"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Corporate /policy number</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policynumber" type="text"  id="policynumber"  size="20" MAXLENGTH="150"   value="<%=policyNumber%>"  readonly="readonly"></td>
            </tr>
            <td  class="text_heading"><strong>Sum insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="10"  value="<%=SUM_INSURED%>"  readonly="readonly"></td>
            </tr>	
			
			  <tr>
            <td  class="text_heading"><strong>Balance SI</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="balancesi" type="text"  id="balancesi"  size="20" MAXLENGTH="10"  value="<%=balanceSi%>"  readonly="readonly"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Claim type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimtype" type="text"  id="claimtype"  size="20" MAXLENGTH="20" value="<%=claimType%>"  readonly="readonly"></td>
            </tr>
			
			<tr>
            <td  class="text_heading"><strong>Policy period</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyperiod" type="text"  id="policyperiod"  size="20" MAXLENGTH="25"  value="<%=policyPeriod%>"  readonly="readonly"></td>
            </tr>
            
            <tr>
            <td  class="text_heading"><strong>Initial/interim/final</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="initial" type="text"  id="initial"  size="20" MAXLENGTH="150"  value="<%=initial%>"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Hospital</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospital" type="text"  id="hospital"  size="20" MAXLENGTH="500"  value="<%=hospitalName%>"  readonly="readonly"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>DOA & DOD(probable)</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="doadod" type="text"  id="doadod"  size="20" MAXLENGTH="25"  value="<%=doaDod%>" readonly="readonly"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Diagnosis</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="500"  value="<%=diagnosis%>"  readonly="readonly"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Treatment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="treatment" type="text"  id="treatment"  size="20" MAXLENGTH="150"  value="<%=treatment%>" ></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Tariff- open or agreed</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="tariff" type="text"  id="tariff"  size="20" MAXLENGTH="150"  value="<%=tariff%>"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Estimate</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="estimate" type="text"  id="estimate"  size="20" MAXLENGTH="10"  value="<%=REQUESTED_AMOUNT%>"  readonly="readonly"></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>Any negotiation/case management</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="negotiations" type="text"  id="negotiations"  size="20" MAXLENGTH="150"  value="<%=negotiations%>" ></td>
            </tr>
            	<tr>
            <td  class="text_heading"><strong>TPA recommendations</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="tparecomm" type="text"  id="tparecomm"  size="20" MAXLENGTH="150"  value="<%=tpaRecommendations%>"  ></td>
            </tr>
			 
		
		<tr>
		
		</tr>
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
 			 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>