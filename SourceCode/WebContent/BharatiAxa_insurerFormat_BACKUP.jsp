<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.BhratiAxaDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearch.js"></script>

<%
	int moduleId=0; 
	 String CLAIMID = "";
   	 String PATIENT_NAME = "";
   	 String EMPLOYEE_NAME = "";
   	 String EMPLOYEE_ID = "";
   	 String CORPORATE_NAME = "";
   	 String SUM_INSURED = "";
   	 String REQUESTED_AMOUNT = "";
   	 String APPROVED_AMOUNT = "";
   	 String EMAILID = "";
   	 String CCEMAILID = "";
   	 String policyDesc="";
    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 GetInsurerPrefillDataListBean finalBean = null;
try{  

	
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	
	if(finalBean.getBhaxaBean() != null)
	{
	
      	  CLAIMID = String.valueOf(finalBean.getBhaxaBean().getClaimid());
    	  PATIENT_NAME = finalBean.getBhaxaBean().getPatientName();
    	  EMPLOYEE_NAME = finalBean.getBhaxaBean().getEmployeeName();
    	  EMPLOYEE_ID = finalBean.getBhaxaBean().getEmployeeId();
    	  CORPORATE_NAME = finalBean.getBhaxaBean().getCorporateName();
    	  SUM_INSURED = finalBean.getBhaxaBean().getSumInsured();
    	  REQUESTED_AMOUNT = finalBean.getBhaxaBean().getRequestedAmount();
    	  APPROVED_AMOUNT = finalBean.getBhaxaBean().getApprovedAmount();
    	  //EMAILID = finalBean.getBhaxaBean().getEmailId();
    	  //CCEMAILID = finalBean.getBhaxaBean().getCcEmailId();
    	  policyDesc= finalBean.getBhaxaBean().getPolicyDescription();
    	  moduleId=finalBean.getBhaxaBean().getModuleId();
    	  rauthflg=finalBean.getBhaxaBean().getReauthFlag();
    	  
	}
	
		//Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
		if (finalBean.getEmailCommonDetaisBean() != null) {
			EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
			CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
		}

		LoginBean lb = new LoginBean();

		session = request.getSession();
		if (session != null) {

			lb = (LoginBean) session.getAttribute("user");
			//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
			name_of_user_obj_1 = session.getAttribute("userName")
					.toString();
			userid_of_user = lb.getModuleUserId(1);
			moduleUserId = lb.getModuleUserId(moduleId);
		}

	} catch (Exception e) {
		System.out.println("ERROR IN BHARATI AXA PREFILL: " + e);
	} finally {

	}
%>




<h3 align="center">View Bharati Axa Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="genxls.jsp" method="post">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="BHAXA">   
        <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		<input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr >	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=CLAIMID%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
            
            </td>
            </tr>
 			 <tr>
            <td  class="text_heading"><strong>Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patientname" type="text"  id="patientname"  size="20" MAXLENGTH="150"  value="<%=PATIENT_NAME%>"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Employee Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="empname" type="text"  id="empname"  size="20" MAXLENGTH="150"   value="<%=EMPLOYEE_NAME%>"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Employee ID</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="empid" type="text"  id="empid"  size="20" MAXLENGTH="50"   value="<%=EMPLOYEE_ID%>"></td>
            </tr>
            <tr>
            <td  class="text_heading"><strong>Corporate Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="corpname" type="text"  id="corpname"  size="20" MAXLENGTH="200"  value="<%=CORPORATE_NAME%>"></td>
            </tr>	
			
			  <tr>
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="10"  value="<%=SUM_INSURED%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Requested Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="reqamt" type="text"  id="reqamt"  size="20" MAXLENGTH="10" value="<%=REQUESTED_AMOUNT%>"></td>
            </tr>
			
			<tr>
            <td  class="text_heading"><strong>Approved Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="apramt" type="text"  id="apramt"  size="20" MAXLENGTH="10"  value="<%=APPROVED_AMOUNT%>"></td>
            </tr>

			 <tr>
			   <td  class="text_heading"><strong>Policy Description</strong> </td>
			   <td colspan="3"  align="left"  class="normal"><textarea name="poldesc" id="poldesc"  rows="10" cols="40"><%=policyDesc%></textarea></td>
	         </tr>
		
		<tr>
		
		</tr>
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
 			 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>