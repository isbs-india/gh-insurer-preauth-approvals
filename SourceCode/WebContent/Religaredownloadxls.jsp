<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.ReligareDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};

</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
Connection con = null;
		
ResultSet rs=null;
		
	 int moduleId=0; 
	 String CLAIMID = "";
	 String PATIENT_NAME = "";
	 String EMPLOYEE_NAME = "";
	 String AGE = "";
	 String GENDER = "";
	 String RELATIONSHIP = "";
	 String SUM_INSURED = "";
	 String  PACKAGE = "";
	 String HOSPITAL_NAME = "";
	 String CITY = "";
	 String DOA = "";
	 String DOD = "";
	 String DURATION_OF_STAY = "";
	 String ROOM_TYPE = "";
	 String DIAGNOSIS = "";
	 String ROOM_RENT_PAID = "";
	 String APPROVED_AMOUNT = "";
	 String REQUESTED_AMOUNT = "";
	 String AMTPAID = "";
	 String AMTCLAIMED = "";
	 String COPAYMENTAMT = "";
	 String  PLANOFTREATMENT = "";
	 String  DR_REAMRKS = "";

	 
	 
	 String EMAILID = "";
	 String CCEMAILID = "";
    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 GetInsurerPrefillDataListBean finalBean = null;
 
try{  

	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	
	if(finalBean.getReligareBean() != null)
	{
    	 CLAIMID = String.valueOf(finalBean.getReligareBean().getClaimid());
    	 PATIENT_NAME = finalBean.getReligareBean().getPatientName();
  		 EMPLOYEE_NAME = finalBean.getReligareBean().getEmployeeName();
  		 AGE = finalBean.getReligareBean().getAge();
  		 GENDER = finalBean.getReligareBean().getGender();
  		 RELATIONSHIP = finalBean.getReligareBean().getRelationShip();
  		 SUM_INSURED = finalBean.getReligareBean().getSumInsured();
  		 PACKAGE = finalBean.getReligareBean().getPackagee();
  		 HOSPITAL_NAME = finalBean.getReligareBean().getHospitalName();
  		 CITY = finalBean.getReligareBean().getCity();
  		 DOA = finalBean.getReligareBean().getDoa();
  		 DOD = finalBean.getReligareBean().getDod();
  		 DURATION_OF_STAY = finalBean.getReligareBean().getDurationOfStay();
  		 ROOM_TYPE = finalBean.getReligareBean().getRoomType();
  		 DIAGNOSIS = finalBean.getReligareBean().getDiagnosis();
  		 ROOM_RENT_PAID = finalBean.getReligareBean().getRoomRentPaid();
  		 APPROVED_AMOUNT = finalBean.getReligareBean().getApprovedAmount();
  		 REQUESTED_AMOUNT = finalBean.getReligareBean().getRequestedAmount();
  		 AMTPAID =finalBean.getReligareBean().getAmtPaid();
  		 AMTCLAIMED = finalBean.getReligareBean().getAmtClaimed();
  		 COPAYMENTAMT = finalBean.getReligareBean().getCopayPer();
  		 PLANOFTREATMENT = finalBean.getReligareBean().getPlanOfTreatment();
  		 DR_REAMRKS = finalBean.getReligareBean().getDrRemarks();
     	  
     	  EMAILID = finalBean.getReligareBean().getEmailId();
     	  CCEMAILID = finalBean.getReligareBean().getCcEmailid();
     	 rauthflg=finalBean.getReligareBean().getReauthFlag();
     	 moduleId=finalBean.getReligareBean().getModuleId();
     	 
      }//if
      
      LoginBean lb=new LoginBean();

  	session= request.getSession();
  	if(session != null) {
  		
  		lb=(LoginBean)session.getAttribute("user");
  		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
  		 name_of_user_obj_1 =session.getAttribute("userName").toString();
  	     userid_of_user = lb.getModuleUserId(1);
  	     moduleUserId = lb.getModuleUserId(moduleId);
  	}

}
catch(Exception e)
{
	System.out.println("ERROR IN RELIGARE PREFILL: "+e);
}
finally
{
	rs.close();
	con.close();
}


%>




<h3 align="center">View religare Details for Claim</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="./GenUiicXls" method="post">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="RE">   
        <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		<input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		<input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
        <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
              
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr >	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
             
         
            </tr>
 			 <tr>
            <td  class="text_heading"><strong>Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patientname" type="text"  id="patientname"  size="20" MAXLENGTH="150" readonly="readonly" value="<%=PATIENT_NAME%>"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Age</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="150"  readonly="readonly"  value="<%=AGE%>"></td>
            </tr>
			 <tr>
            <td  class="text_heading"><strong>Gender</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="gender" type="text"  id="gender"  size="20" MAXLENGTH="50"  readonly="readonly"  value="<%=GENDER%>"></td>
            </tr>
            <tr>
            <td  class="text_heading"><strong>Relationship</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="relationship" type="text"  id="relationship"  size="20" MAXLENGTH="200" readonly="readonly"  value="<%=RELATIONSHIP%>"></td>
            </tr>	
			
			  <tr>
            <td  class="text_heading"><strong>Employee</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="employee" type="text"  id="employee"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=EMPLOYEE_NAME%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="10" readonly="readonly" value="<%=SUM_INSURED%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Hospital</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospital" type="text"  id="hospital"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=HOSPITAL_NAME%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>City</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=CITY%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Package</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="package" type="text"  id="package"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=PACKAGE%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>D.O.A</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="doa" type="text"  id="doa"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=DOA%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>D.O.D</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dod" type="text"  id="dod"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=DOD%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Duration of stay</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="durationofstay" type="text"  id="durationofstay"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=DURATION_OF_STAY%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Room Type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=ROOM_TYPE%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Room Rent</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrent" type="text"  id="roomrent"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=ROOM_RENT_PAID%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Diagnosis</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=DIAGNOSIS%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Treatment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="treatment" type="text"  id="treatment"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=PLANOFTREATMENT%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Claimed amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimedamt" type="text"  id="claimedamt"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=REQUESTED_AMOUNT%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Payable amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="payableamt" type="text"  id="payableamt"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=APPROVED_AMOUNT%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Copay</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="copay" type="text"  id="copay"  size="20" MAXLENGTH="10"  readonly="readonly" value="<%=COPAYMENTAMT%>"></td>
            </tr>
			
			  <tr>
            <td  class="text_heading"><strong>Dr.Remarks</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="drremarks" type="text"  id="drremarks"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=DR_REAMRKS%>"></td>
            </tr>
		
		
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>


		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>