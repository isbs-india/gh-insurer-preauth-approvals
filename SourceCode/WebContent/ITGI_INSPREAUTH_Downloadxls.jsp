<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.util.ProcedureConstants"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.ItgiDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};

</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
GetInsurerPrefillDataListBean finalBean = null;
finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");		


int moduleId=0; 
String CLAIMID="0";



String CARDID = "";
String HOSPITAL_NAME = "";
String ADM_DATE = "";
String ROOM_TYPE = "";
String LENGTH_OF_STAY = "";
String DIAGNOSIS = "";
String TPA_FINAL_BILL_ESTIMATE = "";
String EMAILID = "";
String CCEMAILID = "";

String MOBILE_HOSP_REP = "";
String BENIFICIARY_NAME = "";
String MEDICAL_SURG_DAYCARE = "";

String GR_NAME = "";
String CITY = "";
String MOBILE_DOCTOR = "";
String TELE_BENEFICIARY = "";
String ICU_ADMISSION_ROOM = "";
String ROOM_ICU_TARIFF = "";
String CONSULTANT_CHARGES = "";
String SURGERY_CHARGES = "";

String OT_CHARGES = "";
String IMPLANTS_COST_IF_ANY = "";
String AL_AMOUNT_ASKED_BY_NSP = "";

String PRE_NEGOTIATED_TARIFF = "";
String RMO_NURSING = "";
String MEDICINE_COST = "";
String INVST_COSTS = "";
String ANAESTHESIA_CHARGES = "";
String ANY_OTHER_EXPENCES = "";
String RECOMMENDED_ALL_AMT_BY_TPA = "";

String COMPLAINTS_WITH_DURATION = "";
String CLINICAL_FINDINGS = "";
String TESTS_DONE_SO_FOR = "";
String MEDICALS_DETAILS = "";
String SURGERY_RX = "";

/* String cardid = "";
String hospname = "";
String hospmobileno = "";
String benfname = "";
String admdate = "";
String medsurgdaycare = "";
String roomtype = "";
String grname = "";
String city = "";
String drmobileno = "";
String telebenf = "";
String icuadmnroom = "";
String lenofstay = "";
String roomicutariff = "";
String consultchrgs = "";
String surgerychrgs = "";
String otchrgs = "";
String implntscosts = "";
String alamtaskdbynsp = "";
String prenegotedtariff = "";
String rmonursing = "";
String medcost = "";
String invstcost = "";
String anastchrgs = "";
String anyotherexp = "";
String recmalamtbytpa = "";
String tpafinalbillestm = "";
String complntswithdurn = "";
String clinicalfindngs = "";
String testdone = "";
String diagnosis = "";
String meddetails = "";
String surgeryrx = "";
String emailid="";
String ccemailid=""; */
    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 //GetInsurerPrefillDataListBean finalBean = null;
try{  
	
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
		 
        
	if(finalBean.getItgiBean() != null)
	{
		 
    	  CARDID = finalBean.getItgiBean().getCardId();
    	  CLAIMID = String.valueOf(finalBean.getItgiBean().getClaimid());
    	  HOSPITAL_NAME = finalBean.getItgiBean().getHospitalName();
    	  ADM_DATE = finalBean.getItgiBean().getAdmDate();
    	  ROOM_TYPE = finalBean.getItgiBean().getRoomType();
    	  LENGTH_OF_STAY = finalBean.getItgiBean().getLengthOfStay();
    	  DIAGNOSIS = finalBean.getItgiBean().getDiagnosis();
    	  TPA_FINAL_BILL_ESTIMATE = finalBean.getItgiBean().getTpaFinalBillEstimate();
    	  EMAILID = finalBean.getItgiBean().getEmailId();
    	  CCEMAILID = finalBean.getItgiBean().getCcEmailId(); 
    	  
    	  MOBILE_HOSP_REP = finalBean.getItgiBean().getMobileHospRep();
    	  BENIFICIARY_NAME = finalBean.getItgiBean().getBenificiaryName();
    	  MEDICAL_SURG_DAYCARE = finalBean.getItgiBean().getMedicalSurgDayCare();

    	  GR_NAME = finalBean.getItgiBean().getGrName();
    	  CITY = finalBean.getItgiBean().getCity();
    	  MOBILE_DOCTOR = finalBean.getItgiBean().getMobileDoctor();
    	  TELE_BENEFICIARY = finalBean.getItgiBean().getTeleBeneficiary();
    	  ICU_ADMISSION_ROOM = finalBean.getItgiBean().getIcuAdmissionRoom();

    	  ROOM_ICU_TARIFF = finalBean.getItgiBean().getRoomIcuTariff();
    	  CONSULTANT_CHARGES = finalBean.getItgiBean().getConsultantCharges();
    	  SURGERY_CHARGES = finalBean.getItgiBean().getSurgeryCharges();
    	  
    	  OT_CHARGES = finalBean.getItgiBean().getOtCharges();
    	  IMPLANTS_COST_IF_ANY = finalBean.getItgiBean().getImplantsCostIf_any();
    	  AL_AMOUNT_ASKED_BY_NSP = finalBean.getItgiBean().getAlAmountAskedByNsp();

    	  PRE_NEGOTIATED_TARIFF = finalBean.getItgiBean().getPreNegotiatedTariff();
    	  RMO_NURSING = finalBean.getItgiBean().getRmoNursing();
    	  MEDICINE_COST = finalBean.getItgiBean().getMedicineCost();
    	  INVST_COSTS = finalBean.getItgiBean().getInvstCosts();
    	  ANAESTHESIA_CHARGES = finalBean.getItgiBean().getAnaesthesiaCharges();
    	  ANY_OTHER_EXPENCES = finalBean.getItgiBean().getAnyOtherExpenses();
    	  RECOMMENDED_ALL_AMT_BY_TPA = finalBean.getItgiBean().getRecommendedAlamtBytpa();

    	  COMPLAINTS_WITH_DURATION = finalBean.getItgiBean().getComplaintsWithDuration();
    	  CLINICAL_FINDINGS = finalBean.getItgiBean().getClinicalFindings();
    	  TESTS_DONE_SO_FOR = finalBean.getItgiBean().getTestsDoneSoFar();

    	  MEDICALS_DETAILS = finalBean.getItgiBean().getMedicalDetails();
    	  SURGERY_RX = finalBean.getItgiBean().getSurgeryRx();


    	  
    	  
    	  //moduleId=finalBean.getItgiBean().getModuleId();
    /* 	  emailid=request.getParameter("emailid")!=null?request.getParameter("emailid"):"0";
    	  ccemailid=request.getParameter("ccemailid")!=null?request.getParameter("ccemailid"):"0";
    	
		 cardid=request.getParameter("cardid")!=null?request.getParameter("cardid"):"NA";
		 hospname=request.getParameter("hospname")!=null?request.getParameter("hospname"):"NA";
		 hospmobileno=request.getParameter("hospmobileno")!=null?request.getParameter("hospmobileno"):"NA";
		 benfname=request.getParameter("benfname")!=null?request.getParameter("benfname"):"NA";
		 admdate=request.getParameter("admdate")!=null?request.getParameter("admdate"):"NA";
		 medsurgdaycare=request.getParameter("medsurgdaycare")!=null?request.getParameter("medsurgdaycare"):"NA";
		 roomtype=request.getParameter("roomtype")!=null?request.getParameter("roomtype"):"NA";
		 grname=request.getParameter("grname")!=null?request.getParameter("grname"):"NA";
		 city=request.getParameter("city")!=null?request.getParameter("city"):"NA";
		 drmobileno=request.getParameter("drmobileno")!=null?request.getParameter("drmobileno"):"NA";
		 telebenf=request.getParameter("telebenf")!=null?request.getParameter("telebenf"):"NA";
		 icuadmnroom=request.getParameter("icuadmnroom")!=null?request.getParameter("icuadmnroom"):"NA";
		 lenofstay=request.getParameter("lenofstay")!=null?request.getParameter("lenofstay"):"NA";
		 roomicutariff=request.getParameter("roomicutariff")!=null?request.getParameter("roomicutariff"):"NA";
		 consultchrgs=request.getParameter("consultchrgs")!=null?request.getParameter("consultchrgs"):"NA";
		 surgerychrgs=request.getParameter("surgerychrgs")!=null?request.getParameter("surgerychrgs"):"NA";
		 otchrgs=request.getParameter("otchrgs")!=null?request.getParameter("otchrgs"):"NA";
		 implntscosts=request.getParameter("implntscosts")!=null?request.getParameter("implntscosts"):"NA";
		 alamtaskdbynsp=request.getParameter("alamtaskdbynsp")!=null?request.getParameter("alamtaskdbynsp"):"NA";
		 prenegotedtariff=request.getParameter("prenegotedtariff")!=null?request.getParameter("prenegotedtariff"):"NA";
		 rmonursing=request.getParameter("rmonursing")!=null?request.getParameter("rmonursing"):"NA";
		 medcost=request.getParameter("medcost")!=null?request.getParameter("medcost"):"NA";
		 invstcost=request.getParameter("invstcost")!=null?request.getParameter("invstcost"):"NA";
		 anastchrgs=request.getParameter("anastchrgs")!=null?request.getParameter("anastchrgs"):"NA";
		 anyotherexp=request.getParameter("anyotherexp")!=null?request.getParameter("anyotherexp"):"NA";
		 recmalamtbytpa=request.getParameter("recmalamtbytpa")!=null?request.getParameter("recmalamtbytpa"):"NA";
		 tpafinalbillestm=request.getParameter("tpafinalbillestm")!=null?request.getParameter("tpafinalbillestm"):"NA";
		 complntswithdurn=request.getParameter("complntswithdurn")!=null?request.getParameter("complntswithdurn"):"NA";
		 clinicalfindngs=request.getParameter("clinicalfindngs")!=null?request.getParameter("clinicalfindngs"):"NA";
		 testdone=request.getParameter("testdone")!=null?request.getParameter("testdone"):"NA";
		 diagnosis=request.getParameter("diagnosis")!=null?request.getParameter("diagnosis"):"NA";
		 meddetails=request.getParameter("meddetails")!=null?request.getParameter("meddetails"):"NA";
		 surgeryrx=request.getParameter("surgeryrx")!=null?request.getParameter("surgeryrx"):"NA"; 
		 */
	   
		/*  
		 moduleId=finalBean.getBhaxaBean().getModuleId();
   	    rauthflg=finalBean.getBhaxaBean().getReauthFlag(); */
    	  
    	  
      }//if
      

  	LoginBean lb=new LoginBean();

  	session= request.getSession();
  	if(session != null) {
  		
  		lb=(LoginBean)session.getAttribute("user");
  		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
  		 name_of_user_obj_1 =session.getAttribute("userName").toString();
  	     userid_of_user = lb.getModuleUserId(1);
  	     moduleUserId = lb.getModuleUserId(moduleId);
  	}
      

}
catch(Exception e)
{
	System.out.println("ERROR IN ITGI PREFILL: "+e);
}
finally
{
	//
}


%>




<h3 align="center">View ITGI Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
   
		 <form name="Form1" id="Form1" action="./GenUiicXls" method="post">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="ITGI">  
		<input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		<input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
			<tr>
			<td>&nbsp;</td>	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=CLAIMID%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
              </td>
            </tr>
 			 <tr>
			 	<td>GENERAL DETAILS</td>	
            <td  class="text_heading"><strong>TPA/ EMP ID CARD NO.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="cardid" type="text"  id="cardid"  size="20" MAXLENGTH="50"  value="<%=CARDID%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME_OF_HOSPITAL</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="200"   value="<%=HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MOBILE HOSP REP.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospmobileno" type="text"  id="hospmobileno"  size="20" MAXLENGTH="50"   value="<%=MOBILE_HOSP_REP%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>BENEFICIARY_NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="benfname" type="text"  id="benfname"  size="20" MAXLENGTH="100" value="<%=BENIFICIARY_NAME%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ADM_DATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="admdate" type="text"  id="admdate"  size="20" MAXLENGTH="10"  value="<%=ADM_DATE%>"></td>
            </tr>
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICAL / SURG / DAY CARE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="medsurgdaycare" type="text"  id="medsurgdaycare"  size="20" MAXLENGTH="10"  value="<%=MEDICAL_SURG_DAYCARE%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="10"  value="<%=ROOM_TYPE%>"></td>
            </tr>

       
			   
			  
			  <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>GR. NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="grname" type="text"  id="grname"  size="20" MAXLENGTH="100"  value="<%=GR_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="100"  value="<%=CITY%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MOBILE DOCTOR</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="drmobileno" type="text"  id="drmobileno"  size="20" MAXLENGTH="50"  value="<%=MOBILE_DOCTOR%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TELE -BENEFICIARY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="telebenf" type="text"  id="telebenf"  size="20" MAXLENGTH="100"  value="<%=TELE_BENEFICIARY%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ICU ADMISSION / ROOM</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="icuadmnroom" type="text"  id="icuadmnroom"  size="20" MAXLENGTH="50"   value="<%=ICU_ADMISSION_ROOM%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong> LENGTH OF STAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="lenofstay" type="text"  id="lenofstay"  size="20" MAXLENGTH="10"  value="<%=LENGTH_OF_STAY%>"></td>
            </tr>
			
			  
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			<tr>
			<td nowrap>EXPENSES DETAILS</td>	
            <td  class="text_heading"><strong>ROOM / ICU TARIFF</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomicutariff" type="text"  id="roomicutariff"  size="20" MAXLENGTH="100"   value="<%=ROOM_ICU_TARIFF%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CONSULTANT CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="consultchrgs" type="text"  id="consultchrgs"  size="20" MAXLENGTH="100"  value="<%=CONSULTANT_CHARGES%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>SURGERY CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="surgerychrgs" type="text"  id="surgerychrgs"  size="20" MAXLENGTH="50"    value="<%=SURGERY_CHARGES%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>OT CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="otchrgs" type="text"  id="otchrgs"  size="20" MAXLENGTH="100" value="<%=OT_CHARGES%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>IMPLANTS COST, IF ANY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="implntscosts" type="text"  id="implntscosts"  size="20" MAXLENGTH="10"  value="<%=IMPLANTS_COST_IF_ANY%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>AL_AMOUNT ASKED BY NSP</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="alamtaskdbynsp" type="text"  id="alamtaskdbynsp"  size="20" MAXLENGTH="10"  value="<%=AL_AMOUNT_ASKED_BY_NSP%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PRE-NEGOTIATED TARIFF </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="prenegotedtariff" type="text"  id="prenegotedtariff"  size="20" MAXLENGTH="10"   value="<%=PRE_NEGOTIATED_TARIFF%>"></td>
			</tr>
			<tr>
			<td nowrap>&nbsp;</td>	
            <td  class="text_heading"><strong>RMO/ NURSING </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="rmonursing" type="text"  id="rmonursing"  size="20" MAXLENGTH="100"   value="<%=RMO_NURSING%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICINES COST</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="medcost" type="text"  id="medcost"  size="20" MAXLENGTH="100"  value="<%=MEDICINE_COST%>"></td>
            </tr>
            
            
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>INVST. COSTS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="invstcost" type="text"  id="invstcost"  size="20" MAXLENGTH="50"    value="<%=INVST_COSTS%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ANAESTHESIA CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="anastchrgs" type="text"  id="anastchrgs"  size="20" MAXLENGTH="100" value="<%=ANAESTHESIA_CHARGES%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ANY OTHER EXPENSES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="anyotherexp" type="text"  id="anyotherexp"  size="20" MAXLENGTH="10"  value="<%=ANY_OTHER_EXPENCES%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>RECOMMENDED AL AMT  BY TPA</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="recmalamtbytpa" type="text"  id="recmalamtbytpa"  size="20" MAXLENGTH="10"  value="<%=RECOMMENDED_ALL_AMT_BY_TPA%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TPA FINAL BILL ESTIMATE </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="tpafinalbillestm" type="text"  id="tpafinalbillestm"  size="20" MAXLENGTH="10"   value="<%=TPA_FINAL_BILL_ESTIMATE%>"></td>
			</tr> 
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			
			<tr>
			<td>MEDICAL DETAILS</td>	
            <td  class="text_heading"><strong>COMPLAINTS WITH DURATION: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="complntswithdurn" type="text"  id="complntswithdurn"  size="20" MAXLENGTH="100"  value="<%=COMPLAINTS_WITH_DURATION%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CLINICAL FINDINGS : </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="clinicalfindngs" type="text"  id="clinicalfindngs"  size="20" MAXLENGTH="100"  value="<%=CLINICAL_FINDINGS%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TESTS DONE SO FAR: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="testdone" type="text"  id="testdone"  size="20" MAXLENGTH="50"    value="<%=TESTS_DONE_SO_FOR%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" value="<%=DIAGNOSIS%>"></td>
            </tr>	
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICAL DETAILS: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="meddetails" type="text"  id="meddetails"  size="20" MAXLENGTH="300"  value="<%=MEDICALS_DETAILS%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong> SURGERY Rx:</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="surgeryrx" type="text"  id="surgeryrx"  size="20" MAXLENGTH="100"  value="<%=SURGERY_RX%>"></td>
			
			</tr>
		
		
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
<form name="downloadodoc" action="./DownloadFtpFile" method="post" target="_blank">
<input type="hidden" name="filenme" id="filenme" value="">
<input type="hidden" name="scanningdate" id="scanningdate" value="">
<input type="hidden" name="actfilename" id="actfilename" value="">
<input type="hidden" name="maindocid" id="maindocid" value="">
<input type="hidden" name="subdocid" id="subdocid" value="">
<input type="hidden" name="modulename" id="modulename" value="">
</form>	 			 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>