<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.util.ProcedureConstants"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.ItgiDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};

</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
		
	 int moduleId=0; 
	 String CLAIMID = "";
	 String CARDID = "";
	 String HOSPITAL_NAME = "";
	 String ADM_DATE = "";
	 String ROOM_TYPE = "";
	 String LENGTH_OF_STAY = "";
	 String DIAGNOSIS = "";
	 String TPA_FINAL_BILL_ESTIMATE = "";
	 String EMAILID = "";
	 String CCEMAILID = "";
	 
	 String policyId="";
  	 String cardId="";
  	 String insuredId="";
  	 String ccnUnique="";    	 
    	 int clmid=0;
    	 int rauthflg=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 
			 String MobileHospRep="";
			 String BenificiaryName="";
			 String MedicalSurgDayCare="";
			 String GrName="";
			 String City="";
			 String MobileDoctor="";
			 String TeleBeneficiary="";
			 String IcuAdmissionRoom="";
			 String RoomIcuTariff="";
			 String ConsultantCharges="";
			 String SurgeryCharges="";
			 String OtCharges="";
			 String ImplantsCostIf_any="";
			 String AlAmountAskedByNsp="";
			 String PreNegotiatedTariff="";
			 String RmoNursing="";
			 String MedicineCost="";
			 String InvstCosts="";
			 String AnaesthesiaCharges="";
			 String AnyOtherExpenses="";
			 String RecommendedAlamtBytpa="";
			 String ComplaintsWithDuration="";
			 String ClinicalFindings="";
			 String TestsDoneSoFar="";
			 String MedicalDetails="";
			 String SurgeryRx="";
			 
    	 GetInsurerPrefillDataListBean finalBean = null;
try{  
	
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
		 
        
	if(finalBean.getItgiBean() != null)
	{
		  CLAIMID = String.valueOf(finalBean.getItgiBean().getClaimid());
    	  CARDID = finalBean.getItgiBean().getCardId();
    	  HOSPITAL_NAME = finalBean.getItgiBean().getHospitalName();
    	  ADM_DATE = finalBean.getItgiBean().getAdmDate();
    	  ROOM_TYPE = finalBean.getItgiBean().getRoomType();
    	  LENGTH_OF_STAY = finalBean.getItgiBean().getLengthOfStay();
    	  DIAGNOSIS = finalBean.getItgiBean().getDiagnosis();
    	  TPA_FINAL_BILL_ESTIMATE = finalBean.getItgiBean().getTpaFinalBillEstimate();
    	  //EMAILID = finalBean.getItgiBean().getEmailId();
    	  //CCEMAILID = finalBean.getItgiBean().getCcEmailId();
    	  moduleId=finalBean.getItgiBean().getModuleId();
    	  policyId = finalBean.getItgiBean().getPolicyID();
	     	cardId = finalBean.getItgiBean().getCardId();
	     	insuredId = finalBean.getItgiBean().getInsuredId();
	     	ccnUnique= finalBean.getCcnUnique();
	     	
	     	MobileHospRep = finalBean.getItgiBean().getMobileHospRep();
	     	BenificiaryName = finalBean.getItgiBean().getBenificiaryName();
	     	MedicalSurgDayCare = finalBean.getItgiBean().getMedicalSurgDayCare();
	     	GrName = finalBean.getItgiBean().getGrName();
	     	City = finalBean.getItgiBean().getCity();
	     	if(finalBean.getItgiBean().getMobileDoctor()!=null){
		     	MobileDoctor = finalBean.getItgiBean().getMobileDoctor();
	     	}
	     	TeleBeneficiary = finalBean.getItgiBean().getTeleBeneficiary();
	     	IcuAdmissionRoom = finalBean.getItgiBean().getIcuAdmissionRoom();
	     	if(finalBean.getItgiBean().getMobileDoctor()!=null){
	     	RoomIcuTariff = finalBean.getItgiBean().getRoomIcuTariff();
	     	}
	     	ConsultantCharges = finalBean.getItgiBean().getConsultantCharges();
	     	SurgeryCharges = finalBean.getItgiBean().getSurgeryCharges();
	     	OtCharges = finalBean.getItgiBean().getOtCharges();
	     	ImplantsCostIf_any = finalBean.getItgiBean().getImplantsCostIf_any();
	     	AlAmountAskedByNsp = finalBean.getItgiBean().getAlAmountAskedByNsp();
	     	PreNegotiatedTariff = finalBean.getItgiBean().getPreNegotiatedTariff();
	     	RmoNursing = finalBean.getItgiBean().getRmoNursing();
	     	MedicineCost = finalBean.getItgiBean().getMedicineCost();
	     	InvstCosts = finalBean.getItgiBean().getInvstCosts();
	     	AnaesthesiaCharges = finalBean.getItgiBean().getAnaesthesiaCharges();
	     	AnyOtherExpenses = finalBean.getItgiBean().getAnyOtherExpenses();
	     	RecommendedAlamtBytpa = finalBean.getItgiBean().getRecommendedAlamtBytpa();
	     	if(finalBean.getItgiBean().getComplaintsWithDuration()!=null){
	     	ComplaintsWithDuration = finalBean.getItgiBean().getComplaintsWithDuration();
	     	}
	     	if(finalBean.getItgiBean().getClinicalFindings()!=null){
	     	ClinicalFindings = finalBean.getItgiBean().getClinicalFindings();
	     	}
	     	if(finalBean.getItgiBean().getTestsDoneSoFar()!=null){
	     	TestsDoneSoFar = finalBean.getItgiBean().getTestsDoneSoFar();
	     	}
	     	if(finalBean.getItgiBean().getMedicalDetails()!=null){
	     	MedicalDetails = finalBean.getItgiBean().getMedicalDetails();
	     	}
	     	if(finalBean.getItgiBean().getSurgeryRx()!=null){
	     	SurgeryRx = finalBean.getItgiBean().getSurgeryRx();
	     	}
	     
			
      }//if
      
      
    //Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
    		if (finalBean.getEmailCommonDetaisBean() != null) {
    			EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
    			CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
    		}
      

  	LoginBean lb=new LoginBean();

  	session= request.getSession();
  	if(session != null) {
  		
  		lb=(LoginBean)session.getAttribute("user");
  		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
  		 name_of_user_obj_1 =session.getAttribute("userName").toString();
  	     userid_of_user = lb.getModuleUserId(1);
  	     moduleUserId = lb.getModuleUserId(moduleId);
  	}
      

}
catch(Exception e)
{
	System.out.println("ERROR IN ITGI PREFILL: "+e);
}
finally
{
	//
}


%>




<h3 align="center">View ITGI Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="./GeneratexlsxAction" method="post">
                            
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
        <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="ITGI">  
		<input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		<input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>">
		<input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		
		<input type="hidden" name="policyId" id="policyId" value="<%=policyId%>">                   
        <input type="hidden" name="cardId" id="cardId" value="<%=cardId%>">   
		<input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr>
			<td>&nbsp;</td>	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=ccnUnique%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=CLAIMID%>">
             <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
              </td>
            </tr>
 			 <tr>
			 	<td>GENERAL DETAILS</td>	
            <td  class="text_heading"><strong>TPA/ EMP ID CARD NO.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="cardid" type="text"  id="cardid"  size="20" MAXLENGTH="50"  value="<%=CARDID%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME_OF_HOSPITAL</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="200"   value="<%=HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MOBILE HOSP REP.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospmobileno" type="text"  id="hospmobileno"  size="20" MAXLENGTH="50"   value="<%=MobileHospRep%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>BENEFICIARY_NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="benfname" type="text"  id="benfname"  size="20" MAXLENGTH="100" value="<%=BenificiaryName%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ADM_DATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="admdate" type="text"  id="admdate"  size="20" MAXLENGTH="10"  value="<%=ADM_DATE%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICAL / SURG / DAY CARE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="medsurgdaycare" type="text"  id="medsurgdaycare"  size="20" MAXLENGTH="10"  value="<%=MedicalSurgDayCare%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="10"  value="<%=ROOM_TYPE%>"></td>
            </tr>

       
			   
			  
			  <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>GR. NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="grname" type="text"  id="grname"  size="20" MAXLENGTH="100"  value="<%=GrName%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="100"  value="<%=City%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MOBILE DOCTOR</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="drmobileno" type="text"  id="drmobileno"  size="20" MAXLENGTH="50"  value="<%=MobileDoctor%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TELE -BENEFICIARY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="telebenf" type="text"  id="telebenf"  size="20" MAXLENGTH="100"  value="<%=TeleBeneficiary%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ICU ADMISSION / ROOM</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="icuadmnroom" type="text"  id="icuadmnroom"  size="20" MAXLENGTH="50"   value="<%=IcuAdmissionRoom%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong> LENGTH OF STAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="lenofstay" type="text"  id="lenofstay"  size="20" MAXLENGTH="10"  value="<%=LENGTH_OF_STAY%>"></td>
            </tr>
			
			  
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			<tr>
			<td nowrap>EXPENSES DETAILS</td>	
            <td  class="text_heading"><strong>ROOM / ICU TARIFF</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomicutariff" type="text"  id="roomicutariff"  size="20" MAXLENGTH="100"   value="<%=RoomIcuTariff%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CONSULTANT CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="consultchrgs" type="text"  id="consultchrgs"  size="20" MAXLENGTH="100"  value="<%=ConsultantCharges%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>SURGERY CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="surgerychrgs" type="text"  id="surgerychrgs"  size="20" MAXLENGTH="50"    value="<%=SurgeryCharges%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>OT CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="otchrgs" type="text"  id="otchrgs"  size="20" MAXLENGTH="100" value="<%=OtCharges%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>IMPLANTS COST, IF ANY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="implntscosts" type="text"  id="implntscosts"  size="20" MAXLENGTH="10"  value="<%=ImplantsCostIf_any%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>AL_AMOUNT ASKED BY NSP</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="alamtaskdbynsp" type="text"  id="alamtaskdbynsp"  size="20" MAXLENGTH="10"  value="<%=AlAmountAskedByNsp%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PRE-NEGOTIATED TARIFF </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="prenegotedtariff" type="text"  id="prenegotedtariff"  size="20" MAXLENGTH="10"   value="<%=PreNegotiatedTariff%>"></td>
			</tr>
			<tr>
			<td nowrap>&nbsp;</td>	
            <td  class="text_heading"><strong>RMO/ NURSING </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="rmonursing" type="text"  id="rmonursing"  size="20" MAXLENGTH="100"   value="<%=RmoNursing%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICINES COST</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="medcost" type="text"  id="medcost"  size="20" MAXLENGTH="100"  value="<%=MedicineCost%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>INVST. COSTS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="invstcost" type="text"  id="invstcost"  size="20" MAXLENGTH="50"    value="<%=InvstCosts%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ANAESTHESIA CHARGES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="anastchrgs" type="text"  id="anastchrgs"  size="20" MAXLENGTH="100" value="<%=AnaesthesiaCharges%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ANY OTHER EXPENSES</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="anyotherexp" type="text"  id="anyotherexp"  size="20" MAXLENGTH="10"  value="<%=AnyOtherExpenses%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>RECOMMENDED AL AMT  BY TPA</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="recmalamtbytpa" type="text"  id="recmalamtbytpa"  size="20" MAXLENGTH="10"  value="<%=RecommendedAlamtBytpa%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TPA FINAL BILL ESTIMATE </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="tpafinalbillestm" type="text"  id="tpafinalbillestm"  size="20" MAXLENGTH="10"   value="<%=TPA_FINAL_BILL_ESTIMATE%>"></td>
			</tr> 
			 
			 
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			
			<tr>
			<td>MEDICAL DETAILS</td>	
            <td  class="text_heading"><strong>COMPLAINTS WITH DURATION: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="complntswithdurn" type="text"  id="complntswithdurn"  size="20" MAXLENGTH="100"  value="<%=ComplaintsWithDuration%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CLINICAL FINDINGS : </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="clinicalfindngs" type="text"  id="clinicalfindngs"  size="20" MAXLENGTH="100"  value="<%=ClinicalFindings%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>TESTS DONE SO FAR: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="testdone" type="text"  id="testdone"  size="20" MAXLENGTH="50"    value="<%=TestsDoneSoFar%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" value="<%=DIAGNOSIS%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MEDICAL DETAILS: </strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="meddetails" type="text"  id="meddetails"  size="20" MAXLENGTH="300"  value="<%=MedicalDetails%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong> SURGERY Rx:</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="surgeryrx" type="text"  id="surgeryrx"  size="20" MAXLENGTH="100"  value="<%=SurgeryRx%>"></td>
			
			</tr>
		
		<tr>
		
		</tr>
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
			 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>