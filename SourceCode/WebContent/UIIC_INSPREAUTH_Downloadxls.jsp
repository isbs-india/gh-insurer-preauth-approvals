<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.UiicDataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
		

GetInsurerPrefillDataListBean finalBean = null;
finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");


		int moduleId=0; 
		
		String INSURED_NAME="";
		String PATIENT_NAME="";
		String AGE="";
		String SEX="";
		String RELATION_WITH_INSURED="";
		String BALANCE_SUM_INSURED="";
		String POLICY_NUMBER="";
		String SUM_INSURED="";
		String POLICY_TYPE_INDIVIDUAL_OR_GMC="";
		String DATE_OF_INCEPTION="";
		String HOSPITAL_NAME="";
		String CITY="";
		String STATE="";
		String DIAGNOSIS="";
		String MANAGEMENTORMEDICALOREMERGENCY="";
		String DATEOFADMISSION="";
		String EXPECTEDDURATIONOFSTAY="";
		String ROOMTYPE="";
		String ROOMRENT_ICURENT="";
		String ROOMRENT_ELIGIBILITY="";
		String NO_OF_DAYSSTAYED="";
		String GIPSAPPNAMOUNTREQUESTED="";
		String GIPSAPPN_AMOUNT_RECOMM="";
		String GIPSAPPN_REASONSFORDEDUCTION="";
		String ROOMRENTICURENTAMOUNTREQUESTED="";
		String ROOMRENT_AMOUNT_RECOMM="";
		String ROOMRENT_REASONSFORDEDUCTION="";
		String PROCEDUREAMOUNTREQUESTED="";
		String PROCEDURE_AMOUNT_RECOMM="";
		String PROCEDURE_REASONSFORDEDUCTION="";
		String PHARMACYAMOUNTREQUESTED="";
		String PHARMACY_AMOUNT_RECOMM="";
		String PHARMACY_REASONSFORDEDUCTION="";
		String OTANESTHESIAAMOUNTREQUESTED="";
		String OTANESTHESIA_AMOUNT_RECOMM="";
		String OTANESTHESIAREASONSDEDUCTION="";
		String LABINVESTIGATIONSAMTREQUESTED="";
		String LABINVESTIGATIONSAMTRECOMM="";
		String LABINVESTIGATIONRSNDEDUCTION="";
		String CONSULTATIONAMOUNTREQUESTED="";
		String CONSULTATION_AMOUNT_RECOMM="";
		String CONSULTATIONREASONSFORDEDUC="";
		String OTHERS_AMOUNTREQUESTED="";
		String OTHERS_AMOUNT_RECOMM="";
		String OTHERS_REASONSFORDEDUCTION="";
		String TOTAL_AMOUNTREQUESTED="";
		String TOTAL_AMOUNT_RECOMM="";
		String TOTAL__REASONSFORDEDUCTION="";
		String DISEASE_TREATMENT_ARE_COVERED="";
		String EXPENSESPAID="";
		String ALLHEADERSINBREAKDOWN="";
		String THECOSTOFDIAGNOSTICSEXCEED="";
		String PROCESSINGDOCTORSNAMEANDTPA="";
		String CREATEDDATE="";
		String TPA_NAME="";
		String PPN_NETWORK_HOSPITALS="";
		String CATEGORY_HOSPITALTERTYPLUS="";
		String EMAILID="";
		String CC_EMAILID="";
		String POLICY_DESCRIPTION="";

    	 
    	 String clmid="0";
    	 int rauthflg=0;
    	 int preauthAppNo=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
 
try{  
	
	
	
	
	if(finalBean.getUiicBean() != null)
	{
 	    	//CLAIMID = rs.getString("CLAIMID");
    	 INSURED_NAME=finalBean.getUiicBean().getInsuredName();
  		 PATIENT_NAME=finalBean.getUiicBean().getPatientName();
  		 AGE=finalBean.getUiicBean().getAge();
  		 SEX=finalBean.getUiicBean().getSex();
  		 RELATION_WITH_INSURED=finalBean.getUiicBean().getRelationWithInsured();
  		 BALANCE_SUM_INSURED=finalBean.getUiicBean().getBalanceSumInsured();
  		 POLICY_NUMBER=finalBean.getUiicBean().getPolicyNumber();
  		 SUM_INSURED=finalBean.getUiicBean().getSumInsured();
  		 POLICY_TYPE_INDIVIDUAL_OR_GMC=finalBean.getUiicBean().getPolicyTypeIndividualOrGmc();
  		 DATE_OF_INCEPTION=finalBean.getUiicBean().getDateOfInception();
  		 HOSPITAL_NAME=finalBean.getUiicBean().getHospitalName();
  		 CITY=finalBean.getUiicBean().getCity();
  		 STATE=finalBean.getUiicBean().getState();
  		 DIAGNOSIS=finalBean.getUiicBean().getDiagnosis();
  		 MANAGEMENTORMEDICALOREMERGENCY=finalBean.getUiicBean().getManagementOrMedicalOrEmergency();
  		 DATEOFADMISSION=finalBean.getUiicBean().getDateOfAdmission();
  		 EXPECTEDDURATIONOFSTAY=finalBean.getUiicBean().getExpectedDurationOfStay();
  		 ROOMTYPE=finalBean.getUiicBean().getRoomType();
  		 ROOMRENT_ICURENT=finalBean.getUiicBean().getRoomRentIcuRent();
  		 ROOMRENT_ELIGIBILITY=finalBean.getUiicBean().getRoomRentEligibility();
  		 NO_OF_DAYSSTAYED=finalBean.getUiicBean().getNoOfDaysStayed();
  		 GIPSAPPNAMOUNTREQUESTED=finalBean.getUiicBean().getGipsaPpaAmountRequested();
  		 GIPSAPPN_AMOUNT_RECOMM=finalBean.getUiicBean().getGipsaPpnAmountRecomm();
  		 GIPSAPPN_REASONSFORDEDUCTION=finalBean.getUiicBean().getGipsaPpnReasonsForDeduction();
  		 ROOMRENTICURENTAMOUNTREQUESTED=finalBean.getUiicBean().getRoomRentIcuRentAmountRequested();
  		 ROOMRENT_AMOUNT_RECOMM=finalBean.getUiicBean().getRoomRentAmountRecomm();
  		 ROOMRENT_REASONSFORDEDUCTION=finalBean.getUiicBean().getRoomRentReasonsForDeduction();
  		 PROCEDUREAMOUNTREQUESTED=finalBean.getUiicBean().getProcedureAmountRequested();
  		 PROCEDURE_AMOUNT_RECOMM=finalBean.getUiicBean().getProcedureAmountRecomm();
  		 PROCEDURE_REASONSFORDEDUCTION=finalBean.getUiicBean().getProcedureReasonsForDeduction();
  		 PHARMACYAMOUNTREQUESTED=finalBean.getUiicBean().getPharmacyAmountRequested();
  		 PHARMACY_AMOUNT_RECOMM=finalBean.getUiicBean().getPharmacyAmountRecomm();
  		 PHARMACY_REASONSFORDEDUCTION=finalBean.getUiicBean().getPharmacyReasonsForDeduction();
  		 OTANESTHESIAAMOUNTREQUESTED=finalBean.getUiicBean().getOtanesthesiaAmountRequested();
  		 OTANESTHESIA_AMOUNT_RECOMM=finalBean.getUiicBean().getOtanesthesiaAmountRecomm();
  		 OTANESTHESIAREASONSDEDUCTION=finalBean.getUiicBean().getOtanesthesiaReasonsDeduction();
  		 LABINVESTIGATIONSAMTREQUESTED=finalBean.getUiicBean().getLabInvestigationsAmtRequested();
  		 LABINVESTIGATIONSAMTRECOMM=finalBean.getUiicBean().getLabInvestigationsAmtRecomm();
  		 LABINVESTIGATIONRSNDEDUCTION=finalBean.getUiicBean().getLabInvestigationrsnDeduction();
  		 CONSULTATIONAMOUNTREQUESTED=finalBean.getUiicBean().getConsultationAmountRequested();
  		 CONSULTATION_AMOUNT_RECOMM=finalBean.getUiicBean().getConsultationAmountRecomm();
  		 CONSULTATIONREASONSFORDEDUC=finalBean.getUiicBean().getConsultationReasonsForDeduc();
  		 OTHERS_AMOUNTREQUESTED=finalBean.getUiicBean().getOthersAmountRequested();
  		 OTHERS_AMOUNT_RECOMM=finalBean.getUiicBean().getOthersAmountRecomm();
  		 OTHERS_REASONSFORDEDUCTION=finalBean.getUiicBean().getOthersReasonsForDeduction();
  		 TOTAL_AMOUNTREQUESTED=finalBean.getUiicBean().getTotalAmountRequested();
  		 TOTAL_AMOUNT_RECOMM=finalBean.getUiicBean().getTotalAmountRecomm();
  		 TOTAL__REASONSFORDEDUCTION=finalBean.getUiicBean().getTotalReasonsForDeduction();
  		 DISEASE_TREATMENT_ARE_COVERED=finalBean.getUiicBean().getDiseaseTreatmentAreCovered();
  		 EXPENSESPAID=finalBean.getUiicBean().getExpensesPaid();
  		 ALLHEADERSINBREAKDOWN=finalBean.getUiicBean().getAllHeadersInBreakDown();
  		 THECOSTOFDIAGNOSTICSEXCEED=finalBean.getUiicBean().getTheCostOfDiagnosticsExceed();
  		 PROCESSINGDOCTORSNAMEANDTPA=finalBean.getUiicBean().getProcessingDoctorsNameandtpa();
  		 CREATEDDATE=finalBean.getUiicBean().getCreatedDate();
  		 TPA_NAME=finalBean.getUiicBean().getTpaName();
  		 PPN_NETWORK_HOSPITALS=finalBean.getUiicBean().getPpnNetworkHospitals();
  		 CATEGORY_HOSPITALTERTYPLUS=finalBean.getUiicBean().getCategoryHospitaltertyPlus();
  		 EMAILID=finalBean.getUiicBean().getEmailId();
  		 CC_EMAILID=finalBean.getUiicBean().getCcEmailid();
  		 POLICY_DESCRIPTION=finalBean.getUiicBean().getPolicyDescription();
  		 moduleId=finalBean.getUiicBean().getModuleId();
		 clmid=finalBean.getUiicBean().getClaimid();
		 rauthflg=finalBean.getUiicBean().getReauthFlag();
 
     }//if
     
  
     LoginBean lb=new LoginBean();
 	session= request.getSession();
 	if(session != null) {
 		
 		lb=(LoginBean)session.getAttribute("user");
 		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
 		 name_of_user_obj_1 =session.getAttribute("userName").toString();
 	     userid_of_user = lb.getModuleUserId(1);
 	     moduleUserId = lb.getModuleUserId(moduleId);
 	}
     
     
}
catch(Exception e)
{
	System.out.println("ERROR: "+e);
}
finally
{
	//
}


%>




<h3 align="center">View UIIC Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		 <form name="Form1" id="Form1" action="./GenUiicXls" method="post">
         <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
         <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		 <input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
		 <input type="hidden" name="tpa" id="tpa" value="<%=TPA_NAME%>">  
		 <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="UIIC">  
		 <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		 <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CC_EMAILID%>"> 
		 <input type="hidden" name=moduleName id="moduleName" value="<%//=request.getParameter("moduleName")%>"> 
		 <input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr>
			<td width="263">&nbsp;</td>	
            <td width="227"  class="text_heading"><strong>Claim ID </strong></td>
            <td colspan="3"  align="left"  class="normal"><%=clmid%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
            <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">			            </td>
            </tr>
 			 <tr>
			 	<td >POLICY DETAILS AND <br/>PATIENT IDENTIFICATION DETAILS</td>	
            <td  align="left"  class="normal">&nbsp;</td>
			<td width="168"  align="left"  class="normal">&nbsp;</td>
 			 </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME OF THE INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="insuredname" type="text"  id="insuredname"  size="20" MAXLENGTH="200" readonly="readonly"   value="<%=INSURED_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME OF THE PATIENT</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ptname" type="text"  id="ptname"  size="20" MAXLENGTH="150"  readonly="readonly"   value="<%=PATIENT_NAME%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>AGE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=AGE%>"></td>
            </tr>	
			
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong> SEX</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="sex" type="text"  id="sex"  size="20" MAXLENGTH="10" readonly="readonly"  value="<%=SEX%>"></td>
            </tr>	
			
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>RELATION WITH INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="relation" type="text"  id="relation"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=RELATION_WITH_INSURED%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>BALANCE SUM INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="balsi" type="text"  id="balsi"  size="20" MAXLENGTH="25" readonly="readonly"   value="<%=BALANCE_SUM_INSURED%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>POLICY NUMBER</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno" type="text"  id="policyno"  size="20" MAXLENGTH="150" readonly="readonly"   value="<%=POLICY_NUMBER%>"></td>
            </tr>

       
			   
			  
			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>CLAIM NUMBER</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimid" type="text"  id="claimid"  size="20" MAXLENGTH="15" readonly="readonly"   value="<%=clmid%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>SUM INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="si" type="text"  id="si"  size="20" MAXLENGTH="20" readonly="readonly"   value="<%=SUM_INSURED%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>POLICY TYPE/INDIVIDUAL OR GMC</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="poltypegmc" type="text"  id="poltypegmc"  size="20" MAXLENGTH="75" readonly="readonly"   value="<%=POLICY_TYPE_INDIVIDUAL_OR_GMC%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DATE OF INCEPTION OF POLICY FIRST TAKEN</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="polinceptiondt" type="text"  id="polinceptiondt"  size="20" MAXLENGTH="25" readonly="readonly"   value="<%=DATE_OF_INCEPTION%>"></td>
            </tr>	
			
			
			
			<tr>
			<td > SPECIFIC EXCLUSIONS IN THE POLICY</td>	
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
			 <tr>
			   <td>&nbsp;</td>
			   <td  class="text_heading"><strong>Policy Description</strong> </td>
			   <td colspan="3"  align="left"  class="normal"><textarea name="poldesc" id="poldesc"  rows="10" cols="40" readonly="readonly"><%=POLICY_DESCRIPTION%></textarea></td>
	     </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>HOSPITAL NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="150" readonly="readonly"    value="<%=HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="50"  readonly="readonly"   value="<%=CITY%>"></td>
            </tr>
			
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>STATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="state" type="text"  id="state"  size="20" MAXLENGTH="50"  readonly="readonly"   value="<%=STATE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PPN/NETWORK HOSPITALS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ppnntwrkhosp" type="text"  id="ppnntwrkhosp"  size="20" MAXLENGTH="50"  readonly="readonly"   value=""></td>
            </tr>
			
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CATEGORY OF HOSPITAL /TERTIARY OR TERT PLUS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="cathosptertplus" type="text"  id="cathosptertplus"  size="20" MAXLENGTH="50"  readonly="readonly"   value=""></td>
            </tr>
			
			
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" readonly="readonly"  value="<%=DIAGNOSIS%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MANAGEMENT SURGICAL OR MEDICAL/PLANNED OR EMERGENCY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="mgmtsrplemr" type="text"  id="mgmtsrplemr"  size="20" MAXLENGTH="10"  readonly="readonly"  value=""></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DATE OF ADMISSION</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="doa" type="text"  id="doa"  size="20" MAXLENGTH="10" readonly="readonly"   value="<%=DATEOFADMISSION%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>EXPECTED DURATION OF STAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="totaldurofstay" type="text"  id="totaldurofstay"  size="20" MAXLENGTH="10" readonly="readonly"   value="<%=EXPECTEDDURATIONOFSTAY%>"></td>
            </tr>

       
			   
			  
			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE/ SINGLE,SEMIPRIVATE, GENERAL ETC</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="150" readonly="readonly"   value=""></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT/ ICU RENT PER DAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrenticyprday" type="text"  id="roomrenticyprday"  size="20" MAXLENGTH="150" readonly="readonly"   value=""></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT ELIGIBILITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrentelg" type="text"  id="roomrentelg"  size="20" MAXLENGTH="50" readonly="readonly"   value=""></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NO OF DAYS STAYED IN ROOM AND IN ICU</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noofdaysinicuroom" type="text"  id="noofdaysinicuroom"  value="" readonly="readonly"   size="20" MAXLENGTH="200"></td>
			</tr> 
			
			  
			 <tr>
			 	<td>BREAK DOWN /WORKING SHEET</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>COMPONENTS</td>
			<td>AMOUNT REQUESTED BY HOSPITAL</td>
			<td width="130">AMOUNT RECOMMENDED BY TPA</td>
			<td width="138">REASONS FOR DEDUCTION</td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>GIPSA PPN PACKAGE</td>
			<td><input name="gipsappnhosp" type="text"  id="gipsappnhosp"  size="20" MAXLENGTH="50" readonly="readonly"   value=""></td>
			<td width="130"><input name="gipsappntpa" type="text"  id="gipsappntpa"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			<td width="138"><input name="gipsappnreson" type="text"  id="gipsappnreson"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>ROOM RENT/ICU RENT</td>
			<td><input name="roomrenthosp" type="text"  id="roomrenthosp"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=ROOMRENTICURENTAMOUNTREQUESTED%>"></td>
			<td width="130"><input name="roomrenttpa" type="text"  id="roomrenttpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=ROOMRENT_AMOUNT_RECOMM%>"></td>
			<td width="138"><input name="roomrentded" type="text"  id="roomrentded"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>PROCEDURE(PROFESSIONAL) CHARGES</td>
			<td><input name="profchghosp" type="text"  id="profchghosp"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=PROCEDUREAMOUNTREQUESTED%>"></td>
			<td width="130"><input name="profchgtpa" type="text"  id="profchgtpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=PROCEDURE_AMOUNT_RECOMM%>"></td>
			<td width="138"><input name="profchgded" type="text"  id="profchgded"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>PHARMACY</td>
			<td><input name="phrhosp" type="text"  id="phrhosp"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=PHARMACYAMOUNTREQUESTED%>"></td>
			<td width="130"><input name="phrtpa" type="text"  id="phrtpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=PHARMACY_AMOUNT_RECOMM%>"></td>
			<td width="138"><input name="phrded" type="text"  id="phrded"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>OT + ANESTHESIA CHARGES</td>
			<td><input name="othosp" type="text"  id="othosp"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=OTANESTHESIAAMOUNTREQUESTED%>"></td>
			<td width="130"><input name="ottpa" type="text"  id="ottpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=OTANESTHESIA_AMOUNT_RECOMM%>"></td>
			<td width="138"><input name="otdedu" type="text"  id="otdedu"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>LAB INVESTIGATIONS</td>
			<td><input name="labinvhosp" type="text"  id="labinvhosp"  size="20" MAXLENGTH="50" readonly="readonly"   value="<%=LABINVESTIGATIONSAMTREQUESTED%>"></td>
			<td width="130"><input name="labinvtpa" type="text"  id="labinvtpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=LABINVESTIGATIONSAMTRECOMM%>"></td>
			<td width="138"><input name="labinvded" type="text"  id="labinvded"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>CONSULTATION CHARGES</td>
			<td><input name="conchrhosp" type="text"  id="conchrhosp"  size="20" MAXLENGTH="50" readonly="readonly"   value=""></td>
			<td width="130"><input name="conchrtpa" type="text"  id="conchrtpa"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			<td width="138"><input name="conchrded" type="text"  id="conchrded"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>OTHERS</td>
			<td><input name="othershosp" type="text"  id="othershosp"  size="20" MAXLENGTH="50"  readonly="readonly"  value="<%=OTHERS_AMOUNTREQUESTED%>"></td>
			<td width="130"><input name="otherstpa" type="text"  id="otherstpa"  size="20" readonly="readonly" MAXLENGTH="50"  value="<%=OTHERS_AMOUNT_RECOMM%>"></td>
			<td width="138"><input name="othersdedu" type="text"  id="othersdedu"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>TOTAL</td>
			<td><input name="totalhosp" type="text"  id="totalhosp"  size="20" MAXLENGTH="50" readonly="readonly"   value=""></td>
			<td width="130"><input name="totaltpa" type="text"  id="totaltpa"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			<td width="138"><input name="totaldedu" type="text"  id="totaldedu"  size="20" readonly="readonly" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>DECLARATIONS</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">DISEASE AND TREATMENT ARE COVERED UNDER POLICY TERMS AND CONDITIONS</td>
			<td><select name="disessel" id="disessel">
			<option value="YES">YES</option>
			<option value="NO">NO</option>
			</select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">EXPENSES PAID ARE AS PER REASONABLE AND CUSTOMARY CLAUSE</td>
			<td><select name="expnsel" id="expnsel">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">ALL HEADERS IN BREAKDOWN ARE PAID AS PER POLICY TERMS AND CONDITIONS AND ANY DEVIATION WILL BE LIABLE FOR RECOVERY</td>
			<td><select name="headerssel" id="headerssel">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">IF THE COST OF DIAGNOSTICS EXCEED 5000 RS A SEPARATE SHEET DISPLAYING THE BREAKDOWN OF INVESTIGATIONS NEED TO BE ATTACHED</td>
			<td><select name="diagcostsel" id="diagcostsel" >
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>    
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit" ></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
		 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>