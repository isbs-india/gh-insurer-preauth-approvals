 <%@page import="com.ghpl.persistence.iauth.SendToInsurerManager"%>
 <%@page import="com.ghpl.model.iauth.OSTicketBean"%>
<%@page import="java.util.List"%>
<%@include file="header.jsp" %> 

<link rel="stylesheet" href="css/jquery-ui.css">

 <script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null;};
</script> 
<script type="text/javascript">
function getOthersData(){
	var reqType = document.getElementById("reqType");
	var reqTypeId = reqType.options[reqType.selectedIndex].value;
	//alert(reqTypeId);
	if(reqTypeId=='0'){
		alert("Please select ReqType");
		document.getElementById("reqType").focus();
		return false;
	}else{
		//alert(reqTypeId);
		//document.preauthForm.action="./KayakoDataAction"; 
		//document.preauthForm.submit(); 
		if(reqTypeId=="Mail"){
			document.getElementById("kayakoIdrow").focus();
			document.getElementById("otherrowid").style.display='none';
			document.getElementById("kayakoIdrow").style.display='';
		}else{
			document.getElementById("otherrowid").focus();
			document.getElementById("otherrowid").style.display='';
			document.getElementById("kayakoIdrow").style.display='none';
		}
	}
}
function validateAll(){
	var reqType = document.getElementById("reqType");
	var reqTypeId = reqType.options[reqType.selectedIndex].value;
	//alert(reqTypeId);
	if(reqTypeId=='0'){
		alert("Please select valid ReqType");
		document.getElementById("reqType").focus();
		return false;
	}else{
		if(reqTypeId=='Mail'){
			document.getElementById("kayakoIdrow").style.display='';
			document.getElementById("kayakoId").focus();
			var kayakoId = document.getElementById("kayakoId");
			var kayakoIdval = kayakoId.options[kayakoId.selectedIndex].value;
			if(kayakoIdval=='0'){
				alert("Please select valid KayakoId");
				document.getElementById("kayakoId").focus();
				return false;
			}
		}else{
			var others = document.getElementById("others");
			if(others.value==""){
				alert("Please enter valid others data");
				others.focus();
				return false;
			}
		}
	}

	var typeofdoc1 = document.getElementById("typeofdoc1");
	var typeofdoc1val =typeofdoc1.options[typeofdoc1.selectedIndex].value;
	var typeofdoc2 = document.getElementById("typeofdoc2");
	var typeofdoc2val =typeofdoc2.options[typeofdoc2.selectedIndex].value;
	var typeofdoc3 = document.getElementById("typeofdoc3");
	var typeofdoc3val =typeofdoc3.options[typeofdoc3.selectedIndex].value;
	var typeofdoc4 = document.getElementById("typeofdoc4");
	var typeofdoc4val =typeofdoc4.options[typeofdoc4.selectedIndex].value;
	var typeofdoc5 = document.getElementById("typeofdoc5");
	var typeofdoc5val =typeofdoc5.options[typeofdoc5.selectedIndex].value;
	if(typeofdoc1val==0 && typeofdoc2val ==0 && typeofdoc3val==0 && typeofdoc4val==0 && typeofdoc5val==0){
		alert("please select minimum one document");
		typeofdoc1.focus();
		return false;
	}
	
	var filedoc1 =document.getElementById("filedoc1").value;
	var filedoc2 =document.getElementById("filedoc2").value;
	var filedoc3 =document.getElementById("filedoc3").value;
	var filedoc4 =document.getElementById("filedoc4").value;
	var filedoc5 =document.getElementById("filedoc5").value;
	if(typeofdoc1val != 0 && filedoc1==""){
		alert("please browse file document1");
		document.getElementById("filedoc1").focus();
		return false;
	}
	if(typeofdoc1val ==0 && !filedoc1==""){
		alert("please select document1");
		document.getElementById("typeofdoc1").focus();
		return false;
	}
	if(typeofdoc2val != 0 && filedoc2==""){
		alert("please browse file document2");
		document.getElementById("filedoc2").focus();
		return false;
	}
	if(typeofdoc2val == 0 && !filedoc2==""){
		alert("please select document2");
		document.getElementById("filedoc2").focus();
		return false;
	}
	if(typeofdoc3val != 0 && filedoc3==""){
		alert("please browse file document3");
		document.getElementById("filedoc3").focus();
		return false;
	}
	if(typeofdoc3val ==0 && !filedoc3==""){
		alert("please select document3");
		document.getElementById("typeofdoc3").focus();
		return false;
	}
	if(typeofdoc4val != 0 && filedoc4==""){
		alert("please browse file document4");
		document.getElementById("filedoc4").focus();
		return false;
	}
	if(typeofdoc4val ==0 && !filedoc4==""){
		alert("please select document4");
		document.getElementById("typeofdoc4").focus();
		return false;
	}
	if(typeofdoc5val != 0 && filedoc5==""){
		alert("please browse file document5");
		document.getElementById("filedoc5").focus();
		return false;
	}
	if(typeofdoc5val ==0 && !filedoc5==""){
		alert("please select document5");
		document.getElementById("typeofdoc5").focus();
		return false;
	}
	
	if(typeofdoc2val != 0 && filedoc2!="" || typeofdoc2val == 0 && filedoc2!="" || typeofdoc2val != 0 && filedoc2 ==""){
		if(typeofdoc1val ==0 && filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}if(typeofdoc1val != 0 && filedoc1==""){
			alert("please browse file document1");
			document.getElementById("filedoc1").focus();
			return false;
		}
		if(typeofdoc1val ==0 && !filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}
	}
	if(typeofdoc3val != 0 && filedoc3!="" || typeofdoc3val == 0 && filedoc3!="" || typeofdoc3val != 0 && filedoc3 ==""){
		if(typeofdoc1val ==0 && filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}if(typeofdoc1val != 0 && filedoc1==""){
			alert("please browse file document1");
			document.getElementById("filedoc1").focus();
			return false;
		}
		if(typeofdoc1val ==0 && !filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}
		if(typeofdoc2val ==0 && filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}if(typeofdoc2val != 0 && filedoc2==""){
			alert("please browse file document2");
			document.getElementById("filedoc2").focus();
			return false;
		}
		if(typeofdoc2val ==0 && !filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}
	}
	if(typeofdoc4val != 0 && filedoc4!="" || typeofdoc4val == 0 && filedoc4!="" || typeofdoc4val != 0 && filedoc4 ==""){
		if(typeofdoc1val ==0 && filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}if(typeofdoc1val != 0 && filedoc1==""){
			alert("please browse file document1");
			document.getElementById("filedoc1").focus();
			return false;
		}
		if(typeofdoc1val ==0 && !filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}
		if(typeofdoc2val ==0 && filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}if(typeofdoc2val != 0 && filedoc2==""){
			alert("please browse file document2");
			document.getElementById("filedoc2").focus();
			return false;
		}
		if(typeofdoc2val ==0 && !filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}
		if(typeofdoc3val ==0 && filedoc3==""){
			alert("please select document3");
			document.getElementById("typeofdoc3").focus();
			return false;
		}if(typeofdoc3val != 0 && filedoc3==""){
			alert("please browse file document3");
			document.getElementById("filedoc3").focus();
			return false;
		}
		if(typeofdoc3val ==0 && !filedoc3==""){
			alert("please select document3");
			document.getElementById("typeofdoc3").focus();
			return false;
		}
	}
	if(typeofdoc5val != 0 && filedoc5!="" || typeofdoc5val == 0 && filedoc5!="" || typeofdoc5val != 0 && filedoc5 =="" ){
		if(typeofdoc1val ==0 && filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}if(typeofdoc1val != 0 && filedoc1==""){
			alert("please browse file document1");
			document.getElementById("filedoc1").focus();
			return false;
		}
		if(typeofdoc1val ==0 && !filedoc1==""){
			alert("please select document1");
			document.getElementById("typeofdoc1").focus();
			return false;
		}
		if(typeofdoc2val ==0 && filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}if(typeofdoc2val != 0 && filedoc2==""){
			alert("please browse file document2");
			document.getElementById("filedoc2").focus();
			return false;
		}
		if(typeofdoc2val ==0 && !filedoc2==""){
			alert("please select document2");
			document.getElementById("typeofdoc2").focus();
			return false;
		}
		if(typeofdoc3val ==0 && filedoc3==""){
			alert("please select document3");
			document.getElementById("typeofdoc3").focus();
			return false;
		}if(typeofdoc3val != 0 && filedoc3==""){
			alert("please browse file document3");
			document.getElementById("filedoc3").focus();
			return false;
		}
		if(typeofdoc3val ==0 && !filedoc3==""){
			alert("please select document3");
			document.getElementById("typeofdoc3").focus();
			return false;
		}
		if(typeofdoc4val ==0 && filedoc4==""){
			alert("please select document4");
			document.getElementById("typeofdoc4").focus();
			return false;
		}if(typeofdoc4val != 0 && filedoc4==""){
			alert("please browse file document4");
			document.getElementById("filedoc4").focus();
			return false;
		}
		if(typeofdoc4val ==0 && !filedoc4==""){
			alert("please select document4");
			document.getElementById("typeofdoc4").focus();
			return false;
		}
	}
	var filedocObj1 =document.getElementById("filedoc1");
	var filedocObj2 =document.getElementById("filedoc2");
	var filedocObj3 =document.getElementById("filedoc3");
	var filedocObj4 =document.getElementById("filedoc4");
	var filedocObj5 =document.getElementById("filedoc5");
	 if(filedoc1 !="" ){
		  var x1= checkFile(filedocObj1);
		 if(x1){
			 if(filedoc2 !=""){
				   var x2= checkFile(filedocObj2);
					 if(x2){
						 if(filedoc3 !=""){
							  var x3= checkFile(filedocObj3);
							  if(x3){
								  if(filedoc4 !=""){
									  var x4= checkFile(filedocObj4);
									  if(x4){
										  if(filedoc5 !=""){
											  var x5= checkFile(filedocObj5);
											  if(x5){
												  return x5;
											  }else{
												  return false;
												  filedocObj5.focus();
											  }
										  }else{
											  return true;
										  }
									  }else{
										  return false;
										  filedocObj4.focus();
									  }
								  }else{
									  return true;
								  }
							  }else{
								  return false;
								  filedocObj3.focus();
							  }
						  }else{
							  return true;
						  }
					 }else{
						 return false;
						 filedocObj2.focus();
					 }
	  			}else{
	  				return true;
	  			}
		 }else{
			 return false;
			 filedocObj1.focus();
		 }
	  }
		  
		//alert(filedoc1);
			if ( ( insurerForm.insreply[0].checked == false ) && ( insurerForm.insreply[1].checked == false ) && ( insurerForm.insreply[2].checked == false ))
			{
				alert ( "Please choose insurer approval" );
				return false;
			}
			var insurerremarks =document.getElementById("insurerremarks").value;
			var iChars1 = "!@#$%^&*()+=[]\\\';,/{}|\":<>?";
			if(insurerremarks.trim()==""){
				alert("please select insurer remarks");
				document.getElementById("insurerremarks").focus();
				return false;
			}else{
					for (var i = 0; i < insurerremarks.trim().length; i++) {
						if(iChars1.indexOf(insurerremarks.trim().charAt(i)) != -1){
							alert ("Special Characters are not allowed");
							document.getElementById("insurerremarks").value="";
							document.getElementById("insurerremarks").focus();
						    return false;
						}
					}
				}
}

function checkFile(fieldObj)
{
    var FileName  = fieldObj.value;
    var FileExt = FileName.substr(FileName.lastIndexOf('.')+1);
    var FileSize = fieldObj.files[0].size;
    if ( (FileExt != "pdf" && FileExt != "doc" && FileExt != "docx" && FileExt != "jpeg" && FileExt != "JPEG" && FileExt !="JPG" && FileExt !="jpg" && FileExt != "xls" && FileExt != "xlsx" && FileExt != "png" && FileExt != "tif") || FileSize>=2097152)
    {
        var error ="";
        error += "Please make sure your file is in pdf or doc or xls or png or tif  format and less than or equals 2 MB.\n\n";
        alert(error);
        return false;
    } else{
    	return true;
    }
}

</script>

<%

String ccnUnique=null;
if(request.getParameter("ccnUnique")!=null){
	ccnUnique=request.getParameter("ccnUnique");
}
	String claimId=null;
  if(request.getParameter("claimId")!=null){
	  claimId=request.getParameter("claimId");
  }
  String policyId=null;
	if(request.getParameter("policyId")!=null){
		policyId=request.getParameter("policyId");
	}
	String cardid=null;
	if(request.getParameter("cardid")!=null){
		cardid=request.getParameter("cardid");
	}
	String cardHolderName=null;
	if(request.getParameter("cardHolderName")!=null){
		cardHolderName=request.getParameter("cardHolderName");
	}
	String selfName=null;
	if(request.getParameter("selfName")!=null){
		selfName=request.getParameter("selfName");
	}
	String policyNo=null;
	if(request.getParameter("policyNo")!=null){
		policyNo=request.getParameter("policyNo");
	}
	 String kayakoId=null;
	 if(request.getParameter("kayakoId")!=null){
		 kayakoId=request.getParameter("kayakoId");
		}
	
	 int insuredId=0;
	 if(request.getParameter("insuredId")!=null){
		   insuredId= Integer.parseInt(request.getParameter("insuredId"));
		}
	 String policyHolderName=null;
		if(request.getParameter("policyHolderName")!=null){
	policyHolderName=request.getParameter("policyHolderName");
		}
	
		String reauthFlag=null;
		if(request.getParameter("reauthFlag")!=null){
	reauthFlag=request.getParameter("reauthFlag");
		}
	 
	
	  String status=null;
		if(request.getParameter("status")!=null){
	status=request.getParameter("status");
		}
	 String preauthNo=null;
		if(request.getParameter("preauthNo")!=null){
	preauthNo=request.getParameter("preauthNo");
		}
		
		
   	 String patAddress=null;
		if(request.getParameter("patAddress")!=null){
	patAddress=request.getParameter("patAddress");
		}
	 String patEmail=null;
		if(request.getParameter("patEmail")!=null){
	patEmail=request.getParameter("patEmail");
		}
	 String patMobile=null;
		if(request.getParameter("patMobile")!=null){
	patMobile=request.getParameter("patMobile");
		}
		String moduleId="0";
		if(request.getParameter("moduleId")!=null){
			moduleId=request.getParameter("moduleId");
		}	
%>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" align="center"> 
		<%@include file="leftmenu.jsp" %>
		</td>
		
		<td colspan="4" align="center">

<div id="showUploadDocsDiv" align="center">
<form name="insurerForm" action="./UploadPreauthInsurerAuthDocAction" method="post" 
	  enctype="multipart/form-data" accept-charset="utf-8" onsubmit="return validateAll();" >
	   <input type="hidden" name="moduleId" id="moduleId" value="<%=moduleId%>"/>
	   <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>"/>
     <input type="hidden" name="claimId" id="claimId" value="<%=claimId%>"/>
     <input type="hidden" name="policyId" id="policyId" value="<%=policyId%>"/>
     <input type="hidden" name="cardid" id="cardid" value="<%=cardid%>"/>
     <input type="hidden" name="cardHolderName" id="cardHolderName" value="<%=cardHolderName%>"/>
     <input type="hidden" name="selfName" id="selfName" value="<%=selfName%>"/>
     <input type="hidden" name="policyNo" id="policyNo" value="<%=policyNo%>"/>
     <input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>"/>
     <input type="hidden" name="policyHolderName" id="policyHolderName" value="<%=policyHolderName%>"/>
     <input type="hidden" name="status" id="status" value="<%=status%>"/>
     <input type="hidden" name="reauthFlag" id="reauthFlag" value="<%=reauthFlag%>"/>
     <input type="hidden" name="preauthNo" id="preauthNo" value="<%=preauthNo%>"/>
     <input type="hidden" name="patAddress" id="patAddress" value="<%=patAddress%>"/>
     <input type="hidden" name="patEmail" id="patEmail" value="<%=patEmail%>"/>
     <input type="hidden" name="patMobile" id="patMobile" value="<%=patMobile%>"/>
     
<table width="100%" border="1" align="left" cellpadding="1" cellspacing="1" class="tablebg">
		<tr>
		<h3>Preauth Collection For ClaimID: <%=ccnUnique%></h3>
		</tr>
		<tr>
			<td align="left" width="10%">ReqType: 
				<select name="reqType" id="reqType"  onchange="return getOthersData();">
				            <option value="0">Select</option>
							<option value="Courier">By Courier</option>
							<option value="Mail">By Mail</option>
							<option value="Phone">By Phone</option>
							<option value="Fax">By Fax</option>
							<option value="Letter">By Letter</option>
				</select> 
			 </td>
			  <td>&nbsp;</td>
		</tr>
			<tr id="kayakoIdrow" style="display:none;">
			<td align="left" width="10%">Kayako Id: 
					<%
				
					List<OSTicketBean> ticketList=  SendToInsurerManager.getKayakoDataDropdown();
			%>
				<select name="kayakoId" id="kayakoId">
					<option value="0">Select</option>
					<%for(OSTicketBean ticketBean:ticketList) {%>
					<option value="<%=ticketBean.getTicketId()%>"><%=ticketBean.getTicketId()%></option>
					<%} %>
					
				</select>
			 </td>
			 <td>&nbsp;</td>
		</tr>
		<tr id="otherrowid" style="display:none;">
			<td align="left" width="10%">Others: 
				<input type="text"  name="others" id="others" />
			 </td>
			  <td>&nbsp;</td>
		</tr>
		<tr>
		<td width="36%" align="right">&nbsp;</td>
		<td width="64%">&nbsp;</td>
		</tr>
		<tr>
		<td width="36%" align="right">DOC1: &nbsp;&nbsp;
		<select name="typeofdoc1" id="typeofdoc1">
			<%@include file="/Modules/INSURER_CONFMTN_DOC_UPLOAD.inc" %>
		</select></td>
		<td width="64%"><input type="file" name="filedoc1" id="filedoc1" /></td>
		</tr>
		
		<tr>
		<td width="36%" align="right">DOC2: &nbsp;&nbsp;
		  <select name="typeofdoc2" id="typeofdoc2"><%@include file="/Modules/INSURER_CONFMTN_DOC_UPLOAD.inc" %></select></td>
		<td width="64%"><input type="file" name="filedoc2" id="filedoc2" /></td>
		</tr>
		
		<tr>
		<td width="36%" align="right">DOC3: &nbsp;&nbsp;
		  <select name="typeofdoc3" id="typeofdoc3"><%@include file="/Modules/INSURER_CONFMTN_DOC_UPLOAD.inc" %></select></td>
		<td width="64%"><input type="file" name="filedoc3" id="filedoc3" /></td>
		</tr>
		
		<tr>
		<td width="36%" align="right">DOC4: &nbsp;&nbsp;
		  <select name="typeofdoc4" id="typeofdoc4"><%@include file="/Modules/INSURER_CONFMTN_DOC_UPLOAD.inc" %></select></td>
		<td width="64%"><input type="file" name="filedoc4" id="filedoc4" />
		</td>
		</tr>
		
		<tr>
		<td width="36%" align="right">DOC5: &nbsp;&nbsp;
		  <select name="typeofdoc5" id="typeofdoc5"><%@include file="/Modules/INSURER_CONFMTN_DOC_UPLOAD.inc" %></select></td>
		<td width="64%"><input type="file" name="filedoc5" id="filedoc5" /></td>
		</tr>
			 <tr>
	        <td colspan="2">&nbsp;</td>
	      </tr>
		<tr>
        <td colspan="2" align="center">INSURER APPROVAL</td>
      </tr>
      <tr>
        <td align="left"><p>
          <label>
            <input type="radio" name="insreply" id="insreply" value="1" />
            Insurer OK</label>
          <br />
          <label>
            <input type="radio" name="insreply" id="insreply" value="2" />
            Insurer Not OK</label>
          <br />
          <label>
            <input type="radio" name="insreply" id="insreply" value="3" />
            Insurer Conditional OK</label>
          <br />
        </p></td>
        <td><input type="text" name="insurerremarks" id="insurerremarks" value="" maxlength="500" /> &nbsp; (Please write insurer Remarks)</td>
      </tr>
		
		 <tr><td align="center" colspan="2">
		  <input type="submit" name="submit" value="Upload Docs"/>
		
		  </td></tr>
</table>
</form>
</div>

</td>
</tr>
 
 </table>


<%@include file="footer.jsp" %>