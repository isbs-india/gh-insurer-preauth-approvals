<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.UiicDataBean"%>
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>

<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var totalhosBill=0;
	var hosbill=0;
	
	var TPAamount=0;
	var totalTPAamount=0;
	for(var a=1; a<=8; a++){
		 hosbill = document.getElementById("0"+a).value;
		 hosbill = parseInt(hosbill) || 0;
		 totalhosBill=totalhosBill+hosbill
		 
		 TPAamount = document.getElementById("1"+a).value;
		 TPAamount = parseInt(TPAamount) || 0;
		 totalTPAamount=totalTPAamount+TPAamount
		 
	}
	$("#totalhosp").val(totalhosBill)
	$("#totaltpa").val(totalTPAamount)
})
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
	GetInsurerPrefillDataListBean finalBean = null;
finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");

//System.out.println("DOCS"+finalBean.getDocsBean().getCollectionMainDocTypeId());
//System.out.println("DOCS"+finalBean.getDocsDetailsCorpList().get(1));

int moduleId=0; 
		String INSURED_NAME = "";
		String PATIENT_NAME = "";
		String AGE = "";
		String SEX  = "";
		String RELATION_WITH_INSURED = "";
		String BALANCESUMINSURED  = "";
		String POLICY_NO  = "";
		String SUMINSURED = "";
		String POLICYTYPE  = "";
		String DATE_OF_INCEPTION = "";
		String HOSPITAL_NAME  = "";
		String CITY = "";
		String STATE  = "";
		String NETWORKS  = "";
		String DIAGNOSIS  = "";
		String DATE_OF_ADMISSION  = "";
		String EXPECTED_DURATION_OF_STAY = "";
		String ROOM_RENT_REQUESTED  = "";
		String ROOM_RENT_PAID  = "";
		String PROFESSIONAL_CHARGES_REQ  = "";
		String PROFESSIONAL_CHARGES_PAID = "";
		String PHARMACY_CHARGES_REQ  = "";
		String PHARMACY_CHARGES_PAID  = "";
		String OT_CHARGES_REQ  = "";
		String OT_CHARGES_PAID  = "";
		String LAB_INVES_CHARGES_REQ  = "";
		String LAB_INVES_CHARGES_PAID  = "";
		String OTHER_CHARGES_REQ  = "";
		String OTHER_CHARGES_PAID = "";
		String PROCESSING_DOCTOR = "";
		String TPANAME  = "";
		String EMAILID = "";
    	String CCEMAILID = "";
        String POLICY_DESC="";
    	 
    	 String clmid="0";
    	 String ccnUnique="0";
    	 int rauthflg=0;
    	 int preauthAppNo=0;
    	 
    	 String policyId="";
      	 String cardId="";
      	 String insuredId="";
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 
    	 String PpnNetworkHospitals="";	
    	 String ManagementOrMedicalOrEmergency="";
    	 String RoomType="";
    	 String RoomRentIcuRent="";
    	 String RoomRentEligibility="";
    	 String NoOfDaysStayed="";
    	 String GIPSA_PPN_HOSPAPHB="";
     	 String GIPSA_PPN_RESPONDOD="";
     	 String GIPSA_PPN_TPA="";
     	 String conchrhosp="";
    	 String conchrtpa="";
    	 String conchrded="";
try{  
	
	
	
if(finalBean.getUiicBean() != null)
{
	
	INSURED_NAME = finalBean.getUiicBean().getInsuredName();
	PATIENT_NAME = finalBean.getUiicBean().getPatientName();
	AGE = finalBean.getUiicBean().getAge();
	SEX = finalBean.getUiicBean().getSex();
	RELATION_WITH_INSURED = finalBean.getUiicBean().getRelationWithInsured();
	BALANCESUMINSURED = finalBean.getUiicBean().getBalanceSumInsured();
	POLICY_NO = finalBean.getUiicBean().getPolicyNumber();
	SUMINSURED = finalBean.getUiicBean().getSumInsured();
	POLICYTYPE = finalBean.getUiicBean().getPolicyTypeIndividualOrGmc();
	DATE_OF_INCEPTION = finalBean.getUiicBean().getDateOfInception();
	HOSPITAL_NAME = finalBean.getUiicBean().getHospitalName();
	if(finalBean.getUiicBean().getCity() != null){
		CITY = finalBean.getUiicBean().getCity();
	}
	STATE = finalBean.getUiicBean().getState();
	NETWORKS = finalBean.getUiicBean().getPpnNetworkHospitals();
	DIAGNOSIS = finalBean.getUiicBean().getDiagnosis();
	DATE_OF_ADMISSION = finalBean.getUiicBean().getDateOfAdmission();
	EXPECTED_DURATION_OF_STAY = finalBean.getUiicBean().getExpectedDurationOfStay();
	ROOM_RENT_REQUESTED = finalBean.getUiicBean().getRoomRentIcuRentAmountRequested();
	ROOM_RENT_PAID = finalBean.getUiicBean().getRoomRentAmountRecomm();
	PROFESSIONAL_CHARGES_REQ = finalBean.getUiicBean().getProcedureAmountRequested();
	PROFESSIONAL_CHARGES_PAID = finalBean.getUiicBean().getProcedureAmountRecomm();
	PHARMACY_CHARGES_REQ = finalBean.getUiicBean().getPharmacyAmountRequested();
	PHARMACY_CHARGES_PAID = finalBean.getUiicBean().getPharmacyAmountRecomm();
	OT_CHARGES_REQ = finalBean.getUiicBean().getOtanesthesiaAmountRequested();
	OT_CHARGES_PAID = finalBean.getUiicBean().getOtanesthesiaAmountRecomm();
	LAB_INVES_CHARGES_REQ = finalBean.getUiicBean().getLabInvestigationsAmtRequested();
	LAB_INVES_CHARGES_PAID = finalBean.getUiicBean().getLabInvestigationsAmtRecomm();
	OTHER_CHARGES_REQ = finalBean.getUiicBean().getOthersAmountRequested();
	OTHER_CHARGES_PAID = finalBean.getUiicBean().getOthersAmountRecomm();
	PROCESSING_DOCTOR = finalBean.getUiicBean().getProcessingDoctorsNameandtpa();
	TPANAME = finalBean.getUiicBean().getTpaName();
	EMAILID = finalBean.getUiicBean().getEmailId();
	CCEMAILID = finalBean.getUiicBean().getCcEmailid();
	POLICY_DESC = finalBean.getUiicBean().getPolicyDescription();
	moduleId=finalBean.getUiicBean().getModuleId();
	clmid=finalBean.getUiicBean().getClaimid();
	ccnUnique=finalBean.getUiicBean().getCcnUnique();
	rauthflg=finalBean.getUiicBean().getReauthFlag();
	
	policyId = finalBean.getUiicBean().getPolicyID();
   	cardId = finalBean.getUiicBean().getCardId();
   	insuredId = finalBean.getUiicBean().getInsuredId();
   	
	 if (finalBean.getUiicBean().getPpnNetworkHospitals()!= null) { 
		   	PpnNetworkHospitals = finalBean.getUiicBean().getPpnNetworkHospitals();
    	 }
	 
	 if (finalBean.getUiicBean().getManagementOrMedicalOrEmergency()!= null) { 
            ManagementOrMedicalOrEmergency = finalBean.getUiicBean().getManagementOrMedicalOrEmergency();
	 }
	 
	 if (finalBean.getUiicBean().getRoomType()!= null) { 
        	RoomType = finalBean.getUiicBean().getRoomType();
	 }
	 
	 if (finalBean.getUiicBean().getRoomRentIcuRent()!= null) { 
            RoomRentIcuRent = finalBean.getUiicBean().getRoomRentIcuRent();
	 }
	 else{
		 RoomRentIcuRent="0";
	  }
	 if (finalBean.getUiicBean().getRoomRentEligibility()!= null) { 
   	        RoomRentEligibility = finalBean.getUiicBean().getRoomRentEligibility();
	  }
	
	 if (finalBean.getUiicBean().getNoOfDaysStayed()!= null) { 
   	        NoOfDaysStayed = finalBean.getUiicBean().getNoOfDaysStayed();
	  }
	 else{
		 NoOfDaysStayed="0";
	  }
	
	 if (finalBean.getUiicBean().getGipsaPpaAmountRequested()!= null && finalBean.getUiicBean().getGipsaPpaAmountRequested()!="") {
		 GIPSA_PPN_HOSPAPHB  = finalBean.getUiicBean().getGipsaPpaAmountRequested();
	   	  }
		  else{
			  GIPSA_PPN_HOSPAPHB="0";
		  }
	   	  if (finalBean.getUiicBean().getGipsaPpnAmountRecomm()!= null && finalBean.getUiicBean().getGipsaPpnAmountRecomm()!="") {
	   		GIPSA_PPN_TPA= finalBean.getUiicBean().getGipsaPpnAmountRecomm();
	   	  }
	   	 else{
	   		GIPSA_PPN_TPA="0";
		  }
	   	  if (finalBean.getUiicBean().getGipsaPpnReasonsForDeduction()!= null && finalBean.getUiicBean().getGipsaPpnReasonsForDeduction()!="") {
	   		GIPSA_PPN_RESPONDOD	= finalBean.getUiicBean().getGipsaPpnReasonsForDeduction();
	   	  }
	   	  
	  
	         if (finalBean.getUiicBean().getConsultationAmountRecomm()!= null && finalBean.getUiicBean().getConsultationAmountRecomm()!="") {
	        	 conchrhosp  = finalBean.getUiicBean().getConsultationAmountRecomm();
		   	  }
			  else{
				  conchrhosp="0";
			  }
		   	  if (finalBean.getUiicBean().getConsultationAmountRequested()!= null && finalBean.getUiicBean().getConsultationAmountRequested()!="") {
		   		conchrtpa= finalBean.getUiicBean().getConsultationAmountRequested();
		   	  }
		   	 else{
		   		conchrtpa="0";
			  }
		   	  if (finalBean.getUiicBean().getConsultationReasonsForDeduc()!= null && finalBean.getUiicBean().getConsultationReasonsForDeduc()!="") {
		   		conchrded = finalBean.getUiicBean().getConsultationReasonsForDeduc();
		   	  }
		   	
		
}


		//Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
		if (finalBean.getEmailCommonDetaisBean() != null) {
			EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
			CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
		}

		
		
		LoginBean lb = new LoginBean();
		session = request.getSession();
		if (session != null) {

			lb = (LoginBean) session.getAttribute("user");
			//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
			name_of_user_obj_1 = session.getAttribute("userName")
					.toString();
			userid_of_user = lb.getModuleUserId(1);
			moduleUserId = lb.getModuleUserId(moduleId);
		}

	} catch (Exception e) {
		System.out.println("ERROR: " + e);
	} finally {

	}
%>




<h3 align="center">View UIIC Details for Claim</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		<!--   genxls.jsp -->
		 <form name="Form1" id="Form1" action="./GeneratexlsxAction" method="post">
         <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
         <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		 <input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
		 <input type="hidden" name="tpa" id="tpa" value="<%=TPANAME%>">  
		 <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="UIIC">  
		 <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		 <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>"> 
		 <input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		 <input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		 
		<input type="hidden" name="policyId" id="policyId" value="<%=policyId%>">                   
        <input type="hidden" name="cardId" id="cardId" value="<%=cardId%>">   
		<input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>"> 
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr>
			<td width="263">&nbsp;</td>	
            <td width="227"  class="text_heading"><strong>Claim ID </strong></td>
            <td colspan="3"  align="left"  class="normal"><%=ccnUnique%><% if(rauthflg > 0) { %>-<%=rauthflg%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
             <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>">
            <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>"></td>
            </tr>
 			 <tr>
			 	<td >POLICY DETAILS AND <br/>PATIENT IDENTIFICATION DETAILS</td>	
            <td  align="left"  class="normal">&nbsp;</td>
			<td width="168"  align="left"  class="normal">&nbsp;</td>
 			 </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME OF THE INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="insuredname" type="text"  id="insuredname"  size="20" MAXLENGTH="200"   value="<%=INSURED_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NAME OF THE PATIENT</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ptname" type="text"  id="ptname"  size="20" MAXLENGTH="150"   value="<%=PATIENT_NAME%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>AGE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="10" value="<%=AGE%>"></td>
            </tr>	
			
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong> SEX</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="sex" type="text"  id="sex"  size="20" MAXLENGTH="10" value="<%=SEX%>"></td>
            </tr>	
			
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>RELATION WITH INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="relation" type="text"  id="relation"  size="20" MAXLENGTH="50"  value="<%=RELATION_WITH_INSURED%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>BALANCE SUM INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="balsi" type="text"  id="balsi"  size="20" MAXLENGTH="25"  value="<%=BALANCESUMINSURED%>"></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>POLICY NUMBER</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno" type="text"  id="policyno"  size="20" MAXLENGTH="150"  value="<%=POLICY_NO%>"></td>
            </tr>

			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>CLAIM NUMBER</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimid" type="text"  id="claimid"  size="20" MAXLENGTH="15"  value="<%=clmid%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>SUM INSURED</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="si" type="text"  id="si"  size="20" MAXLENGTH="20"  value="<%=SUMINSURED%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>POLICY TYPE/INDIVIDUAL OR GMC</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="poltypegmc" type="text"  id="poltypegmc"  size="20" MAXLENGTH="75"  value="<%=POLICYTYPE%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DATE OF INCEPTION OF POLICY FIRST TAKEN</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="polinceptiondt" type="text"  id="polinceptiondt"  size="20" MAXLENGTH="25"  value="<%=DATE_OF_INCEPTION%>"></td>
            </tr>	
			
			
			
			<tr>
			<td > SPECIFIC EXCLUSIONS IN THE POLICY</td>	
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
			 <tr>
			   <td>&nbsp;</td>
			   <td  class="text_heading"><strong>Policy Description</strong> </td>
			   <td colspan="3"  align="left"  class="normal"><textarea name="poldesc" id="poldesc"  rows="10" cols="40"><%=POLICY_DESC%></textarea></td>
	     </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>HOSPITAL NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="150"   value="<%=HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="50"   value="<%=CITY%>"></td>
            </tr>
			
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>STATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="state" type="text"  id="state"  size="20" MAXLENGTH="50"   value="<%=STATE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PPN/NETWORK HOSPITALS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ppnntwrkhosp" type="text"  id="ppnntwrkhosp"  size="20" MAXLENGTH="50"   value="<%=PpnNetworkHospitals%>"></td>
            </tr>
			
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CATEGORY OF HOSPITAL /TERTIARY OR TERT PLUS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="cathosptertplus" type="text"  id="cathosptertplus"  size="20" MAXLENGTH="50"   value=""></td>
            </tr>
			
			
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" value="<%=DIAGNOSIS%>"></td>
            </tr>	
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>MANAGEMENT SURGICAL OR MEDICAL/PLANNED OR EMERGENCY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="mgmtsrplemr" type="text"  id="mgmtsrplemr"  size="20" MAXLENGTH="10"  value="<%=ManagementOrMedicalOrEmergency%>""></td>
            </tr>
			
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DATE OF ADMISSION</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="doa" type="text"  id="doa"  size="20" MAXLENGTH="10"  value="<%=DATE_OF_ADMISSION%>"></td>
            </tr>
				
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>EXPECTED DURATION OF STAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="totaldurofstay" type="text"  id="totaldurofstay"  size="20" MAXLENGTH="10"  value="<%=EXPECTED_DURATION_OF_STAY%>"></td>
            </tr>

       
			   
			  
			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE/ SINGLE,SEMIPRIVATE, GENERAL ETC</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="150"  value="<%=RoomType%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT/ ICU RENT PER DAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrenticyprday" type="text"  id="roomrenticyprday"  size="20" MAXLENGTH="150"  value="<%=RoomRentIcuRent%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT ELIGIBILITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrentelg" type="text"  id="roomrentelg"  size="20" MAXLENGTH="50"  value="<%= RoomRentEligibility%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>NO OF DAYS STAYED IN ROOM AND IN ICU</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noofdaysinicuroom" type="text"  id="noofdaysinicuroom"  value="<%=NoOfDaysStayed %>"  size="20" MAXLENGTH="200"></td>
            
			  
			</tr> 
			
			 <tr>
			 	<td>BREAK DOWN /WORKING SHEET</td>	
            <td  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;</td>
            </tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>COMPONENTS</td>
			<td>AMOUNT REQUESTED BY HOSPITAL</td>
			<td width="130">AMOUNT RECOMMENDED BY TPA</td>
			<td width="138">REASONS FOR DEDUCTION</td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>GIPSA PPN PACKAGE</td>
			<td><input name="gipsappnhosp" type="text"  id="01"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_HOSPAPHB%>"></td>
			<td width="130"><input name="gipsappntpa" type="text"  id="11"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_TPA%>"></td>
			<td width="138"><input name="gipsappnreson" type="text"  id="gipsappnreson"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_RESPONDOD%>"></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>ROOM RENT/ICU RENT</td>
			<td><input name="roomrenthosp" type="text"  id="02"  size="20" MAXLENGTH="50"  value="<%=ROOM_RENT_REQUESTED%>"></td>
			<td width="130"><input name="roomrenttpa" type="text"  id="12"  size="20" MAXLENGTH="50"  value="<%=ROOM_RENT_PAID%>"></td>
			<td width="138"><input name="roomrentded" type="text"  id="roomrentded"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>PROCEDURE(PROFESSIONAL) CHARGES</td>
			<td><input name="profchghosp" type="text"  id="03"  size="20" MAXLENGTH="50"  value="<%=PROFESSIONAL_CHARGES_REQ%>"></td>
			<td width="130"><input name="profchgtpa" type="text"  id="13"  size="20" MAXLENGTH="50"  value="<%=PROFESSIONAL_CHARGES_PAID%>"></td>
			<td width="138"><input name="profchgded" type="text"  id="profchgded"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>PHARMACY</td>
			<td><input name="phrhosp" type="text"  id="04"  size="20" MAXLENGTH="50"  value="<%=PHARMACY_CHARGES_REQ%>"></td>
			<td width="130"><input name="phrtpa" type="text"  id="14"  size="20" MAXLENGTH="50"  value="<%=PHARMACY_CHARGES_PAID%>"></td>
			<td width="138"><input name="phrded" type="text"  id="phrded"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>OT + ANESTHESIA CHARGES</td>
			<td><input name="othosp" type="text"  id="05"  size="20" MAXLENGTH="50"  value="<%=OT_CHARGES_REQ%>"></td>
			<td width="130"><input name="ottpa" type="text"  id="15"  size="20" MAXLENGTH="50"  value="<%=OT_CHARGES_PAID%>"></td>
			<td width="138"><input name="otdedu" type="text"  id="otdedu"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>LAB INVESTIGATIONS</td>
			<td><input name="labinvhosp" type="text"  id="06"  size="20" MAXLENGTH="50"  value="<%=LAB_INVES_CHARGES_REQ%>"></td>
			<td width="130"><input name="labinvtpa" type="text"  id="16"  size="20" MAXLENGTH="50"  value="<%=LAB_INVES_CHARGES_PAID%>"></td>
			<td width="138"><input name="labinvded" type="text"  id="labinvded"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>CONSULTATION CHARGES</td>
			<td><input name="conchrhosp" type="text"  id="07"  size="20" MAXLENGTH="50"  value="<%=conchrhosp%>"></td>
			<td width="130"><input name="conchrtpa" type="text"  id="17"  size="20" MAXLENGTH="50"  value="<%=conchrtpa%>"></td>
			<td width="138"><input name="conchrded" type="text"  id="conchrded"  size="20" MAXLENGTH="50"  value="<%=conchrded%>"></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>OTHERS</td>
			<td><input name="othershosp" type="text"  id="08"  size="20" MAXLENGTH="50"  value="<%=OTHER_CHARGES_REQ%>"></td>
			<td width="130"><input name="otherstpa" type="text"  id="18"  size="20" MAXLENGTH="50"  value="<%=OTHER_CHARGES_PAID%>"></td>
			<td width="138"><input name="othersdedu" type="text"  id="othersdedu"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td>TOTAL</td>
			<td><input name="totalhosp" type="text"  id="totalhosp"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="130"><input name="totaltpa" type="text"  id="totaltpa"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="138"><input name="totaldedu" type="text"  id="totaldedu"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			
			<tr>
			<td>DECLARATIONS</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">DISEASE AND TREATMENT ARE COVERED UNDER POLICY TERMS AND CONDITIONS</td>
			<td><select name="disessel" id="disessel">
			<option value="YES">YES</option>
			<option value="NO">NO</option>
			</select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">EXPENSES PAID ARE AS PER REASONABLE AND CUSTOMARY CLAUSE</td>
			<td><select name="expnsel" id="expnsel">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">ALL HEADERS IN BREAKDOWN ARE PAID AS PER POLICY TERMS AND CONDITIONS AND ANY DEVIATION WILL BE LIABLE FOR RECOVERY</td>
			<td><select name="headerssel" id="headerssel">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>
			<td colspan="3">IF THE COST OF DIAGNOSTICS EXCEED 5000 RS A SEPARATE SHEET DISPLAYING THE BREAKDOWN OF INVESTIGATIONS NEED TO BE ATTACHED</td>
			<td><select name="diagcostsel" id="diagcostsel">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
		
		<tr>
		
		</tr>
		
				       
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit" ></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>

</form>  
		   
 		 
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>
