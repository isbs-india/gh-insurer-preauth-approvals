<%@page import="com.ghpl.model.iauth.IAuthBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>

<h3 align="center">INSURED INFORMATION</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		   <c:if test="${fn:length(empList) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   	    <tr>
		   	    	<th>S.No</th><th>Card ID<br/>(EmpID)</th><th>Insured Name<br/>(SumInsured)<br/>(Bano)</th><th>PolicyNo<br/>(Policy HolderName)</th><th>RelationShip<br/>(Gender)</th><th>Policy From<br/>(Policy To)</th><th>DOJ<br/>(DOL)</th>
		   	    	<th>Generate Claim</th>
		   	    </tr>
		   <c:forEach items="${empList}" var="emp"> 
		   <tr>
		   		<td><c:out value="${emp.serialNo}"></c:out></td>
		   		 <td><c:out value="${emp.cardid}"></c:out><br/>(<c:out value="${emp.empid}"></c:out>)</td>
		   		<td><c:out value="${emp.cardHolderName}"></c:out><br/>(<c:out value="${emp.sumInsured}"></c:out>)<br/>(<c:out value="${emp.bano}"></c:out>)</td>
		   		<td><c:out value="${emp.policyNo}"></c:out><br/>(<c:out value="${emp.policyHolderName}"></c:out>)</td>
		   		 <td><c:out value="${emp.relation}"></c:out><br/>(<c:out value="${emp.gender}"></c:out>)</td>
		   		 <td><c:out value="${emp.policyFrom}"></c:out><br/>(<c:out value="${emp.policyTo}"></c:out>)</td>
		   		 <td><c:out value="${emp.doj}"></c:out><br/>(<c:out value="${emp.dol}"></c:out>)</td>
		   		<td>
		   		<form name="form1" method="post" action="./ProcessSheetListAction" target="_blank"> 
		   		<input type="hidden" name="ccnUnique" value="<c:out value="${emp.ccnUnique}"/>"/>
		   		<input type="hidden" name="claimId" value="<c:out value="${emp.claimId}"/>"/>
		   		<input type="hidden" name="moduleId" value="<c:out value="${emp.moduleId }"/>"/>
		   		<input type="hidden" name="finYear" value="<c:out value="${emp.finYear }"/>"/>
		   		<input type="submit" value="Process Sheet" />
		   		</form>
			   			 	
			   		
		   		<%-- <c:if test="${emp.status eq 'T' || emp.status eq 'T1' || emp.status eq '161' || emp.status eq '162'}"> --%>
		   		<c:if test="${emp.status eq 'I' || emp.status eq 'I1' || emp.status eq '161' || emp.status eq '162' || emp.status eq 'T' || emp.status eq 'T1'}">
			   		<form name="forminsurer" method="post" action="preauthdocscollectioninsurer.jsp"> 
			   		<input type="hidden" name="finYear" value="<c:out value="${emp.finYear }"/>"/>
			   		<input type="hidden" name="claimId" value="<c:out value="${emp.claimId}"/>"/>
			   			<input type="hidden" name="ccnUnique" value="<c:out value="${emp.ccnUnique}"/>"/>
		   		<input type="hidden" name="policyId" value="<c:out value="${emp.policyId}"/>"/>
		   			<input type="hidden" name="moduleId" value="<c:out value="${emp.moduleId }"/>"/>
		   		<input type="hidden" name="cardid" value="<c:out value="${emp.cardid}"/>"/>
		   		<input type="hidden" name="policyHolderName" value="<c:out value="${emp.policyHolderName}"/>"/>
		   		<input type="hidden" name="selfName" value="<c:out value="${emp.selfName}"/>"/>
		   		<input type="hidden" name="policyNo" value="<c:out value="${emp.policyNo}"/>"/>
		   		<input type="hidden" name="insuredId" value="<c:out value="${emp.insuredId}"/>"/>
		   		<input type="hidden" name="policyFrom" value="<c:out value="${emp.policyFrom}"/>"/>
		   		<input type="hidden" name="policyTo" value="<c:out value="${emp.policyTo}"/>"/>
		   		<input type="hidden" name="cardHolderName" value="<c:out value="${emp.cardHolderName}"/>"/>
		   		<input type="hidden" name="renewalStatus" value="<c:out value="${emp.renewalStatus}"/>"/>
		   		<input type="hidden" name="sumInsured" value="<c:out value="${emp.sumInsured}"/>"/>
		   		<input type="hidden" name="bano" value="<c:out value="${emp.bano}"/>"/>
		   		<input type="hidden" name="doj" value="<c:out value="${emp.doj}"/>"/>
		   		<input type="hidden" name="dol" value="<c:out value="${emp.dol}"/>"/>
		   		<input type="hidden" name="inceptionDate" value="<c:out value="${emp.inceptionDate}"/>"/>
		   		<input type="hidden" name="uwCode" value="<c:out value="${emp.uwCode}"/>"/>
		   		<input type="hidden" name="uwId" value="<c:out value="${emp.uwId}"/>"/>
		   		<input type="hidden" name="rid" value="<c:out value="${emp.rid}"/>"/>
		   		<input type="hidden" name="issusingofficeid" value="<c:out value="${emp.issusingofficeid}"/>"/>
		   		<input type="hidden" name="selfcardid" value="<c:out value="${emp.selfcardid}"/>"/>
		   		<input type="hidden" name="age" value="<c:out value="${emp.age}"/>"/>
		   		<input type="hidden" name="relation" value="<c:out value="${emp.relation}"/>"/>
		   		<input type="hidden" name="gender" value="<c:out value="${emp.gender}"/>"/>
		   		<input type="hidden" name="dob" value="<c:out value="${emp.dob}"/>"/>
		   		<input type="hidden" name="empid" value="<c:out value="${emp.empid}"/>"/>
		   		<input type="hidden" name="reauthFlag" value="<c:out value="${emp.reauthFlag}"/>"/>
		   		<input type="hidden" name="status" value="<c:out value="${emp.status}"/>"/>
		   		<input type="hidden" name="preauthNo" value="<c:out value="${emp.preauthNo}"/>"/>
		   		<input type="hidden" name="patAddress" value="<c:out value="${emp.patAddress}"/>"/>
		   		<input type="hidden" name="patEmail" value="<c:out value="${emp.patEmail}"/>"/>
		   		<input type="hidden" name="patMobile" value="<c:out value="${emp.patMobile}"/>"/>
			   		<input type="submit" value="UPLOAD AUTH INSURER DOCS" />
			   		</form> 
		   		</c:if>
		   		
		   		  <c:if test="${emp.status eq 'U' || emp.status eq 'U1' || emp.status eq '168' || emp.status eq '169'}">
			   		<form name="forminsurerdenial" method="post" action="preauthdocscollectioninsurerdenial.jsp">
			   		<input type="hidden" name="finYear" value="<c:out value="${emp.finYear }"/>"/>
			   		<input type="hidden" name="claimId" value="<c:out value="${emp.claimId}"/>"/>
			   			<input type="hidden" name="ccnUnique" value="<c:out value="${emp.ccnUnique}"/>"/>
			   			<input type="hidden" name="moduleId" value="<c:out value="${emp.moduleId }"/>"/>
		   		<input type="hidden" name="policyId" value="<c:out value="${emp.policyId}"/>"/>
		   		<input type="hidden" name="cardid" value="<c:out value="${emp.cardid}"/>"/>
		   		<input type="hidden" name="policyHolderName" value="<c:out value="${emp.policyHolderName}"/>"/>
		   		<input type="hidden" name="selfName" value="<c:out value="${emp.selfName}"/>"/>
		   		<input type="hidden" name="policyNo" value="<c:out value="${emp.policyNo}"/>"/>
		   		<input type="hidden" name="insuredId" value="<c:out value="${emp.insuredId}"/>"/>
		   		<input type="hidden" name="policyFrom" value="<c:out value="${emp.policyFrom}"/>"/>
		   		<input type="hidden" name="policyTo" value="<c:out value="${emp.policyTo}"/>"/>
		   		<input type="hidden" name="cardHolderName" value="<c:out value="${emp.cardHolderName}"/>"/>
		   		<input type="hidden" name="renewalStatus" value="<c:out value="${emp.renewalStatus}"/>"/>
		   		<input type="hidden" name="sumInsured" value="<c:out value="${emp.sumInsured}"/>"/>
		   		<input type="hidden" name="bano" value="<c:out value="${emp.bano}"/>"/>
		   		<input type="hidden" name="doj" value="<c:out value="${emp.doj}"/>"/>
		   		<input type="hidden" name="dol" value="<c:out value="${emp.dol}"/>"/>
		   		<input type="hidden" name="inceptionDate" value="<c:out value="${emp.inceptionDate}"/>"/>
		   		<input type="hidden" name="uwCode" value="<c:out value="${emp.uwCode}"/>"/>
		   		<input type="hidden" name="uwId" value="<c:out value="${emp.uwId}"/>"/>
		   		<input type="hidden" name="rid" value="<c:out value="${emp.rid}"/>"/>
		   		<input type="hidden" name="issusingofficeid" value="<c:out value="${emp.issusingofficeid}"/>"/>
		   		<input type="hidden" name="selfcardid" value="<c:out value="${emp.selfcardid}"/>"/>
		   		<input type="hidden" name="age" value="<c:out value="${emp.age}"/>"/>
		   		<input type="hidden" name="relation" value="<c:out value="${emp.relation}"/>"/>
		   		<input type="hidden" name="gender" value="<c:out value="${emp.gender}"/>"/>
		   		<input type="hidden" name="dob" value="<c:out value="${emp.dob}"/>"/>
		   		<input type="hidden" name="empid" value="<c:out value="${emp.empid}"/>"/>
		   		<input type="hidden" name="reauthFlag" value="<c:out value="${emp.reauthFlag}"/>"/>
		   		<input type="hidden" name="status" value="<c:out value="${emp.status}"/>"/>
		   		<input type="hidden" name="preauthNo" value="<c:out value="${emp.preauthNo}"/>"/>
		   		<input type="hidden" name="patAddress" value="<c:out value="${emp.patAddress}"/>"/>
		   		<input type="hidden" name="patEmail" value="<c:out value="${emp.patEmail}"/>"/>
		   		<input type="hidden" name="patMobile" value="<c:out value="${emp.patMobile}"/>"/>
			   		<input type="submit" value="UPLOAD AUTH INSURER DENIAL DOCS" />
			   		</form>
		   		</c:if>
		   		
			   			   		
		   		</td>
		   	</tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(empList) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	    <tr>
		   	    	<th>S.No</th><th>Card ID</th><th>Insured Name<br/>(SumInsured)<br/>(Bano)</th><th>PolicyNo<br/>(Policy HolderName)</th><th>RelationShip</th><th>Policy From<br/>(Policy To)</th><th>DOJ<br/>(DOL)</th>
		   	    	<th>Generate Claim</th>
		   	       </tr>
			   	    <tr align="center">
			   			<td colspan="10" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>