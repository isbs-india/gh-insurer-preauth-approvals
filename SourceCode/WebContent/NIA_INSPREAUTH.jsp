<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.NIADataBean"%>
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>

<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<link href="./js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="./js/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript" src="./js/cycle_plugin.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
<script src="./js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-sliderAccess.js"></script>
<script src="js/bank.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>


<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script>
$(document).ready(function(){
	var totalhosBill=0;
	var hosbill=0;
	
	var TPAamount=0;
	var totalTPAamount=0;
	for(var a=1; a<=8; a++){
		 hosbill = document.getElementById("0"+a).value;
		 hosbill = parseInt(hosbill) || 0;
		 totalhosBill=totalhosBill+hosbill
		 
		 TPAamount = document.getElementById("1"+a).value;
		 TPAamount = parseInt(TPAamount) || 0;
		 totalTPAamount=totalTPAamount+TPAamount
		 
	}
	$("#totalhospaphb").val(totalhosBill)
	$("#totaltpa").val(totalTPAamount)
})
function myTrim(x) {
	return x.replace(/^\s+|\s+$/gm, '');
}
function CustomConfirmation(msg, title_msg){
	if ($('#ConfirmMessage').length == 0) {
	        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
	    } else {
	        $('#ConfirmMessage').html(msg);
	    }

	    $("#ConfirmMessage").dialog({
	        autoOpen: false,
			title: title_msg,
	        show: "blind",
	        hide: "explode",  
			resizable: false,      
	        height: 200,
	        width: 300,
	        modal: true,
			buttons: {
	            "Ok": function() 
	            {
	                $( this ).dialog( "close" );
	            }
			}
	    });
	  
	    $( "#ConfirmMessage" ).dialog("open");
	}
var message = "VALIDATION MESSAGE";
function validateForm(){
	
	 var claimno = document.getElementById("claimno").value;
	 
	  var policyno = document.getElementById("policyno").value;
	 
	 var policyperiod = document.getElementById("policyperiod").value;
	 var insured = document.getElementById("insured").value;
	   var patient = document.getElementById("patient").value;
	    
	var copayment = document.getElementById("copayment").value;
	 var preauthtype = document.getElementById("preauthtype").value;
	   var balsuminsured = document.getElementById("balsuminsured").value;
	 var relation = document.getElementById("relation").value;

	 var gender = document.getElementById("gender").value;
	 var age = document.getElementById("age").value;
	 var suminsured = document.getElementById("suminsured").value;
	 var validity = document.getElementById("validity").value;
	 var procedure = document.getElementById("procedure").value;
	 var amount = document.getElementById("amount").value;
	 var hospname = document.getElementById("hospname").value;
	 var city = document.getElementById("city").value;
	 
	   var state = document.getElementById("state").value;
	 var ppn = document.getElementById("ppn").value;
	 var roomrent = document.getElementById("roomrent").value;
	 var roomtype = document.getElementById("roomtype").value;
	 var disease = document.getElementById("disease").value;
	 var diagnosis = document.getElementById("diagnosis").value;
	 var lineoftreatment = document.getElementById("lineoftreatment").value;
	 var procedureincRefrencerate = document.getElementById("procedureincRefrencerate").value;
	 var averagecost = document.getElementById("averagecost").value;
	 
	  var icdcode = document.getElementById("icdcode").value;
	 var applicablePPN = document.getElementById("applicablePPN").value;
	 var packageRate = document.getElementById("packageRate").value;
	 var dateofAdmission = document.getElementById("dateofAdmission").value;
	 var dateofDischarge = document.getElementById("dateofDischarge").value;
	 var los = document.getElementById("los").value;
	  var noofdaysRoom = document.getElementById("noofdaysRoom").value;
	 var noOfdaysICU = document.getElementById("noOfdaysICU").value;
	 
	 var detailsofidproof = document.getElementById("detailsofidproof").value;
	 var ICUperday = document.getElementById("ICUperday").value;
	 var roomrenteligibility = document.getElementById("roomrenteligibility").value;
	 
	 var gipsappnhospaphb = document.getElementById("01").value;
	 var roomrenthospaphb = document.getElementById("02").value;
	 var profchghospaphb = document.getElementById("03").value;
	 var phrhospaphb = document.getElementById("03").value;
	 var othospaphb = document.getElementById("05").value;
	 var labinvhospaphb = document.getElementById("06").value;
	 var conchrhospaphb = document.getElementById("07").value;
	 var othershospaphb = document.getElementById("08").value;
	 var totalhospaphb = document.getElementById("totalhospaphb").value; 
	
	 var gipsappntpa = document.getElementById("11").value;
	 var roomrenttpa = document.getElementById("12").value;
	 var profchgtpa = document.getElementById("13").value;
	 var phrtpa = document.getElementById("14").value;
	 var ottpa = document.getElementById("15").value;
	 var labinvtpa = document.getElementById("16").value;
	 var conchrtpa = document.getElementById("17").value;
	 var otherstpa = document.getElementById("18").value;
	 var totaltpa = document.getElementById("totaltpa").value; 
	
	 var gipsappnresondod = document.getElementById("gipsappnresondod").value;
	 var roomrentdod = document.getElementById("roomrentdod").value;
	 var profchgdod = document.getElementById("profchgdod").value;
	 var phrdod = document.getElementById("phrdod").value;
	 var otdod = document.getElementById("otdod").value;
	 var labinvdod = document.getElementById("labinvdod").value;
	 var conchrdod = document.getElementById("conchrdod").value;
	 var othersdod = document.getElementById("othersdod").value;  
	
	 
	 
	  var specification = document.getElementById("specification").value;
	 var nameandqual = document.getElementById("nameandqual").value;
	 var dateandtime = document.getElementById("dateandtime").value;

	 var policytype = document.getElementById("policytype").value;
	 var dateofinception = document.getElementById("dateofinception").value;  
	
	 
	
	 if(claimno == "")
	 {
		 CustomConfirmation('Please enter valid Claim No',message);
		 return false; 
	 }
	 if(policyno == "")
	 {
		 CustomConfirmation('Please enter valid Policy No',message);
		 return false; 
	 }
	 if(policyperiod == "")
	 {
		 CustomConfirmation('Please enter valid Policy Period',message);
		 return false; 
	 }
	 if(insured == "")
	 {
		 CustomConfirmation('Please enter valid Insured Name',message);
		 return false; 
	 }
	   if(patient == "")
	 {
		 CustomConfirmation('Please enter valid Patient',message);
		 return false; 
	 }
	  
	 if(copayment == "")
	 {
		 CustomConfirmation('Please enter valid Co-Payment',message);
		 return false; 
	 }
	 if(preauthtype == "")
	 {
		 CustomConfirmation('Please enter valid Preauth Type',message);
		 return false; 
	 }
	  if(balsuminsured == "")
	 {
		 CustomConfirmation('Please enter valid Bal Suminsured',message);
		 return false; 
	 }
	 if(relation == "")
	 {
		 CustomConfirmation('Please enter valid Relation',message);
		 return false; 
	 }
	  if(gender == "")
	 {
		 CustomConfirmation('Please enter valid Gender',message);
		 return false; 
	 }
	 
	 if(age == "")
	 {
		 CustomConfirmation('Please enter valid Age',message);
		 return false; 
	 }
	 if(suminsured == "")
	 {
		 CustomConfirmation('Please enter valid Suminsured',message);
		 return false; 
	 }
	 if(validity == "")
	 {
		 CustomConfirmation('Please enter valid Validity',message);
		 return false; 
	 }
	 if(procedure == "")
	 {
		 CustomConfirmation('Please enter valid Procedure',message);
		 return false; 
	 }
	 if(amount == "")
	 {
		 CustomConfirmation('Please enter valid Amount',message);
		 return false; 
	 }
	 if(hospname == "")
	 {
		 CustomConfirmation('Please enter valid Hospital Name',message);
		 return false; 
	 }
	 if(city == "")
	 {
		 CustomConfirmation('Please enter valid City',message);
		 return false; 
	 }
	  if(state == "")
	 {
		 CustomConfirmation('Please enter valid State',message);
		 return false; 
	 }
	 if(ppn == "")
	 {
		 CustomConfirmation('Please enter valid PPN  & Category',message);
		 return false; 
	 }
	 if(roomrent == "")
	 {
		 CustomConfirmation('Please enter valid Room Rent',message);
		 return false; 
	 }
	 if(roomtype == "")
	 {
		 CustomConfirmation('Please enter valid Roomtype',message);
		 return false; 
	 }
	 if(disease == "")
	 {
		 CustomConfirmation('Please enter valid Disease',message);
		 return false; 
	 }
	 
	 if(diagnosis == "")
	 {
		 CustomConfirmation('Please enter valid Diagnosis',message);
		 return false; 
	 }
	  if(lineoftreatment == "")
	 {
		 CustomConfirmation('Please enter valid Line Of Treatment',message);
		 return false; 
	 }
	 if(procedureincRefrencerate == "")
	 {
		 CustomConfirmation('Please enter valid Procedure inc Refrence rate',message);
		 return false; 
	 }
	 if(averagecost == "")
	 {
		 CustomConfirmation('Please enter valid Average Cost',message);
		 return false; 
	 }
	  if(icdcode == "")
	 {
		 CustomConfirmation('Please enter valid Icd Code',message);
		 return false; 
	 }
	 if(applicablePPN == "")
	 {
		 CustomConfirmation('Please enter valid Applicable PPN',message);
		 return false; 
	 }
	 if(packageRate == "")
	 {
		 CustomConfirmation('Please enter valid Package Rate',message);
		 return false; 
	 }
	 if(dateofAdmission == "")
	 {
		 CustomConfirmation('Please enter valid Date Of Admission',message);
		 return false; 
	 }
	 
	 if(dateofDischarge == "")
	 {
		 CustomConfirmation('Please enter valid Date Of Discharge',message);
		 return false; 
	 }
	 if(los == "")
	 {
		 CustomConfirmation('Please enter valid Los',message);
		 return false; 
	 }
	 	 if(noofdaysRoom == "")
	 {
		 CustomConfirmation('Please enter valid No of days Room',message);
		 return false; 
	 }
	 if(noOfdaysICU == "")
	 {
		 CustomConfirmation('Please enter valid No Of days ICU',message);
		 return false; 
	 }
	 
	 if(detailsofidproof == "")
	 {
		 CustomConfirmation('Please enter valid Details of id proof',message);
		 return false; 
	 }
	 if(ICUperday == "")
	 {
		 CustomConfirmation('Please enter valid ROOM RENT/ ICU RENT PER DAY',message);
		 return false; 
	 }
	 
	 if(roomrenteligibility == "")
	 {
		 CustomConfirmation('Please enter valid Room rent eligibility',message);
		 return false; 
	 }
	 
	  if(gipsappnhospaphb == "")
	 {
		 CustomConfirmation('Please enter valid GIPSA PPN PACKAGE Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(roomrenthospaphb == "")
	 {
		 CustomConfirmation('Please enter valid ROOM RENT/ICU RENT Amount As per Hospital bill ',message);
		 return false; 
	 }
	 if(profchghospaphb == "")
	 {
		 CustomConfirmation('Please enter valid PROCEDURE(PROFESSIONAL) CHARGES Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(phrhospaphb == "")
	 {
		 CustomConfirmation('Please enter valid PHARMACY Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(othospaphb == "")
	 {
		 CustomConfirmation('Please enter valid OT + ANESTHESIA CHARGES Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(labinvhospaphb == "")
	 {
		 CustomConfirmation('Please enter valid LAB INVESTIGATIONS Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(conchrhospaphb == "")
	 {
		 CustomConfirmation('Please enter valid  CONSULTATION CHARGES Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(othershospaphb == "")
	 {
		 CustomConfirmation('Please enter valid OTHERS Amount As per Hospital bill',message);
		 return false; 
	 }
	 if(totalhospaphb == "")
	 {
		 CustomConfirmation('Please enter valid TOTAL Amount As per Hospital bill',message);
		 return false; 
	 }
	 
	 if(gipsappntpa == "")
	 {
		 CustomConfirmation('Please enter valid GIPSA PPN PACKAGE Amount recommended by TPA',message);
		 return false; 
	 }
	 
	 if(roomrenttpa == "")
	 {
		 CustomConfirmation('Please enter valid ROOM RENT/ICU RENT Amount recommended by TPA',message);
		 return false; 
	 }
	 if(profchgtpa == "")
	 {
		 CustomConfirmation('Please enter valid PROCEDURE(PROFESSIONAL) CHARGES Amount recommended by TPA',message);
		 return false; 
	 }
	 if(phrtpa == "")
	 {
		 CustomConfirmation('Please enter valid PHARMACY Amount recommended by TPA',message);
		 return false; 
	 }
	 if(ottpa == "")
	 {
		 CustomConfirmation('Please enter valid OT + ANESTHESIA CHARGES Amount recommended by TPA',message);
		 return false; 
	 }
	 if(labinvtpa == "")
	 {
		 CustomConfirmation('Please enter valid LAB INVESTIGATIONS Amount recommended by TPA ',message);
		 return false; 
	 }
	 if(conchrtpa == "")
	 {
		 CustomConfirmation('Please enter valid CONSULTATION CHARGES Amount recommended by TPA',message);
		 return false; 
	 }
	 if(otherstpa == "")
	 {
		 CustomConfirmation('Please enter valid OTHERS Amount recommended by TPA',message);
		 return false; 
	 }
	 if(totaltpa == "")
	 {
		 CustomConfirmation('Please enter valid TOTAL Amount recommended by TPA',message);
		 return false; 
	 }
	 
	
	 if(gipsappnresondod == "")
	 {
		 CustomConfirmation('Please enter valid GIPSA PPN PACKAGE Details of deduction',message);
		 return false; 
	 }
	 if(roomrentdod == "")
	 {
		 CustomConfirmation('Please enter valid ROOM RENT/ICU RENT Details of deduction',message);
		 return false; 
	 }
	 
	 if(profchgdod == "")
	 {
		 CustomConfirmation('Please enter valid PROCEDURE(PROFESSIONAL) CHARGES Details of deduction',message);
		 return false; 
	 }
	 
	 if(phrdod == "")
	 {
		 CustomConfirmation('Please enter valid PHARMACY Details of deduction',message);
		 return false; 
	 }
	 if(otdod == "")
	 {
		 CustomConfirmation('Please enter valid OT + ANESTHESIA CHARGES Details of deduction ',message);
		 return false; 
	 }
	 if(labinvdod == "")
	 {
		 CustomConfirmation('Please enter valid LAB INVESTIGATIONS Details of deduction',message);
		 return false; 
	 }
	 if(conchrdod == "")
	 {
		 CustomConfirmation('Please enter valid CONSULTATION CHARGES Details of deduction ',message);
		 return false; 
	 }
	 if(othersdod == "")
	 {
		 CustomConfirmation('Please enter valid OTHERS Details of deduction',message);
		 return false; 
	 }
	
	 
	  if(specification == "")
	 {
		 CustomConfirmation('Please enter valid Specification of Implants Used',message);
		 return false; 
	 }
	 if(nameandqual == "")
	 {
		 CustomConfirmation('Please enter valid Name & Qualification of Doctor recommending the claim ',message);
		 return false; 
	 } 
	 if(dateandtime == "")
	 {
		 CustomConfirmation('Please enter valid Date & time of sending format ',message);
		 return false; 
	 } 
	 
	  if(policytype == "")
	 {
		 CustomConfirmation('Please enter valid Policy Type',message);
		 return false; 
	 }
	 if(dateofinception == "")
	 {
		 CustomConfirmation('Please enter valid Date of inception',message);
		 return false; 
	 }  
	  
	 
}

</script>

<script type="text/javascript" src="js/docSearch.js"></script>
<%
		
	int moduleId=0; 
        String CLAIM_ID = "";
        String PROCESSING_DOCTOR = "";	
        String TPANAME = "";	
        String EMAILID = "";	
        String CC_EMAILID = "";	
        String POLICY_DESCRIPTION = "";	
        String CODE = "";	
        String UWID = "";	
        String ISSUINGOFFICE_PKEY = "";
		String INSURED_NAME = "";
		String PATIENT_NAME = "";
		String AGE = "";
		String SEX  = "";
		String RELATION_WITH_INSURED = "";
		long BALANCESUMINSURED  = 0;
		String POLICY_NO  = "";
		long SUMINSURED = 0;
		String POLICYTYPE  = "";
		String POLICY_PERIOD  = "";
		String DATE_OF_INCEPTION = "";
		String HOSPITAL_NAME  = "";
		String CITY = "";
		String STATE  = "";
		String NETWORKS  = "";
		String DIAGNOSIS  = "";
		String DATE_OF_ADMISSION  = "";
		String DATE_OF_DISCHARGE  = "";
		String EXPECTED_DURATION_OF_STAY = "";
		String ROOM_RENT_REQUESTED  = "";
		String ROOM_RENT_PAID  = "";
		String ROOM_RENT_DEDUCTED  = "";
		String PROFESSIONAL_CHARGES_REQ  = "";
		String PROFESSIONAL_CHARGES_PAID = "";
		String PROFESSIONAL_CHARGES_DEDUCTED = "";
		String PHARMACY_CHARGES_REQ  = "";
		String PHARMACY_CHARGES_PAID  = "";
		String PHARMACY_CHARGES_DEDUCTED  = "";
		String OT_CHARGES_REQ  = "";
		String OT_CHARGES_PAID  = "";
		String OT_CHARGES_DEDUCTED  = "";
		String LAB_INVES_CHARGES_REQ  = "";
		String LAB_INVES_CHARGES_PAID  = "";
		String LAB_INVES_CHARGES_DEDUCTED  = "";
		String OTHER_CHARGES_REQ  = "";
		String OTHER_CHARGES_PAID = "";
		String OTHER_CHARGES_DEDUCTED = "";
		
    	String CCEMAILID = "";
        String POLICY_DESC="";
        
        String DISEASE  = "";
        String PROCEDURECODE  = "";
        String AMOUNT  = "";
        String ICD_CODE  = "";
        String ROOM_TYPE  = "";
        
        String PRE_AUTH_TYPE  = "";
        String ROOM_RENT_ELIGIBILITY  = "";
        String DATE_AND_TIME  = "";
        String LETTER_REMARKS  = "";
    	 String ccnUnique="";
    	 String clmid="0";
    	 int rauthflg=0;
    	 int preauthAppNo=0;
    	 
    	 String policyId="";
      	 String cardId="";
      	 String insuredId="";
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 
    	 
      String Copayment="";
   	  String LineOfTreatment="";
   	  String ProcedureIncRefrenceRate="";
   	  String AverageCost="";
   	  String DetailsOfIDproof="";
   	  String RoomrentForSingleRoom="";
   	  String ApplicablePPN="";
   	  String PackageRate="";
   	  String NoOfDaysRoom="";
   	  String NoOfdaysICU="";
   	  String ROOM_RENT_OR_ICU_RENT_PER_DAY="";
   	  String GIPSA_PPN_HOSPAPHB="";
   	  String GIPSA_PPN_RESPONDOD="";
   	  String GIPSA_PPN_TPA="";
   	  String CONSULTATION_CHARGES="";
   	  String TOTAL=""; 
   	  String SpecificationOfImplantsUsed="";
   	  
    	 
    	
    	  
    	 GetInsurerPrefillDataListBean finalBean = null;
 
 try{  
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	if(finalBean.getNIABean() != null)
	{
			INSURED_NAME = finalBean.getNIABean().getInsured(); 
			PATIENT_NAME = finalBean.getNIABean().getPatient(); 
			AGE = finalBean.getNIABean().getAge(); 
			SEX = finalBean.getNIABean().getSex(); 
			RELATION_WITH_INSURED = finalBean.getNIABean().getRelation();
			POLICY_NO = finalBean.getNIABean().getPolicyNo(); 
			POLICYTYPE = finalBean.getNIABean().getPolicyType(); 
			POLICY_PERIOD = finalBean.getNIABean().getPolicyPeriod();
			DATE_OF_INCEPTION = finalBean.getNIABean().getDateOfInception(); 
			HOSPITAL_NAME = finalBean.getNIABean().getHospName(); 
			 if(finalBean.getNIABean().getCity() != null){
			CITY = finalBean.getNIABean().getCity(); 
			        }
			STATE = finalBean.getNIABean().getState(); 
			NETWORKS = finalBean.getNIABean().getNetworks(); 
			DIAGNOSIS = finalBean.getNIABean().getDiagnosis(); 
			DATE_OF_ADMISSION = finalBean.getNIABean().getDateofAdmission(); 
			DATE_OF_DISCHARGE = finalBean.getNIABean().getDateofDischarge();
			EXPECTED_DURATION_OF_STAY = finalBean.getNIABean().getExpectedDurationOfStay();
			BALANCESUMINSURED = finalBean.getNIABean().getBalSuminsured();
			SUMINSURED = finalBean.getNIABean().getSumInsured();
			
			ROOM_RENT_REQUESTED  = finalBean.getNIABean().getRoomRentRequest();
			ROOM_RENT_PAID  = finalBean.getNIABean().getRoomRentPaid();
			ROOM_RENT_DEDUCTED  = finalBean.getNIABean().getRoomRentDeducted();
		    PROFESSIONAL_CHARGES_REQ  = finalBean.getNIABean().getProfChgHospRequest();
		    PROFESSIONAL_CHARGES_PAID = finalBean.getNIABean().getProfChgPaid();
		    PROFESSIONAL_CHARGES_DEDUCTED = finalBean.getNIABean().getProfChgDeducted();
     	    PHARMACY_CHARGES_REQ  = finalBean.getNIABean().getPhrHospRequest();
		    PHARMACY_CHARGES_PAID  = finalBean.getNIABean().getPhrPaid();
		    PHARMACY_CHARGES_DEDUCTED  = finalBean.getNIABean().getPhrDeducted();
		    OT_CHARGES_REQ  = finalBean.getNIABean().getOtHospRequest();
		    OT_CHARGES_PAID  = finalBean.getNIABean().getOtPaid();
		    OT_CHARGES_DEDUCTED  = finalBean.getNIABean().getOtDeducted();
		    LAB_INVES_CHARGES_REQ  = finalBean.getNIABean().getLabInvHospRequest();
		    LAB_INVES_CHARGES_PAID  = finalBean.getNIABean().getLabInvPaid();
		    LAB_INVES_CHARGES_DEDUCTED  = finalBean.getNIABean().getLabInvDeducted();
		    OTHER_CHARGES_REQ  = finalBean.getNIABean().getOthersHospRequest();
		    OTHER_CHARGES_PAID = finalBean.getNIABean().getOthersPaid();
		    OTHER_CHARGES_DEDUCTED = finalBean.getNIABean().getOthersDeducted();
			
			//EMAILID=finalBean.getNIABean().getEmailId();
			moduleId=finalBean.getNIABean().getModuleId();
			clmid=finalBean.getNIABean().getClaimId();
			ccnUnique = finalBean.getCcnUnique();
			rauthflg=finalBean.getNIABean().getReauthFlag();
			//preauthAppNo=finalBean.getNIABean().getPreauthAppNo();
			DISEASE = finalBean.getNIABean().getDisease();
			PROCEDURECODE  = finalBean.getNIABean().getProcedure();
	        if(finalBean.getNIABean().getAmount() != null){
	        AMOUNT  = finalBean.getNIABean().getAmount();
	        }
	        ICD_CODE  = finalBean.getNIABean().getIcdCode();
	        ROOM_TYPE  = finalBean.getNIABean().getRoomType();
	        PROCESSING_DOCTOR  = finalBean.getNIABean().getProcessingDoctor();
	        POLICY_DESCRIPTION = finalBean.getNIABean().getPolicyDiscription();
	        
	        PRE_AUTH_TYPE  = finalBean.getNIABean().getPreauthType();
	        ROOM_RENT_ELIGIBILITY  = finalBean.getNIABean().getRoomRentEligibility();
	        DATE_AND_TIME  = finalBean.getNIABean().getDateAndTime();
	        LETTER_REMARKS  = finalBean.getNIABean().getLetterRemarks();
	        
	        policyId = finalBean.getNIABean().getPolicyID();
	     	cardId = finalBean.getNIABean().getCardId();
	     	insuredId = finalBean.getNIABean().getInsuredId();
	     	
	     	
	     	
	     	 if (finalBean.getNIABean().getCopayment()!= null) { 
	    	  Copayment= finalBean.getNIABean().getCopayment();
	     	 }
	     	 if (finalBean.getNIABean().getLineOfTreatment()!= null) { 
	    	  LineOfTreatment= finalBean.getNIABean().getLineOfTreatment();
	     	 } 
	    	  if (finalBean.getNIABean().getProcedureincRefrencerate()!= null) {
	    	  ProcedureIncRefrenceRate= finalBean.getNIABean().getProcedureincRefrencerate();
	    	  }
	    	  if (finalBean.getNIABean().getAverageCost()!= null) {
	    	  AverageCost= finalBean.getNIABean().getAverageCost();
	    	  }
	    	  if (finalBean.getNIABean().getDetailsOfIdProof()!= null) {
	    	  DetailsOfIDproof= finalBean.getNIABean().getDetailsOfIdProof();
	    	  }
	    	  if (finalBean.getNIABean().getRoomRent()!= null) {
	    	  RoomrentForSingleRoom= finalBean.getNIABean().getRoomRent();
	    	  }	    	 
	    	  if (finalBean.getNIABean().getApplicablePPN()!= null) {
	    	  ApplicablePPN= finalBean.getNIABean().getApplicablePPN();
	    	  }
	    	  if (finalBean.getNIABean().getPackageRate()!= null) {
	    	  PackageRate= finalBean.getNIABean().getPackageRate();
	    	  }
	    	  if (finalBean.getNIABean().getNoOfDaysRoom()!= null) {
	    	  NoOfDaysRoom= finalBean.getNIABean().getNoOfDaysRoom();  
	    	  }
	    	  if (finalBean.getNIABean().getNoOfDaysICU()!= null) {
	    	  NoOfdaysICU= finalBean.getNIABean().getNoOfDaysICU();
	    	  }
	    	  if (finalBean.getNIABean().getICUPerDay()!= null) {
	    	  ROOM_RENT_OR_ICU_RENT_PER_DAY= finalBean.getNIABean().getICUPerDay();  
	    	  }
	    	  if (finalBean.getNIABean().getGipsAppnHospAphb()!= null) {
	    	  GIPSA_PPN_HOSPAPHB= finalBean.getNIABean().getGipsAppnHospAphb();
	    	  }
	    	  if (finalBean.getNIABean().getGipsAppnResonDod()!= null) {
	    	  GIPSA_PPN_RESPONDOD= finalBean.getNIABean().getGipsAppnResonDod();
	    	  }
	    	  if (finalBean.getNIABean().getGipsAppnTpa()!= null) {
	    	  GIPSA_PPN_TPA= finalBean.getNIABean().getGipsAppnTpa();
	    	  }
	    	  if (finalBean.getNIABean().getSpecification()!= null) {
	    	  SpecificationOfImplantsUsed= finalBean.getNIABean().getSpecification();  
	    	  }
	}
	        //Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
			if (finalBean.getEmailCommonDetaisBean() != null) {
				EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
				CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
			}
	LoginBean lb=new LoginBean();
	session= request.getSession();
	if(session != null) {
		
		lb=(LoginBean)session.getAttribute("user");
		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
		 name_of_user_obj_1 =session.getAttribute("userName").toString();
	     userid_of_user = lb.getModuleUserId(1);
	     moduleUserId = lb.getModuleUserId(moduleId);
	}
}
catch(Exception e)
{
	System.out.println("ERROR IN LIST NIA: "+e);
}
finally
{
	//finalBean=null;
} 
%>
<h3 align="center">View NIA Details for Claim</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
   <br/><br/>
		 <!-- <form name="Form1" id="Form1" action="genxls.jsp" method="post"> -->
		  <form name="Form1" id="Form1" action="./GeneratexlsxAction" method="post" onsubmit="return validateForm();" >
         <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
         <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		 <input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
		 <input type="hidden" name="tpa" id="tpa" value="<%=TPANAME%>">  
		 <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="NA">  
		 <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		 <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>"> 
		 <input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		 <input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
		 <input type="hidden" name=preauthAppNo id="preauthAppNo" value="<%=preauthAppNo%>"> 
		 
		<input type="hidden" name="policyId" id="policyId" value="<%=policyId%>">                   
        <input type="hidden" name="cardId" id="cardId" value="<%=cardId%>">   
		<input type="hidden" name="insuredId" id="insuredId" value="<%=insuredId%>">
		 
      <br/>
       <div align="center"> 
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
	   <tr>
	   <td>
	   FORMAT FOR CASHLESS APPROVAL Rs. 1 LAKH AND ABOVE
	   </td>
	   </tr>
 		<tr>
			<td width="263">&nbsp;</td>	
            <td width="227"  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;
             <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>">
            <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
            <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>">
            <input type="hidden" name="claimno" id="claimno" value="<%=clmid%>">
            </td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Claim No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ccnUnique" type="text"  id="ccnUnique"  size="20" MAXLENGTH="200"   value="<%= ccnUnique%>" readonly="readonly"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno" type="text"  id="policyno"  size="20" MAXLENGTH="150"   value="<%= POLICY_NO%>"></td>
            </tr>
             <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy period</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyperiod" type="text"  id="policyperiod"  size="20" MAXLENGTH="150"   value="<%= POLICY_PERIOD%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Insured Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="insured" type="text"  id="insured"  size="20" MAXLENGTH="10" value="<%= INSURED_NAME%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong> Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patient" type="text"  id="patient"  size="20" MAXLENGTH="10" value="<%= PATIENT_NAME%>"></td>
            </tr>	
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy Type</strong></td>
           <td colspan="3"  align="left"  class="normal"><input name="policytype" type="text"  id="policytype"  size="20" MAXLENGTH="50"  value="<%= POLICYTYPE%>"></td>
            </tr>
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>In case of GMC , deviations from Standard Policy</strong></td>
            <td colspan="3"><textarea name="incase" id="" rows="5" cols="72" readonly="readonly"><%= POLICY_DESCRIPTION%></textarea>
           <!--  <td colspan="3"  align="left"  class="normal"><input name="incase" type="text"  id="incase"  size="20" MAXLENGTH="25"  value=""></td> -->
            </tr>
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Copayment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="copayment" type="text"  id="copayment"  size="20" MAXLENGTH="150"  value="<%=Copayment%>"></td>
            </tr>
			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>Preauth Type :Initial /Enhencement/Final</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="preauthtype" type="text"  id="preauthtype"  size="20" MAXLENGTH="15"  value="<%= PRE_AUTH_TYPE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Inception</strong></td>
            
            <td colspan="3"  align="left"  class="normal"><input name="dateofinception" type="text"  id="dateofinception"  size="20" MAXLENGTH="20"  value="<%= DATE_OF_INCEPTION%>"></td>
            </tr>
            <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured1" type="text"  id="suminsured1"  size="20" MAXLENGTH="75"  value="<%= SUMINSURED%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Balance Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="balsuminsured" type="text"  id="balsuminsured"  size="20" MAXLENGTH="75"  value="<%= BALANCESUMINSURED%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Relation with Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="relation" type="text"  id="relation"  size="20" MAXLENGTH="25"  value="<%= RELATION_WITH_INSURED%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Gender</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="gender" type="text"  id="gender"  size="20" MAXLENGTH="25"  value="<%= SEX%>"></td>
            </tr>	
			<tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Age</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="25"  value="<%= AGE%>"></td>
            </tr>	
			<tr>
			<td >Underwriting history of Patient</td>
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
	      <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="150"   value="<%= SUMINSURED%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno1" type="text"  id="policyno1"  size="20" MAXLENGTH="50"   value="<%= POLICY_NO%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy validity</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="validity" type="text"  id="validity"  size="20" MAXLENGTH="50"   value="<%= POLICY_PERIOD%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>HOSPITAL NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="150"   value="<%= HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="50"   value="<%= CITY%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>STATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="state" type="text"  id="state"  size="20" MAXLENGTH="50"   value="<%= STATE%>"></td>
            </tr>
			<tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Disease</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="disease" type="text"  id="disease"  size="20" MAXLENGTH="200" value="<%= DISEASE%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" value="<%= DIAGNOSIS%>"></td>
            </tr>
		    <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Line of treatment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="lineoftreatment" type="text"  id="lineoftreatment"  size="20" MAXLENGTH="200" value="<%=LineOfTreatment%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Procedure inc Refrence rate</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="procedureincRefrencerate" type="text"  id="procedureincRefrencerate"  size="20" MAXLENGTH="200" value="<%=ProcedureIncRefrenceRate%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Average cost</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="averagecost" type="text"  id="averagecost"  size="20" MAXLENGTH="200" value="<%=AverageCost%>"></td>
            </tr>	 
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Admission</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dateofAdmission" type="text"  id="dateofAdmission"  size="20" MAXLENGTH="200" value="<%= DATE_OF_ADMISSION%>"></td>
            </tr>   
			<tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Discharge</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dateofDischarge" type="text"  id="dateofDischarge"  size="20" MAXLENGTH="200" value="<%= DATE_OF_DISCHARGE%>"></td>
            </tr>	
			  <tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>LOS (length of stay)</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="los" type="text"  id="los"  size="20" MAXLENGTH="200" value="<%= EXPECTED_DURATION_OF_STAY%>"></td>
            </tr>
			 <tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Details of ID proof obtained</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="detailsofidproof" type="text"  id="detailsofidproof"  size="20" MAXLENGTH="200" value="<%=DetailsOfIDproof%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE/ SINGLE,SEMIPRIVATE, GENERAL ETC	</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="200" value="<%= ROOM_TYPE%>"></td>
            </tr>	
			  <tr>
			<td >Claim history of Patient</td>
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Disease</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="disease1" type="text"  id="disease1"  size="20" MAXLENGTH="50"   value="<%= DISEASE%>"></td>
            </tr>
             <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Procedure</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="procedure" type="text"  id="procedure"  size="20" MAXLENGTH="50"   value="<%= PROCEDURECODE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="amount" type="text"  id="amount"  size="20" MAXLENGTH="50"   value="<%= AMOUNT%>"></td>
            </tr>  
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PPN  & Category</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ppn" type="text"  id="ppn"  size="20" MAXLENGTH="50"   value="<%= NETWORKS%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Room rent for Single Room</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrent" type="text"  id="roomrent"  size="20" MAXLENGTH="50"   value="<%=RoomrentForSingleRoom%>"></td>
            </tr>
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ICD Code</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="icdcode" type="text"  id="icdcode"  size="20" MAXLENGTH="200" value="<%= ICD_CODE%>"></td>
            </tr> 
			 	 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Applicable PPN</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="applicablePPN" type="text"  id="applicablePPN"  size="20" MAXLENGTH="200" value="<%=ApplicablePPN%>"></td>
            </tr>	
			   <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Package Rate</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="packageRate" type="text"  id="packageRate"  size="20" MAXLENGTH="200" value="<%=PackageRate%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>No. of days Room</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noofdaysRoom" type="text"  id="noofdaysRoom"  size="20" MAXLENGTH="200" value="<%=NoOfDaysRoom%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>No. Of days ICU</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noOfdaysICU" type="text"  id="noOfdaysICU"  size="20" MAXLENGTH="200" value="<%=NoOfdaysICU%>"></td>
            </tr>  
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT/ ICU RENT PER DAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ICUperday" type="text"  id="ICUperday"  size="20" MAXLENGTH="200" value="<%=ROOM_RENT_OR_ICU_RENT_PER_DAY%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT ELIGIBILITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrenteligibility" type="text"  id="roomrenteligibility"  size="20" MAXLENGTH="200" value="<%= ROOM_RENT_ELIGIBILITY%>"></td>
            </tr>   
            
           
			<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="130">Amount As per Hospital bill</td>
			<td width="130">Amount recommended by TPA</td>
			<td width="130">Details of deduction</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>GIPSA PPN PACKAGE</td>
			<td><input name="gipsappnhospaphb" type="text"  id="01"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_HOSPAPHB%>"></td>
			<td width="130"><input name="gipsappntpa" type="text"  id="11"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_TPA%>"></td>
			<td width="138"><input name="gipsappnresondod" type="text"  id="gipsappnresondod"  size="20" MAXLENGTH="50"  value="<%=GIPSA_PPN_RESPONDOD%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>ROOM RENT/ICU RENT</td>
			<td><input name="roomrenthospaphb" type="text"  id="02"  size="20" MAXLENGTH="50"  value="<%= ROOM_RENT_REQUESTED%>"></td>
			<td width="130"><input name="roomrenttpa" type="text"  id="12"  size="20" MAXLENGTH="50"  value="<%= ROOM_RENT_PAID%>"></td>
			<td width="138"><input name="roomrentdod" type="text"  id="roomrentdod"  size="20" MAXLENGTH="50"  value="<%= ROOM_RENT_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>PROCEDURE(PROFESSIONAL) CHARGES</td>
			<td><input name="profchghospaphb" type="text"  id="03"  size="20" MAXLENGTH="50"  value="<%= PROFESSIONAL_CHARGES_REQ%>"></td>
			<td width="130"><input name="profchgtpa" type="text"  id="13"  size="20" MAXLENGTH="50"  value="<%= PROFESSIONAL_CHARGES_PAID%>"></td>
			<td width="138"><input name="profchgdod" type="text"  id="profchgdod"  size="20" MAXLENGTH="50"  value="<%= PROFESSIONAL_CHARGES_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>PHARMACY</td>
			<td><input name="phrhospaphb" type="text"  id="04"  size="20" MAXLENGTH="50"  value="<%= PHARMACY_CHARGES_REQ%>"></td>
			<td width="130"><input name="phrtpa" type="text"  id="14"  size="20" MAXLENGTH="50"  value="<%= PHARMACY_CHARGES_PAID%>"></td>
			<td width="138"><input name="phrdod" type="text"  id="phrdod"  size="20" MAXLENGTH="50"  value="<%= PHARMACY_CHARGES_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>OT + ANESTHESIA CHARGES</td>
			<td><input name="othospaphb" type="text"  id="05"  size="20" MAXLENGTH="50"  value="<%= OT_CHARGES_REQ%>"></td>
			<td width="130"><input name="ottpa" type="text"  id="15"  size="20" MAXLENGTH="50"  value="<%= OT_CHARGES_PAID%>"></td>
			<td width="138"><input name="otdod" type="text"  id="otdod"  size="20" MAXLENGTH="50"  value="<%= OT_CHARGES_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>LAB INVESTIGATIONS</td>
			<td><input name="labinvhospaphb" type="text"  id="06"  size="20" MAXLENGTH="50"  value="<%= LAB_INVES_CHARGES_REQ%>"></td>
			<td width="130"><input name="labinvtpa" type="text"  id="16"  size="20" MAXLENGTH="50"  value="<%= LAB_INVES_CHARGES_PAID%>"></td>
			<td width="138"><input name="labinvdod" type="text"  id="labinvdod"  size="20" MAXLENGTH="50"  value="<%= LAB_INVES_CHARGES_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>CONSULTATION CHARGES</td>
			<td><input name="conchrhospaphb" type="text"  id="07"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="130"><input name="conchrtpa" type="text"  id="17"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="138"><input name="conchrdod" type="text"  id="conchrdod"  size="20" MAXLENGTH="50"  value=""></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>OTHERS</td>
			<td><input name="othershospaphb" type="text"  id="08"  size="20" MAXLENGTH="50"  value="<%= OTHER_CHARGES_REQ%>"></td>
			<td width="130"><input name="otherstpa" type="text"  id="18"  size="20" MAXLENGTH="50"  value="<%= OTHER_CHARGES_PAID%>"></td>
			<td width="138"><input name="othersdod" type="text"  id="othersdod"  size="20" MAXLENGTH="50"  value="<%= OTHER_CHARGES_DEDUCTED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>TOTAL</td>
			<td><input name="totalhospaphb" type="text"  id="totalhospaphb"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="130"><input name="totaltpa" type="text"  id="totaltpa"  size="20" MAXLENGTH="50"  value=""></td>
			<td width="138"><textarea name="totaldod" id="totaldod" rows="5" cols="20" maxlength="100"><%= LETTER_REMARKS%></textarea>
		<%-- 	<td width="138"><input name="totaldod" type="text"  id="totaldod"  size="20" MAXLENGTH="50"  value="<%= LETTER_REMARKS%>"></td> --%>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Specification of Implants Used</td>
			<td><input name="specification" type="text"  id="specification"  size="20" MAXLENGTH="50"  value="<%=SpecificationOfImplantsUsed%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Name & Qualification of Doctor recommending the claim </td>
			<td width="130"><input name="nameandqual" type="text"  id="nameandqual"  size="20" MAXLENGTH="50"  value="<%= PROCESSING_DOCTOR%>"></td>
			</tr>
			<tr>
			<%
			    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
	            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MMM-dd");
		        String[] txtdate = DATE_AND_TIME.split(" ");
		        Date date = format1.parse(txtdate[0]);
		        String displayDate =  format2.format(date)+" "+txtdate[2];
		        
			%>
			<td>&nbsp;</td>
			<td>Date & time of sending format</td>
			<td><input name="dateandtime" type="text"  id="dateandtime"  size="20" MAXLENGTH="50"  value="<%= displayDate%>"></td>
			</tr>
			<tr>
			<td><b>Confirmation by TPA</b>	</td>
			<td colspan="4"><b>We have processed the file as per Policy terms & condition ,applicable capping and we confirm :</b></td>
			</tr>
			<tr><td></td>
			<td>1. Disease & treatment  is covered under Policy	</td>
			<td><select name="diseasetreatment" id="diseasetreatment">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>2.Charges allowed are as per PPN package / policy capping and proportionate deduction has been made for stay in higher room (where ever applicable)</td>
			<td><select name="ppnpackage" id="ppnpackage">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>3. The expences allowed are as per reasonable & custommary clause.	</td>
			<td><select name="expencesallowed" id="expencesallowed">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>4. Any amount allowed in excess by TPA will be liable for recovery For GMCs pls specify conditions of revised GMC deleted & additional conditions imposed if any.</td>
			<td><select name="anyamountallowedinexcessbyTPA" id="anyamountallowedinexcessbyTPA">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td></tr>
            <tr><td></td>
            <td>	
			# If amount of diagnostic exceeds Rs.5000/- separate sheet giving breakup of diagnostic along with TPAs observation on relevance of test shouls be submitted.	
			</td>
			</tr>
				<tr>

		</tr>
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit" ></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>
</form>  
		   
		 
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>