<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header.jsp" %>
<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>

<h3 align="center">SEND TO INSURER FOR APPROVAL</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		   <c:if test="${fn:length(auditList) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   		<tr>
					<th>Index</th>
				    <th>CCN/PreAuth App No</th>
				    <th>CARDID</th>
				    <th>INSURED NAME</th>
				    <th>MODIFIED DATE</th>
                    <th>Module</th>
				    <th>Process</th>
				    <th>Back To Medico</th>
				</tr>
				 <c:forEach items="${auditList}" var="auditData"> 
				 
				  <c:choose>
   					 <c:when test="${auditData.uwCode == 'BX'}">
       					<c:set var="filename" scope="page" value="./GetBhaxaDetailsAction"/>
   					 </c:when>
 				     <c:when test="${auditData.uwCode == 'UI'}">
       					<c:set var="filename" scope="page" value="./GetUiicDetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwId == 'UIK'}">
       					<c:set var="filename" scope="page" value="./GetUiicDetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwCode == 'RL'}">
       					<c:set var="filename" scope="page" value="./GetRelianceDetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwCode == 'IT'}">
       					<c:set var="filename" scope="page" value="./GetItgiDetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwCode == 'RE'}">
       					<c:set var="filename" scope="page" value="./GetReligareDetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwCode == 'NA'}">
       					<c:set var="filename" scope="page" value="./GetNIADetailsAction"/>
   					 </c:when>
   					  <c:when test="${auditData.uwId == 'NIA'}">
       					<c:set var="filename" scope="page" value="./GetNIADetailsAction"/>
   					 </c:when>
   					 <c:when test="${auditData.uwId == 'NCM'}">
						<c:set var="filename" scope="page" value="./GetNIADetailsAction"/>
					 </c:when>
    				 <c:otherwise>
      				    <c:set var="filename" scope="page" value="./DefaultInsAction"/>
   					 </c:otherwise>
				</c:choose> 
				 
				 
				 <c:if test="${fn:contains(auditData.uwId, 'UI')}">
   					<c:set var="filename" scope="page" value="./GetUiicDetailsAction"/>
				</c:if>
				<c:if test="${fn:contains(auditData.uwId, 'NI')}">
					<c:set var="filename" scope="page" value="./GetNIADetailsAction"/>
				</c:if>
				
				 <c:if test="${fn:contains(auditData.uwCode, 'UI')}">
   					<c:set var="filename" scope="page" value="./GetUiicDetailsAction"/>
				</c:if>
				<c:if test="${fn:contains(auditData.uwCode, 'NI')}">
					<c:set var="filename" scope="page" value="./GetNIADetailsAction"/>
				</c:if>
				 
                <tr>
					<td><c:out value="${auditData.serialNo}"/></td>
				    <td><c:out value="${auditData.ccnUnique}"/>/<c:out value="${auditData.preAuthNo}"/></td>
				    <td><c:out value="${auditData.cardId}"/></td>
				    <td><c:out value="${auditData.memberName}"/></td>
				    <td><c:out value="${auditData.createddate}"/></td>
				    <td><c:out value="${auditData.moduleName}"/></td>
				    <td>
                    <form name="sendtoprefill" method="post" action="${filename}">
                    <input type="hidden" name="claimId" id="claimId" value="<c:out value="${auditData.claimId}"/>" />
                    <input type="hidden" name="ccnUnique" id="ccnUnique" value="<c:out value="${auditData.ccnUnique}"/>" />
                    <input type="hidden" name="preAuthno" id="preAuthno" value="<c:out value="${auditData.preAuthNo}"/>" />
                    <input type="hidden" name="reAuthFlag" id="reAuthFlag" value="<c:out value="${auditData.reAuthFlag}"/>" />
                    <input type="hidden" name="moduleId" id="moduleId" value="<c:out value="${auditData.moduleId}"/>" />
                    <input type="hidden" name="moduleName" id="moduleName" value="<c:out value="${auditData.moduleName}"/>" />
                    <input type="hidden" name="statusId" id="statusId" value="<c:out value="${auditData.statusId}"/>" />
                    <input type="hidden" name="uwcode" id="uwcode" value="<c:out value="${auditData.uwCode}"/>" />
                    <input type="hidden" name="uwId" id="uwId" value="<c:out value="${auditData.uwId}"/>" />
                    <input type="submit" name="submit" value="Process"/>
                    </form>
 				    </td>
 				    <td>
				    <form name="sendbacktoaudit" method="post" action="SendBackToAudit.jsp">
                    <input type="hidden" name="claimId" id="claimId" value="<c:out value="${auditData.claimId}"/>" />
                    <input type="hidden" name="ccnUnique" id="ccnUnique" value="<c:out value="${auditData.ccnUnique}"/>" />
                    <input type="hidden" name="preAuthno" id="preAuthno" value="<c:out value="${auditData.preAuthNo}"/>" />
                    <input type="hidden" name="reAuthFlag" id="reAuthFlag" value="<c:out value="${auditData.reAuthFlag}"/>" />
                    <input type="hidden" name="moduleId" id="moduleId" value="<c:out value="${auditData.moduleId}"/>" />
                    <input type="hidden" name="moduleName" id="moduleName" value="<c:out value="${auditData.moduleName}"/>" />
                    <input type="hidden" name="statusId" id="statusId" value="<c:out value="${auditData.statusId}"/>" />
                    <input type="hidden" name="uwcode" id="uwcode" value="<c:out value="${auditData.uwCode}"/>" />
                    <input type="hidden" name="uwId" id="uwId" value="<c:out value="${auditData.uwId}"/>" />
                    <input type="submit" name="submit" value="Send Back To Medico" />
                    </form>
                    </td>
				    </tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   
		   <c:if test="${fn:length(auditList) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	   <tr>
					<th>Index</th>
				    <th>CCN/PreAuth App No</th>
				    <th>CARDID</th>
				    <th>INSURED NAME</th>
				    <th>MODIFIED DATE</th>
                    <th>Module</th>
				    <th>Process</th>
				  </tr>
				 
			   	    <tr align="center">
			   	    <!-- <td colspan="10" align="center"><input type="button" name="nia" onclick="nia();"></td> -->
			   			<td colspan="10" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>