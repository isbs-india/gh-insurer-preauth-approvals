<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.util.ProcedureConstants"%>
<%@ page import="com.ghpl.util.Util"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>


<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};

</script>



<script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(32); //Space
        specialKeys.push(13); //Space
        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right
        
        
        
         function IsAlphaNumeric(e) {
        	        	
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode == 32 || keyCode == 46) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            document.getElementById("error").style.display = ret ? "none" : "inline";
           
            return ret;
        } 
        
        
        function validateForm()
        {

        	if((document.getElementById("backtoauditremarks").value == "") || (document.getElementById("backtoauditremarks").value.length == 0))
        	{
        		alert('Please enter the remarks');
        		return false;
        	} 
         }
        
        
    </script>



<%
		
	 int moduleId=0; 
   	 int claimId=0;
   	 String ccnUnique="";
   	 int preauthAppNo=0;
     int reauthFlag=0;
   	 String name_of_user_obj_1=null;
     int userid_of_user=0;
     int moduleUserId=0;
     int currentStatusId=0;
      	
try{  

  	LoginBean lb=new LoginBean();

  	
  	
  	moduleId=Util.stringToInt(request.getParameter("moduleId"));
    claimId=Util.stringToInt(request.getParameter("claimId"));
    ccnUnique=request.getParameter("ccnUnique");
    preauthAppNo=Util.stringToInt(request.getParameter("preAuthno"));
    reauthFlag=Util.stringToInt(request.getParameter("reAuthFlag"));
    currentStatusId=Util.stringToInt(request.getParameter("statusId")!=null?request.getParameter("statusId"):"0");
  	
  	session= request.getSession();
  	if(session != null) {
  		
  		lb=(LoginBean)session.getAttribute("user");
  		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
  		 name_of_user_obj_1 =session.getAttribute("userName").toString();
  	     userid_of_user = lb.getModuleUserId(1);
  	     moduleUserId = lb.getModuleUserId(moduleId);
  	}
  	
  	
    

}
catch(Exception e)
{
	System.out.println("ERROR IN BACK TO MEDICO FROM SENT TO INSURER- Page Loading: "+e);
}
finally
{
	//
}
%>

<h3 align="center">Back To Medico</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
		
			
   <br/><br/>
		  
		<form name="Form1" id="Form1" action="./BackToAudit" method="post" onSubmit="return validateForm();">
         <input type="hidden" name="ccnUnique" id="ccnUnique" value="<%=ccnUnique%>">           
        <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
        <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		<input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
 		<input type="hidden" name="moduleName" id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		<input type="hidden" name="moduleId" id="moduleId" value="<%=moduleId%>">
		<input type="hidden" name="preAuthAppno" id="preAuthAppno" value="<%=preauthAppNo%>">
		    
		          
      <br/>
       <div align="center"> 
       
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
 

			<tr>
			<td>&nbsp;</td>	
            <td width="151"  class="text_heading"><strong>Claim ID </strong></td>
            <td width="180" colspan="3"  align="left"  class="normal"><%=ccnUnique%><% if(reauthFlag > 0) { %>-<%=reauthFlag%><% } %>
            <input type="hidden" name="clmid" id="clmid" value="<%=claimId%>">
             <input type="hidden" name="rauthflg" id="rauthflg" value="<%=reauthFlag%>">
              </td>
            </tr>
 			
			
			<tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Back To Medico Remarks:</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="backtoauditremarks" type="text"  id="backtoauditremarks" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"
        onpaste="return false;"  size="20" MAXLENGTH="100"  value=""><br/>
        
         <span id="error" style="color: Red; display: none">* Special Characters not allowed</span>
        </td>
        
        
			</tr>
		
		<tr><td colspan="4" align="center">&nbsp;</td>
          </tr>
		<tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit"></td>
          </tr>
		
</table>

</div>

</form>

</td>
</tr>
</table>

<%@include file="footer.jsp" %>