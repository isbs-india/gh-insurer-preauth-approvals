<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@include file="header.jsp" %>
<%@ page import="java.util.*"%>
<%@ page import="com.ghpl.model.iauth.LoginBean"%>
<%@ page import="com.ghpl.model.iauth.GetInsurerPrefillDataListBean"%>
<%@ page import="com.ghpl.model.iauth.DocsDetailsCorpBean"%>
<%@ page import="com.ghpl.model.iauth.NIADataBean"%>

<script type="text/javascript">
function preventBack(){window.history.forward();}
setTimeout("preventBack()", 0);
window.onunload=function(){null};
</script>
<script type="text/javascript" src="js/docSearch.js"></script>
<%
    String ccnUnique="";
	int moduleId=0; 
		String CLAIM_NO	="";
		String POLICYNO_POLICYPERIOD	="";
		String POLICYNO = "";
		String POLICYPERIOD = "";
		String INSURED_NAME	="";
		String PATIENT_NAME	="";
		String DEVIATIONS	="";
		String COPAYMENT	="";
		String PREAUTH_TYPE	="";
		long BALANCESUMINSURED	=0;
		String RELATION	="";
		String GENDER_AGE	="";
		String GENDER	="";
		String AGE	="";
		String PACKAGERATE = "";
		long SUM_INSURED1	=0;
		String POLICYNO_VALIDITY ="";
		String POLICYNO1 = "";
		String VALIDITY = "";
		String DISEASE_PROCEDURE ="";
		String DISEASE ="";
		String PROCEDURE ="";
		String AMOUNT ="";
		String HOSPITAL_NAME	="";
		String CITY_STATE	="";
		String CITY	="";
		String STATE	="";
		String PPN_CATEGORY	="";
		String ROOM_RENT_FOR_SINGLE_ROOM	="";
		String DIASEASE_DIAGNOSIS	="";
		String DIASEASE1	="";
		String DIAGNOSIS	="";
		String LINE_OF_TREATMENT	="";
		String PROCEDURE_INC_REFERENCERATE	="";
		String AVERAGECOST	="";
		String ICD_CODE	="";
		String APPLICABLE_PPN	="";
		String DOA	="";
		String DOD	="";
		String LOS	="";
		String NO_OF_DAYS_ROOM	="";
		String NO_OF_DAYS_ICU	="";
		String DETAILSOFIDPROOF	="";
		String ROOM_TYPE	="";
		String ROOM_RENT_PER_DAY	="";
		String ROOM_RENT_ELIGIBILITY	="";
		String GIPSA_PPN_PACKAGE_REQ	="";
		String ROOM_RENT_REQ	="";
		String PROFESSIONAL_CHARGES_REQ	="";
		
		String PHARMACY_REQ	="";
		String OT_CHARGES_REQ	="";
		String LAB_INVESTIGATIONS_REQ	="";
		String CONSULTATION_CHARGES_REQ	="";
		String OTHERS_REQ	="";
		String TOTAL_REQ	="";
		String AMOUNT_RECOMMENDED_BY_TPA	="";
	
		String GIPSA_PPN_PACKAGE_RECOMM	="";
		String ROOM_RENT_RECOMM	="";
		String PROFESSIONAL_CHARGES_RECOMM	="";	
		String PHARMACY_RECOMM	="";
		String OT_CHARGES_RECOMM	="";
		String LAB_INVESTIGATIONS_RECOMM	="";
		String CONSULTATION_CHARGES_RECOMM	="";
		String OTHERS_RECOMM	="";
		String TOTAL_RECOMM	="";
		
		String GIPSA_PPN_PACKAGE_DED	="";
		String ROOM_RENT_DED	="";
		String PROFESSIONAL_CHARGES_DED	="";
		String PHARMACY_DED	="";
		String OT_CHARGES_DED	="";
		String LAB_INVESTIGATIONS_DED	="";
		String CONSULTATION_CHARGES_DED	="";
		String OTHERS_DED	="";
		String TOTAL_DED	="";
		
		String ID ="";
		String CREATEDDATE ="";
		String CLAIMID ="";
		String REAUTHFLAG ="";
		String EMAILID ="";
		String CC_EMAILID ="";
		String SPEC_IMPLANTS_USED ="";
			
		String PROCESSINGDOCTORSNAMEANDTPA ="";
		String DT_TIME_FORMAT_SENT ="";
		String DISEASE_TREATMENT_ARE_COVERED ="";
		String CHRGS_ALLOW_PPN_PKG ="";
		String EXP_REASONABLE ="";
		String POLICY_TYPE ="";
		String DATE_OF_INCEPTION ="";
    
    	String CCEMAILID = "";
        String POLICY_DESC="";
    	 
    	 String clmid="0";
    	 int rauthflg=0;
    	 int preauthAppNo=0;
    	 
    	 String name_of_user_obj_1=null;
    	 int userid_of_user=0;
    	 int moduleUserId=0;
    	 GetInsurerPrefillDataListBean finalBean = null;
    	 
 try{  
	finalBean=(GetInsurerPrefillDataListBean)request.getAttribute("finalBean");
	
	if(finalBean.getNIABean() != null)
	{
		
		  CLAIM_NO	= finalBean.getNIABean().getClaimNo();
		  POLICYNO_POLICYPERIOD	= finalBean.getNIABean().getPolicynoAndPolicyperiad();
		  
		  String[] a=POLICYNO_POLICYPERIOD.split("_");
		  POLICYNO = a[0];
		  POLICYPERIOD = a[1];
		  
		  INSURED_NAME	= finalBean.getNIABean().getInsured();
		  PATIENT_NAME	= finalBean.getNIABean().getPatient();
		  DEVIATIONS = finalBean.getNIABean().getIncase();
		  COPAYMENT	= finalBean.getNIABean().getCopayment();
		  PREAUTH_TYPE	= finalBean.getNIABean().getPreauthType();
		  BALANCESUMINSURED	= finalBean.getNIABean().getBalSuminsured();
		  RELATION	= finalBean.getNIABean().getRelation();
		  
		  GENDER_AGE	= finalBean.getNIABean().getGenderAndAge();
		  String[] d=GENDER_AGE.split("_");
		  GENDER = d[0];
		  AGE = d[1];
		  
		  SUM_INSURED1	= finalBean.getNIABean().getSumInsured();
		  
		  POLICYNO_VALIDITY = finalBean.getNIABean().getPolicyNoAndValidity();
		  String[] b=POLICYNO_VALIDITY.split("_");
		  POLICYNO1 = b[0];
		  VALIDITY = b[1];
		  
		  DISEASE_PROCEDURE = finalBean.getNIABean().getDiseaseAndProcedure();
		  String[] c=DISEASE_PROCEDURE.split("_");
		  DISEASE =c[0];
		  PROCEDURE =c[1];
		  
		  AMOUNT = finalBean.getNIABean().getAmount();
		  HOSPITAL_NAME	= finalBean.getNIABean().getHospName();
		  
		  CITY_STATE	= finalBean.getNIABean().getCityState();
		  String[] e=CITY_STATE.split("_");
		  CITY =e[0];
		  STATE =e[1];
		  
		  PPN_CATEGORY	= finalBean.getNIABean().getPpnAndCategory();
		  ROOM_RENT_FOR_SINGLE_ROOM	= finalBean.getNIABean().getRoomRent();
		  DIASEASE_DIAGNOSIS = finalBean.getNIABean().getDiseaseAndDiagnosis();
		  
		  String[] f= DIASEASE_DIAGNOSIS.split("_");
		  DIASEASE1 =f[0];
		  DIAGNOSIS =f[1];
		  
		  LINE_OF_TREATMENT	= finalBean.getNIABean().getLineOfTreatment();
		  
		  String[] i=LINE_OF_TREATMENT.split("_");
		  LINE_OF_TREATMENT = i[0];
		  PROCEDURE_INC_REFERENCERATE = i[1];  
		  AVERAGECOST = i[2];
		  
		  ICD_CODE	= finalBean.getNIABean().getIcdCode();
		  
		  APPLICABLE_PPN = finalBean.getNIABean().getApplicablePPN();
		  String[] g=APPLICABLE_PPN.split("_");
		  APPLICABLE_PPN =g[0];
		  PACKAGERATE =g[1];
		  
		  DOA	= finalBean.getNIABean().getDateofAdmission();
		  
		  DOD	= finalBean.getNIABean().getDateofDischarge();
		  String[] h= DOD.split("_");
		  DOD =h[0];
		  LOS =h[1];
		  
		  NO_OF_DAYS_ROOM	= finalBean.getNIABean().getNoOfDaysRoom();
		  NO_OF_DAYS_ICU	= finalBean.getNIABean().getNoOfDaysICU();
		  DETAILSOFIDPROOF	= finalBean.getNIABean().getDetailsOfIdProof();
		  ROOM_TYPE	= finalBean.getNIABean().getRoomType();
		  ROOM_RENT_PER_DAY	= finalBean.getNIABean().getRoomRent();
		  ROOM_RENT_ELIGIBILITY	= finalBean.getNIABean().getRoomRentEligibility();
		  
		  GIPSA_PPN_PACKAGE_REQ	= finalBean.getNIABean().getGipsAppnHospAphb();
		  ROOM_RENT_REQ	= finalBean.getNIABean().getRoomRentRequest();
		  PROFESSIONAL_CHARGES_REQ	= finalBean.getNIABean().getProfChgHospRequest();
		  PHARMACY_REQ	= finalBean.getNIABean().getPhrHospRequest();
		  OT_CHARGES_REQ = finalBean.getNIABean().getOtHospRequest();
		  LAB_INVESTIGATIONS_REQ = finalBean.getNIABean().getLabInvHospRequest();
		  CONSULTATION_CHARGES_REQ	= finalBean.getNIABean().getConChrHospRequest();
		  OTHERS_REQ	= finalBean.getNIABean().getOthersHospRequest();
		  TOTAL_REQ	= finalBean.getNIABean().getTotalHospRequest();
			
		  GIPSA_PPN_PACKAGE_RECOMM	= finalBean.getNIABean().getGipsAppnTpa();
		  ROOM_RENT_RECOMM	= finalBean.getNIABean().getRoomRentPaid();
		  PROFESSIONAL_CHARGES_RECOMM	= finalBean.getNIABean().getProfChgPaid();	
		  PHARMACY_RECOMM	= finalBean.getNIABean().getPhrPaid();
		  OT_CHARGES_RECOMM	= finalBean.getNIABean().getOtPaid();
		  LAB_INVESTIGATIONS_RECOMM	= finalBean.getNIABean().getLabInvPaid();
		  CONSULTATION_CHARGES_RECOMM	= finalBean.getNIABean().getConChrPaid();
		  OTHERS_RECOMM	= finalBean.getNIABean().getOthersPaid();
		  TOTAL_RECOMM	= finalBean.getNIABean().getTotalPaid();
			
		  GIPSA_PPN_PACKAGE_DED	= finalBean.getNIABean().getGipsAppnResonDod();
		  ROOM_RENT_DED	= finalBean.getNIABean().getRoomRentDeducted();
		  PROFESSIONAL_CHARGES_DED	= finalBean.getNIABean().getProfChgDeducted();
		  PHARMACY_DED	= finalBean.getNIABean().getPhrDeducted();
		  OT_CHARGES_DED	= finalBean.getNIABean().getOtDeducted();
		  LAB_INVESTIGATIONS_DED	= finalBean.getNIABean().getLabInvDeducted();
		  CONSULTATION_CHARGES_DED	= finalBean.getNIABean().getConChrDeducted();
		  OTHERS_DED	= finalBean.getNIABean().getOthersDeducted();
		  TOTAL_DED	= finalBean.getNIABean().getTotalDeducted();
		  EMAILID = finalBean.getNIABean().getEmailId();
		  CC_EMAILID = finalBean.getNIABean().getCcEmailId();
		  SPEC_IMPLANTS_USED = finalBean.getNIABean().getSpecification();
		  PROCESSINGDOCTORSNAMEANDTPA = finalBean.getNIABean().getNameAndQual();
		  DT_TIME_FORMAT_SENT = finalBean.getNIABean().getDateAndTime();
		  DISEASE_TREATMENT_ARE_COVERED = finalBean.getNIABean().getDiseaseTreatment();
		  CHRGS_ALLOW_PPN_PKG = finalBean.getNIABean().getPpnPackage();
		  EXP_REASONABLE = finalBean.getNIABean().getExpencesAllowed();
		  POLICY_TYPE = finalBean.getNIABean().getPolicyType();
		  DATE_OF_INCEPTION = finalBean.getNIABean().getDateOfInception();
		  ID = finalBean.getNIABean().getId();
		  CREATEDDATE = finalBean.getNIABean().getCreatedDate();
			
			//EMAILID=finalBean.getNIABean().getEmailId();
			moduleId=finalBean.getNIABean().getModuleId();
			clmid=finalBean.getCcnUnique();
			rauthflg=finalBean.getNIABean().getReauthFlag();
			//preauthAppNo=finalBean.getNIABean().getPreauthAppNo();
	}
	        //Newly added on 18DEC2014 :: Retrieving insurer EmailId, CCEmailId from GHPL DB only (common UW DB - GHPL)
			if (finalBean.getEmailCommonDetaisBean() != null) {
				EMAILID = finalBean.getEmailCommonDetaisBean().getEmailId();
				CCEMAILID = finalBean.getEmailCommonDetaisBean().getCcEmailId();
			}

	LoginBean lb=new LoginBean();
	session= request.getSession();
	if(session != null) {
		lb=(LoginBean)session.getAttribute("user");
		//System.out.println("USERID:"+lb.getModuleUserId(1)+" USERNAME:"+session.getAttribute("userName"));
		 name_of_user_obj_1 =session.getAttribute("userName").toString();
	     userid_of_user = lb.getModuleUserId(1);
	     moduleUserId = lb.getModuleUserId(moduleId);
	}
}
catch(Exception e)
{
	System.out.println("ERROR IN LIST NIA: "+e);
}
finally
{
	//finalBean=null;
} 
%>
<h3 align="center">View NIA Details for Claim</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%"><br/>
   <br/><br/>
		  <form name="Form1" id="Form1" action="./GenUiicXls" method="post">
         <input type="hidden" name="moduleUserId" id="moduleUserId" value="<%=moduleUserId%>">                   
         <input type="hidden" name="userid" id="userid" value="<%=userid_of_user%>">   
		 <input type="hidden" name="drname" id="drname" value="<%=name_of_user_obj_1%>">
		 <input type="hidden" name="INSURER_FLAG" id="INSURER_FLAG" value="NA">  
		 <input type="hidden" name="emailid" id="emailid" value="<%=EMAILID%>">
		 <input type="hidden" name="ccemailid" id="ccemailid" value="<%=CCEMAILID%>"> 
		 <input type="hidden" name=moduleName id="moduleName" value="<%=request.getParameter("moduleName")%>"> 
		 <input type="hidden" name=moduleId id="moduleId" value="<%=moduleId%>">
      <br/>
       <div align="center"> 
       <table class="text_heading" border="0" cellpadding="2" cellspacing="2">
	   <tbody>
	   <tr>
	   <td>
	   FORMAT FOR CASHLESS APPROVAL Rs. 1 LAKH AND ABOVE
	   </td>
	   </tr>
 		<tr>
			<td width="263">&nbsp;</td>	
            <td width="227"  class="text_heading">&nbsp;</td>
            <td colspan="3"  align="left"  class="normal">&nbsp;
            <input type="hidden" name="clmid" id="clmid" value="<%=clmid%>">
            <input type="hidden" name="rauthflg" id="rauthflg" value="<%=rauthflg%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Claim No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="claimno" type="text"  id="claimno"  size="20" MAXLENGTH="200"  readonly="readonly" value="<%= clmid%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno" type="text"  id="policyno"  size="20" MAXLENGTH="150"  readonly="readonly" value="<%= POLICYNO%>"></td>
            </tr>
             <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy period</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyperiod" type="text"  id="policyperiod"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%= POLICYPERIOD%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Insured Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="insured" type="text"  id="insured"  size="20" MAXLENGTH="10" readonly="readonly" value="<%= INSURED_NAME%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong> Patient Name</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="patient" type="text"  id="patient"  size="20" MAXLENGTH="10" readonly="readonly" value="<%= PATIENT_NAME%>"></td>
            </tr>	
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy Type</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policytype" type="text"  id="policytype"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= POLICY_TYPE%>"></td>
            </tr>
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>In case of GMC , deviations from Standard Policy</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="incase" type="text"  id="incase"  size="20" MAXLENGTH="25"  readonly="readonly" value="<%= DEVIATIONS%>"></td>
            </tr>
			  <tr>
			  	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Copayment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="copayment" type="text"  id="copayment"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%= COPAYMENT%>"></td>
            </tr>
			  <tr>
			 	<td height="28">&nbsp;</td>	
            <td  class="text_heading"><strong>Preauth Type :Initial /Enhencement/Final</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="preauthtype" type="text"  id="preauthtype"  size="20" MAXLENGTH="15" readonly="readonly" value="<%= PREAUTH_TYPE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Inception</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dateofinception" type="text"  id="dateofinception"  size="20" MAXLENGTH="20" readonly="readonly" value="<%= DATE_OF_INCEPTION%>"></td>
            </tr>
			<tr>
			<td>&nbsp;</td>	
			<td  class="text_heading"><strong>Sum Insured</strong></td>
			<td colspan="3"  align="left"  class="normal"><input name="suminsured1" type="text"  id="suminsured1"  size="20" MAXLENGTH="75" readonly="readonly"  value="<%= SUM_INSURED1 %>"></td>
			</tr>
			
			<tr>
			<td>&nbsp;</td>	
			<td  class="text_heading"><strong>Balance Sum Insured</strong></td>
			<td colspan="3"  align="left"  class="normal"><input name="balsuminsured" type="text"  id="balsuminsured"  size="20" MAXLENGTH="75" readonly="readonly"  value="<%= BALANCESUMINSURED %>"></td>
			</tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Relation with Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="relation" type="text"  id="relation"  size="20" MAXLENGTH="25" readonly="readonly" value="<%= RELATION%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Gender</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="gender" type="text"  id="gender"  size="20" MAXLENGTH="25" readonly="readonly" value="<%= GENDER%>"></td>
            </tr>	
			<tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Age</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="age" type="text"  id="age"  size="20" MAXLENGTH="25" readonly="readonly" value="<%= AGE%>"></td>
            </tr>	
			<tr>
			<td >Underwriting history of Patient</td>
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
	      <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Sum Insured</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="suminsured" type="text"  id="suminsured"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%= SUM_INSURED1%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy No.</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="policyno1" type="text"  id="policyno1"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= POLICYNO1%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Policy validity</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="validity" type="text"  id="validity"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= VALIDITY%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>HOSPITAL NAME</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="hospname" type="text"  id="hospname"  size="20" MAXLENGTH="150" readonly="readonly"  value="<%= HOSPITAL_NAME%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>CITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="city" type="text"  id="city"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= CITY%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>STATE</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="state" type="text"  id="state"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= STATE%>"></td>
            </tr>
			<tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Disease</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="disease" type="text"  id="disease"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= DIASEASE1%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>DIAGNOSIS</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="diagnosis" type="text"  id="diagnosis"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= DIAGNOSIS%>"></td>
            </tr>	
			<tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Line of treatment</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="lineoftreatment" type="text"  id="lineoftreatment"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= LINE_OF_TREATMENT%>"></td>
            </tr>
            <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Procedure inc Refrence rate</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="procedureincRefrencerate" type="text"  id="procedureincRefrencerate" readonly="readonly" size="20" MAXLENGTH="200" value="<%= PROCEDURE_INC_REFERENCERATE%>"></td>
            </tr>	
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Average cost</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="averagecost" type="text"  id="averagecost"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= AVERAGECOST%>"></td>
            </tr>	 
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Admission</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dateofAdmission" type="text"  id="dateofAdmission"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= DOA%>"></td>
            </tr>   
			<tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Date of Discharge</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="dateofDischarge" type="text"  id="dateofDischarge"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= DOD%>"></td>
            </tr>	
			  <tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>LOS (length of stay)</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="los" type="text"  id="los"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= LOS%>"></td>
            </tr>
			 <tr>
			<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Details of ID proof obtained</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="detailsofidproof" type="text"  id="detailsofidproof"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= DETAILSOFIDPROOF%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM TYPE/ SINGLE,SEMIPRIVATE, GENERAL ETC	</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomtype" type="text"  id="roomtype"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= ROOM_TYPE%>"></td>
            </tr>	
			  <tr>
			<td >Claim history of Patient</td>
            <td  align="left"  class="normal">&nbsp;</td>
			<td colspan="2"  align="left"  class="normal">&nbsp;</td>
		  </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Disease</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="disease1" type="text"  id="disease1"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= DISEASE%>"></td>
            </tr>
             <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Procedure</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="procedure" type="text"  id="procedure"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= PROCEDURE%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Amount</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="amount" type="text"  id="amount"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= AMOUNT%>"></td>
            </tr>  
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>PPN  & Category</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ppn" type="text"  id="ppn"  size="20" MAXLENGTH="50" readonly="readonly"  value="<%= PPN_CATEGORY%>"></td>
            </tr>
			 <tr>
			 	<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Room rent for Single Room</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrent" type="text"  id="roomrent"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= ROOM_RENT_FOR_SINGLE_ROOM%>"></td>
            </tr>
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ICD Code</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="icdcode" type="text"  id="icdcode"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= ICD_CODE%>"></td>
            </tr> 
			 	 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Applicable PPN</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="applicablePPN" type="text"  id="applicablePPN"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= APPLICABLE_PPN%>"></td>
            </tr>	
			   <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>Package Rate</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="packageRate" type="text"  id="packageRate"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= PACKAGERATE%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>No. of days Room</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noofdaysRoom" type="text"  id="noofdaysRoom"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= NO_OF_DAYS_ROOM%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>No. Of days ICU</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="noOfdaysICU" type="text"  id="noOfdaysICU"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= NO_OF_DAYS_ICU%>"></td>
            </tr>  
			  <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT/ ICU RENT PER DAY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="ICUperday" type="text"  id="ICUperday"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= ROOM_RENT_PER_DAY%>"></td>
            </tr>	
			 <tr>
				<td>&nbsp;</td>	
            <td  class="text_heading"><strong>ROOM RENT ELIGIBILITY</strong></td>
            <td colspan="3"  align="left"  class="normal"><input name="roomrenteligibility" type="text"  id="roomrenteligibility"  size="20" MAXLENGTH="200" readonly="readonly" value="<%= ROOM_RENT_ELIGIBILITY%>"></td>
            </tr>   
			<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td width="130">Amount As per Hospital bill</td>
			<td width="130">Amount recommended by TPA</td>
			<td width="130">Details of deduction</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>GIPSA PPN PACKAGE</td>
			<td><input name="gipsappnhospaphb" type="text"  id="gipsappnhosp"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= GIPSA_PPN_PACKAGE_REQ%>"></td>
			<td width="130"><input name="gipsappntpa" type="text"  id="gipsappntpa"  size="20" MAXLENGTH="50"  readonly="readonly" value="<%= GIPSA_PPN_PACKAGE_RECOMM%>"></td>
			<td width="138"><input name="gipsappnresondod" type="text"  id="gipsappnresondod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= GIPSA_PPN_PACKAGE_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>ROOM RENT/ICU RENT</td>
			<td><input name="roomrenthospaphb" type="text"  id="roomrenthosp"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= ROOM_RENT_REQ%>"></td>
			<td width="130"><input name="roomrenttpa" type="text"  id="roomrenttpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= ROOM_RENT_RECOMM%>"></td>
			<td width="138"><input name="roomrentdod" type="text"  id="roomrentdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= ROOM_RENT_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>PROCEDURE(PROFESSIONAL) CHARGES</td>
			<td><input name="profchghospaphb" type="text"  id="profchghosp"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PROFESSIONAL_CHARGES_REQ%>"></td>
			<td width="130"><input name="profchgtpa" type="text"  id="profchgtpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PROFESSIONAL_CHARGES_RECOMM%>"></td>
			<td width="138"><input name="profchgdod" type="text"  id="profchgdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PHARMACY_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>PHARMACY</td>
			<td><input name="phrhospaphb" type="text"  id="phrhosp"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PHARMACY_REQ%>"></td>
			<td width="130"><input name="phrtpa" type="text"  id="phrtpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PHARMACY_RECOMM%>"></td>
			<td width="138"><input name="phrdod" type="text"  id="phrdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PHARMACY_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>OT + ANESTHESIA CHARGES</td>
			<td><input name="othospaphb" type="text"  id="othospaphb"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OT_CHARGES_REQ%>"></td>
			<td width="130"><input name="ottpa" type="text"  id="ottpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OT_CHARGES_RECOMM%>"></td>
			<td width="138"><input name="otdod" type="text"  id="otdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OT_CHARGES_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			 		
			<td>LAB INVESTIGATIONS</td>
			<td><input name="labinvhospaphb" type="text"  id="labinvhosp"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= LAB_INVESTIGATIONS_REQ%>"></td>
			<td width="130"><input name="labinvtpa" type="text"  id="labinvtpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= LAB_INVESTIGATIONS_RECOMM%>"></td>
			<td width="138"><input name="labinvdod" type="text"  id="labinvdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= LAB_INVESTIGATIONS_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>CONSULTATION CHARGES</td>
			<td><input name="conchrhospaphb" type="text"  id="conchrhospaphb"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= CONSULTATION_CHARGES_REQ%>"></td>
			<td width="130"><input name="conchrtpa" type="text"  id="conchrtpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= CONSULTATION_CHARGES_RECOMM%>"></td>
			<td width="138"><input name="conchrdod" type="text"  id="conchrdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= CONSULTATION_CHARGES_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>OTHERS</td>
			<td><input name="othershospaphb" type="text"  id="othershosp"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OTHERS_REQ%>"></td>
			<td width="130"><input name="otherstpa" type="text"  id="otherstpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OTHERS_RECOMM%>"></td>
			<td width="138"><input name="othersdod" type="text"  id="othersdod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= OTHERS_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>TOTAL</td>
			<td><input name="totalhospaphb" type="text"  id="totalhospaphb"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= TOTAL_REQ%>"></td>
			<td width="130"><input name="totaltpa" type="text"  id="totaltpa"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= TOTAL_RECOMM%>"></td>
			<td width="138"><input name="totaldod" type="text"  id="totaldod"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= TOTAL_DED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Specification of Implants Used</td>
			<td><input name="specification" type="text"  id="specification"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= SPEC_IMPLANTS_USED%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Name & Qualification of Doctor recommending the claim </td>
			<td width="130"><input name="nameandqual" type="text"  id="nameandqual"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= PROCESSINGDOCTORSNAMEANDTPA%>"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Date & time of sending format</td>
			<td><input name="dateandtime" type="text"  id="dateandtime"  size="20" MAXLENGTH="50" readonly="readonly" value="<%= DT_TIME_FORMAT_SENT%>"></td>
			</tr>
			<tr>
			<td><b>Confirmation by TPA</b>	</td>
			<td><b>Date & time of sending format</b></td>
			</tr>
			<tr><td></td>
			<td>1. Disease & treatment  is covered under Policy	</td>
			<td><select name="diseasetreatment" id="diseasetreatment">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>2.Charges allowed are as per PPN package / policy capping and proportionate deduction has been made for stay in higher room (where ever applicable)</td>
			<td><select name="ppnpackage" id="ppnpackage">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>3. The expences allowed are as per reasonable & custommary clause.	</td>
			<td><select name="expencesallowed" id="expencesallowed">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
			</tr>
			<tr><td></td>
			<td>4. Any amount allowed in excess by TPA will be liable for recovery For GMCs pls specify conditions of revised GMC deleted & additional conditions imposed if any.</td>
			<td><select name="anyamountallowedinexcessbyTPA" id="anyamountallowedinexcessbyTPA">
             <option value="YES">YES</option>
			<option value="NO">NO</option>
            </select></td>
            <tr><td></td>
            <td>	
			# If amount of diagnostic exceeds Rs.5000/- separate sheet giving breakup of diagnostic along with TPAs observation on relevance of test shouls be submitted.	
			</td>
			</tr>
          <tr><td colspan="4" align="center"> <input type="submit" name="submit" id="submit" value="Submit" ></td>
          </tr>
         </tbody>
	     </table>
    </div>
    <BR>
</form> 
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>